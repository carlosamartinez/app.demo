// globas variables
var pendientes = 0;
var aprobados = 0;
var espera = 0;
var pagos = 0;

$(document).ready(function(){

        $.ajax({
                url : '../Welcome/charts',
                data : { past_charts : 'past_charts' },
                type : 'POST',
                dataType : 'json', 
                success: function(result){
                    pendientes =  parseInt(result.cant_pendiente);
                    aprobados  =  parseInt(result.cant_aprobado);
                    espera     =  parseInt(result.cant_listo_para_pago);
                    pagos      =  parseInt(result.cant_pagado);

                    // --charts pastel- //
                    Chart.defaults.global.customTooltips = function(tooltip){
                      // Tooltip Element
                        var tooltipEl = $('#chartjs-tooltip');
                        // Hide if no tooltip
                        if (!tooltip){
                            tooltipEl.css({
                                opacity: 0
                            });
                            return;
                        }
                        // Set caret Position
                        tooltipEl.removeClass('above below');
                        tooltipEl.addClass(tooltip.yAlign);
                        // Set Text
                        tooltipEl.html(tooltip.text);
                        // Find Y Location on page
                        var top;
                        if (tooltip.yAlign == 'above') {
                            top = tooltip.y - tooltip.caretHeight - tooltip.caretPadding;
                        } else {
                            top = tooltip.y + tooltip.caretHeight + tooltip.caretPadding;
                        }
                        // Display, position, and set styles for font
                        tooltipEl.css({
                            opacity: 1,
                            left: tooltip.chart.canvas.offsetLeft + tooltip.x + 'px',
                            top: tooltip.chart.canvas.offsetTop + top + 'px',
                            fontFamily: tooltip.fontFamily,
                            fontSize: tooltip.fontSize,
                            fontStyle: tooltip.fontStyle,
                        });
                    };

                    var pieData = [
                    {
                        value: pendientes,
                        color: "#008fd1",
                        highlight: "#b9c1ce",
                        label: "Facturas pendientes"
                    }, 
                    {
                        value: aprobados,
                        color: "#24417a",
                        highlight: "#b9c1ce",
                        label: "Facturas aprobadas"
                    }, 
                    {
                        value: espera,
                        color: "#5f687a",
                        highlight: "#b9c1ce",
                        label: "Facturas en espera"
                    }, 
                    {
                        value: pagos,
                        color: "#592db7",
                        highlight: "#b9c1ce",
                        label: "Facturas ya pagadas"
                    }
                    ];

                    var ctx1 = document.getElementById("chart-area1").getContext("2d");
                    window.myPie = new Chart(ctx1).Pie(pieData);

                    var ctx2 = document.getElementById("chart-area2").getContext("2d");
                    window.myPie = new Chart(ctx2).Pie(pieData);
                } 
        });


        function getRandValue(){

                $.ajax({
                    url : '../Welcome/charts_fl',
                    data : { past_charts : 'past_charts' },
                    type : 'POST',
                    dataType : 'json', 
                    success: function(data){

                        console.log(data.Cedula);

                        Porcentaje_chance        = data.Porcentaje_chance.replace("%","");
                        Porcentaje_chance_diario = data.Porcentaje_chance_diario.replace("%","");
                        Porcentaje_astro         = data.Porcentaje_astro.replace("%","");
                        Porcentaje_recargas      = data.Porcentaje_recargas.replace("%","");
                        Porcentaje_giros         = data.Porcentaje_giros.replace("%","");
                        Porcentaje_loteria       = data.Porcentaje_loteria.replace("%","");
                        Porcentaje_recaudos      = data.Porcentaje_recaudos.replace("%",""); 

                        //por medio de este condicional determinamos si los valores viene vacios entonces los convertimos a 0 
                        if(Porcentaje_chance == ''){Porcentaje_chance = 0;}
                        if(Porcentaje_chance_diario == ''){Porcentaje_chance_diario = 0;}
                        if(Porcentaje_astro == ''){Porcentaje_astro = 0;}
                        if(Porcentaje_recargas == ''){Porcentaje_recargas = 0;}
                        if(Porcentaje_giros == ''){Porcentaje_giros = 0;}
                        if(Porcentaje_loteria == ''){Porcentaje_loteria = 0;}
                        if(Porcentaje_recaudos == ''){Porcentaje_recaudos = 0;}


                        var barChartData = {
                          labels : ["Porcentaje chance","Porcentaje chance diario","Porcentaje astro","Porcentaje recargas","Porcentaje giros", "Porcentaje loteria", "Porcentaje recaudos"],
                          datasets : [
                            {
                              fillColor : "rgb(45, 137, 239)",
                              strokeColor : "rgb(58, 90, 104)",
                              highlightFill: "rgb(182, 204, 214)",
                              highlightStroke: "rgb(6, 48, 66)",
                              data : [parseInt(Porcentaje_chance),parseInt(Porcentaje_chance_diario),parseInt(Porcentaje_astro),parseInt(Porcentaje_recargas),parseInt(Porcentaje_giros),parseInt(Porcentaje_loteria),parseInt(Porcentaje_recaudos)]
                            },
                            {
                                fillColor : "rgba(151,187,205,0.5)",
                                strokeColor : "rgba(151,187,205,0.8)",
                                highlightFill : "rgba(151,187,205,0.75)",
                                highlightStroke : "rgba(151,187,205,1)",
                                data : [parseInt(Porcentaje_chance),parseInt(Porcentaje_chance_diario),parseInt(Porcentaje_astro),parseInt(Porcentaje_recargas),parseInt(Porcentaje_giros),parseInt(Porcentaje_loteria),parseInt(Porcentaje_recaudos)]
                            }
                           
                          ]
                        }

                        var ctx = document.getElementById("canvas").getContext("2d");
                            window.myBar = new Chart(ctx).Bar(barChartData,{
                              responsive : true
                        });

                        $("#lider").text(data.Lider);
                        $("#asesor").text(data.Nombre);

                    }
                });
        }

        setInterval(getRandValue, 10000);

});






