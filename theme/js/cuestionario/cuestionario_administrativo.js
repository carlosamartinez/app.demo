// validacion de campos vacios cuestionario administrativo
$("#cuestionario_administrativo").submit(function(){

    posee = document.getElementsByName("posee[]");
    var seleccionado_posee = false;
    for(var i=0; i<posee.length; i++) {    
        if(posee[i].checked) {
            seleccionado_posee = true;
        }
    }

    condiciones = document.getElementsByName("condiciones[]");
    var seleccionado_condiciones = false;
    for(var i=0; i<condiciones.length; i++) {    
        if(condiciones[i].checked) {
            seleccionado_condiciones = true;
        }
    }

    equipamento = document.getElementsByName("equipamento[]");
    var seleccionado_equipamento = false;
    for(var i=0; i<equipamento.length; i++) {    
        if(equipamento[i].checked) {
            seleccionado_equipamento = true;
        }
    }

    //Validamos campos obligatorios de la pestaña Información personal
    if( $("#nombre").val().length < 1 || $("#apellidos").val().length < 1 || $("#cedula_vendedor").val().length < 1 
        || $("#fecha_expedicion").val().length < 1 || $("#fecha_nacimiento").val().length < 1 || $("#lugar_nacimiento").val().length < 1 
        || $("#depto_nacimiento").val().length < 1 || $("#nacionalidad").val().length < 1 || $("#genero").val() == '' 
        || $("#edad").val().length < 1 || $("#email").val().length < 1 || $("#tipo_sangre").val() == '' 
        || $("#estado_civil").val().length < 1 || $("#numero_celular").val().length < 1 || $("#talla_camisa").val().length < 1 
        || $("#talla_pantalon").val().length < 1 || $("#talla_zapatos").val().length < 1
        
        || $("#cotizante_salud").val()=='' || $("#tipo_afiliado").val()=='' 
        || $("#entidad_salud").val().length < 1 || $("#parentesco_cotizante").val()==''
        
        || $("#tipo_de_vivienda").val()=='' || $("#tiempo_sector").val()=='' || $("#barrio").val().length < 1 
        || $("#direccion_residencia").val().length < 1 || $("#estrato").val().length < 1)
    {  
        alert('Por favor verifique los campos obligatorios de la pestaña "Información Personal"');  
        return false;  
    }//Validamos campos obligatorios de la pestaña Composición familiar
    else if($("#conyuge").val()=='' || $("#composicion_familiar").val().length < 1 || $("#total_personas").val().length < 1)
    {  
        alert('Por favor verifique los campos obligatorios de la pestaña "Composición familiar"');  
        return false;  
    }//Validamos campos obligatorios de la pestaña Características vivienda
    else if( $("#sector_residencial").val()=='' || $("#estado_vivienda").val()=='' || $("#tipo_vivienda").val()=='' 
        || $("#tenencia_vivienda").val()=='' || $("#aspectos_fisicos").val()=='' || $("#tipo_estructura").val() == '' 
        || $("#vivienda_social").val()=='' || seleccionado_posee==false || seleccionado_condiciones==false 
        || seleccionado_equipamento==false)
    {  
        alert('Por favor verifique los campos obligatorios de la pestaña "Características vivienda"');  
        return false;  
    }//Validamos campos obligatorios de la pestaña Formación académica
    else if( $("#grado_escolaridad").val()=='' || $("#institucion_bachillerato").val().length < 1 || $("#profesion").val().length < 1
        || $("#nombre_formacion").val()=='' || $("#gustaria_estudiar").val().length < 1 || $("#cursa_actualmente").val()==''
        || $("#conoce_ingles").val()=='' || $("#nivel_ingles").val()=='' || $("#otros_idiomas").val().length < 1)
    {  
        alert('Por favor verifique los campos obligatorios de la pestaña "Formación académica"');  
        return false;  
    }//Validamos campos obligatorios de la pestaña Experiencia laboral
    else if( $("#experiencia_laboral").val() == '')
    {  
        alert('Por favor verifique los campos obligatorios de la pestaña "Experiencia laboral"');  
        return false;  
    }//Validamos campos obligatorios de la pestaña Información laboral
    else if( $("#jefe_inmediato_actual").val().length < 1 || $("#cargo_actual").val().length < 1 || $("#estado_laboral").val()== '' 
        || $("#tipo_contrato").val()== '' || $("#fecha_ingreso").val().length < 1)
    {  
        alert('Por favor verifique los campos obligatorios de la pestaña "Información laboral"');  
        return false;  
    }

}); 

function mostrarBotonAdministrativo(){

    boton = document.getElementById("botonGuardarAdministrativo");
    habeas = document.getElementById("habeasCheckAdministrativo");
    if (habeas.checked) {
        boton.style.display='block';
    }
    else {
        boton.style.display='none';
    }
}

function fondo($num){

    switch($num) {
    case 1:
        $('#divPrincipal').css("background-image", "url(../../../theme/img/cuestionario/1.-informacion-personal.png)");
        break;
    case 2:
        $('#divPrincipal').css("background-image", "url(../../../theme/img/cuestionario/2.-composicion-familiar.png)");
        break;
    case 3:
        $('#divPrincipal').css("background-image", "url(../../../theme/img/cuestionario/3.-caracteristicas-vivienda.png)");
        break;
    case 4:
        $('#divPrincipal').css("background-image", "url(../../../theme/img/cuestionario/4.-formacion-academica.png)");
        break;
    case 5:
        $('#divPrincipal').css("background-image", "url(../../../theme/img/cuestionario/5.-experiencia-laboral.png)");
        break;
    case 6:
        $('#divPrincipal').css("background-image", "url(../../../theme/img/cuestionario/6.-informacón-laboral.png)");
        break;
    }
    
}