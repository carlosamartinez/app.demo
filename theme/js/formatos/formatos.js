$(document).ready(function(){
    
    var eps = $("#eps_valor").val();
    var afp = $("#afp_valor").val();
    var cesantias = $("#cesantias_valor").val();

    $("#eps option").each(function(){
       var item = $(this).attr('value'); 
       if(item == eps){  
            $("#eps").val(item);
       }
    });

    $("#afp option").each(function(){
       var item = $(this).attr('value'); 
       if(item == afp){  
            $("#afp").val(item);
       }
    });

    $("#cesantias option").each(function(){
       var item = $(this).attr('value'); 
       if(item == cesantias){  
            $("#cesantias").val(item);
       }
    });

});

$("#form_gestion_formatos").submit(function(){  
    
    tipo_formato = document.getElementsByName("tipo_formato");
    var seleccionado_tipo_formato = false;
    for(var i=0; i<tipo_formato.length; i++) {    
        if(tipo_formato[i].checked) {
            seleccionado_tipo_formato = true;
        }
    }

    if ((seleccionado_tipo_formato)==false) {
        alert("Por favor seleccione un tipo de formato");  
        return false;
    }
           
});

$(function($){
  $.datepicker.regional['es'] = {
    closeText: 'Cerrar',
    prevText: '&#x3c;Ant',
    nextText: 'Sig&#x3e;',
    currentText: 'Hoy',
    monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
    'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
    monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
    'Jul','Ago','Sep','Oct','Nov','Dic'],
    dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
    dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
    dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
    weekHeader: 'Sm',
    dateFormat: 'dd/mm/yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
  $.datepicker.setDefaults($.datepicker.regional['es']);
});


$("#fecha_ingreso").datepicker({
  dateFormat: 'yy-mm-dd'
});

$("#fecha_retiro").datepicker({
  dateFormat: 'yy-mm-dd'
});

