var id_modifica_concepto = '';
var btn_save = '';

$(document).ready(function(){

    $(".dialog").css('display', 'none');   


    setTimeout(function(){
            $(".visibility_close").fadeOut(1500);
            $(".visibility_close").remove();
        },3000);



    $.ajax({
    	url : '../Digital/load_conceptos',
    	// data : { id : 123 },
    	type : 'POST',
    	dataType : 'json', 
    	success: function(result){
            var concepto_defect = $('#concepto_apr').val();
    		var toAppend = '';
    	    $.each(result,function(i,o){	
    		   	toAppend += '<option>'+o+'</option>';
    		});
    		$('#concepto').append(toAppend);
            $("#concepto option").each(function(){
               var item = $(this).attr('value');
               if(item == concepto_defect){  
                    $("#concepto").val(item);
               }
            });
    	} 
    });


    $.ajax({
        url : '../Digital/load_ubicaciones',
        // data : { id : 123 },
        type : 'POST',
        dataType : 'json', 
        success: function(resulta){
            var ubicacion_defect = $('#ubicacion_apr').val();
            var toAppend2 = '';
            $.each(resulta,function(ii,oo){    
                toAppend2 += '<option>'+oo+'</option>';
            });
            $('#ubicacion').append(toAppend2);

            $("#ubicacion option").each(function(){
               var item = $(this).attr('value'); 
               if(item == ubicacion_defect){  
                    $("#ubicacion").val(item);
               }
            });
        } 
    });


    $.ajax({
        url : '../Digital/load_usuario_aprueba',
        type : 'POST',
        dataType : 'json', 
        success: function(resultad){
            var user_defect = $('#persona_apr').val();
            var toAppend3 = '';
            $.each(resultad,function(iii,ooo){ 
                toAppend3 += '<option value="'+ooo.id_usuario+'">'+ooo.nombres+'</option>';
            });
            $('#usuario_aprueba').append(toAppend3);
            $('#search_users').append(toAppend3);



            $("#usuario_aprueba option").each(function(){
               var item = $(this).attr('value'); 
               if(item == user_defect){  
                    $("#usuario_aprueba").val(item);
               }
            });

        } 
    });

});
    $( "#pendientes_reports" ).click(function() {
      $( "#pendientes_action_report" ).submit();
    });

    $("#aprobados_reports").click(function() {
      $( "#aprobados_action_report" ).submit();
    });
    $( "#listo_pago_reports" ).click(function() {
      $( "#listo_pago_action_report" ).submit();
    });

    $("#pagados_reports").click(function() {
      $( "#pagados_action_report" ).submit();
    });



    // evento al guardar informacion en el formulario de conceptos
    $("#guardar_concepto").click(function(){
      var nombre_concepto       = $('#concepto').val();
      var observacion_concepto  = $('#observaciones').val();
      if(nombre_concepto == '' && observacion_concepto == '')
      {
        alert('usted debe de diligenciar todos los campos');
      }else{ 
          $.ajax({
              url : '../Conceptos/create_save_conceptos',
              data : { nombre_concepto : nombre_concepto, observacion_concepto : observacion_concepto },
              type : 'POST',
              dataType : 'json', 
              success: function(resultad){
                  if(resultad)
                  {
                    alert('Registro exitoso');

                    $('#concepto').val('');
                    $('#observaciones').val('');

                    $.ajax({
                        url : '../Conceptos/load_registros_conceptos',
                        type : 'POST',
                        dataType : 'json', 
                        success: function(return_info){
                            $('.concepto_fila').remove();
                            var filas = '';
                            $.each(return_info,function(iii,ooo){    
                                    filas = "<tr class='even concepto_fila'>";
                                    filas += "<td>"+ooo.nombre_concepto+"</td>";
                                    filas += "<td>"+ooo.observaciones+"</td>";
                                    filas += "<td><button type='submit' id='"+ooo.id_concepto+"' onclick='buscar(this);' class='btn btn-success'><i class='halflings-icon zoom-in'></i></button><button type='submit' id='"+ooo.id_concepto+"' onclick='delet(this);' class='btn btn-danger'><i class='halflings-icon trash'></i></button></td>";
                                    filas += "</tr>";
                                    $('.concepto_table').append(filas);
                            });                                      
                        } 
                    });
                  }else{
                    alert('El registro ya existe');
                  }
              } 
          });
        }
    });


    $("#search_users").change(function() {
      
        var id_search = $("#search_users").val();
        $.ajax({
            url : '../Usuarios/load_unico_user',
            data : {id_usuario : id_search},
            type : 'POST',
            dataType : 'json', 
            success: function(respues_user_unico){

                  btn_save    = '<button type="submit" class="btn btn-primary save">Crear</button>';
              var btn_edit    = '<input type="submit" name="modificar_registro" id="modificar_registro" class="btn btn-success" value="Modificar" />';
              var btn_delet   = '<input type="submit" name="eliminar_registro" id="eliminar_registro" class="btn btn-danger" value="Eliminar" />';
              var btn_limpiar = '<button onclick="limpiar_cl();" id="limpiar_cl" class="btn btn-info">Limpiar</button>'
              var input_id    = '<input type="hidden" id="id_change" name="id_change" value="'+id_search+'" />';

              $('#nombres').val(respues_user_unico.nombres);
              $('#cedula_i').val(respues_user_unico.cedula);
              // $('#dependencia').val(respues_user_unico.dependencia);
              $('#correo').val(respues_user_unico.correo);
              $('#password').val(respues_user_unico.password);
              $('#password2').val(respues_user_unico.password);

              $("#rol option").each(function(){
                 var item = $(this).attr('value'); 
                 if(item == respues_user_unico.rol){  
                      $("#rol").val(item);
                 }
              });

              $("#area option").each(function(){
                 var item = $(this).attr('value'); 
                 if(item == respues_user_unico.dependencia){  
                      $("#area").val(item);
                 }
              });

              $('.save').remove();

              if(!$('#modificar_registro').length)
              {
                $('.users_modifiqy').append(btn_edit,btn_delet,input_id);
                $('.search_elemt').append(btn_limpiar);
              }else{
                  $('#id_change').remove();
                  $('.users_modifiqy').append(input_id);
              }
                
            } 
        });
        
    });

function limpiar_cl()
{

  $('#modificar_registro').remove();
  $('#eliminar_registro').remove();
  $('#id_change').remove();
  $('#limpiar_cl').remove();

  $('#nombres').val('');
  $('#cedula_i').val('');
  $('#dependencia').val('');
  $('#correo').val('');
  $('#password').val('');
  $('#password2').val('');

  $('.users_modifiqy').append(btn_save);

}

function delet(atribute){


    var id = $(atribute).attr('id');
    limpiar();

    $.ajax({
        url : '../Conceptos/delete_regis',
        data : { id : id },
        type : 'POST',
        dataType : 'json', 
        success: function(respuesta){
            $('.concepto_fila').remove();
            $.each(respuesta,function(iii,ooo){    
                filas = "<tr class='even concepto_fila'>";
                filas += "<td>"+ooo.nombre_concepto+"</td>";
                filas += "<td>"+ooo.observaciones+"</td>";
                filas += "<td><button type='submit' id='"+ooo.id_concepto+"' onclick='buscar(this);' class='btn btn-success'><i class='halflings-icon zoom-in'></i></button><button type='submit' id='"+ooo.id_concepto+"' onclick='delet(this);' class='btn btn-danger'><i class='halflings-icon trash'></i></button></td>";
                filas += "</tr>";
                $('.concepto_table').append(filas);
            });
        } 
    });   

}

// funcion que nos trar la informacion del registro que solicito el usuario 
function buscar(atribute){

    $('#concepto').val('');
    $('#observaciones').val('');

    var id = $(atribute).attr('id');
    id_modifica_concepto = id;

   $.ajax({
       url : '../Conceptos/loading_unico_concepto',
       data : { id : id },
       type : 'POST',
       dataType : 'json', 
       success: function(return_unico){
            // console.log(return_unico);
            $('#concepto').val(return_unico.nombre_concepto);
            $('#observaciones').val(return_unico.observaciones);

            var concepto_text = $('#concepto').val();
         
            if(concepto_text != '')
            {  
               if (!$('#modificar_concepto').length){
                //Aquí pondríamos el código que queremos ejecutar
                   var butones_modificar = '<button id="modificar_concepto" onclick="modificar();" class="btn btn-info">Modificar</button>';
                       butones_modificar += '<button id="clear_concepto" onclick="limpiar();" class="btn btn-danger">Limpiar</button>';
                   $("#guardar_concepto").after(butones_modificar); 
                   $('#guardar_concepto').attr('disabled','disabled');
               }
            }   
       } 
   });    
}

$("#compras_submit" ).click(function(){
  $( "#compras_form" ).submit();
});

$("#director_submit" ).click(function(){
  $( "#director_form" ).submit();
});

$("#control_submit" ).click(function(){
  $( "#control_form" ).submit();
});

$("#contabilidad_submit" ).click(function(){
  $( "#contabilidad_form" ).submit();
});

$("#pagos_submit" ).click(function(){
  $( "#pagos_form" ).submit();
});

$("#datosFormatos_submit" ).click(function(){
  $( "#datosFormatos_form" ).submit();
});

$("#gestionFormatos_submit" ).click(function(){
  $( "#gestionFormatos_form" ).submit();
});

// function para limpiar datos y volver habilitar boton de guardar
function limpiar()
{
     $('#concepto').val('');
     $('#observaciones').val('');
     $('#guardar_concepto').removeAttr("disabled");
     $('#clear_concepto').remove();
     $('#modificar_concepto').remove();
}

function modificar()
{
    var concepto      = $('#concepto').val();
    var observaciones = $('#observaciones').val();

    $.ajax({
        url : '../Conceptos/update_regis',
        data : {id : id_modifica_concepto, concepto : concepto, observaciones : observaciones},
        type : 'POST',
        dataType : 'json', 
        success: function(respuesta){

            $('.concepto_fila').remove();
            var filas = '';
            $.each(respuesta,function(iii,ooo){    
                    filas = "<tr class='even concepto_fila'>";
                    filas += "<td>"+ooo.nombre_concepto+"</td>";
                    filas += "<td>"+ooo.observaciones+"</td>";
                    filas += "<td><button type='submit' id='"+ooo.id_concepto+"' onclick='buscar(this);' class='btn btn-success'><i class='halflings-icon zoom-in'></i></button><button type='submit' id='"+ooo.id_concepto+"' onclick='delet(this);' class='btn btn-danger'><i class='halflings-icon trash'></i></button></td>";
                    filas += "</tr>";
                    $('.concepto_table').append(filas);
            }); 
            limpiar();
            alert('actualizacion del concepto exitosa');
            
        } 
    }); 
}

function view_image_document(atrib)
{
  var img_view = $(atrib).attr('id');
  var res = img_view.split(".");
  
  if(res[1] == 'pdf'){
    $("#image_pdf").remove();
    $("#image_plamar").remove();
    $(".gets_docu").after('<object id="image_pdf" type="application/pdf" data="'+img_view+'" width="900" height="500"></object>');
  }else if(res[1] == 'jpg'){
    $("#image_pdf").remove();
    $("#image_plamar").remove();
    $(".gets_docu").after('<img src="'+img_view+'" id="image_plamar" class="img-responsive" alt="Imagen del documento">')
  }

}



 function get_obs(at)
 {
    var id = $(at).attr('id');
    $(".form-gla_negado4_"+id).submit(function(){
      return false;
    });
    $("#dialog_"+id).dialog();  
 }


 function get_submit(atrib)
 {
    var id = $(atrib).attr('id');
    // console.log("#form-gla_negado4_"+id+"")
    // form-gla_negado4_1
      $.ajax({  
        url : 'insert_date_gety',
        data: $("#form-gla_negado4_"+id+"").serialize(),
        type : 'POST',
        dataType : 'json', 
        success: function(result){
               
        } 

      });
 }


