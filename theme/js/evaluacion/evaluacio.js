$(document).ready(function(){
	$("#dialog").dialog({ autoOpen: false});             
});

$("#new_evaluation").click(function(){
	$(function(){
	    $( "#dialog" ).dialog('open');
	});
});

var cantidad = 0;
var nombre_evaluacion = '';
var descrip = '';

$('form')[0].reset(); 

 // funcion para generar el boton y recorrer los inputs
function myFunction(){
	
	var ban = 0;
	$("#caract_pro input, textarea").each(function(index, elemet){
		if($(elemet).val().length < 1){
			ban = 1;
		}else{
			cantidad = $('#cantidad_preguntas').val();
			nombre_evaluacion = $('#nombre_evaluacion').val();
			descrip = $("#descripcion").val();
		}
	});

	if(!ban){		
		if($("#generar").length < 1){
			$('#caract_pro').after('<button id="generar" type="button" onclick="con()" class="btn btn-success btn-lg">Generar evaluación</button>');
		}
	}else{
		$("#generar").remove();
	}
}
var descripcion				= '';
var nombre_evaluacion_bd	= '';

function con()
{
	nombre_evaluacion_bd = $("#nombre_evaluacion").val();
	descripcion = $("#descripcion").val();

	$("#dialog").dialog({ open: false, height: 800, width: 1300 });
	$("#caract_pro").remove();
	$("#generar").remove();

	var htm_new = '<div class"caract_pro">';
	htm_new +=	'<center>';
	htm_new +=  	'<button type="button" class="btn btn-primary"><i>Evaluación de '+nombre_evaluacion+'</i></button>';
	htm_new +=	'</center>';
	htm_new +=	'</br>';
	htm_new +=	'<p><i>'+descrip+'<i></p>';
	htm_new +=	'</br>';

	for(var i = 1; i <= cantidad; i++){

		htm_new +=  '<div class="control-group pregunta'+i+' 	quest" style="margin-top: 4%;border-style: groove;">';
		htm_new +=	'<label class="control-label"><i>Digite la pregunta con sus opciones de respuestas y la verdadera respuesta</i></label>';
		htm_new +=	'<div class="controls">';
		htm_new +=		 '<div class="form-group">';
		htm_new +=		 	'<b>'+i+'.</b> <input class="input-xlarge focused" id="nombre_evaluacion_'+i+'" style="margin-right: 5%;" name="" type="text" value="">';
		htm_new +=			'<b>Cuantas opciones de respuesta?</b>'
		htm_new +=      	'<select id="cant_options_'+i+'" class="wi_sty_" onchange="view_option(this)"><option>Seleccione</opction><option>1</option><option>2</option><option>3</option><option>4</option><option>5</option></select>';
		htm_new +=		 '</div>';
		htm_new +=	'</div>';
		htm_new += '</div>';

	}	
		htm_new += '</div>';

	$("#dialog").append(htm_new);	
	$("#dialog").dialog('open');
}

var pas;
var num_form
var option_selection = 0;

// por medio de esta funcion podemos realizar la 
// logica de construccion de las posibles respuestas del usuario
function view_option(atributte)
{
	// eliminamos los elementos de ponderacion siempre y
	// cuando el usuario no halla gestionado los campos respectivos
	if($("#ponderacion").length >= 1){
		$("#ponderacion").remove();
	}

	var num_concatena = $(atributte).attr('id');
	var asig = num_concatena.split("_");
	var sles = asig[2];
	var cantidad_digitos= $("#nombre_evaluacion_"+sles).val();

	if(cantidad_digitos.length < 5 || cantidad_digitos.value=='') {  
        alert("Se le recomienda que la pregunta tenga por lo menos 5 caracteres");   
   	}else{
   		if(cantidad_digitos.length > 5 || cantidad_digitos.value!='') {  
     
	var res = $(atributte).attr('id').split("_");
		num_form 			= res[2];
	var id_atri				= $(atributte).attr('id');
	var option_selection 	= $("#"+id_atri).val();
	$(".respuestas_true_"+num_form+"").remove();

	var abecedario = ["A", "B", "C", "D", "E"];
		pas = 'opciones_respuestas_'+num_form;

	var html_option_respues = '';
		html_option_respues += '<div class="opciones_respuestas_'+num_form+'">';

	for(var i = 0; i < option_selection; i++){
		html_option_respues += '<b style="margin-left: 5px;">'+abecedario[i]+'</b><input style="margin-right: 7px;" class="opciones_respuestas_'+num_form+'" onchange="evalu(this);" type="text" name="">';
	}
		html_option_respues += '</div>';
		$('.opciones_respuestas_'+num_form+'').remove();
		$('.pregunta'+num_form).append(html_option_respues);		
		}
	}
}

function evalu(atributte)
{
	var ban_btn = 1;
	var pas = $(atributte).attr('class');
	var id_atri= $(atributte).attr('class');
	var asig = id_atri.split("_");
	num_form = asig[2];

	$("."+pas+" input").each(function(index, elemento){		
		if($(elemento).attr('value') == ''){
			ban_btn = 0;
		}
	});
	
	if(ban_btn){

		var	select_respuestas_true = '<div class="respuestas_true_'+num_form+'">';
			select_respuestas_true += '<label>Seleccione la cantidad de respuestas verdaderas</label>'
			select_respuestas_true += 	'<select id="true_'+num_form+'" class="wi_sty" onchange="true_option(this)">';

			var con = 0;
			select_respuestas_true +='<option>seleccione...</option>' // combo respuestas verdaderas
			$(".opciones_respuestas_"+num_form+" b").each(function(){
				con++;
				select_respuestas_true += '<option>'+con+'</option>';
			});			
			select_respuestas_true += 	'</select>';
			select_respuestas_true += '</div>';

			if(!$(".respuestas_true_"+num_form+"").length){
				$(".pregunta"+num_form+"").append(select_respuestas_true);
			}
	}else{
				$(".respuestas_true_"+num_form+"").remove();
	}	
}

function true_option(atributte){

	var id_atri	= $(atributte).attr('id');
 	var option_selection = $("#"+id_atri).val();
 	var asig = id_atri.split("_");

 	//console.log('option_selection en true option ' +option_selection );

	num_form = asig[1];

	var option =  '';
		option += '<option>Seleccione...</option>' //ultimo combo
	$(".opciones_respuestas_"+num_form+" b").each(function(index, elemento){
		option += '<option>'+$(elemento).text()+'</option>';
	});

	var select = '<div class="elems_true_true'+num_form+'">';

	for(var i = 0; i < option_selection; i++){	
		select += '<select id="selection_option_'+i+'" class="wi_sty" style="margin-right: 1%;" onchange="valid_porcentajes(this)">';
		select +=   option; 
		select += '</select>';
	}
		select += '</div>';

	$(".elems_true_true"+num_form+"").remove();
	$("#true_"+num_form+"").after(select);

	option = $("#true_"+num_form+" option:selected").text();

	if (option == 'seleccione...') {
		$("#ponderacion").remove();
	}
}

function valid_porcentajes(atributte){

	var pun_count = 1;
	var bandera_find = 0;

	var id_atri	= $(atributte).attr('id');

 	var selection_option = $("#"+id_atri).val();
	// console.log('selection_option ' + selection_option);

 	var asignn = id_atri.split("_");
	// console.log('asignn . . ' + asignn)

 	num_form = asignn[2]; //contador 

	var id = id_atri.split("_");
	var option_selection = $(".wi_sty").val();	

		if (selection_option != 'Seleccione...') {
			$(".quest").each(function(){
				if($(".elems_true_true"+pun_count+" select").length < 1){
					bandera_find = 0;
				} else {
					$(".elems_true_true"+pun_count+" select").each(function(index, elemento){
						if($(elemento).val() == 'Seleccione...' ){
							bandera_find = 0;
						}else{
							bandera_find = 1;
							if ($(elemento) == 0 ){
								alert('hola');
							}
 							if($(bandera_find).length == 1 ){
								$("#ponderacion").remove();
						}
					}
				});
					pun_count++;
					$("#ponderacion").remove();
				}
			});	
		}

	if(bandera_find) {

		if($("#ponderacion").length < 1){
			con_option = '<option>Seleccione..</option>';
			cantidad_pregunt = 0;

			$(".quest").each(function(){
				cantidad_pregunt++;
				con_option += '<option>'+cantidad_pregunt+'</option>';

			});

			html_ponderado ='';
			html_ponderado +=	'<div id="ponderacion" class="content">';
			html_ponderado +=		'<h1><b>Ponderación</b></h1>'
			html_ponderado +=		'<p>En total son <b id="cantidad_preguntas">'+cantidad_pregunt+'</b> preguntas  <b>= 100</b> puntos</p>';
			html_ponderado +=		'<div class="ponderacion">';
			html_ponderado +=			'<div class="row-fluid">';
			html_ponderado +=				'<div class="span3">';
			html_ponderado +=					'<div class="">';
			html_ponderado +=						'<label>De la pregunta</label>';		
			html_ponderado +=						'<select name="" id="desde_one" class="ponderacion_val operation1" >';
			html_ponderado +=						  	con_option;
			html_ponderado +=						'</select>';
			html_ponderado +=					'</div>';				  
			html_ponderado +=				'</div>';
			html_ponderado +=				'<div class="span3">';
			html_ponderado +=					'<div class="">';
			html_ponderado +=						  '<label>Hasta la pregunta</label>';		
			html_ponderado +=						  '<select name="" id="hasta_one" class="ponderacion_val operation1" >';
			html_ponderado +=						  	con_option;
			html_ponderado +=						  '</select>';
			html_ponderado +=					'</div>';	  
			html_ponderado +=				'</div>';
			html_ponderado +=				'<div class="span3">';
			html_ponderado +=					'<div class="">';
			html_ponderado +=						'<label>Es equivalente</label>';
			html_ponderado +=						  '<input id="equivalente_porcentaje_one" class="ponderacion_val operation1"  onchange="btn_save_view()" type="number">';
			html_ponderado +=					'</div>';
			html_ponderado +=				'</div>';
			html_ponderado +=				'<div class="span3">';
			html_ponderado +=					'<div class="">';
			html_ponderado +=						'<label>Pregunta unitaria</label>';
			html_ponderado +=						  '<input id="equivalente_unitaria_one" class="ponderacion_val" type="text" disabled="disabled">';
			html_ponderado +=					'</div>';
			html_ponderado +=				'</div>';
			html_ponderado +=			'</div>';
			html_ponderado +=			'<div class="row-fluid">';
			html_ponderado +=				'<div class="span3">';
			html_ponderado +=					'<div class="">';
			html_ponderado +=						'<label>De la pregunta</label>';		
			html_ponderado +=						'<select name="" id="desde_two" class="ponderacion_val operation2">';
			html_ponderado +=						  	con_option;
			html_ponderado +=						'</select>';
			html_ponderado +=					'</div>';				  
			html_ponderado +=				'</div>';
			html_ponderado +=				'<div class="span3">';
			html_ponderado +=					'<div class="">';
			html_ponderado +=						  '<label>Hasta la pregunta</label>';		
			html_ponderado +=						  '<select name="" id="hasta_two" class="ponderacion_val operation2" >';
			html_ponderado +=						  	con_option;
			html_ponderado +=						  '</select>';
			html_ponderado +=					'</div>';	  
			html_ponderado +=				'</div>';
			html_ponderado +=				'<div class="span3">';
			html_ponderado +=					'<div class="">';
			html_ponderado +=						'<label>Es equivalente</label>';
			html_ponderado +=						  '<input id="equivalente_porcentaje_two" type="number" onchange="btn_save_view()" class="ponderacion_val operation2" >';
			html_ponderado +=					'</div>';
			html_ponderado +=				'</div>';
			html_ponderado +=				'<div class="span3">';
			html_ponderado +=					'<div class="">';
			html_ponderado +=						'<label>Pregunta unitaria</label>';
			html_ponderado +=						  '<input id="equivalente_unitaria_two" type="number" class="ponderacion_val" disabled="disabled">';
			html_ponderado +=					'</div>';
			html_ponderado +=				'</div>';
			html_ponderado +=			'</div>';
			html_ponderado +=			'<div class="row-fluid">';
			html_ponderado +=				'<h1><b>Calificación</b></h1>';
			html_ponderado +=			'</div>';				
			html_ponderado +=			'<div class="row-fluid">';	
			html_ponderado +=				'<div class="span3">';
			html_ponderado +=					'<h3>Excelente</h3>';
			html_ponderado +=					'<div class="">';
			html_ponderado +=						'<label>Desde:</label>';
			html_ponderado +=						  '<input id="desde_exelente" class="ponderacion_val" type="number">';
			html_ponderado +=					'</div>';
			html_ponderado +=					'<div class="">';
			html_ponderado +=						'<label>Hasta:</label>';
			html_ponderado +=						  '<input id="hasta_exelente" class="ponderacion_val" type="number">';
			html_ponderado +=					'</div>';
			html_ponderado +=				'</div>';
			html_ponderado +=				'<div class="span3">';
			html_ponderado +=					'<h3>Bueno</h3>';
			html_ponderado +=					'<div class="">';
			html_ponderado +=						'<label>Desde:</label>';
			html_ponderado +=						  '<input id="desde_bueno" class="ponderacion_val" type="number">';
			html_ponderado +=					'</div>';
			html_ponderado +=					'<div class="">';
			html_ponderado +=						'<label>Hasta:</label>';
			html_ponderado +=						  '<input id="hasta_bueno"  class="ponderacion_val" type="number">';
			html_ponderado +=					'</div>';
			html_ponderado +=				'</div>';
			html_ponderado +=				'<div class="span3">';
			html_ponderado +=					'<h3>Aceptable</h3>';
			html_ponderado +=					'<div class="">';
			html_ponderado +=						'<label>Desde:</label>';
			html_ponderado +=						  '<input id="desde_aceptable" class="ponderacion_val"  type="number">';
			html_ponderado +=					'</div>';
			html_ponderado +=					'<div class="">';
			html_ponderado +=						'<label>Hasta:</label>';
			html_ponderado +=						  '<input id="hasta_Aceptable" class="ponderacion_val" type="number">';
			html_ponderado +=					'</div>';
			html_ponderado +=				'</div>';
			html_ponderado +=				'<div class="span3">';
			html_ponderado +=					'<h3>Deficiente</h3>';
			html_ponderado +=					'<div class="">';
			html_ponderado +=						'<label>Desde:</label>';
			html_ponderado +=						  '<input id="desde_deficiente" class="ponderacion_val" type="number">';
			html_ponderado +=					'</div>';
			html_ponderado +=					'<div class="controls">';
			html_ponderado +=						'<label>Hasta:</label>';
			html_ponderado +=						  '<input id="hasta_deficiente" class="ponderacion_val" onchange="btn_save_view()" type="number"> ';
			html_ponderado +=					'</div>';
			html_ponderado +=				'</div>';
			html_ponderado +=			'</div>';									
			html_ponderado +=		'</div>';
			html_ponderado +=	'</div>';

			$("#dialog").append(html_ponderado);
		}	
	}
	else {
		if (bandera_find == 0 || selection_option === 'Seleccione...') {
			$("#ponderacion").remove();
		}	
	}
}


function btn_save_view(){
	var operation1 = 1;
	$('.operation1').each(function(index, elemento){
		if($(elemento).val() == ''){
			operation1 = 0;
		}
	});

	if(operation1){
		var desde = $("#desde_one").val();
		var hasta = $("#hasta_one").val();	
		var unitario_one = (hasta - desde);
		unitario_one = unitario_one+1;
		var total1 = parseFloat($("#equivalente_porcentaje_one").val()) / parseFloat(unitario_one);
		$("#equivalente_unitaria_one").val(total1);

	}

	var operation2 = 1;
	$('.operation2').each(function(index, elemento){		
		if($(elemento).val() == ''){
			operation2 = 0;
		}
	});
	
	if(operation2){
		var desde_two = $("#desde_two").val();
		var hasta_two = $("#hasta_two").val();	
		var unitario_two = (hasta_two - desde_two);
		unitario_two = unitario_two+1;
		var total2 = parseFloat($("#equivalente_porcentaje_two").val()) / parseFloat(unitario_two);
		$("#equivalente_unitaria_two").val(total2);		
	}
	
	validacion_btn_save = 1;
	$(".ponderacion_val").each(function(index, elemento){
		if($(elemento).val() == ''){
			validacion_btn_save = 0;
		}
	});

	if(validacion_btn_save ){
		$(".ponderacion").after("<button id='guardar_evaluacion' class='btn btn-primary' onclick='captura_date()'>Guardar evaluación</button>");
	}
}

var n = 1;
var array_objets =[];

function captura_date(){
    contador = 0;
    $(".quest").each(function(index, elemento){
    	contador++;
    	recorrer_cuestions(contador);
    });
    calificaciones();
    save_info_evaluacion();
    console.log(array_objets);
}

function recorrer_cuestions(contador){
	
	var miObjeto = new Object();
	miObjeto.pregunta  = $("#nombre_evaluacion_"+contador+"").val();
	$(".opciones_respuestas_"+contador+" input").each(function(index, elemento){	
		miObjeto.opciones_respuestas += '_'+$(elemento).val();			
	});
	$(".elems_true_true"+contador+" select").each(function(index, elemento){
		miObjeto.respuestas_verdaderas += '_'+$(elemento).val();	
	});
	array_objets[n] = miObjeto;	
	n++;
}


function calificaciones(){
	var miObjeto = new Object();

	miObjeto.desde_one 						= $("#desde_one").val();
	miObjeto.hasta_one 						= $("#hasta_one").val();
	miObjeto.equivalente_porcentaje_one		= $("#equivalente_porcentaje_one").val();
	miObjeto.equivalente_unitaria_one		= $("#equivalente_unitaria_one").val();

	miObjeto.desde_two 						= $("#desde_two").val();
	miObjeto.hasta_two 						= $("#hasta_two").val();
	miObjeto.equivalente_porcentaje_two		= $("#equivalente_porcentaje_two").val();
	miObjeto.equivalente_unitaria_two		= $("#equivalente_unitaria_two").val();

	miObjeto.calificacion_exelente_desde	= $("#desde_exelente").val();
	miObjeto.calificacion_exelente_hasta	= $("#hasta_exelente").val();
	miObjeto.calificacion_bueno_desde		= $("#desde_bueno").val();
	miObjeto.calificacion_bueno_hasta		= $("#hasta_bueno").val();
	miObjeto.calificacion_aceptable_desde	= $("#desde_aceptable").val();
	miObjeto.calificacion_aceptable_hasta	= $("#hasta_Aceptable").val();
	miObjeto.calificacion_deficiente_desde	= $("#desde_deficiente").val();
	miObjeto.calificacion_deficiente_hasta	= $("#hasta_deficiente").val();
	
	array_objets['calificaciones'] = miObjeto;

}


function save_info_evaluacion()
{
	$.ajax({
		url : '../Evaluacion/get_save_evalutions',
		data : { array:array_objets, calificaciones:array_objets.calificaciones, nombre_evaluacion:nombre_evaluacion_bd, descripcion:descripcion},
		type : 'POST',
		dataType : 'json', 
		success: function(result){	  		
	  		if(result){ 
	  			alert('La evaluación se guardó con exito');
	  		}else{
	  			alert('No se pudo realizar el ingreso de la evaluación');
	  		}
		} 
	});
}

//-------------------- eventos js de la parte donde el usuario presentara la evaluacion ------------------------//

function valid_cedula_evalua(){

	cedula = $("#cedula_presenta_evalu").val();	
	$.ajax({
		url : 'get_evaluaciones_cedula',
		data : { cedula : cedula },
		type : 'POST',
		dataType : 'json', 
		success: function(result){

			if(result){		

				$(".op").remove();

				html_date_pers = '<div class="col-xs-2 op">';
				html_date_pers +=	'<label class="form-control-label" for="inputSuccess1">Nombres</label>';
				html_date_pers +=	'<input type="text" class="form-control form-control-success ipi" value="'+result.nombres+'" disabled>';
				html_date_pers += '</div>';

				html_date_pers += '<div class="col-xs-2 op">';
				html_date_pers +=	'<label class="form-control-label" for="inputSuccess1">Apellidos</label>';
				html_date_pers +=	'<input type="text" class="form-control form-control-success ipi" value="'+result.apellidos+'" disabled>';
				html_date_pers += '</div>';

				html_date_pers += '<div class="col-xs-2 op">';
				html_date_pers +=	'<label class="form-control-label" for="inputSuccess1">Cargo laboral</label>';
				html_date_pers +=	'<input type="text" class="form-control form-control-success ipi" value="'+result.cargo_laboral+'" disabled>';
				html_date_pers += '</div>';

				html_date_pers += '<div class="col-xs-2 op">';
				html_date_pers +=	'<label class="form-control-label" for="inputSuccess1">Departamento</label>';
				html_date_pers +=	'<input type="text" class="form-control form-control-success ipi" value="'+result.departamento+'" disabled>';
				html_date_pers += '</div>';

				html_date_pers += '<div class="col-xs-2 op">';
				html_date_pers +=	'<label class="form-control-label" for="inputSuccess1">Id empleado</label>';
				html_date_pers +=	'<input type="text" class="form-control form-control-success ipi" value="'+result.id_empleado+'" disabled>';
				html_date_pers += '</div>';

				var evaluacion_selec = $(".c-select").val();

				if(evaluacion_selec != 'Seleccione...'){
				
					html_date_pers += '<div class="col-xs-2 col-md-offset-5 op" style="padding-top: 20px;">';
					html_date_pers += 	'<button type="button" onclick="construc_examen()" class="btn btn-success-outline"  >Realizar evaluacion</button>';
					html_date_pers += '</div>';
				}
	
				$("#camp-cedula").append(html_date_pers);
				$("#message-notific").text('Cedula existente');
				$("#camp-cedula").attr('class', 'form-group has-success');

			}else{

				$(".op").remove();
				$("#camp-cedula").attr('class', 'form-group');

				html_date_pers = '<div class="col-xs-3 op">';
				html_date_pers +=	'<label class="form-control-label" for="inputSuccess1">Nombres</label>';
				html_date_pers +=	'<input type="text" class="form-control form-control-success ipi"  onchange="valid_btn_gnr()">';
				html_date_pers += '</div>';

				html_date_pers += '<div class="col-xs-3 op">';
				html_date_pers +=	'<label class="form-control-label" for="inputSuccess1">Apellidos</label>';
				html_date_pers +=	'<input type="text" class="form-control form-control-success ipi" onchange="valid_btn_gnr()">';
				html_date_pers += '</div>';

				html_date_pers += '<div class="col-xs-2 op">';
				html_date_pers +=	'<label class="form-control-label" for="inputSuccess1">Cargo laboral</label>';
				html_date_pers +=	'<input type="text" class="form-control form-control-success ipi" onchange="valid_btn_gnr()">';
				html_date_pers += '</div>';

				html_date_pers += '<div class="col-xs-2 op">';
				html_date_pers +=	'<label class="form-control-label" for="inputSuccess1">Area organizacional</label>';
				html_date_pers +=	'<input type="text" class="form-control form-control-success ipi" onchange="valid_btn_gnr()" >';
				html_date_pers += '</div>';


				$("#camp-cedula").append(html_date_pers);
				$("#message-notific").text('Cedula');
				
			}
		}
	});

}

function valid_btn_gnr()
{
	var btn_ban_evl = 1;
	if($(".ipi").length > 0){
		$(".ipi").each(function(index, elemeto){
			if($(elemeto).val().length <= 0){
				 btn_ban_evl = 0;
			}
		});
	}else{
		btn_ban_evl = 0;
	}

	if($(".c-select").val() == 'Seleccione...'){
		btn_ban_evl = 0;
		$(".btn-success-outline").remove();
	}
	if(btn_ban_evl){
		if($(".btn-success-outline").length <= 0){
			var html_btn_gnr = '<div class="col-xs-2 col-md-offset-5 op" style="padding-top: 20px;">';
				html_btn_gnr += 	'<button type="button" onclick="construc_examen()" class="btn btn-success-outline">Realizar evaluacion</button>';
				html_btn_gnr += '</div>';
			$("#camp-cedula").append(html_btn_gnr);
		}		
	}
}

function construc_examen()
{
	evaluacion = $(".c-select").val();

	$.ajax({

		  url: "get_evaluacion_html",
		  global: false, 
		  type: "POST", 
		  dataType : 'html', 
		  data: {evaluacion : evaluacion}, 
		  cache: false,
		  beforeSend: function() {
		    $('#gif').append('<img src="../../theme/images/01-progress.gif" id="img_load" class="img-rounded center-block" alt="...">');
		  },
		  success: function(html){
		    $('#gif').html(html);
		    console.log(html);
		  }
	});	  	
}