
$(document).ready(function(){
	// por medio de este ajax ejecutamos los alertas de los empleados que estan proximos a salir a vacaciones
	var dependencia = $("#dependencia").val();
	$.ajax({
		url : '../Aprobaciones/get_cant_emp_vacas_notif',
		data : { dependencia:dependencia, notificacion: 'notificacion' },
		type : 'POST',
		dataType : 'json',
		success: function(result){
			cont = 0
		    $.each(result,function(ind,elem){
			   	// por medio de este metodo mandamos un mensaje de notificacion donde le mostramos a los diferentes
			   	// jefes de areas para que este pueda identificar que tiene empleados pendientes por sacar a vaciones
			   	if(result.length > 4){
			   		$('#testy').toastee({
			   			header: 'Alerta!!',
			   			type: 'error',
			   			message: 'Usted tiene mas de 4 empleados pendientes por sacar a vacaciones'
			   		});
			   		return false;
			   	}else{

			   		$('#testy').toastee({
			   			header: 'Alerta!!',
			   			type: 'error',
			   			message: 'Usted tiene a '+elem+' pendiente por sacar a vacaciones'
			   		});
			   	}
			});
		}
	});

});

//asignamos datepicker al input donde se gestionan las vacaciones del empleado (entrada y salida)
$(function(){
    $("#datepicker_salida").datepicker({dateFormat: 'yy-mm-dd'});
	$("#datepicker_retorna").datepicker({dateFormat: 'yy-mm-dd'});
});

//funcion encagada de mostrarnos si existe
$(function($){
	 $.datepicker.regional['es'] = {
		 closeText: 'Cerrar',
		 prevText: '&#x3c;Ant',
		 nextText: 'Sig&#x3e;',
		 currentText: 'Hoy',
		 monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		 'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		 monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		 'Jul','Ago','Sep','Oct','Nov','Dic'],
		 dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		 dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		 dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		 weekHeader: 'Sm',
		 dateFormat: 'dd/mm/yy',
		 firstDay: 1,
		 isRTL: false,
		 showMonthAfterYear: false,
		 yearSuffix: ''};
	 	 $.datepicker.setDefaults($.datepicker.regional['es']);
 });
 
var option_sleccionada =  0;
var id_empleado;

// funcion encargada de gestionar la vista del iframe externo
$(function(){

    $("#dialog").dialog({
      autoOpen: false,
      show:{
        effect: "blind",
        duration: 1000
      },
      hide:{
        effect: "explode",
        duration: 1000
      }
    });

    
    $(".opener").click(function(){
        var item = $(this).attr('id');
        var array_select = item.split("-");
        
        id_empleado = array_select[0];
		$("#name_emple_procces").text(array_select[1]+' '+array_select[2]);
      	$("#dialog").dialog("open");
    });

    $("#eventos_selection").change(function(){

			var value = $(this).attr("id");
    		var option_selec = $("#"+value+" option:selected").text();
			html_form = '';

    	switch(option_selec){
    	    case 'Tomar vacaciones completas':
    	    				option_sleccionada = 1;
							/*eliminamos el formulario obsoleto*/
							$('.date_vacaciones').remove();
							$('#btn_save_vaca').remove();

							/*construimos la el formulario adaptdo a la opcion que elusuario resta*/
							html_form += '<div class="row date_vacaciones">';
							html_form += '<div class="span2">';
							html_form += '<label>Indique fecha de salida</label>';
							html_form += '<input type="text" id="datepicker_salida" class="ful" onchange="valid_form_vaca()" name="datepicker">';
							html_form += '<label>Indique fecha de retorno</label>';
							html_form += '<input type="text" id="datepicker_retorna" class="ful" onchange="valid_form_vaca()" name="datepicker">';
							html_form += '</div>';
							html_form += '</div>';
							/*enviamos el formulario*/
							$("#eventos_selection").after(html_form);

    	        break;
    	    case 'Solo unos dias y los demas pagos':
    	        			// removemos los campos anteriores para q el usuario
    	        			option_sleccionada = 2;
							$('.date_vacaciones').remove();
							$('#btn_save_vaca').remove();

							html_form += '<div class="row date_vacaciones">';
							html_form += '<div class="span2">';
							html_form += '<label>Indique fecha de salida</label>';
							html_form += '<input type="text" id="datepicker_salida" class="ful" onchange="valid_form_vaca()" name="datepicker">';
							html_form += '<label>Indique fecha de retorno</label>';
							html_form += '<input type="text" id="datepicker_retorna" class="ful" onchange="valid_form_vaca()" name="datepicker">';
							html_form += '</div>';
							html_form += '<div class="span2">';
							html_form += '<label>Indique cuantos dias</label>';
							html_form += '<input type="number" class="ful" onchange="valid_form_vaca()" id="dias_tomados">';
							html_form += '</div>';
							html_form += '</div>';

							$("#eventos_selection").after(html_form);
    	        break;

    	    case 'Vacaciones pagadas':
    	    				option_sleccionada = 3;
							$('.date_vacaciones').remove();
							if($("#btn_save_vaca").length < 1){
								$("#dialog").append('<button id="btn_save_vaca" type="submit" onclick="guardar_vaca()" class="btn btn-primary">Guardar</button>');
							}
    	        break;
    	}

			$(function(){
			    $("#datepicker_salida").datepicker();
					$("#datepicker_retorna").datepicker();
			});
    });

});

// por medio de esta funcion podremos validar que
// el usuario halla gestonado todos los campos del formulario
function valid_form_vaca(){
  var ban = 0;
	$(".date_vacaciones input").each(function(index, elemento){

		if($(elemento).val().length < 1){
				 ban = 1;
		}else{
			ban = 0;
		}
  });
	// console.log(ban);
	if(!ban){
		if($("#btn_save_vaca").length < 1){
			$("#dialog").append('<button id="btn_save_vaca" type="submit" onclick="guardar_vaca()" class="btn btn-primary">Guardar</button>');
		}
	}else{
			$("#btn_save_vaca").remove();
	}

}


function guardar_vaca(){

	var fecha   = new Date();

	var fecha_inicial 		  = $("#datepicker_salida").val();
	var fecha_final   		  = $("#datepicker_retorna").val();
	var periodo 	  		  = fecha.getFullYear();
	var cantidad_dias_tomados = $("#dias_tomados").val();

	var dd = fecha.getDate();
	var mm = fecha.getMonth()+1; 

	var fecha_establece_bandera = sumaFecha(20, dd+'/'+mm+'/'+periodo);
	var array_select_fech = fecha_establece_bandera.split("/");
	fecha_establece_bandera = array_select_fech[2]+'-'+array_select_fech[1]+'-'+array_select_fech[0];

	// alerta para identificar los diferentes años de vacaciones
	switch(option_sleccionada){	
		
	    case 1:
	    	cantidad_dias_tomados = 15;
	    	var observacion = 'N/A';
	    break;

	    case 2:
	    	var observacion = 'SOLO UNOS DIAS';
	    break;

	    case 3:
	    	var observacion = 'VACACIONES PAGADAS';
	    	cantidad_dias_tomados = 0;
	    	fecha_inicial = '0000-00-00';	
	    	fecha_final   = '0000-00-00';
	    break;

    }

    var bandera_view_vacaciones = 1;

	$.ajax({
		url : '../Aprobaciones/insert_registre_vacaciones',
		data : { id_empleado:id_empleado, fecha_inicial:fecha_inicial, fecha_final:fecha_final, periodo:periodo, cantidad_dias_tomados:cantidad_dias_tomados, observacion:observacion, fecha_establece_bandera:fecha_establece_bandera, bandera_view_bacaciones:bandera_view_vacaciones },
		type : 'POST',
		dataType : 'json',
		success: function(result){

			var reload = 1;
			$.each(result, function(index, value){
			  console.log( index + ": " + value );
			  if(!value){
			  	reload = 0;
			  }
			});
			if(reload){
				location.reload(true);
			}
			
		}
	});

}

sumaFecha = function(d, fecha)
{
	 var Fecha = new Date();
	 var sFecha = fecha || (Fecha.getDate() + "/" + (Fecha.getMonth() +1) + "/" + Fecha.getFullYear());
	 var sep = sFecha.indexOf('/') != -1 ? '/' : '-'; 
	 var aFecha = sFecha.split(sep);
	 var fecha = aFecha[2]+'/'+aFecha[1]+'/'+aFecha[0];
	 fecha= new Date(fecha);
	 fecha.setDate(fecha.getDate()+parseInt(d));
	 var anno=fecha.getFullYear();
	 var mes= fecha.getMonth()+1;
	 var dia= fecha.getDate();
	 mes = (mes < 10) ? ("0" + mes) : mes;
	 dia = (dia < 10) ? ("0" + dia) : dia;
	 var fechaFinal = dia+sep+mes+sep+anno;
	 return (fechaFinal);
 }

 

