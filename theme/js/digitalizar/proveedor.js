$("#formulario_p").submit(function(){  
	
    resp_tributaria = document.getElementsByName("resp_tributaria");
 	var seleccionado_resp_tributaria = false;
	for(var i=0; i<resp_tributaria.length; i++) {    
	  	if(resp_tributaria[i].checked) {
	    	seleccionado_resp_tributaria = true;
	  	}
	}

	tipo_empresa = document.getElementsByName("tipo_empresa");
 	var seleccionado_tipo_empresa = false;
	for(var i=0; i<tipo_empresa.length; i++) {    
	  	if(tipo_empresa[i].checked) {
	    	seleccionado_tipo_empresa = true;
	  	}
	}

	forma_pago = document.getElementsByName("forma_pago");
 	var seleccionado_forma_pago = false;
	for(var i=0; i<forma_pago.length; i++) {    
	  	if(forma_pago[i].checked) {
	    	seleccionado_forma_pago = true;
	  	}
	}

	if($("#nombre").val().length < 1 || $("#direccion").val().length < 1 || $("#ciudad").val().length < 1 || $("#pais").val().length < 1 || $("#telefono").val().length < 1 || $("#contacto").val().length < 1 || $("#email").val().length < 1 || (seleccionado_resp_tributaria)==false || (seleccionado_tipo_empresa)==false  || (seleccionado_forma_pago)==false || $("#fecha_diligenciamiento").val().length < 1){  
        alert("Los campos con * son obligatorios por favor verifique que estan todos diligenciados");  
        return false;
    }
    if ($("#identificacion").val().length < 1 && $("#nit").val().length < 1) {
    	alert("Asegúrese de asignar una Identificacion o un Nit");  
        return false;
    }
    if ($("#identificacion").val().length > 1 && $("#nit").val().length > 1) {
    	alert("No puede asignar una Identificacion y un Nit a un mismo proveedor");  
        return false;
    }
           
});

// colocamos el datapicker en español
$(function($){
  $.datepicker.regional['es'] = {
    closeText: 'Cerrar',
    prevText: '&#x3c;Ant',
    nextText: 'Sig&#x3e;',
    currentText: 'Hoy',
    monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
    'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
    monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
    'Jul','Ago','Sep','Oct','Nov','Dic'],
    dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
    dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
    dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
    weekHeader: 'Sm',
    dateFormat: 'dd/mm/yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
  $.datepicker.setDefaults($.datepicker.regional['es']);
});


$("#fecha_diligenciamiento").datepicker({
  dateFormat: 'yy-mm-dd'
});

function mostrarIdentificacion(){
	document.getElementById('campoIdentificacion').style.display='block';
	document.getElementById('campoNit').style.display='none';
}

function mostrarNit(){
	document.getElementById('campoNit').style.display='block';
	document.getElementById('campoIdentificacion').style.display='none';
}


// function mostrarCampoPago(){
// 	if (forma_pago[5].checked == true) {
//   	document.getElementById('otra_forma_pago').style.display='block';
//   	// forma_pago = ($("#otro_tipo"));
//   	// seleccionado_forma_pago = true;
//   	}
// 	else{
//   		document.getElementById('otra_forma_pago').style.display='none';
// 	}
// }