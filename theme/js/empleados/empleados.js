$(document).ready(function(){
	// validacion que hacemos para determinar si 
	// inicializamos la tabla siempre y cuando esta este en el dom
	if($('#example').length)
	{
		// inicializamos la tabla apartir de la libreria de tabletools
	    var table = $('#example').DataTable();
		// generamoe el evento en la tabla de table tools que 
		// cuando le den click a cualquier fila de la tabla nos devuelva el numero de la cedula 
	    // $('#example tbody').on('click', 'tr',function(){
	    //     var data = table.row( this ).data();
	    //     alert('You clicked on '+data[0]+'\'s row');
	    // });
	} 

    //al momento que cargamos el form escondemos la opcion de cargar foto esta se mostrara
    //cuando el usuario indique que realmente quiere que funcione
    // $(".photo_off").css("display", 'none');
    $(".off-document").css("display", 'none');
    $("#img_carnets").css("display", 'none');

    // get item load -> agregado en la bd
    var arl_defect = $('#arl_apr').val();
    var eps_defect = $('#eps_apr').val();
    var pension_defect = $('#pension_apr').val();
    var cesantias_defect = $('#cesantias_apr').val();
    var area = $("#area_f").val();

    // recorremos las opciones para colocar el item que esta en la bd
    $("#arl option").each(function(){
       var item = $(this).attr('value'); 
       if(item == arl_defect){  
            $("#arl").val(item);
       }
    });
    
    // recorremos las opciones para colocar el item que esta en la bd
    $("#eps option").each(function(){
       var item = $(this).attr('value'); 
       if(item == eps_defect){  
            $("#eps").val(item);
       }
    });
    
    // recorremos las opciones para colocar el item que esta en la bd
    $("#pension option").each(function(){
       var item = $(this).attr('value'); 
       if(item == pension_defect){  
            $("#pension").val(item);
       }
    });

    // recorremos las opciones para colocar el item que esta en la bd
    $("#cesantias option").each(function(){
       var item = $(this).attr('value'); 
       if(item == cesantias_defect){  
            $("#cesantias").val(item);
       }
    });


    $("#area option").each(function(){
       var item = $(this).attr('value'); 
       if(item == area){  
            $("#area").val(item);
       }
    });

});

// colocamos el datapicker en español
$(function($){
  $.datepicker.regional['es'] = {
    closeText: 'Cerrar',
    prevText: '&#x3c;Ant',
    nextText: 'Sig&#x3e;',
    currentText: 'Hoy',
    monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
    'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
    monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
    'Jul','Ago','Sep','Oct','Nov','Dic'],
    dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
    dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
    dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
    weekHeader: 'Sm',
    dateFormat: 'dd/mm/yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
  $.datepicker.setDefaults($.datepicker.regional['es']);
});

//cargamos los atributos de el boton donde se mostrara la foto de perfil
$("#foto_perfil_full").fileinput({
	showCaption: false,
	browseClass: "btn btn sty",
	fileType: "any"
});

//cargamos los atributos del boton donde se adjuntaran los diferentes documentos digitalizados
$("#documentos_adjuntos").fileinput({
	showCaption: false,
	browseClass: "btn btn sty",
	fileType: "any"
});

$("#medidas").fileinput({
    showCaption: false,
    browseClass: "btn btn sty",
    fileType: "any"
});

// delete parent elemt html
$('button.fileinput-remove').siblings('button').remove();

// inicializamos datapicker al input
$("#fecha_ingreso").datepicker({
  dateFormat: 'yy-mm-dd'
});


// validacion de campos vacios 
$("#formulario").submit(function(){  
    if($("#nombres").val().length < 1 || $("#apellidos").val().length < 1 || $("#cedula").val().length < 1 || $("#direccion").val().length < 1 || $("#celular").val().length < 1 || $("#arl").val() == 'Seleccione...' || $("#eps").val() == 'Seleccione...' || $("#pension").val() == 'Seleccione...' || $("#cesantias").val() == 'Seleccione...' || $("#cargo").val().length < 1 || $("#area").val() == 'Seleccione..' || $("#documentos_adjuntos").val().length < 1){  
        alert("Los campos con * son obligatorios por favor verifique que estan todos diligenciados");  
        return false;  
    }
    // else if($("#foto_perfil_full").val().length < 1 ){    
    // 	alert("Por favor coloque la foto");
    // 	return false;
    // }
    else if(bandera.length > 1){    
        $('.sentasi').css('display', 'block');
        alert("El numero de cedula que usted indica ya esta en la base de datos");
        return false;
    }        
}); 

// validacion de campos vacios 
$("#formulario_edit").submit(function(){  
    if($("#nombres").val().length < 1 || $("#apellidos").val().length < 1 || $("#cedula").val().length < 1 || $("#direccion").val().length < 1 || $("#celular").val().length < 1 || $("#arl").val() == 'Seleccione...' || $("#eps").val() == 'Seleccione...' || $("#pension").val() == 'Seleccione...' || $("#cesantias").val() == 'Seleccione...' || $("#cargo").val().length < 1 || $("#area").val() == 'Seleccione..'){  
        alert("Los campos con * son obligatorios por favor verifique que estan todos diligenciados");  
        return false;  
    }else if($("#foto_perfil_full").val().length < 1 ){
        alert("Por favor coloque la foto");
        return false;
    }else if(bandera.length > 1){
        $('.sentasi').css('display', 'block');
        alert("El numero de cedula que usted indica ya esta en la base de datos");
        return false;
    }        
}); 

// por medio de estos eventos click el sistema 
// podra acceder a las opciones de aprobaciones del menu
$( "#compras_submit" ).click(function(){
  $( "#compras_form" ).submit();
});

$( "#director_submit" ).click(function(){
  $( "#director_form" ).submit();
});

$( "#control_submit" ).click(function() {
  $( "#control_form" ).submit();
});

$( "#contabilidad_submit" ).click(function() {
  $( "#contabilidad_form" ).submit();
});
$( "#pagos_submit" ).click(function() {
  $( "#pagos_form" ).submit();
});


// por medio de esta funcion obtenemos la
function get_func_edit(ee)
{
    var id = $(ee).attr('id');
	var res = id.split("_");
    $("#form_edt_"+res[2]).submit();
}

function get_func_hjo_basic(ee)
{
    var id = $(ee).attr('id');
    var res = id.split("_");
    $("#form_hojaBasica_"+res[2]).submit();
}

function get_func_hjo_compl(ee)
{
    var id = $(ee).attr('id');
    var res = id.split("_");
    // var id = $(ee).attr('id').slice(-1);
    $("#form_hojaCompleta_"+res[2]).submit();
}

function get_func_dicip(ee)
{
    var id = $(ee).attr('id').slice(-1);
    // alert("form_diciplinarias"+id);
    $("#form_diciplinarias"+id).submit();
}

function get_func_carnet(ee)
{
  alert('Esta opción se encuentra actualmente en desarrollo');    
}

// por medio de este evento el usuario podra habilitar 
// la edicion de una nuea foto al prefil de la hija de vida
// photo = 1;
// $('#check_pho').change(function(){
//     if(photo){
//         photo = 0;
//         $('.photo_open').css('display', 'none');

//           var new_photo ='<div class="span2 tol">';
//               new_photo +=          '<div class="photo_off">';
//               new_photo +=              '<h3>Foto 3x4</h3>';
//               new_photo +=              '<div class="form-group">';
//               new_photo +=                  '<input id="foto_perfil_full" name="image_perfil" type="file" />';
//               new_photo +=              '</div>';
//               new_photo +=          '</div>';  
//               new_photo +=      '</div>';

//          $(".photo_open").after(new_photo);
//     }else{
//         $('.photo_open').css('display', 'block');
//         $(".photo_off").remove();
//         photo = 1;
//     }
// });

// mostramos la opcion donde el usuario podra montar los documentos-anexos
doc = 1;
$('#check_doc').change(function(){
    if(doc){
        $(".off-document").css("display", 'block');
        doc = 0;
    }else{
        $(".off-document").css("display", 'none');
        doc = 1;
    }
});

carnet = 1;
$('#check_carn').change(function(){
    if(carnet){
        $("#img_carnets").css("display", 'block');
        $(".alk").css("display","none");
        carnet = 0;
    }else{
        $("#img_carnets").css("display", 'none');
        $(".alk").css("display","block");
        carnet = 1;
    }
});






function delect_documentos(at){

    var setencia = confirm("Seguro que desea eliminar!");
    if(setencia){
    
        id = $(at).attr('id');
        var res = id.split("=");
        class_del = $(at).attr('name');
        $.ajax({
            url : '../Empleados/delete_documento',
            data : { doc : res[0], id : res[1] },
            type : 'POST',
            dataType : 'json', 
            success: function(resulta){
                if(resulta == 1){
                    var aviso = '<div class="alert alert-danger fade in visibility_close">';
                        aviso +=     '<strong>Info!</strong> Se elimino el documento con exito';
                        aviso += '</div>';
                        $(".breadcrumb").after(aviso);
                        // prolongamos un periodo de tiempo para mostrar el aviso en la vista
                        setTimeout(function(){
                                $(".visibility_close").fadeOut(1500);
                                $(".visibility_close").remove();
                            },3000);
                }
            } 
        });

        $('.'+class_del).remove();

    }
}



function delect_documentos_medidas(at){

    var setencia = confirm("Ojo se va a eliminar la medida diciplinaria!");
    if(setencia){
        id = $(at).attr('id');
        var res = id.split("=");
        class_del = $(at).attr('name');

        $.ajax({
            url : '../Empleados/delete_documento_medidas',
            data : { doc : res[0], id : res[1] },
            type : 'POST',
            dataType : 'json', 
            success: function(resulta){
                if(resulta == 1){
                    var aviso = '<div class="alert alert-danger fade in visibility_close">';
                        aviso +=     '<strong>Info!</strong> Se elimino la medida diciplinaria';
                        aviso += '</div>';
                        $(".breadcrumb").after(aviso);
                        // prolongamos un periodo de tiempo para mostrar el aviso en la vista
                        setTimeout(function(){
                                $(".visibility_close").fadeOut(1500);
                                $(".visibility_close").remove();
                            },3000);
                }
            } 
        });

        $('.'+class_del).remove();
    }
}
