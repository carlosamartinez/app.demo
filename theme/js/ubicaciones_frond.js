    var id_modifica_ubicacion = '';
    // evento al guardar informacion en el formulario de conceptos
    $("#guardar_ubicacion").click(function(){
      var nombre_ubicacion       = $('#ubicacion').val();
      var observacion_ubicacion  = $('#observaciones_ubicacion').val();
      if(nombre_ubicacion == '' && observacion_ubicacion == '')
      {
        alert('usted debe de diligenciar todos los campos');
      }else{ 
          $.ajax({
              url : '../Ubicaciones/create_save_ubicaciones',
              data : { nombre_ubicacion : nombre_ubicacion, observacion_ubicacion : observacion_ubicacion },
              type : 'POST',
              dataType : 'json', 
              success: function(resultad){
                  if(resultad)
                  {
                    alert('Registro exitoso');

                    $('#ubicacion').val('');
                    $('#observaciones_ubicacion').val('');

                    $.ajax({
                        url : '../Ubicaciones/load_registros_ubicaciones',
                        type : 'POST',
                        dataType : 'json', 
                        success: function(return_info){
                            $('.concepto_fila_ubicaciones').remove();
                            var filas = '';
                            $.each(return_info,function(iii,ooo){    
                                    filas = "<tr class='even concepto_fila_ubicaciones'>";
                                    filas += "<td>"+ooo.nombre_ubicacion+"</td>";
                                    filas += "<td>"+ooo.observaciones+"</td>";
                                    filas += "<td><button type='submit' id='"+ooo.id_ubicacion+"' onclick='buscar_l(this);' class='btn btn-success'><i class='halflings-icon zoom-in'></i></button><button type='submit' id='"+ooo.id_ubicacion+"' onclick='delet_l(this);' class='btn btn-danger'><i class='halflings-icon trash'></i></button></td>";
                                    filas += "</tr>";
                                    $('.concepto_table_ubicaciones').append(filas);
                            });                                      
                        } 
                    });
                  }else{
                    alert('El registro ya existe');
                  }
              } 
          });
        }
    });


function delet_l(atribute){

    var id = $(atribute).attr('id');
    limpiar_l();

    $.ajax({
        url : '../Ubicaciones/delete_regis',
        data : { id : id },
        type : 'POST',
        dataType : 'json', 
        success: function(respuesta){
            $('.concepto_fila_ubicaciones').remove();
            $.each(respuesta,function(iii,ooo){    
                filas = "<tr class='even concepto_fila_ubicaciones'>";
                filas += "<td>"+ooo.nombre_ubicacion+"</td>";
                filas += "<td>"+ooo.observaciones+"</td>";
                filas += "<td><button type='submit' id='"+ooo.id_ubicacion+"' onclick='buscar_l(this);' class='btn btn-success'><i class='halflings-icon zoom-in'></i></button><button type='submit' id='"+ooo.id_ubicacion+"' onclick='delet_l(this);' class='btn btn-danger'><i class='halflings-icon trash'></i></button></td>";
                filas += "</tr>";
                $('.concepto_table_ubicaciones').append(filas);
                
            });

            alert('se elimino el registro con éxito');
        } 
    });   

}

// funcion que nos trar la informacion del registro que solicito el usuario 
function buscar_l(atribute){

    $('#ubicacion').val('');
    $('#observaciones_ubicacion').val('');

    var id = $(atribute).attr('id');
    id_modifica_ubicacion = id;

   $.ajax({
       url : '../Ubicaciones/loading_unico_ubicaciones',
       data : { id : id },
       type : 'POST',
       dataType : 'json', 
       success: function(return_unico){
            // console.log(return_unico);
            $('#ubicacion').val(return_unico.nombre_ubicacion);
            $('#observaciones_ubicacion').val(return_unico.observaciones);

            var concepto_text = $('#ubicacion').val();
         
            if(concepto_text != '')
            {  
               if (!$('#modificar_ubicacion').length){
                //Aquí pondríamos el código que queremos ejecutar
                   var butones_modificar = '<button id="modificar_ubicacion" onclick="modificar_l();" class="btn btn-info">Modificar</button>';
                       butones_modificar += '<button id="clear_ubicacion" onclick="limpiar_l();" class="btn btn-danger">Limpiar</button>';
                   $("#guardar_ubicacion").after(butones_modificar); 
                   $('#guardar_ubicacion').attr('disabled','disabled');
               }
            }
                   
              
       } 
   });    

}

// function para limpiar datos y volver habilitar boton de guardar
function limpiar_l()
{
     $('#ubicacion').val('');
     $('#observaciones_ubicacion').val('');
     $('#guardar_ubicacion').removeAttr("disabled");
     $('#clear_ubicacion').remove();
     $('#modificar_ubicacion').remove();
}

function modificar_l()
{
    var ubicacion      = $('#ubicacion').val();
    var observaciones  = $('#observaciones_ubicacion').val();

    $.ajax({
        url : '../Ubicaciones/update_regis',
        data : {id : id_modifica_ubicacion, ubicacion : ubicacion, observaciones : observaciones},
        type : 'POST',
        dataType : 'json', 
        success: function(respuesta){

            $('.concepto_fila_ubicaciones').remove();
            var filas = '';
            $.each(respuesta,function(iii,ooo){    
                    filas = "<tr class='even concepto_fila_ubicaciones'>";
                    filas += "<td>"+ooo.nombre_ubicacion+"</td>";
                    filas += "<td>"+ooo.observaciones+"</td>";
                    filas += "<td><button type='submit' id='"+ooo.id_ubicacion+"' onclick='buscar_l(this);' class='btn btn-success'><i class='halflings-icon zoom-in'></i></button><button type='submit' id='"+ooo.id_ubicacion+"' onclick='delet_l(this);' class='btn btn-danger'><i class='halflings-icon trash'></i></button></td>";
                    filas += "</tr>";
                    $('.concepto_table_ubicaciones').append(filas);
            }); 
            limpiar_l();
            alert('actualización de la ubicación realizada éxito');
            
        } 
    }); 
}