<!DOCTYPE html>
<html lang="en">
<head>
	
	<!-- start: Meta -->
	<meta charset="utf-8">
	<title>Digitalización</title>
	<meta name="description" content="Bootstrap Metro Dashboard">
	<meta name="author" content="Dennis Ji">
	<meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	<!-- end: Meta -->
	
	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->
	
	<!-- start: CSS kjkjkjk-->
	<link id="bootstrap-style" href="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/css/bootstrap.min.css" rel="stylesheet">
	<link href="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style" href="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/css/style-responsive.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
	<!-- end: CSS -->
	
	<link rel="shortcut icon" href="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/img/favicon.ico">
	<!-- end: Favicon -->
</head>

<body>
	<!-- start: Header -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>

				<a class="brand">
					<img src="../../../theme/img/ochoa_logo_blanco.png" class="img-responsive" alt="Responsive image">
					<span class="style_title_nav">Cuestionario</span>
				</a>
								
				<!-- start: Header Menu -->
				<div class="nav-no-collapse header-nav">
					<ul class="nav pull-right">
						<h3 style="text-align: center;">version 1.1</h3>
						<!-- start: User Dropdown -->
						<li class="dropdown">
							<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="halflings-icon white user"></i> 
								<input id="cedula" type="hidden" value="<?php echo $cedula; ?>">
									<?php 
										echo $nombres;
									?>
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li class="dropdown-menu-title">
								</li>
								<form class="form-gla" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Welcome/cerrar_sesion" method="post" accept-charset="utf-8">
									<li class="aline-menu">
										<i class="halflings-icon remove color_ico"></i>
										<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
										<input type="submit" value="Cerrar" class="btn-menu-lg">
									</li>	
								</form>
							</ul>
						</li>
						<!-- end: User Dropdown -->
					</ul>
				</div>
				<!-- end: Header Menu -->
				
			</div>
		</div>
	</div>


	<div class="container-fluid-full">
		<div class="row-fluid">

			<div id="sidebar-left" class="span2">
				<div class="nav-collapse sidebar-nav">
					<ul class="nav nav-tabs nav-stacked main-menu">
						<form class="form-gla" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/cuestionario/cuestionario/index" method="post" accept-charset="utf-8">
							<li class="aline-menu">
								<i class="icon-home color_ico"></i>
								<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
								<input type="submit" value="Inicio" class="btn-menu-lg">
							</li>	
						</form>
					</ul>
				</div>
			</div>

			<div id="content" class="span11">

				<ul class="breadcrumb" style="background-color: white;">
					<li>
						<i class="icon-paste color_fla"></i>
							<a>Presentar cuestionario</a> 
						<i class="icon-angle-right color_fla"></i>
						<!-- <input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>	 -->
					</li>
				</ul>

				<?php if($mensaje == 'exito'){ ?>
					<div class="alert alert-success fade in">
						<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
						<strong>Info! </strong>Cuestionario realizado con éxito.
					</div>
				<?php } ?>

				<?php if($mensaje == 'error'){ ?>
					<div class="alert alert-warning fade in">
						<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
						<strong>Ojo! </strong>La cédula ingresada ya ha realizado este cuestionario.
					</div>
				<?php } ?>

			<!-- Cuestionario Administrativo -->
			<label style="text-align:left; color:#cc1b1b; font-size:12px;"><b>Los campos con * son obligatorios</b></label>
				<div class="row-fluid">	
					<div class="box span12">
						<div class="box-header" data-original-title="">
							<h2><i class="halflings-icon th"></i><span class="break"></span><b>Cuestionario Administrativo</b></h2>
							<div class="box-icon">
								<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
								<!-- <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a> -->
							</div>
						</div>

						<div class="box-content" style="display: block;">		
							<fieldset> 
								<div class="row-fluid">	
									<div class="bs-example" id="divPrincipal" style="background-image: url(../../../theme/img/cuestionario/1.-informacion-personal.png);">
									    <ul class="nav nav-tabs">
									        <li class="active">
									        	<a data-toggle="tab" id="informacion_even" class="font_tabs" href="#informacion_personal_administrativo" onclick="fondo(1)">Información personal</a>
									        </li>
									        <li>
									        	<a data-toggle="tab" id="afiliaciones_even" class="font_tabs" href="#composicionFamiliar_administrativo" onclick="fondo(2)">Composición familiar</a>
									        </li>
									        <li>
									        	<a data-toggle="tab" id="caracteristicas_even" class="font_tabs" href="#caracteristicas_vivienda_administrativo" onclick="fondo(3)">Características vivienda</a>
									        </li>
									        <li>
									        	<a data-toggle="tab" id="formacion_even" class="font_tabs" href="#formacion_academica_administrativo" onclick="fondo(4)">Formación académica</a>
									        </li>
									        <li>
									        	<a data-toggle="tab" id="experiencia_even" class="font_tabs" href="#experienciaLaboral_administrativo" onclick="fondo(5)">Experiencia laboral</a>
									        </li>
									        <li>
									        	<a data-toggle="tab" id="laboral_even" class="font_tabs" href="#informacion_laboral_administrativo" onclick="fondo(6)">Información laboral</a>
									        </li>									        
										</ul>

										<form class="form-horizontal" id="cuestionario_administrativo" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/cuestionario/cuestionario/datosCuestionario" enctype="multipart/form-data" method="post">

											<div class="tab-content">
							        			<div id="informacion_personal_administrativo" class="tab-pane fade in active"><!-- PESTAÑAS DE INFORMACIÓN PERSONAL -->
							            		
										            <div class="row-fluid">
										            	<div class="box-content" style="display: block; padding:0px;">
							            					<div class="span12">
							            						<div class="control-group">
																	<label style="text-align:left; color:#008fd1; font-size:18px;"><b>Información básica</b></label>
																</div>
							            					</div>
							            				</div>
							            			</div>

							            			<div class="row-fluid">
														<div class="box-content" style="display: block; padding:0px;">
							            					<div class="span12">
												            	<div class="span6">
												            		<input name="tipo_cuestionario_pregunta" value="Tipo de empleado" type="hidden">
												            		<input name="tipo_cuestionario" value="Administrativo" type="hidden">
																	
																	<div class="control-group">
																		<label><b>1- Nombres</b></label>
																		<div>
																			<input name="nombre_pregunta" value="Nombres" type="hidden">
																			<input id="nombre" name="nombre" type="text" >
																			<span class="obligatorio">*</span>
																		</div>
																	</div>

																	<div class="control-group">
																		<label><b>2- Apellidos</b></label>
																		<div>
																			<input value="Apellidos" name="apellidos_pregunta" type="hidden">
																			<input id="apellidos" name="apellidos" type="text" >
																			<span class="obligatorio">*</span>
																		</div>
																	</div>
																
																	<div class="control-group">
																		<label><b>3- Cédula</b></label>
																		<div>
																			<input name="cedula_evaluado_pregunta" value="Cédula" type="hidden">
																			<input id="cedula_vendedor" name="cedula_vendedor" type="text">
																			<span class="obligatorio">*</span>
																		</div>
																	</div>

																	<div class="control-group">
																		<label><b>4- Fecha de expedición de la cédula</b></label>
																		<div>
																			<input value="Fecha de expedición de la cédula" name="fecha_expedicion_pregunta" type="hidden">
																			<input id="fecha_expedicion" name="fecha_expedicion" type="date" >
																			<span class="obligatorio">*</span>
																		</div>
																	</div>
																
																	<div class="control-group">
																		<label><b>5- Fecha de nacimiento</b></label>
																		<div>
																			<input value="Fecha de nacimiento" name="fecha_nacimiento_pregunta" type="hidden">
																			<input id="fecha_nacimiento" name="fecha_nacimiento" type="date" >
																			<span class="obligatorio">*</span>
																		</div>
																	</div>

																	<div class="control-group">
																		<label><b>6- Lugar de nacimiento</b></label>
																		<div>
																			<input value="Lugar de nacimiento" name="lugar_nacimiento_pregunta" type="hidden">
																			<input id="lugar_nacimiento" name="lugar_nacimiento" type="text" >
																			<span class="obligatorio">*</span>
																		</div>
																	</div>
																
																	<div class="control-group">
																		<label><b>7- Departamento de nacimiento</b></label>
																		<div>
																			<input value="Departamento de nacimiento" name="depto_nacimiento_pregunta" type="hidden">
																			<input id="depto_nacimiento" name="depto_nacimiento" type="text" >
																			<span class="obligatorio">*</span>
																		</div>
																	</div>

																	<div class="control-group">
																		<label><b>8- Nacionalidad</b></label>
																		<div>
																			<input value="Nacionalidad" name="nacionalidad_pregunta" type="hidden">
																			<input id="nacionalidad" name="nacionalidad" type="text" >
																			<span class="obligatorio">*</span>
																		</div>
																	</div>

																	<div class="control-group">
																		<label><b>9- Libreta militar</b></label>
																		<div>
																			<input value="Libreta militar" name="libreta_militar_pregunta" type="hidden">
																			<input id="libreta_militar" name="libreta_militar" type="text" >
																		</div>
																	</div>
																	
												            	</div>

												            	<div class="span6">
																	
																	<div class="control-group">
																		<label><b>10- Género</b></label>
																		<div>
																			<input value="Género" name="genero_pregunta" type="hidden">
																		  	<select style="color:black;" id="genero" name="genero" >
																		  		<option></option>
																		  		<option>Masculino</option>
																		  		<option>Femenino</option>
																			</select>
																	  		<span class="obligatorio">*</span>
																		</div>
																  	</div>

																  	<div class="control-group">
																		<label><b>11- Edad</b></label>
																		<div>
																			<input value="Edad" name="edad_pregunta" type="hidden">
																			<input id="edad" name="edad" type="text" >
																			<span class="obligatorio">*</span>
																		</div>
																	</div>
																
																  	<div class="control-group">
																		<label><b>12- Correo electrónico</b></label>
																		<div>
																			<input value="Correo electrónico" name="email_pregunta" type="hidden">
																			<input id="email" name="email" type="text" >
																			<span class="obligatorio">*</span>
																		</div>
																	</div>

																	<div class="control-group">
																		<label><b>13- Tipo de sangre</b></label>
																		<div>
																			<input value="Tipo de sangre" name="tipo_sangre_pregunta" type="hidden">
																		  	<select style="color:black;" id="tipo_sangre" name="tipo_sangre" >
																		  		<option></option>
																		  		<option>O+</option>
																		  		<option>A+</option>
																		  		<option>B+</option>
																		  		<option>AB+</option>
																		  		<option>O-</option>
																		  		<option>A-</option>
																		  		<option>B-</option>
																		  		<option>AB-</option>
																			</select>
																	  		<span class="obligatorio">*</span>
																		</div>
																  	</div>
																
																	<div class="control-group">
																		<label><b>14- Estado civil</b></label>
																		<div>
																			<input value="Estado civil" name="estado_civil_pregunta" type="hidden">
																			<input id="estado_civil" name="estado_civil" type="text" >
																			<span class="obligatorio">*</span>
																		</div>
																	</div>

																	<div class="control-group">
																		<label><b>15- Número celular</b></label>
																		<div>
																			<input value="Número celular" name="numero_celular_pregunta" type="hidden">
																			<input id="numero_celular" name="numero_celular" type="text" >
																			<span class="obligatorio">*</span>
																		</div>
																	</div>
																
																	<div class="control-group">
																		<label><b>16- Talla de camisa</b></label>
																		<div>
																			<input value="Talla de camisa" name="talla_camisa_pregunta" type="hidden">
																			<input id="talla_camisa" name="talla_camisa" type="text" >
																			<span class="obligatorio">*</span>
																		</div>
																	</div>

																	<div class="control-group">
																		<label><b>17- Talla de pantalón</b></label>
																		<div>
																			<input value="Talla de pantalón" name="talla_pantalon_pregunta" type="hidden">
																			<input id="talla_pantalon" name="talla_pantalon" type="text" >
																			<span class="obligatorio">*</span>
																		</div>
																	</div>
																	
																	<div class="control-group">
																		<label><b>18- Talla de zapatos</b></label>
																		<div>
																			<input value="Talla de zapatos" name="talla_zapatos_pregunta" type="hidden">
																			<input id="talla_zapatos" name="talla_zapatos" type="text" >
																			<span class="obligatorio">*</span>
																		</div>
																	</div>
												            	</div>

												            </div>
												        </div>
												    </div>

												    <div class="row-fluid">
										            	<div class="box-content" style="display: block; padding:0px;">
							            					<div class="span12">
							            						<div class="control-group">
																	<label style="text-align:left; color:#008fd1; font-size:18px;"><b>Información de afiliaciones</b></label>
																</div>
							            					</div>
							            				</div>
							            			</div>

							            			<div class="row-fluid">
														<div class="box-content" style="display: block; padding:0px;">
															<div class="span12">
																<div class="span6">

																	<div class="control-group">
																		<label><b>19- ¿Cotizante de servicio de salud?</b></label>
																		<div>
																			<input value="¿Cotizante de servicio de salud?" name="cotizante_salud_pregunta" type="hidden">
																		  	<select style="color:black;" id="cotizante_salud" name="cotizante_salud" >
																		  		<option></option>
																		  		<option>Sí</option>
																		  		<option>No</option>
																			</select>
																	  		<span class="obligatorio">*</span>
																		</div>
																  	</div>

																  	<div class="control-group">
																		<label><b>20- Tipo de afiliado</b></label>
																		<div>
																			<input value="Tipo de afiliado" name="tipo_afiliado_pregunta" type="hidden">
																		  	<select style="color:black;" id="tipo_afiliado" name="tipo_afiliado" >
																		  		<option></option>
																		  		<option>Contributivo</option>
																		  		<option>Subsidiado</option>
																		  		<option>Beneficiario</option>
																			</select>
																	  		<span class="obligatorio">*</span>
																		</div>
																  	</div>

																  	<div class="control-group">
																		<label><b>21- Nombre de la entidad de salud</b></label>
																		<div>
																			<input value="Nombre de la entidad de salud" name="entidad_salud_pregunta" type="hidden">
																			<input id="entidad_salud" name="entidad_salud" type="text" >
																			<span class="obligatorio">*</span>
																		</div>
																	</div>

																</div>

																<div class="span6">

																  	<div class="control-group">
																		<label><b>22- Parentesco con el cotizante</b></label>
																		<div>
																			<input value="Parentesco con el cotizante" name="parentesco_cotizante_pregunta" type="hidden">
																		  	<select style="color:black;" id="parentesco_cotizante" name="parentesco_cotizante" >
																		  		<option></option>
																		  		<option>Cónyuge o compañero (a) permanente</option>
																		  		<option>Hijo(a)</option>
																		  		<option>Padre o madre</option>
																		  		<option>Segundo grado de consanguinidad</option>
																		  		<option>Tercer grado de consanguinidad</option>
																		  		<option>Menor de 12 años sin consanguinidad</option>
																		  		<option>Padre o madre del cónyuge</option>
																		  		<option>Cabeza del grupo o cotizante principal</option>
																			</select>
																	  		<span class="obligatorio">*</span>
																		</div>
																  	</div>

																  	<div class="control-group">
																		<label><b>23- Afiliación de fondo de pensiones</b></label>
																		<div>
																			<input value="Afiliación de fondo de pensiones" name="afp_pregunta" type="hidden">
																			<input id="afp" name="afp" type="text" >
																		</div>
																	</div>
																
																  	<div class="control-group">
																		<label><b>24- Cesantías</b></label>
																		<div>
																			<input value="Cesantías" name="cesantias_pregunta" type="hidden">
																			<input id="cesantias" name="cesantias" type="text" >
																		</div>
																	</div>

																</div>
															</div>
														</div>
													</div>

													<div class="row-fluid">
										            	<div class="box-content" style="display: block; padding:0px;">
							            					<div class="span12">
							            						<div class="control-group">
																	<label style="text-align:left; color:#008fd1; font-size:18px;"><b>Información de la residencia</b></label>
																</div>
							            					</div>
							            				</div>
							            			</div>

							            			<div class="row-fluid">
														<div class="box-content" style="display: block; padding:0px;">
															<div class="span12">
																<div class="span6">

																	<div class="control-group">
																		<label><b>25- Tipo de vivienda</b></label>
																		<div>
																			<input value="Tipo de vivienda" name="tipo_de_vivienda_pregunta" type="hidden">
																		  	<select style="color:black;" id="tipo_de_vivienda" name="tipo_de_vivienda" >
																		  		<option></option>
																		  		<option>Alquilada</option>
																		  		<option>Familiar</option>
																		  		<option>Propia</option>
																			</select>
																	  		<span class="obligatorio">*</span>
																		</div>
																  	</div>

																  	<div class="control-group">
																		<label><b>26- Tiempo viviendo en el sector</b></label>
																		<div>
																			<input value="Tiempo viviendo en el sector" name="tiempo_sector_pregunta" type="hidden">
																		  	<select style="color:black;" id="tiempo_sector" name="tiempo_sector" >
																		  		<option></option>
																		  		<option>0 a 1 año</option>
																		  		<option>1 a 3 años</option>
																		  		<option>3 o más años</option>
																			</select>
																	  		<span class="obligatorio">*</span>
																		</div>
																  	</div>
																
																	<div class="control-group">
																		<label><b>27- Barrio</b></label>
																		<div>
																			<input value="Barrio" name="barrio_pregunta" type="hidden">
																			<input id="barrio" name="barrio" type="text" >
																			<span class="obligatorio">*</span>
																		</div>
																	</div>

																</div>

																<div class="span6">

																	<div class="control-group">
																		<label><b>28- Dirección de residencia</b></label>
																		<div>
																			<input value="Dirección de residencia" name="direccion_residencia_pregunta" type="hidden">
																			<input id="direccion_residencia" name="direccion_residencia" type="text" >
																			<span class="obligatorio">*</span>
																		</div>
																	</div>

																	<div class="control-group">
																		<label><b>29- Teléfono de residencia</b></label>
																		<div>
																			<input value="Teléfono de residencia" name="telefono_residencia_pregunta" type="hidden">
																			<input id="telefono_residencia" name="telefono_residencia" type="text" >
																		</div>
																	</div>
																
																  	<div class="control-group">
																		<label><b>30- Estrato</b></label>
																		<div>
																			<input value="Estrato" name="estrato_pregunta" type="hidden">
																			<input id="estrato" name="estrato" type="text" >
																			<span class="obligatorio">*</span>
																		</div>
																	</div>													

																</div>
															</div>
														</div>
													</div>
												    
												</div><!-- FIN PESTAÑAS DE INFORMACIÓN PERSONAL -->

												<div id="composicionFamiliar_administrativo" class="tab-pane fade"><!-- PESTAÑA DE COMPOSICIÓN FAMILIAR -->

													<div class="row-fluid">
										            	<div class="box-content" style="display: block; padding:0px;">
							            					<div class="span12">
							            						<div class="control-group">
																	<label style="text-align:left; color:#008fd1; font-size:18px;"><b>Composición familiar</b></label>
																</div>
							            					</div>
							            				</div>
							            			</div>
				            					
													<div class="row-fluid">
														<div class="box-content" style="display: block; padding:0px;">
							        						<div class="span12">
																<div class="span6">

																	<div class="control-group">
																		<label><b>31- Número de hijos a cargo</b></label>
																		<div>
																			<input value="Número de hijos a cargo" name="hijos_cargo_pregunta" type="hidden">
																			<input id="hijos_cargo" name="hijos_cargo" type="text" >
																		</div>
																	</div>

																	<div class="control-group">
																		<label><b>32- Número de padres a cargo</b></label>
																		<div>
																			<input value="Número de padres a cargo" name="padres_cargo_pregunta" type="hidden">
																			<input id="padres_cargo" name="padres_cargo" type="text" >
																		</div>
																	</div>

																	<div class="control-group">
																		<label><b>33- Edad de hijos a cargo</b></label>
																		<div>
																			<input value="Edad de hijos a cargo" name="edad_hijos_cargo_pregunta" type="hidden">
																			<input id="edad_hijos_cargo" name="edad_hijos_cargo" type="text" >
																		</div>
																	</div>

																	<div class="control-group">
																		<label><b>34- Edad de padres a cargo</b></label>
																		<div>
																			<input value="Edad de padres a cargo" name="edad_padres_cargo_pregunta" type="hidden">
																			<input id="edad_padres_cargo" name="edad_padres_cargo" type="text" >
																		</div>
																	</div>

																	<div class="control-group">
																		<label><b>35- ¿Cónyuge?</b></label>
																		<div>
																			<input value="Cónyuge" name="conyuge_pregunta" type="hidden">
																		  	<select style="color:black;" id="conyuge" name="conyuge" >
																		  		<option></option>
																		  		<option>Si</option>
																		  		<option>No</option>
																			</select>
																	  		<span class="obligatorio">*</span>
																		</div>
																  	</div>

																  	<div class="control-group">
																		<label><b>36- Nombre del cónyuge</b></label>
																		<div>
																			<input value="Nombre del cónyuge" name="nombre_conyuge_pregunta" type="hidden">
																			<input id="nombre_conyuge" name="nombre_conyuge" type="text" >
																		</div>
																	</div>																

																</div>

																<div class="span6">

																	<div class="control-group">
																		<label><b>37- Cédula del cónyuge</b></label>
																		<div>
																			<input name="cedula_conyuge_pregunta" value="Cédula del cónyuge" type="hidden">
																			<input id="cedula_conyuge" name="cedula_conyuge" type="text" >
																		</div>
																	</div>																	

																	<div class="control-group">
																		<label><b>38- Nombre de la EPS del cónyuge</b></label>
																		<div>
																			<input value="Nombre de la EPS del cónyuge" name="eps_conyuge_pregunta" type="hidden">
																			<input id="eps_conyuge" name="eps_conyuge" type="text" >
																		</div>
																  	</div>

																  	<div class="control-group">
																		<label><b>39- Labor del cónyuge</b></label>
																		<div>
																			<input value="Labor del cónyuge" name="labor_conyuge_pregunta" type="hidden">
																			<input id="labor_conyuge" name="labor_conyuge" type="text" >
																		</div>
																	</div>

																	<div class="control-group">
																		<label><b>40- Estado de cónyuge</b></label>
																		<div>
																			<input value="Estado cónyuge" name="estado_conyuge_pregunta" type="hidden">
																		  	<select style="color:black;" id="estado_conyuge" name="estado_conyuge" >
																		  		<option></option>
																		  		<option>Dependiente</option>
																		  		<option>Independiente</option>
																			</select>
																		</div>
																  	</div>

																  	<div class="control-group">
																		<label><b>41- Núcleo familiar (Ejemplo: Madre, padre, hijo)</b></label>
																		<div>
																			<input value="Composición familiar" name="composicion_familiar_pregunta" type="hidden">
																			<input id="composicion_familiar" name="composicion_familiar" type="text" >
																			<span class="obligatorio">*</span>
																		</div>
																	</div>

																  	<div class="control-group">
																		<label><b>42- Total de personas a cargo</b></label>
																		<div>
																			<input value="Total de personas a cargo" name="total_personas_pregunta" type="hidden">
																			<input id="total_personas" name="total_personas" type="text" >
																			<span class="obligatorio">*</span>
																		</div>
																	</div>	

																</div>
															</div>
														</div>
													</div>
													
												</div><!-- FIN PESTAÑA DE COMPOSICIÓN FAMILIAR -->

												<div id="caracteristicas_vivienda_administrativo" class="tab-pane fade"><!-- PESTAÑA DE CARACTERÍSTICAS VIVIENDA -->

													<div class="row-fluid">
										            	<div class="box-content" style="display: block; padding:0px;">
							            					<div class="span12">
							            						<div class="control-group">
																	<label style="text-align:left; color:#008fd1; font-size:18px;"><b>Características de la vivienda</b></label>
																</div>
							            					</div>
							            				</div>
							            			</div>

													<div class="row-fluid">
														<div class="box-content" style="display: block; padding:0px;">
							            					<div class="span12">
												            	<div class="span6">

												            		<div class="control-group">
																		<label><b>43- Sector residencial</b></label>
																		<div>
																			<input value="Sector residencial" name="sector_residencial_pregunta" type="hidden">
																		  	<select style="color:black;" id="sector_residencial" name="sector_residencial" >
																		  		<option></option>
																		  		<option>Residencial</option>
																		  		<option>Comercial</option>
																		  		<option>Industrial</option>
																		  		<option>Popular</option>
																		  		<option>Marginal</option>
																		  		<option>Invasión</option>
																			</select>
																	  		<span class="obligatorio">*</span>
																		</div>
																  	</div>

																  	<div class="control-group">
																		<label><b>44- Estado de la vivienda</b></label>
																		<div>
																			<input value="Estado de la vivienda" name="estado_vivienda_pregunta" type="hidden">
																		  	<select style="color:black;" id="estado_vivienda" name="estado_vivienda" >
																		  		<option></option>
																		  		<option>Excelente</option>
																		  		<option>Buena</option>
																		  		<option>Regular</option>
																		  		<option>Mala</option>
																			</select>
																	  		<span class="obligatorio">*</span>
																		</div>
																  	</div>

																  	<div class="control-group">
																		<label><b>45- Tipo de vivienda</b></label>
																		<div>
																			<input value="Tipo de vivienda" name="tipo_vivienda_pregunta" type="hidden">
																		  	<select style="color:black;" id="tipo_vivienda" name="tipo_vivienda" >
																		  		<option></option>
																		  		<option>Casa independiente</option>
																		  		<option>Casa lota</option>
																		  		<option>Casa Compartidad</option>
																		  		<option>Apartamento independiente</option>
																		  		<option>Apartamento compartido</option>
																		  		<option>Inquilinato</option>
																		  		<option>Cuarto</option>
																			</select>
																	  		<span class="obligatorio">*</span>
																		</div>
																  	</div>

																  	<div class="control-group">
																		<label><b>46- Tenencia</b></label>
																		<div>
																			<input value="Tenencia" name="tenencia_vivienda_pregunta" type="hidden">
																		  	<select style="color:black;" id="tenencia_vivienda" name="tenencia_vivienda" >
																		  		<option></option>
																		  		<option>Propia</option>
																		  		<option>Arrendada</option>
																		  		<option>En financiamento</option>
																		  		<option>Familiar</option>
																		  		<option>Otro</option>
																			</select>
																	  		<span class="obligatorio">*</span>
																		</div>
																  	</div>
																	
												            	</div>

												            	<div class="span6">

												            		<div class="control-group">
																		<label><b>47- Aspectos físicos</b></label>
																		<div>
																			<input value="Aspectos físicos" name="aspectos_fisicos_pregunta" type="hidden">
																		  	<select style="color:black;" id="aspectos_fisicos" name="aspectos_fisicos" >
																		  		<option></option>
																		  		<option>Terminada</option>
																		  		<option>En construcción</option>
																		  		<option>Obra negra</option>
																		  		<option>Remodelación</option>
																			</select>
																	  		<span class="obligatorio">*</span>
																		</div>
																  	</div>

																  	<div class="control-group">
																		<label><b>48- Tipo de estructura</b></label>
																		<div>
																			<input value="Tipo de estructura" name="tipo_estructura_pregunta" type="hidden">
																		  	<select style="color:black;" id="tipo_estructura" name="tipo_estructura" >
																		  		<option></option>
																		  		<option>Bahareque</option>
																		  		<option>Material</option>
																		  		<option>Prefabricada</option>
																		  		<option>Madera</option>
																		  		<option>Cartón</option>
																		  	</select>
																	  		<span class="obligatorio">*</span>
																		</div>
																  	</div>

																  	<div class="control-group">
																		<label><b>49- ¿La vivienda se encuentra dentro de algún programa de vivienda social?</b></label>
																		<div>
																			<input value="¿La vivienda se encuentra dentro de algún programa de vivienda social?" name="vivienda_social_pregunta" type="hidden">
																		  	<select style="color:black;" id="vivienda_social" name="vivienda_social" >
																		  		<option></option>
																		  		<option>Si</option>
																		  		<option>No</option>
																			</select>
																	  		<span class="obligatorio">*</span>
																		</div>
																  	</div>
												            		
												            	</div>

												            </div>
												        </div>
												    </div>
				            					
													<div class="row-fluid">
							        					<div class="box-content" style="display: block; padding:0px;">
							        						<div class="span12">
																<div class="span6">

																  	<!-- <label style="color:#008fd1; font-size:16px;">47- La vivienda posee:</label> -->
																  	<label><b>50- La vivienda posee: </b><span class="obligatorio">*</span></label>
																  	<div>
																  		<input value="La vivienda posee" name="posee_pregunta" type="hidden">
																		<label>
																			<div class="checker" id="uniform-inlineCheckbox1"><span class=""><input type="checkbox" name="posee[]" id="posee1" value="Agua"></span></div> Agua
																		</label>
																		<label>
																			<div class="checker" id="uniform-inlineCheckbox2"><span class=""><input type="checkbox" name="posee[]" id="posee2" value="Alcantarillado"></span></div> Alcantarillado
																		</label>
																		<label>
																			<div class="checker" id="uniform-inlineCheckbox3"><span class=""><input type="checkbox" name="posee[]" id="posee3" value="Servicios sanitarios"></span></div> Servicios sanitarios
																		</label>
																		<label>
																			<div class="checker" id="uniform-inlineCheckbox4"><span class=""><input type="checkbox" name="posee[]" id="posee4" value="Gas por tuberías"></span></div> Gas por tuberías
																		</label>
																		<label>
																			<div class="checker" id="uniform-inlineCheckbox5"><span class=""><input type="checkbox" name="posee[]" id="posee5" value="Luz"></span></div> Luz
																		</label>
																		<label>
																			<div class="checker" id="uniform-inlineCheckbox6"><span class=""><input type="checkbox" name="posee[]" id="posee6" value="Teléfono"></span></div> Teléfono
																		</label>
																		<label>
																			<div class="checker" id="uniform-inlineCheckbox7"><span class=""><input type="checkbox" name="posee[]" id="posee7" value="Cocina independiente"></span></div> Cocina independiente
																		</label>
																		<label>
																			<div class="checker" id="uniform-inlineCheckbox8"><span class=""><input type="checkbox" name="posee[]" id="posee8" value="Recolección de basuras"></span></div> Recolección de basuras
																		</label>
																		<label>
																			<div class="checker" id="uniform-inlineCheckbox9"><span class=""><input type="checkbox" name="posee[]" id="posee9" value="Sector social"></span></div> Sector social
																		</label>
																	</div>

																	<br><br>

																	<!-- <label style="color:#008fd1; font-size:16px;">49- Condiciones hogar:</label> -->
																	<label><b>51- Condiciones del hogar: </b><span class="obligatorio">*</span></label>
																  	<div>
																		<input value="Condiciones de hogar" name="condiciones_pregunta" type="hidden">
																		<label>
																			<div class="checker" id="uniform-inlineCheckbox1"><span class=""><input type="checkbox" name="condiciones[]" id="condiciones1" value="Habitaciones"></span></div> Habitaciones
																		</label>
																		<label>
																			<div class="checker" id="uniform-inlineCheckbox2"><span class=""><input type="checkbox" name="condiciones[]" id="condiciones2" value="Sala"></span></div> Sala
																		</label>
																		<label>
																			<div class="checker" id="uniform-inlineCheckbox3"><span class=""><input type="checkbox" name="condiciones[]" id="condiciones3" value="Garaje"></span></div> Garaje
																		</label>
																		<label>
																			<div class="checker" id="uniform-inlineCheckbox4"><span class=""><input type="checkbox" name="condiciones[]" id="condiciones4" value="Cocina"></span></div> Cocina
																		</label>
																		<label>
																			<div class="checker" id="uniform-inlineCheckbox5"><span class=""><input type="checkbox" name="condiciones[]" id="condiciones5" value="Comedor"></span></div> Comedor
																		</label>
																		<label>
																			<div class="checker" id="uniform-inlineCheckbox6"><span class=""><input type="checkbox" name="condiciones[]" id="condiciones6" value="Baños"></span></div> Baños
																		</label>
																		<label>
																			<div class="checker" id="uniform-inlineCheckbox7"><span class=""><input type="checkbox" name="condiciones[]" id="condiciones7" value="Patio"></span></div> Patio
																		</label>
																		<label>
																			<div class="checker" id="uniform-inlineCheckbox8"><span class=""><input type="checkbox" name="condiciones[]" id="condiciones8" value="Lavadero"></span></div> Lavadero
																		</label>
																	</div>
																</div>

																<div class="span6">

																	<!-- <label class="control-label" style="color:#008fd1; font-size:16px;"><b>Equipamento del hogar</b></label> -->
																	<!-- <label style="color:#008fd1; font-size:16px;">48- Equipamento del hogar</label> -->
																	<label><b>52- Equipamiento del hogar: </b><span class="obligatorio">*</span></label>
																  	<div>
																		<input value="Equipamiento del hogar" name="equipamento_pregunta" type="hidden">
																		<label>
																			<div class="checker" id="uniform-inlineCheckbox1"><span class=""><input type="checkbox" name="equipamento[]" id="equipamento1" value="Microondas"></span></div> Microondas
																		</label>
																		<label>
																			<div class="checker" id="uniform-inlineCheckbox2"><span class=""><input type="checkbox" name="equipamento[]" id="equipamento2" value="Lampara"></span></div> Lámpara
																		</label>
																		<label>
																			<div class="checker" id="uniform-inlineCheckbox3"><span class=""><input type="checkbox" name="equipamento[]" id="equipamento3" value="Licuadora"></span></div> Licuadora
																		</label>
																		<label>
																			<div class="checker" id="uniform-inlineCheckbox4"><span class=""><input type="checkbox" name="equipamento[]" id="equipamento4" value="Juegos de sala"></span></div> Juegos de sala
																		</label>
																		<label>
																			<div class="checker" id="uniform-inlineCheckbox5"><span class=""><input type="checkbox" name="equipamento[]" id="equipamento5" value="Juego de alcoba"></span></div> Juego de alcoba
																		</label>
																		<label>
																			<div class="checker" id="uniform-inlineCheckbox6"><span class=""><input type="checkbox" name="equipamento[]" id="equipamento6" value="Juegos de comedor"></span></div> Juegos de comedor
																		</label>
																		<label>
																			<div class="checker" id="uniform-inlineCheckbox7"><span class=""><input type="checkbox" name="equipamento[]" id="equipamento7" value="Biblioteca"></span></div> Biblioteca
																		</label>
																		<label>
																			<div class="checker" id="uniform-inlineCheckbox8"><span class=""><input type="checkbox" name="equipamento[]" id="equipamento8" value="Computador"></span></div> Computador
																		</label>
																		<label>
																			<div class="checker" id="uniform-inlineCheckbox9"><span class=""><input type="checkbox" name="equipamento[]" id="equipamento9" value="Impresora"></span></div> Impresora
																		</label>
																		<label>
																			<div class="checker" id="uniform-inlineCheckbox10"><span class=""><input type="checkbox" name="equipamento[]" id="equipamento10" value="Ventilador"></span></div> Ventilador
																		</label>
																		<label>
																			<div class="checker" id="uniform-inlineCheckbox11"><span class=""><input type="checkbox" name="equipamento[]" id="equipamento11" value="Estufa"></span></div> Estufa
																		</label>
																		<label>
																			<div class="checker" id="uniform-inlineCheckbox12"><span class=""><input type="checkbox" name="equipamento[]" id="equipamento12" value="Arrocera"></span></div> Arrocera
																		</label>
																		<label>
																			<div class="checker" id="uniform-inlineCheckbox13"><span class=""><input type="checkbox" name="equipamento[]" id="equipamento13" value="Nevera"></span></div> Nevera
																		</label>
																		<label>
																			<div class="checker" id="uniform-inlineCheckbox14"><span class=""><input type="checkbox" name="equipamento[]" id="equipamento14" value="Televisores"></span></div> Televisores
																		</label>
																		<label>
																			<div class="checker" id="uniform-inlineCheckbox15"><span class=""><input type="checkbox" name="equipamento[]" id="equipamento15" value="Equipo de sonido"></span></div> Equipo de sonido
																		</label>
																		<label>
																			<div class="checker" id="uniform-inlineCheckbox16"><span class=""><input type="checkbox" name="equipamento[]" id="equipamento16" value="Videograbadora"></span></div> Videograbadora
																		</label>
																		<label>
																			<div class="checker" id="uniform-inlineCheckbox17"><span class=""><input type="checkbox" name="equipamento[]" id="equipamento17" value="Plancha"></span></div> Plancha
																		</label>
																		<label>
																			<div class="checker" id="uniform-inlineCheckbox18"><span class=""><input type="checkbox" name="equipamento[]" id="equipamento18" value="Muebles"></span></div> Muebles
																		</label>
																		<label>
																			<div class="checker" id="uniform-inlineCheckbox19"><span class=""><input type="checkbox" name="equipamento[]" id="equipamento19" value="Lavadora"></span></div> Lavadora
																		</label>
																		<label>
																			<div class="checker" id="uniform-inlineCheckbox20"><span class=""><input type="checkbox" name="equipamento[]" id="equipamento20" value="Grabadora"></span></div> Grabadora
																		</label>
																		<label>
																			<div class="checker" id="uniform-inlineCheckbox21"><span class=""><input type="checkbox" name="equipamento[]" id="equipamento21" value="DVD"></span></div> DVD
																		</label>
																		<label>
																			<div class="checker" id="uniform-inlineCheckbox22"><span class=""><input type="checkbox" name="equipamento[]" id="equipamento22" value="Cafetera"></span></div> Cafetera
																		</label>
																	</div>																	

																</div>
															</div>
														</div>
													</div>
													
												</div><!-- FIN PESTAÑA DE CARACTERÍSTICAS VIVIENDA -->

												<div id="formacion_academica_administrativo" class="tab-pane fade"><!-- PESTAÑA DE FORMACIÓN ACADÉMICA -->

													<div class="row-fluid">
										            	<div class="box-content" style="display: block; padding:0px;">
							            					<div class="span12">
							            						<div class="control-group">
																	<label style="text-align:left; color:#008fd1; font-size:18px;"><b>Formación académica</b></label>
																</div>
							            					</div>
							            				</div>
							            			</div>

							            			<div class="row-fluid">
										            	<div class="box-content" style="display: block; padding:0px;">
							            					<div class="span12">
							            						<div class="span6">

							            							<div class="control-group">
																		<label><b>53- Grado de escolaridad</b></label>
																		<div>
																			<input value="Grado de escolaridad" name="grado_escolaridad_pregunta" type="hidden">
																		  	<select style="color:black;" id="grado_escolaridad" name="grado_escolaridad" >
																		  		<option></option>
																		  		<option>Secundaria completa</option>
																		  		<option>Estudios técnicos</option>
																		  		<option>Tecnólogo</option>
																		  		<option>Profesional</option>
																		  		<option>Maestría y doctorado</option>
																		  		<option>No especificado</option>
																		  	</select>
																	  		<span class="obligatorio">*</span>
																		</div>
																  	</div>

																  	<div class="control-group">
																		<label><b>54- Institución de bachillerato</b></label>
																		<div>
																			<input value="Institución de bachillerato" name="institucion_bachillerato_pregunta" type="hidden">
																			<input id="institucion_bachillerato" name="institucion_bachillerato" type="text" >
																			<span class="obligatorio">*</span>
																		</div>
																	</div>

																	<div class="control-group">
																		<label><b>55- Profesión</b></label>
																		<div>
																			<input value="Profesión" name="profesion_pregunta" type="hidden">
																			<input id="profesion" name="profesion" type="text" >
																			<span class="obligatorio">*</span>
																		</div>
																	</div>

																	<div class="control-group">
																		<label><b>56- Nombre de la formación que ha recibido</b></label>
																		<div>
																			<input value="Nombre de la formación que ha recibido" name="nombre_formacion_pregunta" type="hidden">
																		  	<select style="color:black;" id="nombre_formacion" name="nombre_formacion" >
																		  		<option></option>
																		  		<option>Cursos</option>
																		  		<option>Técnico</option>
																		  		<option>Tecnólogo</option>
																		  		<option>Profesional</option>
																		  		<option>Especialización</option>
																		  		<option>No aplica</option>
																		  	</select>
																	  		<span class="obligatorio">*</span>
																		</div>
																  	</div>

																  	<div class="control-group">
																		<label><b>57- Nombre del título adquirido</b></label>
																		<div>
																			<input value="Nombre del título adquirido" name="nombre_titulo_pregunta" type="hidden">
																			<input id="nombre_titulo" name="nombre_titulo" type="text" >
																		</div>
																	</div>

																	<div class="control-group">
																		<label><b>58- Lugar de formación</b></label>
																		<div>
																			<input value="Lugar de formación" name="lugar_formacion_pregunta" type="hidden">
																			<input id="lugar_formacion" name="lugar_formacion" type="text" >
																		</div>
																	</div>

																	<div class="control-group">
																		<label><b>59- Duración de formación</b></label>
																		<div>
																			<input value="Duración de formación" name="duracion_formacion_pregunta" type="hidden">
																			<input id="duracion_formacion" name="duracion_formacion" type="text" >
																		</div>
																	</div>

																	<div class="control-group">
																		<label><b>60- ¿Qué le gustaría estudiar?</b></label>
																		<div>
																			<input value="¿Qué le gustaría estudiar?" name="gustaria_estudiar_pregunta" type="hidden">
																			<input id="gustaria_estudiar" name="gustaria_estudiar" type="text" >
																			<span class="obligatorio">*</span>
																		</div>
																	</div>

							            						</div>

							            						<div class="span6">
							            							
							            							<div class="control-group">
																		<label><b>61- ¿Cursa estudios actualmente?</b></label>
																		<div>
																			<input value="¿Cursa estudios actualmente?" name="cursa_actualmente_pregunta" type="hidden">
																		  	<select style="color:black;" id="cursa_actualmente" name="cursa_actualmente" >
																		  		<option></option>
																		  		<option>Si</option>
																		  		<option>No</option>
																		  	</select>
																	  		<span class="obligatorio">*</span>
																		</div>
																  	</div>

																  	<div class="control-group">
																		<label><b>62- Centro de estudios que cursa</b></label>
																		<div>
																			<input value="Centro de estudios que cursa" name="centro_estudios_actual_pregunta" type="hidden">
																			<input id="centro_estudios_actual" name="centro_estudios_actual" type="text" >
																		</div>
																	</div>

																	<div class="control-group">
																		<label><b>63- ¿Qué tipos de estudios cursa?</b></label>
																		<div>
																			<input value="¿Qué tipos de estudios cursa?" name="tipo_estudio_actual_pregunta" type="hidden">
																			<input id="tipo_estudio_actual" name="tipo_estudio_actual" type="text" >
																		</div>
																	</div>

																	<div class="control-group">
																		<label><b>64- Horario de clases</b></label>
																		<div>
																			<input value="Horario de clases" name="horario_clases_pregunta" type="hidden">
																			<input id="horario_clases" name="horario_clases" type="text" >
																		</div>
																	</div>

																	<div class="control-group">
																		<label><b>65- ¿Conocimiento en el idioma inglés?</b></label>
																		<div>
																			<input value="¿Conocimiento en el idioma inglés?" name="conoce_ingles_pregunta" type="hidden">
																		  	<select style="color:black;" id="conoce_ingles" name="conoce_ingles" >
																		  		<option></option>
																		  		<option>Si</option>
																		  		<option>No</option>
																		  	</select>
																	  		<span class="obligatorio">*</span>
																		</div>
																  	</div>

																  	<div class="control-group">
																		<label><b>66- Nivel de inglés</b></label>
																		<div>
																			<input value="Nivel de inglés" name="nivel_ingles_pregunta" type="hidden">
																		  	<select style="color:black;" id="nivel_ingles" name="nivel_ingles" >
																		  		<option></option>
																		  		<option>Bueno</option>
																		  		<option>Regular</option>
																		  		<option>Malo</option>
																		  	</select>
																	  		<span class="obligatorio">*</span>
																		</div>
																  	</div>

																  	<div class="control-group">
																		<label><b>67- ¿Tiene conocimiento en otro idioma?</b></label>
																		<div>
																			<input value="¿Tiene conocimiento en otro idioma?" name="otros_idiomas_pregunta" type="hidden">
																			<select style="color:black;" id="otros_idiomas" name="otros_idiomas" >
																		  		<option></option>
																		  		<option>Sí</option>
																		  		<option>No</option>
																		  	</select>
																			
																			<span class="obligatorio">*</span>
																		</div>
																	</div>

																  	<div class="control-group">
																		<label><b>68- ¿Cuál otro idioma?</b></label>
																		<div>
																			<input value="¿Cuál otro idioma?" name="cual_idioma_pregunta" type="hidden">
																		  	<input id="cual_idioma" name="cual_idioma" type="text" >
																		</div>
																  	</div>

							            						</div>
							            					</div>
							            				</div>
							            			</div>
													
												</div><!-- FIN PESTAÑA DE FORMACIÓN ACADÉMICA -->

												<div id="experienciaLaboral_administrativo" class="tab-pane fade"><!-- PESTAÑA DE EXPERIENCIA LABORAL -->

													<div class="row-fluid">
										            	<div class="box-content" style="display: block; padding:0px;">
							            					<div class="span12">
							            						<div class="control-group">
																	<label style="text-align:left; color:#008fd1; font-size:18px;"><b>Experiencia laboral</b></label>
																</div>
							            					</div>
							            				</div>
							            			</div>

													<div class="row-fluid">
										            	<div class="box-content" style="display: block; padding:0px;">
							            					<div class="span12">
							            						
							            						<div class="control-group">
																	<label><b>69- ¿Tiene experiencia laboral?</b></label>
																	<div>
																		<input value="¿tiene experiencia laboral?" name="experiencia_laboral_pregunta" type="hidden">
																	  	<select style="color:black;" id="experiencia_laboral" name="experiencia_laboral" >
																	  		<option></option>
																	  		<option>Si</option>
																	  		<option>No</option>
																	  	</select>
																  		<span class="obligatorio">*</span>
																	</div>
															  	</div>
							            					</div>
							            				</div>
							            			</div>

							            			<div class="row-fluid">
														<div class="box-content" style="display: block; padding:0px;">
							            					<div class="span12">
												            	<div class="span6">
																	<div class="control-group">
																		<label><b>70- Primer empresa u oficio</b></label>
																		<div>
																			<input value="Empresa 1" name="empresa1_pregunta" type="hidden">
																			<input id="empresa1" name="empresa1" type="text" >
																		</div>
																	</div>

																	<div class="control-group">
																		<label><b>71- Cargo</b></label>
																		<div>
																			<input value="Cargo 1" name="cargo1_pregunta" type="hidden">
																			<input id="cargo1" name="cargo1" type="text" >
																		</div>
																	</div>

																	<div class="control-group">
																		<label><b>72- Año de inicio</b></label>
																		<div>
																			<input value="Año de inicio 1" name="año_inicio1_pregunta" type="hidden">
																			<input id="año_inicio1" name="año_inicio1" type="text" >
																		</div>
																	</div>

																	<div class="control-group">
																		<label><b>73- Año de finalización</b></label>
																		<div>
																			<input value="Año de finalización 1" name="año_fin1_pregunta" type="hidden">
																			<input id="año_fin1" name="año_fin1" type="text" >
																		</div>
																	</div>
												            	</div>

												            	<div class="span6">
												            		<div class="control-group">
																		<label><b>74- Segunda empresa u oficio</b></label>
																		<div>
																			<input value="Empresa 2" name="empresa2_pregunta" type="hidden">
																			<input id="empresa2" name="empresa2" type="text" >
																		</div>
																	</div>

																	<div class="control-group">
																		<label><b>75- Cargo</b></label>
																		<div>
																			<input value="Cargo 2" name="cargo2_pregunta" type="hidden">
																			<input id="cargo2" name="cargo2" type="text" >
																		</div>
																	</div>

																	<div class="control-group">
																		<label><b>76- Año de inicio</b></label>
																		<div>
																			<input value="Año de inicio 2" name="año_inicio2_pregunta" type="hidden">
																			<input id="año_inicio2" name="año_inicio2" type="text" >
																		</div>
																	</div>

																	<div class="control-group">
																		<label><b>77- Año de finalización</b></label>
																		<div>
																			<input value="Año de finalización 2" name="año_fin2_pregunta" type="hidden">
																			<input id="año_fin2" name="año_fin2" type="text" >
																		</div>
																	</div>
												            	</div>

												            </div>
												        </div>
												    </div>
													
												</div><!-- FIN PESTAÑA DE EXPERIENCIA LABORAL -->

												<div id="informacion_laboral_administrativo" class="tab-pane fade"><!-- PESTAÑA DE INFORMACIÓN LABORAL -->

													<div class="row-fluid">
										            	<div class="box-content" style="display: block; padding:0px;">
							            					<div class="span12">
							            						<div class="control-group">
																	<label style="text-align:left; color:#008fd1; font-size:18px;"><b>Información laboral</b></label>
																</div>
							            					</div>
							            				</div>
							            			</div>
				            					
													<div class="row-fluid">
							        					<div class="box-content" style="display: block; padding:0px;">
							        						<div class="span12">
																<div class="span6">
																	
																	<div class="control-group">
																		<label><b>78- Jefe inmediato</b></label>
																		<div>
																			<input value="Jefe inmediato actual" name="jefe_inmediato_actual_pregunta" type="hidden">
																			<input id="jefe_inmediato_actual" name="jefe_inmediato_actual" type="text" >
																			<span class="obligatorio">*</span>
																		</div>
																	</div>

																	<div class="control-group">
																		<label><b>79- Cargo actual</b></label>
																		<div>
																			<input value="Cargo actual" name="cargo_actual_pregunta" type="hidden">
																			<input id="cargo_actual" name="cargo_actual" type="text" >
																			<span class="obligatorio">*</span>
																		</div>
																	</div>

																	<div class="control-group">
																		<label><b>80- Estado laboral</b></label>
																		<div>
																			<input value="Estado laboral" name="estado_laboral_pregunta" type="hidden">
																		  	<select style="color:black;" id="estado_laboral" name="estado_laboral" >
																		  		<option></option>
																		  		<option>Activo</option>
																		  		<option>Inactivo</option>
																		  		<option>Incapacitado</option>
																		  	</select>
																	  		<span class="obligatorio">*</span>
																		</div>
																  	</div>

																  	<div class="control-group">
																		<label><b>81- Tipo de contrato</b></label>
																		<div>
																			<input value="Tipo de contrato" name="tipo_contrato_pregunta" type="hidden">
																		  	<select style="color:black;" id="tipo_contrato" name="tipo_contrato" >
																		  		<option></option>
																		  		<option>Contrato de trabajo a término fijo</option>
																		  		<option>Contrato de trabajo a término indefinido</option>
																		  	</select>
																	  		<span class="obligatorio">*</span>
																		</div>
																  	</div>

																</div>

																<div class="span6">																	

																	<div class="control-group">
																		<label><b>82- Fecha de ingreso</b></label>
																		<div>
																			<input value="Fecha de ingreso" name="fecha_ingreso_pregunta" type="hidden">
																			<input id="fecha_ingreso" name="fecha_ingreso" type="date" >
																			<span class="obligatorio">*</span>
																		</div>
																	</div>

																	<div class="control-group">
																		<label><b>83- Fecha de retiro</b></label>
																		<div>
																			<input value="Fecha de retiro" name="fecha_retiro_pregunta" type="hidden">
																			<input id="fecha_retiro" name="fecha_retiro" type="date" >
																		</div>
																	</div>

																	<div class="control-group">
																		<label><b>84- Reingreso</b></label>
																		<div>
																			<input value="Reingreso" name="reingreso_pregunta" type="hidden">
																			<input id="reingreso" name="reingreso" type="text" >
																		</div>
																	</div>

																	<input value="Código del vendedor" name="codigo_vendedor_pregunta" type="hidden">
																	<input value="Centro de costos" name="centro_costos_pregunta" type="hidden">
																	<input value="Porcentaje de comisión" name="comision_pregunta" type="hidden">
																	<input value="Zona punto de venta" name="zona_venta_pregunta" type="hidden">
																	<input value="Tipo punto de venta" name="tipo_punto_pregunta" type="hidden">
																	<input value="Municipio punto de venta" name="municipio_punto_pregunta" type="hidden">
																	<input value="Dirección punto de venta" name="direccion_punto_pregunta" type="hidden">
																	<input value="Tipología de horario semanal en el punto de venta inicio" name="tipologia_semana_inicio_pregunta" type="hidden">
																	<input value="Tipología de horario semanal en el punto de venta fin" name="tipologia_semana_fin_pregunta" type="hidden">
																	<input value="Tipología horario dominical festivo punto de venta inicio" name="tipologia_dominical_inicio_pregunta" type="hidden">
																	<input value="Tipología horario dominical festivo punto de venta fin" name="tipologia_dominical_fin_pregunta" type="hidden">
																	<input value="Tecnología" name="tecnologia_pregunta" type="hidden">
																	<input value="Serie" name="serie_pregunta" type="hidden">

																</div>

															</div>
														</div>
													</div>

													<div class="row-fluid">
							        					<div class="box-content" style="display: block; padding:0px;">
							        						<div class="span10">

							        							<p style="text-align:justify; font-size:13px; font-style:italic;"><b>Al diligenciar los anteriores datos, autorizo de manera voluntaria, previa, explícita, informada e inequívoca a APUESTAS OCHOA S.A. para tratar mis datos personales de acuerdo con su Política de Tratamiento de Datos Personales y para los fines relacionados con su objeto social y en especial para fines legales, contractuales, comerciales. La información obtenida para el Tratamiento de mis datos personales la suministraré de forma voluntaria y es verídica.  De igual manera, autorizo el tratamiento de datos sensibles plasmados en la Ley 1581 de 2012 en sus artículos 5 y 6 que rezan: “ Artículo  5°. Datos sensibles. Para los propósitos de la presente ley, se entiende por datos sensibles aquellos que afectan la intimidad del Titular o cuyo uso indebido puede generar su discriminación, tales como aquellos que revelen el origen racial o étnico, la orientación política, las convicciones religiosas o filosóficas, la pertenencia a sindicatos, organizaciones sociales, de derechos humanos o que promueva intereses de cualquier partido político o que garanticen los derechos y garantías de partidos políticos de oposición así como los datos relativos a la salud, a la vida sexual y los datos biométricos. Artículo  6°. Tratamiento de datos sensibles. Se prohíbe el Tratamiento de datos sensibles, excepto cuando: a) El Titular haya dado su autorización explícita a dicho Tratamiento, salvo en los casos que por ley no sea requerido el otorgamiento de dicha autorización; b) El Tratamiento sea necesario para salvaguardar el interés vital del Titular y este se encuentre física o jurídicamente incapacitado. En estos eventos, los representantes legales deberán otorgar su autorización; c) El Tratamiento sea efectuado en el curso de las actividades legítimas y con las debidas garantías por parte de una fundación, ONG, asociación o cualquier otro organismo sin ánimo de lucro, cuya finalidad sea política, filosófica, religiosa o sindical, siempre que se refieran exclusivamente a sus miembros o a las personas que mantengan contactos regulares por razón de su finalidad. En estos eventos, los datos no se podrán suministrar a terceros sin la autorización del Titular; d) El Tratamiento se refiera a datos que sean necesarios para el reconocimiento, ejercicio o defensa de un derecho en un proceso judicial; e) El Tratamiento tenga una finalidad histórica, estadística o científica. En este evento deberán adoptarse las medidas conducentes a la supresión de identidad de los Titulares.” Todo lo anterior lo autorizo conforme a la Ley 1581 de 2012 reglamentada parcialmente por el Decreto Nacional 1377 de 2013, además del Decreto Único 1074 de 2015 en su capítulo 25 que reglamenta parcialmente la ley 1581 / 2012.</b></p>
							        							<input type="hidden" name="textoHabeas" value="Al diligenciar los anteriores datos, autorizo de manera voluntaria, previa, explícita, informada e inequívoca a APUESTAS OCHOA S.A. para tratar mis datos personales de acuerdo con su Política de Tratamiento de Datos Personales y para los fines relacionados con su objeto social y en especial para fines legales, contractuales, comerciales. La información obtenida para el Tratamiento de mis datos personales la suministraré de forma voluntaria y es verídica.  De igual manera, autorizo el tratamiento de datos sensibles plasmados en la Ley 1581 de 2012 en sus artículos 5 y 6 que rezan: “ Artículo  5°. Datos sensibles. Para los propósitos de la presente ley, se entiende por datos sensibles aquellos que afectan la intimidad del Titular o cuyo uso indebido puede generar su discriminación, tales como aquellos que revelen el origen racial o étnico, la orientación política, las convicciones religiosas o filosóficas, la pertenencia a sindicatos, organizaciones sociales, de derechos humanos o que promueva intereses de cualquier partido político o que garanticen los derechos y garantías de partidos políticos de oposición así como los datos relativos a la salud, a la vida sexual y los datos biométricos. Artículo  6°. Tratamiento de datos sensibles. Se prohíbe el Tratamiento de datos sensibles, excepto cuando: a) El Titular haya dado su autorización explícita a dicho Tratamiento, salvo en los casos que por ley no sea requerido el otorgamiento de dicha autorización; b) El Tratamiento sea necesario para salvaguardar el interés vital del Titular y este se encuentre física o jurídicamente incapacitado. En estos eventos, los representantes legales deberán otorgar su autorización; c) El Tratamiento sea efectuado en el curso de las actividades legítimas y con las debidas garantías por parte de una fundación, ONG, asociación o cualquier otro organismo sin ánimo de lucro, cuya finalidad sea política, filosófica, religiosa o sindical, siempre que se refieran exclusivamente a sus miembros o a las personas que mantengan contactos regulares por razón de su finalidad. En estos eventos, los datos no se podrán suministrar a terceros sin la autorización del Titular; d) El Tratamiento se refiera a datos que sean necesarios para el reconocimiento, ejercicio o defensa de un derecho en un proceso judicial; e) El Tratamiento tenga una finalidad histórica, estadística o científica. En este evento deberán adoptarse las medidas conducentes a la supresión de identidad de los Titulares.” Todo lo anterior lo autorizo conforme a la Ley 1581 de 2012 reglamentada parcialmente por el Decreto Nacional 1377 de 2013, además del Decreto Único 1074 de 2015 en su capítulo 25 que reglamenta parcialmente la ley 1581 / 2012.">

							        							<label style="font-size:17px;">
							        								<div class="checker" id="uniform-inlineCheckbox1"><input type="checkbox" name="habeas" id="habeasCheckAdministrativo" value="Acepto" onchange="mostrarBotonAdministrativo()"></div> Acepto
							        							</label>
							        						
						        							
							        						</div>
							        					</div>
							        				</div>
													
													<div style="display:none" id="botonGuardarAdministrativo">
							        					<button type="submit" class="btn btn-primary">Guardar</button>
							        				</div>
													
												</div><!-- FIN PESTAÑA DE INFORMACIÓN LABORAL -->
											

											</div>
											
										
										</form>
									</div>
								</div>
							</fieldset>
														
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Settings</h3>
		</div>
		<div class="modal-body">
			<p>Here settings can be configured...</p>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
			<a href="#" class="btn btn-primary">Save changes</a>
		</div>
	</div>
	
	<div class="clearfix"></div>
	<footer>
		<p>
			<span style="text-align:left;float:left">&copy; 2016 <a href="http://http://www.apuestasochoa.com.co/">Desarrollo interno - apuestasochoa</a></span>
		</p>
	</footer>
	<!-- start: JavaScript-->
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery-1.9.1.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery-migrate-1.0.0.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery-ui-1.10.0.custom.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.ui.touch-punch.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/modernizr.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/bootstrap.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.cookie.js"></script>
		<script src='http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/fullcalendar.min.js'></script>
		<script src='http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.dataTables.min.js'></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/excanvas.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.flot.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.flot.pie.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.flot.stack.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.flot.resize.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.chosen.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.uniform.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.cleditor.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.noty.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.elfinder.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.raty.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.iphone.toggle.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.uploadify-3.1.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.gritter.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.imagesloaded.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.masonry.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.knob.modified.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.sparkline.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/counter.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/retina.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/custom.js"></script>
		<!-- <script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/apuestasochoa.js"></script> -->
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/evaluacion/validarEvaluacion.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/cuestionario/cuestionario_administrativo.js"></script>
	<!-- end: JavaScript-->
	
</body>
</html>