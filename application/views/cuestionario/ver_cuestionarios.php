<!DOCTYPE html>
<html lang="en">
<head>
	<!-- start: Meta -->
	<meta charset="utf-8">
	<title>Inicio</title>
	<meta name="description" content="Bootstrap Metro Dashboard">
	<meta name="author" content="Dennis Ji">
	<meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	<!-- end: Meta -->
	
	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->
	
	<!-- start: CSS-->
	<link id="bootstrap-style" href="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/css/bootstrap.min.css" rel="stylesheet">
	<link href="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style" href="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/css/style-responsive.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
	<!-- end: CSS -->
	<!-- end: Favicon -->	
	<link rel="shortcut icon" href="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/img/favicon.ico">
</head>

<body>
		<!-- start: Header -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>

				<a class="brand" href="index.html">
					<img src="../../../theme/img/ochoa_logo_blanco.png" class="img-responsive" alt="Responsive image">
					<span class="style_title_nav">Digitalización de archivos</span>
				</a>
								
				<!-- start: Header Menu -->
				<div class="nav-no-collapse header-nav">
					<ul class="nav pull-right">
						<h3 style="text-align: center;">version 1.1</h3>
 						<!-- start: User Dropdown -->
						<li class="dropdown">
							<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="halflings-icon white user"></i> 
								<input id="cedula" type="hidden" value="<?php echo $cedula; ?>">
									<?php 
										echo $nombres;
									?>
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li class="dropdown-menu-title">
								</li>
								
								<form class="form-gla" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Welcome/cerrar_sesion" method="post" accept-charset="utf-8">
									<li class="aline-menu">
										<i class="halflings-icon remove color_ico"></i>
										<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
										<input type="submit" value="Cerrar" class="btn-menu-lg btn_cerra_off">
									</li>	
								</form>	
							</ul>
						</li>
						<!-- end: User Dropdown -->
					</ul>
				</div>
				<!-- end: Header Menu -->
			</div>
		</div>
	</div>
	<!-- start: Header -->
		<div class="container-fluid-full">
		<div class="row-fluid">
			
				<!-- start: Main Menu -->
				<div id="sidebar-left" class="span2">
					<div class="nav-collapse sidebar-nav">
						<ul class="nav nav-tabs nav-stacked main-menu">
							
							<form class="form-gla" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Welcome/re_inicio" method="post" accept-charset="utf-8">
								<li class="aline-menu">
									<i class="icon-home color_ico"></i>
									<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
									<input type="submit" value="Inicio" class="btn-menu-lg">
								</li>	
							</form>

							<li class="aline-menu">
								<a class="dropmenu" href="#"><i class="icon-folder-close-alt"></i><span class="hidden-tablet btn-menu-lg">Digitalización</span></a>
								<ul style="display: none;">
									<?php if($rol == 'Administrador' or $rol == 'Registro' or $rol == 'Calificador 1-etapa y 4-etapa' or $rol == 'Calificador 2-etapa' or $rol == 'Calificador 3-etapa' or $rol == 'Pagador' or $rol == 'Contabilidad' or $rol == 'Jefe recursos'){ ?>
										<form class="form-gla" id="digitalizacion_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Digital/index" method="post" accept-charset="utf-8">
											<li>
												<a class="submenu subMen" id="digitalizacion_submit"><i class="icon-check"></i>
													<span class="hidden-tablet"> Digitalización</span>
												</a>
											</li>
											<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
											<input type="hidden" name="Digitalización" value="Digitalización" class="btn-menu-lg">
										</form>	
									<?php } ?>

									<?php if($rol == 'Administrador' or $rol == 'Calificador 3-etapa'){ ?>
										<form class="form-gla" id="formato_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Digital/index" method="post" accept-charset="utf-8">
											<li>
												<a class="submenu subMen" id="formato_submit"><i class="icon-check"></i>
													<span class="hidden-tablet"> Creación formatos</span>
												</a>
											</li>
											<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
											<input type="hidden" name="Creación_formatos" value="Creación_formatos" class="btn-menu-lg">
										</form>	
									<?php } ?>
								</ul>
							</li>

							<?php if($rol == 'Administrador' or $rol == 'Registro' or $rol == 'Calificador 1-etapa y 4-etapa' or $rol == 'Recursos humanos'){ ?>
							<form class="form-gla" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Conceptos/index" method="post" accept-charset="utf-8">
								<li class="aline-menu">
									<i class="icon-pencil color_ico"></i>
									<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
									<input type="submit" value="Conceptos" class="btn-menu-lg">
								</li>	
							</form>
							<?php } ?>

							<li class="aline-menu">
								<a class="dropmenu" href="#"><i class="icon-folder-close-alt"></i><span class="hidden-tablet btn-menu-lg">Aprobaciones</span></a>
								<ul style="display: none;">
									<?php if($rol == 'Administrador' or $rol == 'Calificador 1-etapa y 4-etapa'){ ?>
										<form class="form-gla" id="compras_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Aprobaciones/index" method="post" accept-charset="utf-8">
											<li>
												<a class="submenu subMen" id="compras_submit"><i class="icon-check"></i>
													<span class="hidden-tablet"> Compras</span>
												</a>
											</li>
											<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
											<input type="hidden" name="etapa-1-4" value="Compras" class="btn-menu-lg">
										</form>	
									<?php } ?>	

									<?php if($rol == 'Administrador' or $rol == 'Calificador 2-etapa'  or $rol == 'Jefe recursos'){ ?>
										<form class="form-gla" id="director_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Aprobaciones/index" method="post" accept-charset="utf-8">
											<li>
												<a class="submenu subMen" id="director_submit"><i class="icon-signin"></i>
													<span class="hidden-tablet"> Director de area</span>
												</a>
											</li>
											<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
											<input type="hidden" name="etapa-2" value="Director de área" class="btn-menu-lg">
										</form>	
									<?php } ?>
									
									<?php if($rol == 'Administrador' or $rol == 'Calificador 3-etapa'){ ?>
							    	<form class="form-gla" id="control_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Aprobaciones/index" method="post" accept-charset="utf-8">
										<li>
											<a class="submenu subMen" id="control_submit"><i class="icon-download-alt"></i>
												<span class="hidden-tablet"> Control interno</span>
											</a>
										</li>
										<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
										<input type="hidden" name="etapa-3" value="Control interno" class="btn-menu-lg">
									</form>	
									<?php } ?>
									
									<?php if($rol == 'Administrador' or $rol == 'Contabilidad'){ ?>
							    	<form class="form-gla" id="contabilidad_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Aprobaciones/view_contabilidad" method="post" accept-charset="utf-8">
										<li>
											<a class="submenu subMen" id="contabilidad_submit"><i class="icon-tasks"></i>
												<span class="hidden-tablet"> Contabilidad</span>
											</a>
										</li>
										<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
										<input type="hidden" value="Contabilidad" class="btn-menu-lg">
									</form>	
									<?php } ?>
									
									<?php if($rol == 'Administrador' or $rol == 'Pagador'){ ?>
									<form class="form-gla" id="pagos_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Aprobaciones/view_pagos" method="post" accept-charset="utf-8">
										<li>
											<a class="submenu subMen" id="pagos_submit"><i class="icon-money"></i>
												<span class="hidden-tablet"> Pagos</span>
											</a>
										</li>
										<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
										<input type="hidden" value="Pagos" class="btn-menu-lg">
									</form>	
									<?php } ?>

								</ul>	
							</li>
							
				
							<li class="aline-menu">
								<a class="dropmenu" href="#"><i class="icon-group color_ico"></i><span class="hidden-tablet btn-menu-lg">Empleados</span></a>
								<ul style="display: none;">
									<?php if($rol == 'Administrador' or $rol == 'Jefe recursos' or $rol == 'Recursos humanos' or $rol == 'Formatos'){ ?>
										<form class="form-gla" id="empleados_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/empleados/Empleados/index" method="post" accept-charset="utf-8">
											<li>
												<a class="submenu subMen" id="empleados_submit"><i class="icon-group color_ico"></i>
													<span class="hidden-tablet"> Registros</span>
												</a>
											</li>	
											<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
										</form>
									<?php } ?>	

									<?php if($rol == 'Administrador' or $rol == 'Jefe recursos'){ ?>
										<form class="form-gla" id="reportes_empleados_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/empleados/Empleados/reportes" method="post" accept-charset="utf-8">
											<li>
												<a class="submenu subMen" id="reportes_empleados_submit"><i class="icon-check"></i>
													<span class="hidden-tablet"> Reportes</span>
												</a>
											</li>
											<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>										
										</form>	
									<?php } ?>
								</ul>
							</li>

							

							<?php if($rol == 'Administrador'){ ?>
							<form class="form-gla" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Usuarios/index" method="post" accept-charset="utf-8">
								<li class="aline-menu">
									<i class="icon-user-md color_ico"></i>
									<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
									<input type="submit" value="Usuarios" class="btn-menu-lg">
								</li>	
							</form>
							<?php } ?>

							<?php if($rol == 'Administrador' or $rol == 'Jefe recursos'){ ?>
							<form class="form-gla" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/evaluacion/Evaluacion/index" method="post" accept-charset="utf-8">
								<li class="aline-menu">
									<i class="icon-user-md color_ico"></i>
									<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
									<input type="submit" value="Evaluacion" class="btn-menu-lg">
								</li>	
							</form>
							<?php } ?>

							<li class="aline-menu">
								<a class="dropmenu" href="#"><i class="icon-list-alt"></i><span class="hidden-tablet btn-menu-lg">Formatos</span></a>
								<ul style="display: none;">
									<?php if($rol == 'Administrador' or $rol == 'Formatos'){ ?>
										<form class="form-gla" id="datosFormatos_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/formatos/formatos/index" method="post" accept-charset="utf-8">
											<li>
												<a class="submenu subMen" id="datosFormatos_submit"><i class="icon-pencil"></i>
													<span class="hidden-tablet"> Ingresar datos</span>
												</a>
											</li>
											<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
											<input type="hidden" name="Ingresar_datos" value="Ingresar_datos" class="btn-menu-lg">
										</form>	
									<?php } ?>

									<?php if($rol == 'Administrador' or $rol == 'Formatos'){ ?>
										<form class="form-gla" id="gestionFormatos_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/formatos/formatos/index" method="post" accept-charset="utf-8">
											<li>
												<a class="submenu subMen" id="gestionFormatos_submit"><i class="icon-list-alt"></i>
													<span class="hidden-tablet"> Generar formatos</span>
												</a>
											</li>
											<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
											<input type="hidden" name="Gestión_formatos" value="Gestión_formatos" class="btn-menu-lg">
										</form>	
									<?php } ?>
								</ul>
							</li>

							<?php if($rol == 'Administrador' or $rol == 'Jefe recursos'){ ?>
							<form class="form-gla" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/cuestionario/cuestionario/registros" method="post" accept-charset="utf-8">
								<li class="aline-menu">
									<i class="icon-user-md color_ico"></i>
									<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
									<input type="submit" value="Cuestionario" class="btn-menu-lg">
								</li>	
							</form>
							<?php } ?>

							

						</ul>
					</div>
				</div>
			<!-- end: Main Menu -->
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<div id="content" class="span10">
				<ul class="breadcrumb">
					<li>
						<i class="icon-home color_fla"></i>
						<a>Cuestionario</a> 
						<i class="icon-angle-right color_fla"></i>
					</li>
				</ul>

				<div class="box span11">
					<div class="box-header">
						<h2>
							<i class="halflings-icon th"></i>
							<span class="break"></span>Datos del cuestionario
						</h2>
						<div class="box-icon">
								<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
								<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div>
					</div>

					<div class="box-content" style="display: block;">
						<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper" role="grid">
							<div class="row-fluid">
								<div class="span12">

								<?php
									
										$nombreVendedor = $cuestionario['0']->nombreVendedor;
										$cedulaVendedor = $cuestionario['0']->cedulaVendedor;
										$jsonCuestionario = $cuestionario['0']->jsonCuestionario;
										$id  			   = $cuestionario['0']->id_evaluacion;

										$array_preguntas 		= json_decode($jsonCuestionario);
										

										/*$cont .= '<div class="control-group quest" style="margin-top: 2%;border-style: groove; padding:5px;">';
										$cont .= 	'<div class="controls">';
										$cont .=		'<label>'.'<b>Nombre del vendedor: </b>'.$cuestionario['0']->nombreVendedor.'</label>';
										$cont .= '		<input name="nombre_vendedor" type="hidden" value ="'.$cuestionario['0']->nombreVendedor.'">';
										$cont .=	'</div>';

										$cont .= 	'<div class="controls">';
										$cont .=		'<label>'.'<b>Documento del vendedor: </b>'.$cuestionario['0']->cedulaVendedor.'</label>';
										$cont .= '		<input name="cedula_vendedor" type="hidden" value ="'.$cuestionario['0']->cedulaVendedor.'">';
										$cont .=	'</div>';*/


										/*echo "<pre>";
											print_r($array_preguntas);
										echo "</pre>";
										die();*/

									foreach($array_preguntas as $array_preguntas){

			        					$pregunta 	= $array_preguntas->pregunta;
			        					$respuesta 	= $array_preguntas->respuesta;
			        					
			        					$cantidad_preguntas = count($pregunta);

			        					
			        					for ($i=0; $i < $cantidad_preguntas; $i++) { 
			        						$total_preguntas += $cantidad_preguntas;
			        					} 

										$cont .='<div class="control-group" style="margin-top:2%; border-style: solid; border-width: 2px; border-color:#008fd1; padding:7px;>';

			        					for($i=1; $i <= $cantidad_preguntas ; $i++){ 
											
											/*$cont .='<label class="control-label"> '.'<span class="icon-pencil"></span>&nbsp<b>Pregunta '.$total_preguntas.'</b></label>';*/


											$cont .='<label id="campo_pregunta['.$i.']" name="pregunta_cuestionario_'.$total_preguntas.'_[]"><span class="icon-pencil"></span>&nbsp<b style="color:#24417a; font-size:16px">'.$total_preguntas.'- '.$pregunta.'</b></label>';
											$cont .= '<input name="json_cuestionario[]" type="hidden" value ="'.$pregunta.'">';


											$cont .='<label style="font-size:16px;" id="campo_pregunta['.$i.']" name="respuesta_cuestionario_'.$total_preguntas.'_[]">'.$respuesta.'</label>';
											$cont .= '<input name="json_cuestionario[]" type="hidden" value ="'.$respuesta.'">';
												
										}

										$cont .='</div>'; 

			        				}//fin foreach

										echo $cont;	 

										echo '<input name="id_evaluacion" type="hidden" value ="'.$id.'">';
									?>
											
								</div>
							</div>
						</div>
					</div>

				</div>

			</div>		

		</div>
	
			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->
		
	<footer>
		<p>
			<span style="text-align:left;float:left">&copy; 2016 <a href="http://http://www.apuestasochoa.com.co/">Desarrollo interno - apuestasochoa</a></span>
		</p>
	</footer>
	<!-- start: JavaScript-->
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery-1.9.1.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery-migrate-1.0.0.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery-ui-1.10.0.custom.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.ui.touch-punch.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/modernizr.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/bootstrap.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.cookie.js"></script>
		<script src='http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/fullcalendar.min.js'></script>
		<script src='http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.dataTables.min.js'></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/excanvas.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.flot.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.flot.pie.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.flot.stack.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.flot.resize.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.chosen.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.uniform.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.cleditor.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.noty.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.elfinder.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.raty.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.iphone.toggle.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.uploadify-3.1.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.gritter.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.imagesloaded.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.masonry.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.knob.modified.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.sparkline.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/counter.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/retina.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/custom.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/Chart.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/apuestasochoa.js"></script>
		<!-- js logica de graficos trabajando de la mano de la libreria chartsjs -->
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/apuestasochoa.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/prueba/evaluar_proveedor.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/inicio/logic_charts.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/empleados/nuevo.js"></script>

	<!-- end: JavaScript-->
</body>
</html>
