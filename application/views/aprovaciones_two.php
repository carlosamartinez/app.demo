<!DOCTYPE html>
<html lang="en">
<head>
	<!-- start: Meta -->
	<meta charset="utf-8">
	<title>Digitalización</title>
	<meta name="description" content="Bootstrap Metro Dashboard">
	<meta name="author" content="Dennis Ji">
	<meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	<!-- end: Meta -->

	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->

	<!-- start: CSS kjkjkjk-->
	<link id="bootstrap-style" href="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/css/bootstrap.min.css" rel="stylesheet">
	<link href="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style" href="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/css/style-responsive.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
	<!-- end: CSS -->
	<link rel="shortcut icon" href="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/img/favicon.ico">
	<!-- end: Favicon -->
	<!-- estilo de la libreria de notificaciones -->
	<link href="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/css/styles.css" rel="stylesheet">
</head>

<body>
	<!-- start: Header -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>

				<a class="brand" href="index.html">
					<img src="../../theme/img/ochoa_logo_blanco.png" class="img-responsive" alt="Responsive image">
					<span class="style_title_nav">Digitalización de archivos</span>
				</a>

				<!-- start: Header Menu -->
				<div class="nav-no-collapse header-nav">
					<ul class="nav pull-right">
						<h3 style="text-align: center;">version 1.1</h3>
						<!-- start: User Dropdown -->

						<li class="dropdown">
							<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="halflings-icon white user"></i>
								<input id="cedula" type="hidden" value="<?php echo $cedula; ?>">
									<?php
										echo $nombres;
									?>
									<input id="dependencia" type="hidden" value="<?php echo $dependencia; ?>">
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li class="dropdown-menu-title">
								</li>

								<form class="form-gla" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Welcome/cerrar_sesion" method="post" accept-charset="utf-8">
									<li class="aline-menu">
										<i class="halflings-icon remove color_ico"></i>
										<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
										<input type="submit" value="Cerrar" class="btn-menu-lg">
									</li>
								</form>
							</ul>
						</li>
						<!-- end: User Dropdown -->
					</ul>
				</div>
				<!-- end: Header Menu -->

			</div>
		</div>
	</div>

	<!-- start: Header -->
	<div class="container-fluid-full">
		<div class="row-fluid">

				<!-- start: Main Menu -->
				<div id="sidebar-left" class="span2">
					<div class="nav-collapse sidebar-nav">
						<ul class="nav nav-tabs nav-stacked main-menu">

							<form class="form-gla" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Welcome/re_inicio" method="post" accept-charset="utf-8">
								<li class="aline-menu">
									<i class="icon-home color_ico"></i>
									<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
									<input type="submit" value="Inicio" class="btn-menu-lg">
								</li>
							</form>

							<li class="aline-menu">
								<a class="dropmenu" href="#"><i class="icon-folder-close-alt"></i><span class="hidden-tablet btn-menu-lg">Digitalización</span></a>
								<ul style="display: none;">
									<?php if($rol == 'Administrador' or $rol == 'Registro' or $rol == 'Calificador 1-etapa y 4-etapa' or $rol == 'Calificador 2-etapa' or $rol == 'Calificador 3-etapa' or $rol == 'Pagador' or $rol == 'Contabilidad' or $rol == 'Jefe recursos'){ ?>
										<form class="form-gla" id="digitalizacion_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Digital/index" method="post" accept-charset="utf-8">
											<li>
												<a class="submenu subMen" id="digitalizacion_submit"><i class="icon-check"></i>
													<span class="hidden-tablet"> Digitalización</span>
												</a>
											</li>
											<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
											<input type="hidden" name="Digitalización" value="Digitalización" class="btn-menu-lg">
										</form>	
									<?php } ?>

									<?php if($rol == 'Administrador' or $rol == 'Calificador 3-etapa'){ ?>
										<form class="form-gla" id="formato_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Digital/index" method="post" accept-charset="utf-8">
											<li>
												<a class="submenu subMen" id="formato_submit"><i class="icon-check"></i>
													<span class="hidden-tablet"> Creación formatos</span>
												</a>
											</li>
											<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
											<input type="hidden" name="Creación_formatos" value="Creación_formatos" class="btn-menu-lg">
										</form>	
									<?php } ?>
								</ul>
							</li>

							<?php if($rol == 'Administrador' or $rol == 'Registro' or $rol == 'Calificador 1-etapa y 4-etapa' or $rol == 'Recursos humanos'){ ?>
							<form class="form-gla" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Conceptos/index" method="post" accept-charset="utf-8">
								<li class="aline-menu">
									<i class="icon-pencil color_ico"></i>
									<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
									<input type="submit" value="Conceptos" class="btn-menu-lg">
								</li>
							</form>
							<?php } ?>

							<li class="aline-menu">
								<a class="dropmenu" href="#"><i class="icon-folder-close-alt"></i><span class="hidden-tablet btn-menu-lg">Aprobaciones</span></a>
								<ul style="display: none;">
									<?php if($rol == 'Administrador' or $rol == 'Calificador 1-etapa y 4-etapa'){ ?>
										<form class="form-gla" id="compras_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Aprobaciones/index" method="post" accept-charset="utf-8">
											<li>
												<a class="submenu subMen" id="compras_submit"><i class="icon-check"></i>
													<span class="hidden-tablet"> Compras</span>
												</a>
											</li>
											<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
											<input type="hidden" name="etapa-1-4" value="Compras" class="btn-menu-lg">
										</form>
									<?php } ?>

									<?php if($rol == 'Administrador' or $rol == 'Calificador 2-etapa' or $rol == 'Jefe recursos'){ ?>
										<form class="form-gla" id="director_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Aprobaciones/index" method="post" accept-charset="utf-8">
											<li>
												<a class="submenu subMen" id="director_submit"><i class="icon-signin"></i>
													<span class="hidden-tablet"> Director de area</span>
												</a>
											</li>
											<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
											<input type="hidden" name="etapa-2" value="Director de área" class="btn-menu-lg">
										</form>
									<?php } ?>

									<?php if($rol == 'Administrador' or $rol == 'Calificador 3-etapa'){ ?>
							    	<form class="form-gla" id="control_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Aprobaciones/index" method="post" accept-charset="utf-8">
										<li>
											<a class="submenu subMen" id="control_submit"><i class="icon-download-alt"></i>
												<span class="hidden-tablet"> Control interno</span>
											</a>
										</li>
										<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
										<input type="hidden" name="etapa-3" value="Control interno" class="btn-menu-lg">
									</form>
									<?php } ?>

									<?php if($rol == 'Administrador' or $rol == 'Contabilidad'){ ?>
							    	<form class="form-gla" id="contabilidad_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Aprobaciones/view_contabilidad" method="post" accept-charset="utf-8">
										<li>
											<a class="submenu subMen" id="contabilidad_submit"><i class="icon-tasks"></i>
												<span class="hidden-tablet"> Contabilidad</span>
											</a>
										</li>
										<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
										<input type="hidden" value="Contabilidad" class="btn-menu-lg">
									</form>
									<?php } ?>

									<?php if($rol == 'Administrador' or $rol == 'Pagador'){ ?>
									<form class="form-gla" id="pagos_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Aprobaciones/view_pagos" method="post" accept-charset="utf-8">
										<li>
											<a class="submenu subMen" id="pagos_submit"><i class="icon-money"></i>
												<span class="hidden-tablet"> Pagos</span>
											</a>
										</li>
										<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
										<input type="hidden" value="Pagos" class="btn-menu-lg">
									</form>
									<?php } ?>

								</ul>
							</li>


							<li class="aline-menu">
								<a class="dropmenu" href="#"><i class="icon-group color_ico"></i><span class="hidden-tablet btn-menu-lg">Empleados</span></a>
								<ul style="display: none;">
									<?php if($rol == 'Administrador' or $rol == 'Jefe recursos' or $rol == 'Recursos humanos' or $rol == 'Formatos'){ ?>
										<form class="form-gla" id="empleados_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/empleados/Empleados/index" method="post" accept-charset="utf-8">
											<li>
												<a class="submenu subMen" id="empleados_submit"><i class="icon-group color_ico"></i>
													<span class="hidden-tablet"> Registros</span>
												</a>
											</li>	
											<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
										</form>
									<?php } ?>	

									<?php if($rol == 'Administrador' or $rol == 'Jefe recursos'){ ?>
										<form class="form-gla" id="reportes_empleados_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/empleados/Empleados/reportes" method="post" accept-charset="utf-8">
											<li>
												<a class="submenu subMen" id="reportes_empleados_submit"><i class="icon-check"></i>
													<span class="hidden-tablet"> Reportes</span>
												</a>
											</li>
											<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>										
										</form>	
									<?php } ?>
								</ul>
							</li>



							<?php if($rol == 'Administrador'){ ?>
							<form class="form-gla" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Usuarios/index" method="post" accept-charset="utf-8">
								<li class="aline-menu">
									<i class="icon-user-md color_ico"></i>
									<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
									<input type="submit" value="Usuarios" class="btn-menu-lg">
								</li>
							</form>
							<?php } ?>


							<?php if($rol == 'Administrador' or $rol == 'Jefe recursos'){ ?>
							<form class="form-gla" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/evaluacion/Evaluacion/index" method="post" accept-charset="utf-8">
								<li class="aline-menu">
									<i class="icon-user-md color_ico"></i>
									<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
									<input type="submit" value="Evaluacion" class="btn-menu-lg">
								</li>	
							</form>
							<?php } ?>

							<li class="aline-menu">
								<a class="dropmenu" href="#"><i class="icon-list-alt"></i><span class="hidden-tablet btn-menu-lg">Formatos</span></a>
								<ul style="display: none;">
									<?php if($rol == 'Administrador' or $rol == 'Formatos'){ ?>
										<form class="form-gla" id="datosFormatos_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/formatos/formatos/index" method="post" accept-charset="utf-8">
											<li>
												<a class="submenu subMen" id="datosFormatos_submit"><i class="icon-pencil"></i>
													<span class="hidden-tablet"> Ingresar datos</span>
												</a>
											</li>
											<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
											<input type="hidden" name="Ingresar_datos" value="Ingresar_datos" class="btn-menu-lg">
										</form>	
									<?php } ?>

									<?php if($rol == 'Administrador' or $rol == 'Formatos'){ ?>
										<form class="form-gla" id="gestionFormatos_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/formatos/formatos/index" method="post" accept-charset="utf-8">
											<li>
												<a class="submenu subMen" id="gestionFormatos_submit"><i class="icon-list-alt"></i>
													<span class="hidden-tablet"> Generar formatos</span>
												</a>
											</li>
											<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
											<input type="hidden" name="Gestión_formatos" value="Gestión_formatos" class="btn-menu-lg">
										</form>	
									<?php } ?>
								</ul>
							</li>

							<?php if($rol == 'Administrador' or $rol == 'Jefe recursos'){ ?>
							<form class="form-gla" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/cuestionario/cuestionario/registros" method="post" accept-charset="utf-8">
								<li class="aline-menu">
									<i class="icon-user-md color_ico"></i>
									<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
									<input type="submit" value="Cuestionario" class="btn-menu-lg">
								</li>	
							</form>
							<?php } ?>



						</ul>
					</div>
				</div>
			<!-- end: Main Menu -->

			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>

			<div id="content" class="span10">
				<ul class="breadcrumb">
					<li>
						<i class="icon-signin color_fla"></i>
						<a>Aprobaciones 2 etapa</a>
						<i class="icon-angle-right color_fla"></i>
					</li>
				</ul>

				<?php
					if(isset($message)){
						if($message){?>
						<div class="alert alert-info fade in">
						    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
						    <strong>Info!</strong> El documento fue cambiado de estado.
						</div>
				<?php
						}
					}
				 ?>

			
				<div class="box span11">
					<div class="det">
						<div class="span12">
							<form action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Aprobaciones/test" enctype="multipart/form-data" method="post">
								<label for="">Filtrar por</label>
								<select name="filter" id="acciones_grid_pupilos">
								<?php if($seleted == 'empleados de area') {?>
									<option selected="selected">empleados de area</option>
								<?php }else{  ?>
									<option>empleados de area</option>
								<?php } if($seleted == 'activos'){?>	
									<option selected="selected">activos</option>
								<?php }else { ?>	
									<option>activos</option>
								<?php } if($seleted == 'Empleados en vacaciones') {?>
									<option selected="selected">Empleados en vacaciones</option>
								<?php } else{ ?>	
									<option>Empleados en vacaciones</option>
								<?php } if($seleted == 'proximos a vacaciones') {?>
									<option selected="selected">proximos a vacaciones</option>
								<?php } else{ ?>
									<option>proximos a vacaciones</option>
								<?php } ?>		
								</select>
								<input type="hidden" name="cedula" value="<?php echo $cedula; ?>">
								<input type="hidden" name="dependencia" value="<?php echo $dependencia; ?>">
								<button type="submit" class="btn btn-success" style="width: 3%;height: 3%;"><i class="halflings-icon zoom-in"></i></button>
							</form>	
						</div>
					</div>


					<div class="box-header" data-original-title="">
						<h2>
							<i class="halflings-icon th"></i>
							<span class="break"></span>
							Empleados proximos a salir a vacaciones
						</h2>
						<div class="box-icon">
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div>
					</div>


					<div class="box-content" style="display: block;">
						<!-- elemento donde iria la notificacion -->
						<div id="testy" style="margin: 0 auto;"></div>

						<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper" role="grid">
							<table class="table table-striped table-bordered bootstrap-datatable datatable dataTable cebra" id="DataTables_Table_0" aria-describedby="DataTables_Table_0_info">
							  <thead>
								  <tr role="row">
								  	 <th>nombres</th>
								  	 <th>apellidos</th>
								  	 <th>fecha_ingreso</th>
								  	 <th>cargo_laboral</th>
								  	 <th>departamento</th>
								  	 <?php if($seleted == 'proximos a vacaciones'){ ?>
								  	 <th style="color: #C7464B;">Acciones</th>
								  	 <?php } ?>
								  	 <?php if($seleted == 'activos' or $seleted == 'Empleados en vacaciones'){ ?>
								  	 <th style="color: #C7464B;">Estado</th>
								  	 <?php } ?>
								  </tr>
							  </thead>

						  	  <tbody role="alert" aria-live="polite" aria-relevant="all">
						  	  	<?php
						  	  		$body_html = '';
						  	  		for($i=0; $i < count($employee_next_vaca); $i++){
						  	  			$body_html .='<tr>';
						  	  			$body_html .='<td>'.$employee_next_vaca[$i]->nombres.'</td>';
						  	  			$body_html .='<td>'.$employee_next_vaca[$i]->apellidos.'</td>';
						  	  			$body_html .='<td>'.$employee_next_vaca[$i]->fecha_ingreso.'</td>';
						  	  			$body_html .='<td>'.$employee_next_vaca[$i]->cargo_laboral.'</td>';
						  	  			$body_html .='<td>'.$employee_next_vaca[$i]->departamento.'</td>';	
						  	  			if($seleted == 'proximos a vacaciones'){ 					  	  			
						  	  			$body_html .='<td><a class="opener" id="'.$employee_next_vaca[$i]->id_empleado.'-'.$employee_next_vaca[$i]->nombres.'-'.$employee_next_vaca[$i]->apellidos.'"><i class="icon-pencil zoom_i" style="color: black;"></i></a></td>';
						  	  			}
						  	  			if($seleted == 'activos'){ 					  	  			
						  	  				$body_html .='<td><img src="../../theme/img/laborando.png" class="img-responsive" alt="Responsive image">ACTIVO</td>';
						  	  			}
						  	  			if($seleted == 'Empleados en vacaciones'){ 					  	  			
						  	  				$body_html .='<td><img src="../../theme/img/vacaciones.png" class="img-responsive" alt="Responsive image">EN VACACIONES</td>';
						  	  			}
						  	  			$body_html .='</tr>';
						  	  		}
						  	  		echo $body_html;
						  	  	 ?>
							  </tbody>
							</table>
					 	</div>

						<!-- internal iframe ? formulario externo que se muestra solo cuando el jefe de area desea aprobar vacaciones de algun empleado -->
						<div id="dialog" title="Proceso de vacaciones">
						  <h3 id="name_emple_procces"></h3>
              				<label>Acciones a tomar</label>
						  <select name="eventos_selection" id="eventos_selection">
						  	<option>Seleccione...</option>
						  	<option>Tomar vacaciones completas</option>
						  	<option>Solo unos dias y los demas pagos</option>
						  	<option>Vacaciones pagadas</option>
						  </select>
						</div>


					</div>
				</div>




				<div class="box span11">
					<div class="box-header" data-original-title="">
						<h2>
							<i class="halflings-icon th"></i>
							<span class="break"></span>
							Estados-Documentos
						</h2>
						<div class="box-icon">
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div>
					</div>

					<div class="box-content gets_docu" style="display: block;">
						<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper" role="grid">

							<table class="table table-striped table-bordered bootstrap-datatable datatable dataTable cebra" id="DataTables_Table_0" aria-describedby="DataTables_Table_0_info">
							  <thead>
								  <tr role="row">
								  	<!-- <th>Dependencia - creación</th> -->
								  	<th>Fecha de digitalización</th>
								  	<th>Nro de factura</th>
								  	<th>Concepto</th>
								  	<th>Nombre-documento</th>
								  	<th>Ubicación- aprobación</th>
								  	<!-- <th>Persona-aprueba</th> -->
								  	<th>Estado</th>
								  	<th>Acciones</th>
								  </tr>
							  </thead>

						  	  <tbody role="alert" aria-live="polite" aria-relevant="all">
						  	  	<?php
						  	  		for ($i=0; $i < count($table); $i++){
						  	  			$des_rut = explode('/', $table[$i]->ruta_imagen);
						  	  			$nombre_ruta = end($des_rut);
						  	  			echo "<tr class='even'>";
						  	  			// echo "<td>".$table[$i]->dependencia."</td>";
						  	  			echo "<td>".$table[$i]->fecha_ingreso."</td>";
						  	  			echo "<td>".$table[$i]->nro_factura."</td>";
						  	  			echo "<td>".$table[$i]->concepto."</td>";
						  	  			echo "<td>".$nombre_ruta."</td>";
						  	  			echo "<td>".$table[$i]->ubicacion."</td>";
						  	  			// echo "<td>".$table[$i]->usuario_aprueba."</td>";

						  	  			switch ($table[$i]->estado){
						  	  				case 'Aprobado':
						  	  					echo "<td>".$table[$i]->estado."<img src='../../theme/img/aprobado.png' class='img-responsive' alt='Responsive image'></td>";
						  	  					break;
						  	  				case 'Pendiente':
						  	  					echo "<td>".$table[$i]->estado."<img src='../../theme/img/pendiente.png' class='img-responsive' alt='Responsive image'></td>";
						  	  					break;
						  	  				case 'Pagado':
						  	  					echo "<td>".$table[$i]->estado."<img src='../../theme/img/pagado.png' class='img-responsive' alt='Responsive image'></td>";
						  	  					break;
						  	  				case 'Negado':
						  	  					echo "<td>".$table[$i]->estado."<img src='../../theme/img/negado.png' class='img-responsive' alt='Responsive image'></td>";
						  	  					break;
						  	  			}

						  	  			$htm_btn ='<td>';
						  	  			$htm_btn .=	'<form class="form-gla" action="http://'.$_SERVER['HTTP_HOST'].'/prueba/index.php/Aprobaciones/update_estado_apro" method="post" accept-charset="utf-8">';
						  	  			$htm_btn .=		'<input type="hidden" name="cedula" value="'.$cedula.'">';
						  	  			$htm_btn .=		'<input class="test_dep" type="hidden" name="dependencia" value="'.$dependencia.'">';
						  	  			$htm_btn .=		'<input type="hidden" name="id_documento" value="'.$table[$i]->id_documento.'">';
						  	  			$htm_btn .=		'<input type="hidden" name="etapa_2" value="1">';
						  	  			$htm_btn .=		'<button id="'.$table[$i]->id_documento.'" type="submit" class="btn btn-success" title="Aprobar 2-etapa">';
						  	  			$htm_btn .=			'<i class="halflings-icon ok" style="padding-left: 7px; text-align: right;">2</i>';
						  	  			$htm_btn .=		'</button>';
						  	  			$htm_btn .=	'</form>';

						  	  			$htm_btn .=	'<form class="form-gla" id="form-gla_negadosegunda_'.$table[$i]->id_documento.'" action="http://'.$_SERVER['HTTP_HOST'].'/prueba/index.php/Aprobaciones/update_estado_apro" method="post">';
						  	  			$htm_btn .=	  '<input type="hidden" name="cedula" value="'.$cedula.'">';
						  	  			$htm_btn .=	  '<input type="hidden" name="dependencia" value="'.$dependencia.'">';
						  	  			$htm_btn .=	  '<input type="hidden" name="id_documento" value="'.$table[$i]->id_documento.'">';
						  	  			$htm_btn .=	  '<input type="hidden" name="etapa_2" value="2">';
						  	  			$htm_btn .=	  '<input type="hidden" name="negado_2" value="1">';									
						  	  			$htm_btn .=	  '<button id="'.$table[$i]->id_documento.'" type="submit" class="btn btn-danger" onclick="get_obs(this);" title="Desaprobar">';
						  	  			$htm_btn .=	  	'<i class="icon-ban-circle ok" style="padding-left: 7px; text-align: right;"></i>';
						  	  			$htm_btn .=	  '</button>';
						  	  			$htm_btn .=	    '<label for="">Observaciones (solo si va a desaprobar!!)</label>';
						  	  			$htm_btn .=	    '<textarea id="observaciones" name="observaciones"></textarea>';
						  	  			$htm_btn .=	'</form>';

						  	  			$htm_btn .=	'<a style="cursor: pointer;" id="'.$table[$i]->ruta_imagen.'" onclick="view_image_document(this);">';
						  	  			$htm_btn .=		'<li class="glyphicons-icon eye_open"></li>';
						  	  			$htm_btn .=	'</a>';
						  	  			$htm_btn .='</td>';
						  	  			echo $htm_btn;
						  	  			echo "</tr>";
						  	  		}
						  	  	 ?>
							  </tbody>
							</table>
					 	</div>
					</div>
				</div>
			</div>
			<!-- end: Content -->
		</div><!--/#content.span10-->
	</div><!--/fluid-row-->

	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Settings</h3>
		</div>
		<div class="modal-body">
			<p>Here settings can be configured...</p>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
			<a href="#" class="btn btn-primary">Save changes</a>
		</div>
	</div>

	<div class="clearfix"></div>
	<footer>
		<p>
			<span style="text-align:left;float:left">&copy; 2016 <a href="http://http://www.apuestasochoa.com.co/">Desarrollo interno - apuestasochoa</a></span>
		</p>
	</footer>
	<!-- start: JavaScript-->
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery-1.9.1.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery-migrate-1.0.0.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery-ui-1.10.0.custom.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.ui.touch-punch.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/modernizr.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/bootstrap.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.cookie.js"></script>
		<script src='http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/fullcalendar.min.js'></script>
		<script src='http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.dataTables.min.js'></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/excanvas.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.flot.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.flot.pie.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.flot.stack.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.flot.resize.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.chosen.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.uniform.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.cleditor.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.noty.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.elfinder.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.raty.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.iphone.toggle.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.uploadify-3.1.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.gritter.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.imagesloaded.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.masonry.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.knob.modified.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.sparkline.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/counter.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/retina.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/custom.js"></script>

		<!-- incluimo libreria de notificaciones -->
		<script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.toastee.0.1.js"></script>
		<script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/main.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/apuestasochoa.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/aprobaciones.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/prueba/evaluar_proveedor.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/empleados/nuevo.js"></script>

		<!-- incluimos libreria de dialog para mostrar formularios externos -->
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
		<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

		<!-- end: JavaScript-->

</body>
</html>
