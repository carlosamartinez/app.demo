	
			<div id="content" class="span10">
				<ul class="breadcrumb">
					<li>
						<i class="icon-folder-open color_fla"></i>
						<a>Proveedores</a> 
						<i class="icon-angle-right color_fla"></i>
					</li>
				</ul>
				<?php 
					if(isset($message)){
						if($message){?>
						<div class="alert alert-info fade in">
						    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
						    <strong>Info!</strong> Usted debe de diligenciar los campos marcados con <b>*</b>.
						</div>
				<?php 
						}
					}
				 ?>		
				

			
				<div class="row-fluid">	
					<div class="box span12">
						<div class="box-header" data-original-title="">
							<h2><i class="halflings-icon edit"></i><span class="break"></span>Informacion de archivo</h2>
							<div class="box-icon">
								<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
								<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
							</div>
						</div>

						<div class="box-content" style="display: block;">
						
						<form class="form-horizontal" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Digital/insert_dates" enctype="multipart/form-data" method="post">
								<fieldset>
								  
								  <div class="control-group">
									<label class="control-label" for="focusedInput">Cedula usuario</label>
									<div class="controls">
									  <input class="input-xlarge focused" type="text" value="<?php echo $cedula; ?>" disabled/>
									  <input class="input-xlarge focused" id="cedula_edit" name="cedula_user_digitaliza" type="hidden" value="<?php echo $cedula; ?>"/>
									</div>
								  </div>

								  <div class="control-group">
									<label class="control-label" for="focusedInput">Dependencia</label>
									<div class="controls">
									  <input class="input-xlarge focused" type="text" value="<?php echo $dependencia; ?>" disabled/>
									  <input class="input-xlarge focused" name="dependencia" type="hidden" value="<?php echo $dependencia; ?>"/>
									</div>
								  </div>
									<?php 
										$hoy = getdate();
										$fecha = $hoy['year']."-".$hoy['mon']."-".$hoy['mday'];
									 ?>
								    <div class="control-group">
								  	<label class="control-label" for="focusedInput">Fecha de ingreso</label>
								  	<div class="controls">
								  	  <input class="input-xlarge focused"  type="text" value="<?php echo $fecha; ?>" disabled/>
								  	  <input class="input-xlarge focused" name="fecha_ingreso" type="hidden" value="<?php echo $fecha; ?>"/>
								  	</div>
								    </div>
								  
								  <div class="control-group">
									<label class="control-label" for="focusedInput">Nro factura</label>
									<div class="controls">
									  <input class="input-xlarge focused" name="nro_factura" type="text" value="">
									  <span class='obligatorio'>*</span>
									</div>
								  </div>

								   <div class="control-group">
									  <label class="control-label" for="focusedInput">Concepto</label>
									  	<div class="controls">
									  	  <select id="concepto" name="concepto"></select>
									  	  <span class='obligatorio'>*</span>
									  	</div>
								   </div>


								  <div class="control-group">
									<label class="control-label" for="focusedInput">Archivo</label>
									<div class="controls">
									  <input name="uploadedfile" type="file" />
							  	  	  <span class='obligatorio'>*</span>
									</div>
								  </div>

					              <!-- <div id="img_content1" class="control-group span11">
					  				<label class="control-label" for="focusedInput">Archivo</label>
					  				<div class="controls">
					  				  <input type="file" class="image_hoja" name="uploadedfile[]" multiple="multiple" id="documentos_adjuntos">
					  				  <span class="obligatorio">*</span>
					  				</div>
					              </div> -->
								  
							      <div class="control-group">
							  	   <label class="control-label" for="focusedInput">Ubicacion - Aprobación</label>
							  	  	 <div class="controls">
							  	  	   <select id="ubicacion" name="ubicacion"></select>
							  	  	   <span class='obligatorio'>*</span>
							  	  	 </div>
							      </div>

						          <div class="control-group">
						      	   <label class="control-label" for="focusedInput">Persona-aprueba</label>
						      	  	 <div class="controls">
						      	  	   <select id="usuario_aprueba" name="usuario_aprueba"></select>
						      	  	   <span class='obligatorio'>*</span>
						      	  	 </div>
						          </div>
				
									
	  							  </div>
								  <div class="form-actions">
									<button type="submit" class="btn btn-primary">Crear</button>
								  </div>
								</fieldset>
							  </form>
						
						</div>
					</div>			
				</div>
			</div>
			<!-- end: Content -->
		