<!DOCTYPE html>
<html lang="en">
<head>
	
	<!-- start: Meta -->
	<meta charset="utf-8">
	<title>Digitalización</title>
	<meta name="description" content="Bootstrap Metro Dashboard">
	<meta name="author" content="Dennis Ji">
	<meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	<!-- end: Meta -->
	
	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->
	
	<!-- start: CSS kjkjkjk-->
	<link id="bootstrap-style" href="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/css/bootstrap.min.css" rel="stylesheet">
	<link href="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style" href="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/css/style-responsive.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
	<!-- end: CSS -->
	
	<link rel="shortcut icon" href="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/img/favicon.ico">
	<!-- end: Favicon -->
</head>

<body>
	<!-- start: Header -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>

				<a class="brand">
					<img src="../../../theme/img/ochoa_logo_blanco.png" class="img-responsive" alt="Responsive image">
					<span class="style_title_nav">Evaluación</span>
				</a>
								
				<!-- start: Header Menu -->
				<div class="nav-no-collapse header-nav">
					<ul class="nav pull-right">
						<h3 style="text-align: center;">version 1.1</h3>
						<!-- start: User Dropdown -->
						<li class="dropdown">
							<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="halflings-icon white user"></i> 
								<input id="cedula" type="hidden" value="<?php echo $cedula; ?>">
									<?php 
										echo $nombreEvaluado;
									?>
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li class="dropdown-menu-title">
								</li>
								<form class="form-gla" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Welcome/cerrar_sesion" method="post" accept-charset="utf-8">
									<li class="aline-menu">
										<i class="halflings-icon remove color_ico"></i>
										<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
										<input type="submit" value="Cerrar" class="btn-menu-lg">
									</li>	
								</form>
							</ul>
						</li>
						<!-- end: User Dropdown -->
					</ul>
				</div>
				<!-- end: Header Menu -->
				
			</div>
		</div>
	</div>

	<div class="container-fluid-full">
		<div class="row-fluid">

			<div id="sidebar-left" class="span2">
				<div class="nav-collapse sidebar-nav">
					<ul class="nav nav-tabs nav-stacked main-menu">
						<form class="form-gla" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/evaluacion/Evaluacion/realizar_evaluacion" method="post" accept-charset="utf-8">
							<li class="aline-menu">
								<i class="icon-home color_ico"></i>
								<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
								<input type="submit" value="Inicio" class="btn-menu-lg">
							</li>	
						</form>
					</ul>
				</div>
			</div>

			<form class="form-gla" id="realizar_evaluacion" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/evaluacion/Evaluacion/calificarEvaluacion" method="post" enctype="multipart/form-data" accept-charset="utf-8">
			
				<div id="content" class="span10">

					<ul class="breadcrumb" style="background-color: white;">
						<li>
							<i class="icon-paste color_fla"></i>
								<a>Evaluación</a> 
							<i class="icon-angle-right color_fla"></i>
						</li>
					</ul>

				<div class="box span11">
					<div class="box-header">
						<h2>
							<i class="halflings-icon th"></i>
							<span class="break"></span>Realizar evaluación
						</h2>
						<!-- <div class="box-icon">
								<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
								<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div> -->
					</div>



				<div class="box-content" style="display: block;">
					<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper" role="grid">
						<div class="row-fluid">
							<div class="span12">

							<?php 
								
									$nombre_evaluacion = $datos_editar['0']->nombre_evaluacion;
									$json_cuestionario = $datos_editar['0']->json_cuestionario;
									$descripcion 	   = $datos_editar['0']->descripcion;
									$calificaciones	   = $datos_editar['0']->calificaciones;
									$id  			   = $datos_editar['0']->id_evaluacion;	

									$array_preguntas 		= json_decode($json_cuestionario);
									$array_calificaciones 	= json_decode($calificaciones); 

									

									$cont .= '<div class="control-group quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; padding:5px;">';
									$cont .= 		'<div class="controls">';
									$cont .=			'<label id="nombre_entrevistado" name="nombre_entrevistado">'.'<b style="color:#24417a";>Nombre de la persona evaluada: </b>'.$nombreEvaluado.'</label>';
									$cont .= '<input name="nombreEvaluado" type="hidden" value ="'.$nombreEvaluado.'">';
									$cont .=		'</div>';

									$cont .= 		'<div class="controls">';
									$cont .=			'<label id="documento_entrevistado" name="documento_entrevistado">'.'<b style="color:#24417a";>Documento del entrevistado: </b>'.$cedulaEvaluado.'</label>';
									$cont .= '<input name="cedulaEvaluado" type="hidden" value ="'.$cedulaEvaluado.'">';
									$cont .=		'</div>';

									$cont .= 		'<div class="controls">';
									$cont .=			'<label id="nombre_evaluacion" name="nombre_evaluacion">'.'<b style="color:#24417a";>Nombre de la evaluación: </b>'.$nombre_evaluacion.'</label>';
									$cont .= '<input name="nombre_evaluacion" type="hidden" value ="'.$nombre_evaluacion.'">';
									$cont .=		'</div>';

									$cont .=		'<div class="controls">';
									$cont .=	 		'<label id="descripcion" name="descripcion">'.'<b style="color:#24417a";>Descripción de la evaluación: </b>'.$descripcion.'</label>';
									$cont .=		'</div>';
									$cont .= '<input name="descripcion" type="hidden" value ="'.$descripcion.'">';
									$cont .='</div>';

									$contador = 1;

								foreach($array_preguntas as $array_preguntas){

		        					$pregunta 					 = $array_preguntas->pregunta;           					        					   
		        					/*$opciones_respuestas	     = $array_preguntas->opciones_respuestas;
		        					$respuestas_verdaderas  	 = $array_preguntas->respuestas_verdaderas;*/
		        					
		        					$options_posibles			 = substr($array_preguntas->opciones_respuestas,10);//estas son las posibles respuestas 
		        					$respuestas_v 	 			 = substr($array_preguntas->respuestas_verdaderas, 10);//estas son las resppuestas verdaderas

		        					$array_posibles_preguntas 	 = explode("_", $options_posibles);//Estas son las posibles respuestas 
		        					$array_respuestas_verdaderas = explode("_", $respuestas_v);//Estas son las respuestas y estan en arreglos

		        					$cantidad_preguntas 		 = count($pregunta);//Trae la cantidad de preguntas
		        					$opciones_resp				 = count($array_posibles_preguntas);//cuenta cuantas posibles respuestas hay
		        					$resp_verdad 				 = count($array_respuestas_verdaderas);

		        					
		        					for ($i=0; $i < $cantidad_preguntas; $i++) { 
		        						$total_preguntas += $cantidad_preguntas;//Cuenta cuantas preguntas hay, sirve como autoincrementable
		        					} 
		        					
		        					$abecedario = ["A: ", "B: ", "C: ", "D: ", "E: "];
		        					$abecedarioRespuestas = ["A", "B", "C", "D", "E"];

									// $cont .='<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; padding:5px;>';

									switch ($contador){
										case 1:
			$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/1.Super-astro.png); width: 82%; background-repeat: no-repeat; padding: 46px;>';
											break;
										case 2:
										// die('here 2');
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/2.-chance.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 3:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/3.-Megachance.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 4:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/4.-doble-play.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 5:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/5.-semanario.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 6:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/6.-avon.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 7:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/7.-super-giros.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 8:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/7.-super-giros.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 9:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/8.-colombia-mayor.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 10:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/8.-colombia-mayor.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 11:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/7.-super-giros.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 12:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/7.-super-giros.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 13:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/7.-super-giros.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 14:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/7.-super-giros.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 15:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/7.-super-giros.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 16:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/9.-recargas.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 17:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/10.-soat.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 18:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/10.-soat.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';											
											break;
										case 19:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/12.-loteria.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											# code...
											break;
										case 20:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/13.-deportivas.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 21:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/13.-deportivas.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 22:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/14.-la-ofrenda.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 23:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/15.-juegos-localizados.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 24:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/15.-juegos-localizados.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 25:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/15.-juegos-localizados.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 26:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/15.-juegos-localizados.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 27:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/15.-juegos-localizados.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 28:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/15.-juegos-localizados.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 29:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/16.-procredito.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 30:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/16.-procredito.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 31:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/16.-procredito.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 32:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/17.ochoa.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 33:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/18.-premios.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 34:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/17.ochoa.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 35:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/17.ochoa.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 36:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/17.ochoa.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;	
										case 37:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/17.ochoa.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 38:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/17.ochoa.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;	
										case 39:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/17.ochoa.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 40:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/17.ochoa.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 41:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/17.ochoa.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 42:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/19.-siplaft.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 43:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/19.-siplaft.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 44:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/20.saro.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 45:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/20.saro.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 46:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/20.saro.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 47:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/20.saro.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 48:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/20.saro.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 49:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/20.saro.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 50:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/20.saro.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 51:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/1.Super-astro.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 52:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/2.-chance.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 53:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/2.-chance.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 54:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/2.-chance.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 55:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/3.-Megachance.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;											
										case 56:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/2.-chance.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 57:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/12.-loteria.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 58:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/17.ochoa.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 59:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/21.calidad.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;
										case 60:
											$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; background-image: url(../../../theme/img/12.-loteria.png); padding: 46px; width: 82%; background-repeat: no-repeat;>';
											break;									
										default:
											$cont .='<div class="control-group" quest" style="margin-top: 2%;border-style: solid; border-width: 2px; border-color:#008fd1; padding:5px;>';
											break;
									}	

									

		        					for($i=1; $i <= $cantidad_preguntas ; $i++){ 
										
										$cont .='<label id="lbl_pregunta['.$total_preguntas.']" class="control-label" for="focusedInput"> '.'<span class="icon-pencil" style="color:#24417a";></span>&nbsp<b style="color:#24417a";>Pregunta '.$total_preguntas.'</b></label>';
										$cont .='<label id="campo_pregunta['.$i.']" style="font-family: sans-serif; font-size: 150%; font-style: oblique; padding-top:15px; padding-bottom:15px;" name="pregunta_cuestionario_'.$total_preguntas.'_[]">'.$pregunta.'</label>';
										$cont .= '<input name="json_cuestionario[]" type="hidden" value ="'.$pregunta.'">';
										
										for ($p=0; $p < $opciones_resp; $p++) { 
											$cont .='<label id="posibles_resp['.$p.']" name="json_cuestionario[]"><b style="color:#24417a";>'.$abecedario[$p].'</b>'.$array_posibles_preguntas[$p].'</label>';
										}
											
										$cont .='<label class="input-xlarge focused" for="focusedInput"><span class="icon-edit" style="color:#24417a";></span>&nbsp<b style="color:#24417a";>Seleccione:</b></label>';

										for ($l=0; $l <$resp_verdad ; $l++){ 

											$cont .= '<input name="respuestasVerdaderas_'.$total_preguntas.'_[]" type="hidden" value ="'.$array_respuestas_verdaderas[$l].'">';

											$cont.= '<select style="color: black;" name="respuestasSeleccionadas_'.$total_preguntas.'_[]" id="respuestaSeleccionada['.$l.']" required>';
												$cont.= '<option></option>';
												for ($i=0; $i < $opciones_resp; $i++) { 
													$cont.= "<option>".$abecedarioRespuestas[$i]."</option>";
												}
											$cont.='</select>&nbsp';
										}
									}

									
																	
										
								

									$cont .='</div>'; //div class

									if($contador == 50){
										$cont .= '<img src="../../../theme/img/resume.png" class="img-responsive" alt="Responsive image">';
									}

									$contador++;
		        				}//fin foreach

		        					$desde_one 						= $array_calificaciones->desde_one;
		        					$cont .= '<input name="desde_one" type="hidden" value ="'.$desde_one.'">';
									$hasta_one 						= $array_calificaciones->hasta_one;
									$cont .= '<input name="hasta_one" type="hidden" value ="'.$hasta_one.'">';
									$equivalente_porcentaje_one 	= $array_calificaciones->equivalente_porcentaje_one;
									$cont .= '<input name="equivalente_porcentaje_one" type="hidden" value ="'.$equivalente_porcentaje_one.'">';
									$equivalente_unitaria_one 		= $array_calificaciones->equivalente_unitaria_one;
									$cont .= '<input name="equivalente_unitaria_one" type="hidden" value ="'.$equivalente_unitaria_one.'">';

									$desde_two 						= $array_calificaciones->desde_two;
									$cont .= '<input name="desde_two" type="hidden" value ="'.$desde_two.'">';
									$hasta_two 						= $array_calificaciones->hasta_two;
									$cont .= '<input name="hasta_two" type="hidden" value ="'.$hasta_two.'">';
									$equivalente_porcentaje_two		= $array_calificaciones->equivalente_porcentaje_two;
									$cont .= '<input name="equivalente_porcentaje_two" type="hidden" value ="'.$equivalente_porcentaje_two.'">';
									$equivalente_unitaria_two		= $array_calificaciones->equivalente_unitaria_two;
									$cont .= '<input name="equivalente_unitaria_two" type="hidden" value ="'.$equivalente_unitaria_two.'">';

									$calificacion_excelente_desde	= $array_calificaciones->calificacion_exelente_desde;
									$cont .= '<input name="calificacion_excelente_desde" type="hidden" value ="'.$calificacion_excelente_desde.'">';
									$calificacion_excelente_hasta	= $array_calificaciones->calificacion_exelente_hasta;
									$cont .= '<input name="calificacion_excelente_hasta" type="hidden" value ="'.$calificacion_excelente_hasta.'">';
									$calificacion_bueno_desde		= $array_calificaciones->calificacion_bueno_desde;
									$cont .= '<input name="calificacion_bueno_desde" type="hidden" value ="'.$calificacion_bueno_desde.'">';
									$calificacion_bueno_hasta		= $array_calificaciones->calificacion_bueno_hasta;
									$cont .= '<input name="calificacion_bueno_hasta" type="hidden" value ="'.$calificacion_bueno_hasta.'">';
									$calificacion_aceptable_desde 	= $array_calificaciones->calificacion_aceptable_desde;
									$cont .= '<input name="calificacion_aceptable_desde" type="hidden" value ="'.$calificacion_aceptable_desde.'">';
									$calificacion_aceptable_hasta	= $array_calificaciones->calificacion_aceptable_hasta;
									$cont .= '<input name="calificacion_aceptable_hasta" type="hidden" value ="'.$calificacion_aceptable_hasta.'">';
									$calificacion_deficiente_desde	= $array_calificaciones->calificacion_deficiente_desde;
									$cont .= '<input name="calificacion_deficiente_desde" type="hidden" value ="'.$calificacion_deficiente_desde.'">';
									$calificacion_deficiente_hasta	= $array_calificaciones->calificacion_deficiente_hasta;
									$cont .= '<input name="calificacion_deficiente_hasta" type="hidden" value ="'.$calificacion_deficiente_hasta.'">';

									echo $cont;	 

									echo '<input name="id_evaluacion" type="hidden" value ="'.$id.'">';
								?>
									<div class="controls group">
										<button id="guardar_evaluacion" class="btn btn-primary" type="submit">Finalizar evaluación</button>
									</div>	
							</div>
						</div>
					</div>
				</div>

				</div>
			</div>

		</form>
	</div>
</div>




	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Settings</h3>
		</div>
		<div class="modal-body">
			<p>Here settings can be configured...</p>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
			<a href="#" class="btn btn-primary">Save changes</a>
		</div>
	</div>
	
	<div class="clearfix"></div>
	<footer>
		<p>
			<span style="text-align:left;float:left">&copy; 2016 <a href="http://http://www.apuestasochoa.com.co/">Desarrollo interno - apuestasochoa</a></span>
		</p>
	</footer>
	<!-- start: JavaScript-->
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery-1.9.1.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery-migrate-1.0.0.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery-ui-1.10.0.custom.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.ui.touch-punch.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/modernizr.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/bootstrap.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.cookie.js"></script>
		<script src='http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/fullcalendar.min.js'></script>
		<script src='http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.dataTables.min.js'></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/excanvas.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.flot.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.flot.pie.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.flot.stack.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.flot.resize.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.chosen.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.uniform.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.cleditor.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.noty.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.elfinder.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.raty.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.iphone.toggle.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.uploadify-3.1.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.gritter.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.imagesloaded.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.masonry.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.knob.modified.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.sparkline.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/counter.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/retina.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/custom.js"></script>
		<!-- <script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/apuestasochoa.js"></script> -->
	<!-- end: JavaScript-->
	
</body>
</html>