<form class="form-gla" id="realizar_evaluacion" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/evaluacion/Evaluacion/calificarEvaluacion" method="post" enctype="multipart/form-data" accept-charset="utf-8">
			
				<div id="content" class="span10">

					<ul class="breadcrumb" style="background-color: white;">
						<li>
							<i class="icon-paste color_fla"></i>
								<a>Evaluación</a> 
							<i class="icon-angle-right color_fla"></i>
						</li>
					</ul>

				<div class="box span11">
					<div class="box-header">
						<h2>
							<i class="halflings-icon th"></i>
							<span class="break"></span>Realizar evaluación
						</h2>
						<div class="box-icon">
								<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
								<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div>
					</div>

				<div class="box-content" style="display: block;">
					<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper" role="grid">
						<div class="row-fluid">
							<div class="span12">

							<?php 
								
									$nombreEvaluacion = $calificacion['0']->nombreEvaluacion;
									$jsonCuestionario = $calificacion['0']->jsonCuestionario;
									$descripcion 	   = $calificacion['0']->descripcion;
									$calificaciones	   = $calificacion['0']->calificaciones;
									$id  			   = $calificacion['0']->id_evaluacion;	

									$array_preguntas 		= json_decode($jsonCuestionario);
									$array_calificaciones 	= json_decode($calificaciones); 

									

									$cont .= '<div class="control-group quest" style="margin-top: 2%;border-style: groove; padding:5px;">';
									$cont .= 		'<div class="controls">';
									$cont .=			'<label id="nombre_entrevistado" name="nombre_entrevistado">'.'<b>Nombre de la persona evaluada: </b>'.$calificacion['0']->nombreEvaluado.'</label>';
									$cont .= '<input name="nombreEvaluado" type="hidden" value ="'.$calificacion['0']->nombreEvaluado.'">';
									$cont .=		'</div>';

									$cont .= 		'<div class="controls">';
									$cont .=			'<label id="documento_entrevistado" name="documento_entrevistado">'.'<b>Documento del entrevistado: </b>'.$calificacion['0']->cedulaEvaluado.'</label>';
									$cont .= '<input name="cedulaEvaluado" type="hidden" value ="'.$calificacion['0']->cedulaEvaluado.'">';
									$cont .=		'</div>';

									$cont .= 		'<div class="controls">';
									$cont .=			'<label id="nombre_evaluacion" name="nombre_evaluacion">'.'<b>Nombre de la evaluación: </b>'.$nombreEvaluacion.'</label>';
									$cont .= '<input name="nombre_evaluacion" type="hidden" value ="'.$nombreEvaluacion.'">';
									$cont .=		'</div>';

									$cont .=		'<div class="controls">';
									$cont .=	 		'<label id="descripcion" name="descripcion">'.'<b>Descripción de la evaluación: </b>'.$descripcion.'</label>';
									$cont .=		'</div>';
									$cont .= '<input name="descripcion" type="hidden" value ="'.$descripcion.'">';
									$cont .='</div>';

									// echo "<pre>";
									// 	print_r($array_preguntas);
									// echo "</pre>";
									// die();

								foreach($array_preguntas as $array_preguntas){

		        					$pregunta 					 = $array_preguntas->pregunta;           					        					   
		        					/*$opciones_respuestas	     = $array_preguntas->opciones_respuestas;
		        					$respuestas_verdaderas  	 = $array_preguntas->respuestas_verdaderas;*/
		        					
		        					$respuestas_seleccionadas	 = $array_preguntas->respuestas_seleccionadas;
		        					$respuestas = explode("_", $respuestas_seleccionadas);
		        					$cantidad_respuestas = count($respuestas);

		        					$cantidad_preguntas 		 = count($pregunta);//Trae la cantidad de preguntas

		        					
		        					for ($i=0; $i < $cantidad_preguntas; $i++) { 
		        						$total_preguntas += $cantidad_preguntas;//Cuenta cuantas preguntas hay, sirve como autoincrementable
		        					} 

									$cont .='<div class="control-group" quest" style="margin-top: 2%;border-style: groove; padding:5px;>';	 	

		        					for($i=1; $i <= $cantidad_preguntas ; $i++){ 
										
										$cont .='<label id="lbl_pregunta['.$total_preguntas.']" class="control-label" for="focusedInput"> '.'<span class="icon-pencil"></span>&nbsp<b>Pregunta '.$total_preguntas.'</b></label>';
										$cont .='<label id="campo_pregunta['.$i.']" name="pregunta_cuestionario_'.$total_preguntas.'_[]">'.$pregunta.'</label>';
										$cont .= '<input name="json_cuestionario[]" type="hidden" value ="'.$pregunta.'">';
										
										for ($p=0; $p < $cantidad_respuestas; $p++) { 
											$cont .='<label id="posibles_resp['.$p.']" name="json_cuestionario[]">'.$respuestas[$p].'</label>';
										}
											
									}

									$cont .='</div>'; //div class 
		        				}//fin foreach

		        					$desde_one 						= $array_calificaciones->desde_one;
		        					$cont .= '<input name="desde_one" type="hidden" value ="'.$desde_one.'">';
									$hasta_one 						= $array_calificaciones->hasta_one;
									$cont .= '<input name="hasta_one" type="hidden" value ="'.$hasta_one.'">';
									$equivalente_porcentaje_one 	= $array_calificaciones->equivalente_porcentaje_one;
									$cont .= '<input name="equivalente_porcentaje_one" type="hidden" value ="'.$equivalente_porcentaje_one.'">';
									$equivalente_unitaria_one 		= $array_calificaciones->equivalente_unitaria_one;
									$cont .= '<input name="equivalente_unitaria_one" type="hidden" value ="'.$equivalente_unitaria_one.'">';

									$desde_two 						= $array_calificaciones->desde_two;
									$cont .= '<input name="desde_two" type="hidden" value ="'.$desde_two.'">';
									$hasta_two 						= $array_calificaciones->hasta_two;
									$cont .= '<input name="hasta_two" type="hidden" value ="'.$hasta_two.'">';
									$equivalente_porcentaje_two		= $array_calificaciones->equivalente_porcentaje_two;
									$cont .= '<input name="equivalente_porcentaje_two" type="hidden" value ="'.$equivalente_porcentaje_two.'">';
									$equivalente_unitaria_two		= $array_calificaciones->equivalente_unitaria_two;
									$cont .= '<input name="equivalente_unitaria_two" type="hidden" value ="'.$equivalente_unitaria_two.'">';

									$calificacion_excelente_desde	= $array_calificaciones->calificacion_exelente_desde;
									$cont .= '<input name="calificacion_excelente_desde" type="hidden" value ="'.$calificacion_excelente_desde.'">';
									$calificacion_excelente_hasta	= $array_calificaciones->calificacion_exelente_hasta;
									$cont .= '<input name="calificacion_excelente_hasta" type="hidden" value ="'.$calificacion_excelente_hasta.'">';
									$calificacion_bueno_desde		= $array_calificaciones->calificacion_bueno_desde;
									$cont .= '<input name="calificacion_bueno_desde" type="hidden" value ="'.$calificacion_bueno_desde.'">';
									$calificacion_bueno_hasta		= $array_calificaciones->calificacion_bueno_hasta;
									$cont .= '<input name="calificacion_bueno_hasta" type="hidden" value ="'.$calificacion_bueno_hasta.'">';
									$calificacion_aceptable_desde 	= $array_calificaciones->calificacion_aceptable_desde;
									$cont .= '<input name="calificacion_aceptable_desde" type="hidden" value ="'.$calificacion_aceptable_desde.'">';
									$calificacion_aceptable_hasta	= $array_calificaciones->calificacion_aceptable_hasta;
									$cont .= '<input name="calificacion_aceptable_hasta" type="hidden" value ="'.$calificacion_aceptable_hasta.'">';
									$calificacion_deficiente_desde	= $array_calificaciones->calificacion_deficiente_desde;
									$cont .= '<input name="calificacion_deficiente_desde" type="hidden" value ="'.$calificacion_deficiente_desde.'">';
									$calificacion_deficiente_hasta	= $array_calificaciones->calificacion_deficiente_hasta;
									$cont .= '<input name="calificacion_deficiente_hasta" type="hidden" value ="'.$calificacion_deficiente_hasta.'">';

									echo $cont;	 

									echo '<input name="id_evaluacion" type="hidden" value ="'.$id.'">';
								?>
										
							</div>
						</div>
					</div>
				</div>

				</div>
			</div>

		</form>