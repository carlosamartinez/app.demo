
    <nav class="navbar navbar-fixed-top navbar-dark bg-primary">
        <button class="navbar-toggler hidden-sm-up pull-right" type="button" data-toggle="collapse" data-target="#collapsingNavbar">
            ☰
        </button>
        <a class="navbar-brand" href="#">Pruebas</a>
        <div class="collapse navbar-toggleable-xs" id="collapsingNavbar">
            <ul class="nav navbar-nav pull-right">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home <span class="sr-only">Home</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#features">calificaciones</a>
                </li>
            </ul>
        </div>
    </nav>

	<div class="container-fluid" id="main">
	    <div class="row row-offcanvas row-offcanvas-left">       
	        <!--/col-->
	        <div class="col-md-12 col-lg-12 main">
	            <!--toggle sidebar button-->
	            <p class="hidden-md-up">
	                <button type="button" class="btn btn-primary-outline btn-sm" data-toggle="offcanvas"><i class="fa fa-chevron-left"></i> Menu</button>
	            </p>

	            <h3 class="display-1 hidden-xs-down">
	            Pruebas apuestas 8a
	            </h3>
	            <p class="lead">(Presentación de pruebas)</p>

	            <div class="alert alert-warning fade collapse" role="alert" id="myAlert">
	                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	                    <span aria-hidden="true">×</span>
	                    <span class="sr-only">Close</span>
	                </button>
	                <strong>Holy guacamole!</strong> It's free.. this is an example theme.
	            </div>


	            <div class="row btn_prueba">
	                <div class="col-md-12 col-sm-12">
	                    <div class="bd-example" data-example-id="">	                       
	                        <button type="button" class="btn btn-lg btn-primary col-md-8 col-md-offset-2 popover-dismiss" data-toggle="popover" title="" data-content="1.Por favor de click en el boton verde. 2. a continuacion seleccinone la evaluacion y ingrese su cedula 3. acontinuacion usted encontrara los campos de nombres apellidos etc por favor diligencielos !si no estan diligenciados¡">Click aqui</button>
	                        <div role="button" tabindex="0" aria-label="Download" class="Download remove" data-progressbar-label="Downloading item..."></div> <br>  
	                    </div>
	                </div>	               
	            </div>
	            <!--/row-->

	            

	            <div class="row">
	            	<div id="gif" class="col-md-12">
	            		
	            	</div>
	            </div>




	            <!-- frame evaluacion  -->
	            <div id="gridSystemModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridModalLabel" aria-hidden="true">
	              <div class="modal-dialog" role="document">
	                <div class="modal-content">
	                  <div class="modal-header">
	                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                    <h4 class="modal-title" id="gridModalLabel">Modal title</h4>
	                  </div>
	                  <div class="modal-body">
	                    <div class="container-fluid bd-example-row">
	                      <div class="row">
	                        <div class="col-md-4">.col-md-4</div>
	                        <div class="col-md-4 col-md-offset-4">.col-md-4 .col-md-offset-4</div>
	                      </div>
	                      <div class="row">
	                        <div class="col-md-3 col-md-offset-3">.col-md-3 .col-md-offset-3</div>
	                        <div class="col-md-2 col-md-offset-4">.col-md-2 .col-md-offset-4</div>
	                      </div>
	                      <div class="row">
	                        <div class="col-md-6 col-md-offset-3">.col-md-6 .col-md-offset-3</div>
	                      </div>
	                      <div class="row">
	                        <div class="col-sm-9">
	                          Level 1: .col-sm-9
	                          <div class="row">
	                            <div class="col-xs-8 col-sm-6">
	                              Level 2: .col-xs-8 .col-sm-6
	                            </div>
	                            <div class="col-xs-4 col-sm-6">
	                              Level 2: .col-xs-4 .col-sm-6
	                            </div>
	                          </div>
	                        </div>
	                      </div>
	                    </div>
	                  </div>
	                  <div class="modal-footer">
	                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	                    <button type="button" class="btn btn-primary">Save changes</button>
	                  </div>
	                </div>
	              </div>
	            </div>
	            <!-- frame evaluacion !!!!! -->

	        	<button id="ok" style="" data-toggle="modal" data-target="#gridSystemModal" >ok</button>
	

	            <!-- <div class="row placeholders">
	                <div class="col-xs-6 col-sm-3 placeholder text-center">
	                    <img src="//placehold.it/200/dddddd/fff?text=1" class="center-block img-responsive img-circle" alt="Generic placeholder thumbnail">
	                    <h4>Responsive</h4>
	                    <span class="text-muted">Device agnostic</span>
	                </div>
	                <div class="col-xs-6 col-sm-3 placeholder text-center">
	                    <img src="//placehold.it/200/e4e4e4/fff?text=2" class="center-block img-responsive img-circle" alt="Generic placeholder thumbnail">
	                    <h4>Frontend</h4>
	                    <span class="text-muted">UI / UX oriented</span>
	                </div>
	                <div class="col-xs-6 col-sm-3 placeholder text-center">
	                    <img src="//placehold.it/200/d6d6d6/fff?text=3" class="center-block img-responsive img-circle" alt="Generic placeholder thumbnail">
	                    <h4>HTML5</h4>
	                    <span class="text-muted">Standards-based</span>
	                </div>
	                <div class="col-xs-6 col-sm-3 placeholder text-center">
	                    <img src="//placehold.it/200/e0e0e0/fff?text=4" class="center-block img-responsive img-circle" alt="Generic placeholder thumbnail">
	                    <h4>Framework</h4>
	                    <span class="text-muted">CSS and JavaScript</span>
	                </div>
	            </div> -->
	        </div>
	        <!--/main col-->
	    </div>
	</div>