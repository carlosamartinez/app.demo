<div id="content" class="span10">
		
		<ul class="breadcrumb">
			<li>
				<i class="icon-paste color_fla"></i>
				<a>Evaluación</a> 
				<i class="icon-angle-right color_fla"></i>
			</li>
		</ul>
		
		<table class="span11">
			<td>
				<form >
					<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>	
					<button id="new_evaluation" type="button" class="btn btn-primary btn-lg">Crear una nueva evaluación</button>		
				</form>
				<form class='form-gla' action="http://<?php echo $_SERVER['HTTP_HOST']; ?>/prueba/index.php/evaluacion/Evaluacion/reporte_evaluacion" method='post' accept-charset='utf-8'>
				<input type='hidden' name='tipo' value='1'>
				<button type='submit' class='btn btn-primary btn-lg'>Reporte evaluación 1-30</button>
				</form>
				<form class='form-gla' action="http://<?php echo $_SERVER['HTTP_HOST']; ?>/prueba/index.php/evaluacion/Evaluacion/reporte_evaluacion" method='post' accept-charset='utf-8'>
				<input type='hidden' name='tipo' value='2'>
				<button type='submit' class='btn btn-primary btn-lg'>Reporte evaluación 31-60</button>
				</form>

				<!-- <form  class="form-gla" id="actualizar_evaluacion" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/evaluacion/Evaluacion/realizar_evaluacion" method="post" accept-charset="utf-8">
				<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
					<button id="realizar_evaluacion" type="submit" class="btn btn-primary">Realizar una evaluación</button>
				</form> -->

				<!-- <h1>Evaluación del empleado</h1> -->
			</td>
		</table>

		<div class="box span11">
			<div class="box-header">
				<h2>
					<i class="halflings-icon th"></i>
					<span class="break"></span>Evaluaciones existentes
				</h2>
				<div class="box-icon">
						<a href="#" class="btn-minimize"><i class="halflings-icon chevron-down"></i></a>
						<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
				</div>
			</div>
				<div class="box-content" style="display: none;">
					<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper" role="grid">
						<table class="table table-striped table-bordered bootstrap-datatable datatable dataTable" id="DataTables_Table_0" aria-describedby="DataTables_Table_0_info">
							<thead>
								<tr role="row">
									<th>Nombre de la evaluación</th>
									<th>Descripción de la evaluación</th>
									<th>Acciones</th>
								</tr>
							</thead>
								<tbody role="alert" aria-live="polite" aria-relevant="all">
									<?php  
										for ($i=0; $i < count($info_table) ; $i++) { 
					  	  					echo "<tr class='even'>";
					  	  					echo "<td>".$info_table[$i]->nombre_evaluacion."</td>";
					  	  					echo "<td>".$info_table[$i]->descripcion."</td>";

					  	  					/*echo "<td><form class='form-gla' action='http://".$_SERVER['HTTP_HOST']."/prueba/index.php/evaluacion/Evaluacion/actualizar_evaluacion' method='post' accept-charset='utf-8'><input type='hidden' name='cedula' value='".$cedula."'><input type='hidden' name='id' value='".$info_table[$i]->id_evaluacion."'><button type='submit' class='btn btn-success'><i class='halflings-icon zoom-in'></i></button></form>*/

					  	  					echo "<td><form class='form-gla' action='http://".$_SERVER['HTTP_HOST']."/prueba/index.php/evaluacion/Evaluacion/delete_evaluacion' method='post' accept-charset='utf-8'><input type='hidden' name='cedula' value='".$cedula."'><input type='hidden' name='id' value='".$info_table[$i]->id_evaluacion."'><button type='submit' class='btn btn-danger'><i class='halflings-icon trash'></i></button></form></td>";
					  	  					echo "</tr>";
											}
									?>
								</tbody>
						</table>
					</div>
				</div>
			</div>

			<div class="box span11"> <!-- Inicia tabla Ranking de proveedores -->
					<div class="box-header" data-original-title="">
						<h2>
							<i class="halflings-icon th"></i>
							<span class="break"></span>
							Calificación de evaluaciones
						</h2>
						<div class="box-icon">
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-down"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div>
					</div>

					<div class="box-content" style="display: none;">
						<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper" role="grid">
							
							<table class="table table-striped table-bordered bootstrap-datatable datatable dataTable" id="DataTables_Table_0" aria-describedby="DataTables_Table_0_info">
							  <thead>
								  <tr role="row">
								  	<th>Nombre</th>
								  	<th>Cédula</th>
								  	<th>Evaluación</th>
								  	<th>Descripción</th>
								  	<th>Puntaje</th>
								  	<th>Calificación</th>
								  	<th>Acciones</th>
								  </tr>
							  </thead>   
							  
						  	  <tbody role="alert" aria-live="polite" aria-relevant="all">
						  	  	<?php  
									for ($i=0; $i < count($info_table_c) ; $i++) { 
				  	  					echo "<tr class='even'>";
				  	  					echo "<td>".$info_table_c[$i]->nombreEvaluado."</td>";
				  	  					echo "<td>".$info_table_c[$i]->cedulaEvaluado."</td>";
				  	  					echo "<td>".$info_table_c[$i]->nombreEvaluacion."</td>";
				  	  					echo "<td>".$info_table_c[$i]->descripcion."</td>";
				  	  					echo "<td>".$info_table_c[$i]->puntaje."</td>";
				  	  					echo "<td>".$info_table_c[$i]->calificacion."</td>";

				  	  					echo "<td><form class='form-gla' action='http://".$_SERVER['HTTP_HOST']."/prueba/index.php/evaluacion/Evaluacion/ver_evaluacion' method='post' accept-charset='utf-8'><input type='hidden' name='cedula' value='".$cedula."'><input type='hidden' name='id' value='".$info_table_c[$i]->idCalificacion."'><button type='submit' class='btn btn-info'><i class='halflings-icon zoom-in'></i></button></form>
				  	  					<form class='form-gla' action='http://".$_SERVER['HTTP_HOST']."/prueba/index.php/evaluacion/Evaluacion/borrarEvaluado' method='post' accept-charset='utf-8'><input type='hidden' name='cedula' value='".$cedula."'><input type='hidden' name='id' value='".$info_table_c[$i]->idCalificacion."'><button type='submit' class='btn btn-danger'><i class='halflings-icon trash'></i></button></form></td>";
				  	  					echo "</tr>";
										}
								?>
							  </tbody>
							</table>
					 	</div>            
					</div>
					
				</div> <!-- Finaliza tabla Ranking de calificaciones -->

		<form class="form-horizontal" id="formulario_evaluacion" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/evaluacion/Evaluacion/visualizarBoton" enctype="multipart/form-data" method="post">
			<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
			<div class="box span12">
				<div class="control-group">
					<label class="control-label" for="focusedInput">¿Habilitar evaluaciones?</label>
					<div style="clear:both"></div>
					  <label class="radio">
						<div class="radio" id="uniform-optionsRadios1">
							<input type="radio" name="habilitarEvaluacion" id="habilitarEvaluacion" value="Si" <?php if($visualizarBoton=='Si') echo "checked=true"?>>
						</div>
						<span style="margin-left:20px;">Sí</span>
					  </label>
					  <label class="radio">
						<div class="radio" id="uniform-optionsRadios1">
							<input type="radio" name="habilitarEvaluacion" id="habilitarEvaluacion" value="No" <?php if($visualizarBoton=='No') echo "checked=true"?>>
						</div>
						<span style="margin-left:20px;">No</span>
					  </label>			
				</div>
				<button type="submit" class="btn btn-primary">Confirmar</button>
			</div>
		</form>
			
			<div id="dialog" title="Evaluación">			
				<div id="caract_pro">
	
					<div class="control-group">
						<label class="control-label" for="focusedInput">Nombre de la evaluación</label>
						<div class="controls">
						  <input class="input-xlarge focused" id="nombre_evaluacion" name="" onchange="myFunction()" type="text" value="">
						</div>
					</div>

					<div class="control-group">
						<label class="control-label" for="focusedInput">Cantidad de preguntas</label>
						<div class="controls">
						  <input id="cantidad_preguntas" name="" onchange="myFunction()" type="number" value="">
						</div>
					</div>

					<div class="control-group">
						<label class="control-label" for="focusedInput">Descripción de la evaluación</label>
						<div class="controls">
						  <textarea name="" id="descripcion" cols="30" onchange="myFunction()" rows="10"></textarea>
						</div>
					</div>					
				</div>
			</div>
		</div>
	<!-- end: Content -->