<form class="form-gla" id="actualizar_evaluacion" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/evaluacion/Evaluacion/testy" method="post" enctype="multipart/form-data" accept-charset="utf-8">

	<div id="content" class="span10">

		<ul class="breadcrumb">
			<li>
				<i class="icon-paste color_fla"></i>
					<a>Evaluación</a> 
				<i class="icon-angle-right color_fla"></i>

			<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'> 
			</li>
		</ul>

		<div class="box span11">
			<div class="box-header">
				<h2>
					<i class="halflings-icon th"></i>
					<span class="break"></span>Actualizar evaluación
				</h2>
				<div class="box-icon">
						<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
						<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
				</div>
			</div>
						
			<div class="box-content" style="display: block;">
				<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper" role="grid">
					<div class="row-fluid">
						<div class="span12">

						<?php 
						
							$nombre_evaluacion = $datos_editar['0']->nombre_evaluacion;
							$json_cuestionario = $datos_editar['0']->json_cuestionario;
							$descripcion 	   = $datos_editar['0']->descripcion;
							$calificaciones	   = $datos_editar['0']->calificaciones;
							$id  			   = $datos_editar['0']->id_evaluacion;	

							$array_preguntas 		= json_decode($json_cuestionario);
							$array_calificaciones 	= json_decode($calificaciones); 

							$desde_one 										 = $array_calificaciones->desde_one;
							$hasta_one 										 = $array_calificaciones->hasta_one;
							$equivalente_porcentaje_one 	 = $array_calificaciones->equivalente_porcentaje_one;
							$equivalente_unitaria_one 		 = $array_calificaciones->equivalente_unitaria_one;
							$desde_two 										 = $array_calificaciones->desde_two;
							$hasta_two 										 = $array_calificaciones->hasta_two;
							$equivalente_porcentaje_two 	 = $array_calificaciones->equivalente_porcentaje_two;
							$equivalente_unitaria_two 		 = $array_calificaciones->equivalente_unitaria_two;
							$calificacion_excelente_desde  = $array_calificaciones->calificacion_exelente_desde;
							$calificacion_excelente_hasta  = $array_calificaciones->calificacion_exelente_hasta;
							$calificacion_bueno_desde 		 = $array_calificaciones->calificacion_bueno_desde;
							$calificacion_bueno_hasta 		 = $array_calificaciones->calificacion_bueno_hasta;
							$calificacion_aceptable_desde  = $array_calificaciones->calificacion_aceptable_desde;
							$calificacion_aceptable_hasta  = $array_calificaciones->calificacion_aceptable_hasta;
							$calificacion_deficiente_desde = $array_calificaciones->calificacion_deficiente_desde;
							$calificacion_deficiente_hasta = $array_calificaciones->calificacion_deficiente_hasta;
 
							$cantidad_preguntas;
							$cont 							= '';
							$posibles_opciones_resp 		= '';
							$array_posibles_preguntas 		= '';
							$array_respuestas_verdaderas 	= '';

							$cont .= '<div class="control-group quest" style="margin-top: 2%;border-style: groove;">';
							$cont .= 	'<label class="control-label" for="focusedInput">Nombre de la evaluación.</label>';
							$cont .= 		'<div class="controls">';
							$cont .=			'<input class="input-xlarge focused" id="nombre_evaluacion" name="nombre_evaluacion" type="text" placeholder="Nombre" value="'.$nombre_evaluacion.'">';
							$cont .=		'</div>';
							$cont .= '</div>';

							$cont .= '<div class="control-group" quest" style="margin-top: 2%;border-style: groove;>';
							$cont .=	'<label class="control-label" for="focusedInput">Descripción de la evaluación.</label>';
							$cont .=		'<div class="controls">';
							$cont .=	 		'<input class="input-xlarge focused" id="descripcion" name="descripcion" placeholder="Descripción" value="'.$descripcion.'">';
							$cont .=		'</div>';
							$cont .='</div>';

						$contador = 0;
						foreach($array_preguntas as $array_preguntas){

        					$pregunta 					 = $array_preguntas->pregunta;           					        					   
        					$opciones_respuestas	     = $array_preguntas->opciones_respuestas;
        					$respuestas_verdaderas  	 = $array_preguntas->respuestas_verdaderas;

        					$opciones_respuestas_verd 	 = $opciones_respuestas;
        					$opciones 					 = str_replace("_", ",", $opciones_respuestas_verd) ;
        					$resp_verd 					 = str_replace("_", ",", $respuestas_verdaderas);
        					   
        					$resultado 					 = count($pregunta);//Trae la cantidad de preguntas
        					$posibles_opciones 			 = count($opciones);//saber cuantas preguntas hay
        					$options_posibles			 = substr($opciones,10);//estas son las posibles respuestas 
        					$respuestas_v 	 			 = substr($resp_verd, 10);//estas son las resppuestas verdaderas

        					$array_posibles_preguntas 	 = explode(",", $options_posibles);//Estas son las posibles respuestas verdaderas pero estan en arreglos
        					$array_respuestas_verdaderas = explode(",", $respuestas_v);//Estas son las respuestas y estan en arreglos

        					$lz 					 	 = count($array_posibles_preguntas);//cuenta cuantas posibles respuestas hay
        					$resp_verdad 				 = count($array_respuestas_verdaderas);

        					
        					for ($i=0; $i < $resultado; $i++) { 
        						$cantidad_preguntas += $resultado;//Cuenta cuantas preguntas hay, sirve como autoincrementable
        					} 
        					
        					$abecedario = ["A: ", "B: ", "C: ", "D: ", "E: "];

							$cont .='<div class="control-group" quest" style="margin-top: 2%;border-style: groove;>';	 	

        					for($i=1; $i <= $resultado ; $i++){ 
								
							$cont .='<label id="lbl_pregunta['.$resultado.']" class="control-label" for="focusedInput"> '.$cantidad_preguntas." pregunta: ".'</label>';
							$cont .='<input class="input-xlarge focused" id="campo_pregunta['.$i.']" name="json_cuestionario[]"  type="text"  value="'.$pregunta.'">';
							$cont .='<label class="control-label" for="focusedInput"> Opciones de respuesta: '.$cantidad_preguntas[$i].'</label>';
							
								for ($p=0; $p < $lz; $p++) { 
									$cont .=$abecedario[$p].'<input class="input-xlarge focused" id="posibles_resp['.$p.']" name="json_cuestionario[]" type="text" value="'.$array_posibles_preguntas[$p].'"> &nbsp';	
				
								}
								
									
								$cont .='<label class="input-xlarge focused" for="focusedInput"> '."Respuestas verdaderas: ".'</label>';

								for ($l=0; $l <$resp_verdad ; $l++) { 
									$cont .='<input class="form-control" type="text" id="resp_verdaderas['.$l.']" name="json_cuestionario[]" value="'.$array_respuestas_verdaderas[$l].'"> &nbsp';											

								}
								$contador++;
						
							}
							$cont .='</div>'; //div class 
        				}//fin foreach


        				

        					/*-----------------------------------------------------------------------------------------------------------------------------*/

        					 $cont .='<h1><b>Ponderación</b></h1>'; 
        					 $cont .= 	'<label> En total son '.$cantidad_preguntas." preguntas y equivalen a 100 puntos".'</label>';
        					 $cont .=		'<div id="ponderacion" class="content">';
							 $cont .=			'<div class="span12">';
							 $cont .= 				'<div class="control-group quest" style="margin-top: 1%;border-style: groove;">';
        					 $cont .= 					'<label>De la pregunta: </label>';
        					 $cont .=  					'<input id="desde_one" name="calificaciones[]" class="ponderacion_val operation1" type="text" value="'.$desde_one.'">  &nbsp';
        					 $cont .=					'<label class="control-label" for="focusedInput">Hasta la pregunta: </label>';
        					 $cont .=					'<input id="hasta_one" name="calificaciones[]" class="ponderacion_val operation1" type="text" value="'.$hasta_one.'"> &nbsp';
							 $cont .=					'<label class="control-label" for="focusedInput">Es equivale a: </label>';
							 $cont .=				'<div class="controls">';
							 $cont .=					'<input id="equivalente_porcentaje_one"  name="calificaciones[]" class="ponderacion_val operation1" type="text" onchange="btn_save_view()" value="'.$equivalente_porcentaje_one.'">';
							 $cont .= 					'<label class="control-label" for="focusedInput">Pregunta unitaria: </label>';
							 $cont .=					'<input id="equivalente_porcentaje_one" name="calificaciones[]" class="input-xlarge focused" type="text" value="'.$equivalente_unitaria_one.'"disabled>'; 
							 $cont .=			'</div>'; //fin control-group
							 $cont .=		'</div>'; //fin control-group
							 $cont .='</div>'; //fin control-group

        					 $cont .='<div id="ponderacion" class="content">';
							 $cont .='<div class="span12">';
							 $cont .= 	'<div class="control-group quest" style="margin-top: 1%;border-style: groove;">';
							 $cont .= 		'<label class="control-label" for="focusedInput">De la pregunta: </label>'; 
        					 $cont .=  		'<input id="$desde_two" name="calificaciones[]" class="input-xlarge focused" type="text" value="'.$desde_two.'">  &nbsp';
							 $cont .= 		'<label class="control-label" for="focusedInput">De la pregunta: </label>';
        					 $cont .=  		'<input id="hasta_two" name="calificaciones[]" class="input-xlarge focused" type="text" value="'.$hasta_two.'">  &nbsp';
							 $cont .=				'<div class="controls">';
							 $cont .=		'<label class="control-label" for="focusedInput">Es equivale a: </label>';
        					 $cont .=		'<input id="equivalente_porcentaje_two" name="calificaciones[]" class="input-xlarge focused" type="text" value="'.$equivalente_porcentaje_two.'">';
							 $cont .= 		'<label class="control-label"  for="focusedInput">Pregunta unitaria: </label>';
							 $cont .=		'<input id="$equivalente_unitaria_two" name="calificaciones[]" class="input-xlarge focused" type="text" value="'.$equivalente_unitaria_two.'" disabled>';
							 $cont .=	'</div>'; //fin control-group
							 $cont .='</div>'; //fin control-group
							 $cont .='</div>'; //fin control-group

							 $cont .= '<h1><b>Calificación</b></h1>';
							 $cont .='<div class="span5">';
							 $cont .= 	'<div class="control-group quest" style="margin-top: 1%;border-style: groove;">';
							 $cont .=		'<h2><b>Excelente</b></h2>';
							 $cont .=			'<label class="control-label" for="focusedInput">Desde: </label>'; 
							 $cont .=			 '<input id="desde_excelente"  name="calificaciones[]" class="input-xlarge focused" type="text" value="'.$calificacion_excelente_desde.'">';
							 $cont .=				'<div class="controls">';							  
							 $cont .=					'<label class="control-label" for="focusedInput">Hasta: </label>';
							 $cont .=					'<input id="hasta_excelente" name="calificaciones[]" class="input-xlarge focused" type="text"  value="'.$calificacion_excelente_hasta.'">';
							 $cont .=				'</div>';//controls							 
							 $cont .=	'</div>'; //fin control-group
							 $cont .='</div>'; //fin control-group

							 $cont .='<div class="span5">';
							 $cont .= 	'<div class="control-group quest" style="margin-top: 1%;border-style: groove;">';
							 $cont .=		'<h2><b>Bueno</b></h2>';
							 $cont .=			'<label class="control-label" for="focusedInput">Desde: </label>'; 
							 $cont .=			'<input id="desde_bueno" name="calificaciones[]" class="input-xlarge focused" type="text" value="'.$calificacion_bueno_desde.'">';
							 $cont .=				'<div class="controls">';   
							 $cont .=					'<label class="control-label" for="focusedInput">Hasta: </label>';
							 $cont .=					'<input id="hasta_bueno" name="calificaciones[]" class="input-xlarge focused" type="text" value="'.$calificacion_bueno_hasta.'">';
							 $cont .=				'</div>';//controls							 
							 $cont .=	'</div>'; //fin control-group
							 $cont .='</div>'; //fin control-group

							 $cont .='<div class="span5">';
							 $cont .=	'<div class="control-group quest" style="margin-top: 1%;border-style: groove;">';
							 $cont .=		'<h2><b>Aceptable</b></h2>';
							 $cont .=			'<label class="control-label" for="focusedInput">Desde: </label>';
							 $cont .=			'<input id="desde_aceptable" name="calificaciones[]" class="input-xlarge focused" type="text" value="'.$calificacion_aceptable_desde.'">';
							 $cont .=				'<div class="controls">';  
							 $cont .=					'<label class="control-label" for="focusedInput">Hasta: </label>';
							 $cont .=					'<input id="hasta_aceptable" name="calificaciones[]" class="input-xlarge focused" type="text" value="'.$calificacion_aceptable_hasta.'">';
							 $cont .=				'</div>';//controls							 
							 $cont .=	'</div>'; //fin control-group
							 $cont .='</div>'; //fin control-group

							 $cont .='<div class="span5">';
							 $cont .=	'<div class="control-group quest" style="margin-top: 1%;border-style: groove;">';
							 $cont .=		'<h2><b>Deficiente</b></h2>';
							 $cont .=			'<label class="control-label" for="focusedInput">Desde: </label>';
							 $cont .=			'<input id="desde_deficiente" name="calificaciones[]" class="input-xlarge focused" type="text" value="'.$calificacion_deficiente_desde.'">';
							 $cont .=				'<div class="controls">';  
							 $cont .=					'<label class="control-label" for="focusedInput">Hasta: </label>';
							 $cont .=					'<input id="hasta_deficiente" name="calificaciones[]" class="input-xlarge focused" type="text" value="'.$calificacion_deficiente_hasta.'">';
							 $cont .=				'</div>';//controls							 
							 $cont .=	'</div>'; //fin control-group
							 $cont .='</div>'; //fin
							 $cont .='</div>'; //finb 
							 $cont .='</div>'; //fin			 

							echo $cont;	 

							echo '<input name="id_evaluacion" type="hidden" value ="'.$id.'">';
						?>	
		
				 <div>
					<div class="controls group">
								<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'> 
						<button id="guardar_evaluacion" class="btn btn-primary" type="submit">Guardar evaluación</button>
					</div>
				 </div>
				</div>	 
			</div>          
		</div>				
	 </div>
		</div>
	</div>
</form>	

