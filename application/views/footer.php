	<!-- <footer> -->
		<!-- start: JavaScript-->
		<script src="theme/js/jquery-1.9.1.min.js"></script>
		<script src="theme/js/jquery-migrate-1.0.0.min.js"></script>
		<script src="theme/js/jquery-ui-1.10.0.custom.min.js"></script>
		<script src="theme/js/jquery.ui.touch-punch.js"></script>
		<script src="theme/js/modernizr.js"></script>
		<script src="theme/js/bootstrap.min.js"></script>
		<script src="theme/js/jquery.cookie.js"></script>
		<script src='theme/js/fullcalendar.min.js'></script>
		<script src='theme/js/jquery.dataTables.min.js'></script>
		<script src="theme/js/excanvas.js"></script>
		<script src="theme/js/jquery.flot.js"></script>
		<script src="theme/js/jquery.flot.pie.js"></script>
		<script src="theme/js/jquery.flot.stack.js"></script>
		<script src="theme/js/jquery.flot.resize.min.js"></script>
		<script src="theme/js/jquery.chosen.min.js"></script>
		<script src="theme/js/jquery.uniform.min.js"></script>
		<script src="theme/js/jquery.cleditor.min.js"></script>
		<script src="theme/js/jquery.noty.js"></script>
		<script src="theme/js/jquery.elfinder.min.js"></script>
		<script src="theme/js/jquery.raty.min.js"></script>
		<script src="theme/js/jquery.iphone.toggle.js"></script>
		<script src="theme/js/jquery.uploadify-3.1.min.js"></script>
		<script src="theme/js/jquery.gritter.min.js"></script>
		<script src="theme/js/jquery.imagesloaded.js"></script>
		<script src="theme/js/jquery.masonry.min.js"></script>
		<script src="theme/js/jquery.knob.modified.js"></script>
		<script src="theme/js/jquery.sparkline.min.js"></script>
		<script src="theme/js/counter.js"></script>
		<script src="theme/js/retina.js"></script>
		<script src="theme/js/custom.js"></script>
	<!-- </footer> -->

</html>