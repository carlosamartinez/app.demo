<!DOCTYPE html>
<html lang="en">
<head>
	
	<!-- start: Meta -->
	<meta charset="utf-8">
	<title>Digitalización</title>
	<meta name="description" content="Bootstrap Metro Dashboard">
	<meta name="author" content="Dennis Ji">
	<meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	<!-- end: Meta -->
	
	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->
	
	<!-- start: CSS -->
	<link id="bootstrap-style" href="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/css/bootstrap.min.css" rel="stylesheet">
	<link href="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style" href="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/css/style-responsive.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
	<!-- end: CSS -->
	
	<link rel="shortcut icon" href="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/img/favicon.ico">
	<!-- end: Favicon -->
</head>

<body>
	<!-- start: Header -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>

				<a class="brand" href="index.html">
					<img src="../../../theme/img/ochoa_logo_blanco.png" class="img-responsive" alt="Responsive image">
					<span class="style_title_nav">Digitalización de archivos</span>
				</a>
								
				<!-- start: Header Menu -->
				<div class="nav-no-collapse header-nav">
					<ul class="nav pull-right">
						<h3 style="text-align: center;">version 1.1</h3>
						<!-- start: User Dropdown -->
						<li class="dropdown">
							<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="halflings-icon white user"></i> 
								<input id="cedula" type="hidden" value="<?php echo $cedula; ?>">
									<?php 
										echo $nombres;
									?>
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li class="dropdown-menu-title">
								</li>
								<form class="form-gla" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Welcome/cerrar_sesion" method="post" accept-charset="utf-8">
									<li class="aline-menu">
										<i class="halflings-icon remove color_ico"></i>
										<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
										<input type="submit" value="Cerrar" class="btn-menu-lg">
									</li>	
								</form>	
								<!-- <li><a href="http://localhost/prueba/index.php/Welcome/form_profile"><i class="halflings-icon user"></i> Perfil</a></li> -->
								<!-- <li><a href="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/"><i class="halflings-icon off"></i> cerrar sesion</a></li> -->
							</ul>
						</li>
						<!-- end: User Dropdown -->
					</ul>
				</div>
				<!-- end: Header Menu -->
				
			</div>
		</div>
	</div>

	<!-- start: Header -->
		<div class="container-fluid-full">
		<div class="row-fluid">
				
				<!-- start: Main Menu -->
				<div id="sidebar-left" class="span2">
					<div class="nav-collapse sidebar-nav">
						<ul class="nav nav-tabs nav-stacked main-menu">
							
							<form class="form-gla" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Welcome/re_inicio" method="post" accept-charset="utf-8">
								<li class="aline-menu">
									<i class="icon-home color_ico"></i>
									<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
									<input type="submit" value="Inicio" class="btn-menu-lg">
								</li>	
							</form>

							<li class="aline-menu">
								<a class="dropmenu" href="#"><i class="icon-folder-close-alt"></i><span class="hidden-tablet btn-menu-lg">Digitalización</span></a>
								<ul style="display: none;">
									<?php if($rol == 'Administrador' or $rol == 'Registro' or $rol == 'Calificador 1-etapa y 4-etapa' or $rol == 'Calificador 2-etapa' or $rol == 'Calificador 3-etapa' or $rol == 'Pagador' or $rol == 'Contabilidad' or $rol == 'Jefe recursos'){ ?>
										<form class="form-gla" id="digitalizacion_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Digital/index" method="post" accept-charset="utf-8">
											<li>
												<a class="submenu subMen" id="digitalizacion_submit"><i class="icon-check"></i>
													<span class="hidden-tablet"> Digitalización</span>
												</a>
											</li>
											<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
											<input type="hidden" name="Digitalización" value="Digitalización" class="btn-menu-lg">
										</form>	
									<?php } ?>

									<?php if($rol == 'Administrador' or $rol == 'Calificador 3-etapa'){ ?>
										<form class="form-gla" id="formato_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Digital/index" method="post" accept-charset="utf-8">
											<li>
												<a class="submenu subMen" id="formato_submit"><i class="icon-check"></i>
													<span class="hidden-tablet"> Creación formatos</span>
												</a>
											</li>
											<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
											<input type="hidden" name="Creación_formatos" value="Creación_formatos" class="btn-menu-lg">
										</form>	
									<?php } ?>
								</ul>
							</li>

							<?php if($rol == 'Administrador' or $rol == 'Registro' or $rol == 'Calificador 1-etapa y 4-etapa' or $rol == 'Recursos humanos'){ ?>
							<form class="form-gla" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Conceptos/index" method="post" accept-charset="utf-8">
								<li class="aline-menu">
									<i class="icon-pencil color_ico"></i>
									<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
									<input type="submit" value="Conceptos" class="btn-menu-lg">
								</li>	
							</form>
							<?php } ?>

							<li class="aline-menu">
								<a class="dropmenu" href="#"><i class="icon-folder-close-alt"></i><span class="hidden-tablet btn-menu-lg">Aprobaciones</span></a>
								<ul style="display: none;">
									<?php if($rol == 'Administrador' or $rol == 'Calificador 1-etapa y 4-etapa'){ ?>
										<form class="form-gla" id="compras_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Aprobaciones/index" method="post" accept-charset="utf-8">
											<li>
												<a class="submenu subMen" id="compras_submit"><i class="icon-check"></i>
													<span class="hidden-tablet"> Compras</span>
												</a>
											</li>
											<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
											<input type="hidden" name="etapa-1-4" value="Compras" class="btn-menu-lg">
										</form>	
									<?php } ?>	

									<?php if($rol == 'Administrador' or $rol == 'Calificador 2-etapa' or $rol == 'Jefe recursos'){ ?>
										<form class="form-gla" id="director_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Aprobaciones/index" method="post" accept-charset="utf-8">
											<li>
												<a class="submenu subMen" id="director_submit"><i class="icon-signin"></i>
													<span class="hidden-tablet"> Director de area</span>
												</a>
											</li>
											<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
											<input type="hidden" name="etapa-2" value="Director de área" class="btn-menu-lg">
										</form>	
									<?php } ?>
									
									<?php if($rol == 'Administrador' or $rol == 'Calificador 3-etapa'){ ?>
							    	<form class="form-gla" id="control_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Aprobaciones/index" method="post" accept-charset="utf-8">
										<li>
											<a class="submenu subMen" id="control_submit"><i class="icon-download-alt"></i>
												<span class="hidden-tablet"> Control interno</span>
											</a>
										</li>
										<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
										<input type="hidden" name="etapa-3" value="Control interno" class="btn-menu-lg">
									</form>	
									<?php } ?>
									
									<?php if($rol == 'Administrador' or $rol == 'Contabilidad'){ ?>
							    	<form class="form-gla" id="contabilidad_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Aprobaciones/view_contabilidad" method="post" accept-charset="utf-8">
										<li>
											<a class="submenu subMen" id="contabilidad_submit"><i class="icon-tasks"></i>
												<span class="hidden-tablet"> Contabilidad</span>
											</a>
										</li>
										<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
										<input type="hidden" value="Contabilidad" class="btn-menu-lg">
									</form>	
									<?php } ?>
									
									<?php if($rol == 'Administrador' or $rol == 'Pagador'){ ?>
									<form class="form-gla" id="pagos_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Aprobaciones/view_pagos" method="post" accept-charset="utf-8">
										<li>
											<a class="submenu subMen" id="pagos_submit"><i class="icon-money"></i>
												<span class="hidden-tablet"> Pagos</span>
											</a>
										</li>
										<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
										<input type="hidden" value="Pagos" class="btn-menu-lg">
									</form>	
									<?php } ?>

									<?php if($rol == 'Administrador' or $rol == 'Calificador 3-etapa'){ ?>
                                    <form class="form-gla" id="reportes_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Aprobaciones/reportes" method="post" accept-charset="utf-8">
                                        <li>
                                            <a class="submenu subMen" id="reportes_submit"><i class="icon-check"></i>
                                                <span class="hidden-tablet"> Reportes </span>
                                            </a>
                                        </li>
                                        <input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
                                        <input type="hidden" name="reportes" value="Reportes" class="btn-menu-lg">
                                    </form>    
                                    <?php } ?>

								</ul>	
							</li>
							
					
							<li class="aline-menu">
								<a class="dropmenu" href="#"><i class="icon-group color_ico"></i><span class="hidden-tablet btn-menu-lg">Empleados</span></a>
								<ul style="display: none;">
									<?php if($rol == 'Administrador' or $rol == 'Jefe recursos' or $rol == 'Recursos humanos' or $rol == 'Formatos'){ ?>
										<form class="form-gla" id="empleados_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/empleados/Empleados/index" method="post" accept-charset="utf-8">
											<li>
												<a class="submenu subMen" id="empleados_submit"><i class="icon-group color_ico"></i>
													<span class="hidden-tablet"> Registros</span>
												</a>
											</li>	
											<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
										</form>
									<?php } ?>	

									<?php if($rol == 'Administrador' or $rol == 'Jefe recursos'){ ?>
										<form class="form-gla" id="reportes_empleados_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/empleados/Empleados/reportes" method="post" accept-charset="utf-8">
											<li>
												<a class="submenu subMen" id="reportes_empleados_submit"><i class="icon-check"></i>
													<span class="hidden-tablet"> Reportes</span>
												</a>
											</li>
											<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>										
										</form>	
									<?php } ?>
								</ul>
							</li>

							

							<?php if($rol == 'Administrador'){ ?>
							<form class="form-gla" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Usuarios/index" method="post" accept-charset="utf-8">
								<li class="aline-menu">
									<i class="icon-user-md color_ico"></i>
									<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
									<input type="submit" value="Usuarios" class="btn-menu-lg">
								</li>	
							</form>
							<?php } ?>


							<?php if($rol == 'Administrador' or $rol == 'Jefe recursos'){ ?>
							<form class="form-gla" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/evaluacion/Evaluacion/index" method="post" accept-charset="utf-8">
								<li class="aline-menu">
									<i class="icon-user-md color_ico"></i>
									<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
									<input type="submit" value="Evaluacion" class="btn-menu-lg">
								</li>	
							</form>
							<?php } ?>

							<li class="aline-menu">
								<a class="dropmenu" href="#"><i class="icon-list-alt"></i><span class="hidden-tablet btn-menu-lg">Formatos</span></a>
								<ul style="display: none;">
									<?php if($rol == 'Administrador' or $rol == 'Formatos'){ ?>
										<form class="form-gla" id="datosFormatos_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/formatos/formatos/index" method="post" accept-charset="utf-8">
											<li>
												<a class="submenu subMen" id="datosFormatos_submit"><i class="icon-pencil"></i>
													<span class="hidden-tablet"> Ingresar datos</span>
												</a>
											</li>
											<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
											<input type="hidden" name="Ingresar_datos" value="Ingresar_datos" class="btn-menu-lg">
										</form>	
									<?php } ?>

									<?php if($rol == 'Administrador' or $rol == 'Formatos'){ ?>
										<form class="form-gla" id="gestionFormatos_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/formatos/formatos/index" method="post" accept-charset="utf-8">
											<li>
												<a class="submenu subMen" id="gestionFormatos_submit"><i class="icon-list-alt"></i>
													<span class="hidden-tablet"> Generar formatos</span>
												</a>
											</li>
											<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
											<input type="hidden" name="Gestión_formatos" value="Gestión_formatos" class="btn-menu-lg">
										</form>	
									<?php } ?>
								</ul>
							</li>	

							<?php if($rol == 'Administrador' or $rol == 'Jefe recursos'){ ?>
							<form class="form-gla" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/cuestionario/cuestionario/registros" method="post" accept-charset="utf-8">
								<li class="aline-menu">
									<i class="icon-user-md color_ico"></i>
									<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
									<input type="submit" value="Cuestionario" class="btn-menu-lg">
								</li>	
							</form>
							<?php } ?>								

						</ul>
					</div>
				</div>
			<!-- end: Main Menu -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<div id="content" class="span10">
				<ul class="breadcrumb">
					<li>
						<i class="icon-folder-open color_fla"></i>
						<a>Formatos</a> 
						<i class="icon-angle-right color_fla"></i>
					</li>
				</ul>

				<?php if($mensaje == 'exito'){ ?>
					<div class="alert alert-success fade in">
						<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
						<strong>Info! </strong>Registro de la historia laboral exitosa.
					</div>
				<?php } ?>

				<?php if($mensaje == 'actualizado'){ ?>
					<div class="alert alert-success fade in">
						<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
						<strong>Info! </strong>Actualización de la historia laboral exitosa.
					</div>
				<?php } ?>

				<?php if($mensaje == 'cedulaExistente'){ ?>
					<div class="alert alert-warning fade in">
						<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
						<strong>Ojo! </strong>No se pudo hacer el registro porque la cédula ya existe.
					</div>
				<?php } ?>

				<?php if($mensaje == 'eliminado'){ ?>
					<div class="alert alert-success fade in">
						<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
						<strong>Info! </strong>Registro eliminado exitosamente.
					</div>
				<?php } ?>
				
				<div class="row-fluid">	
					<div class="box span12">
						<div class="box-header" data-original-title="">
							<h2><i class="halflings-icon edit"></i><span class="break"></span>Ingresar datos historial laboral</h2>
							<div class="box-icon">
								<a href="#" class="btn-minimize"><i class="halflings-icon chevron-down"></i></a>
								<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
							</div>
						</div>

						<div class="box-content" style="display: none;">
						
						<form class="form-horizontal" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/formatos/formatos/crearFormato" enctype="multipart/form-data" method="post">
							<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
							<fieldset>
								<div class="row-fluid">
							        <div class="box-content" style="display: block;">
										<div class="span12">
											<div class="span6">
												<div class="control-group">
													<label class="control-label" for="focusedInput">Nombre</label>
													<div class="controls">
											  			<input class="input-xlarge focused" name="nombre" type="text" placeholder="Ingrese nombre" required>
														<span class='obligatorio'>*</span>
													</div>
										  		</div>

										  		<div class="control-group">
													<label class="control-label" for="focusedInput">Cédula</label>
													<div class="controls">
											  			<input class="input-xlarge focused" name="cedula_laboral" type="number" placeholder="Ingrese cédula" required>
														<span class='obligatorio'>*</span>
													</div>
										  		</div>

										  		<div class="control-group">
													<label class="control-label" for="focusedInput">Código</label>
													<div class="controls">
											  			<input class="input-xlarge focused" name="codigo" type="text" placeholder="Ingrese código" required>
														<span class='obligatorio'>*</span>
													</div>
										  		</div>

										  		<div class="control-group">
													<label class="control-label" for="focusedInput">Teléfono</label>
													<div class="controls">
											  			<input class="input-xlarge focused" name="telefono_laboral" type="number" placeholder="Ingrese teléfono" required>
														<span class='obligatorio'>*</span>
													</div>
										  		</div>

										  		<div class="control-group">
													<label class="control-label" for="focusedInput">Cargo</label>
													<div class="controls">
											  			<input class="input-xlarge focused" name="cargo" type="text" placeholder="Ingrese cargo" required>
														<span class='obligatorio'>*</span>
													</div>
										  		</div>

										  		<div class="control-group">
													<label class="control-label" for="focusedInput">Área</label>
													<div class="controls">
											  			<input class="input-xlarge focused" name="area" type="text" placeholder="Ingrese área" required>
														<span class='obligatorio'>*</span>
													</div>
										  		</div>

										  		<div class="control-group ">
						            				<label class="control-label" for="focusedInput">Fecha de ingreso</label>
						            				<div class="controls">
						            					<input id="fecha_ingreso" class="input-xlarge focused" name="fecha_ingreso" type="text" placeholder="Ingrese fecha de ingreso" required>
						            					<span class="obligatorio">*</span>
						            				</div>
						            		    </div>

										  		<div class="control-group ">
						            				<label class="control-label" for="focusedInput">Fecha de retiro</label>
						            				<div class="controls">
						            					<input id="fecha_retiro" class="input-xlarge focused" name="fecha_retiro" type="text" placeholder="Ingrese fecha de retiro" required>
						            					<span class="obligatorio">*</span>
						            				</div>
						            		    </div>

										  		<div class="control-group">
													<label class="control-label" for="focusedInput">Tipo de contrato</label>
													<div class="controls">
											  			<input class="input-xlarge focused" name="tipo_contrato" type="text" placeholder="Ingrese tipo de contrato" required>
														<span class='obligatorio'>*</span>
													</div>
										  		</div>

										  		<div class="control-group">
													<label class="control-label" for="focusedInput">Comparendos educativos</label>
													<div class="controls">
											  			<input class="input-xlarge focused" name="comparendos_educativos" type="number" placeholder="Ingrese comparendos educativos" required>
														<span class='obligatorio'>*</span>
													</div>
										  		</div>

											</div>

											<div class="span6">

												<div class="control-group">
													<label class="control-label" for="focusedInput">Llamados de atención</label>
													<div class="controls">
											  			<input class="input-xlarge focused" name="llamados_atencion" type="number" placeholder="Ingrese llamados de atención" required>
														<span class='obligatorio'>*</span>
													</div>
										  		</div>

												<div class="control-group">
													<label class="control-label" for="focusedInput">Suspensiones</label>
													<div class="controls">
											  			<input class="input-xlarge focused" name="suspensiones" type="number" placeholder="Ingrese suspenciones" required>
														<span class='obligatorio'>*</span>
													</div>
										  		</div>

										  		<div class="control-group">
													<label class="control-label" for="focusedInput">Días ausentismos</label>
													<div class="controls">
											  			<input class="input-xlarge focused" name="dias_ausentismos" type="number" placeholder="Ingrese días de ausentismo" required>
														<span class='obligatorio'>*</span>
													</div>
										  		</div>

										  		<div class="control-group">
													<label class="control-label" for="focusedInput">Días laborados</label>
													<div class="controls">
											  			<input class="input-xlarge focused" name="dias_laborados" type="number" placeholder="Ingrese días laborados" required>
														<span class='obligatorio'>*</span>
													</div>
										  		</div>

										  		<div class="control-group">
													<label class="control-label" for="focusedInput">EPS</label>
													<div class="controls">
													  <select class="input-xlarge focused" id="eps" name="eps" style="color:black;" required>
													  	<option></option>
													  	<option>NUEVA EMPRESA PROMOTORA DE SALUD S.A.</option>
													  	<option>SURA EPS</option>
													  	<option>SALUD TOTAL</option>
													  	<option>CAFESALUD ENTIDAD PROMOTORA DE SALUD S.A.</option>
													  	<option>CONSORCIO SAYP 2011</option>
													  	<option >COOMEVA ENTIDAD PROMOTORA DE SALUD</option>
													  	<option>ENTIDAD PROMOTORA DE SALUD SERVICIO OCCIDENTAL</option>
													  	<option>ENTIDAD PROMOTORA DE SALUD SANITAS LTDA</option>
													  	<option>ASOCIACION MUTUAL LA ESPERANZA ASMET SALUD ES</option>
													  	<option>FOSYGA</option>
													  </select>
													  <span class="obligatorio">*</span>
													</div>
									              </div>

										  		<div class="control-group ">
													<label class="control-label" for="focusedInput">AFP</label>
													<div class="controls">
													  <select class="input-xlarge focused" id="afp" name="afp" style="color:black;" required>
													  	<option></option>
													  	<option>PROTECCION</option>
													  	<option>PORVENIR S.A</option>
													  	<option>ADMINISTRADORA COLOMBIANA DE PENSIONES COLPEN</option>
													  	<option>COLFONDOS</option>
														<option>NO APLICA</option>
													  </select>
													  <span class="obligatorio">*</span>
													</div>
									              </div>

										  		<div class="control-group ">
													<label class="control-label" for="focusedInput">Cesantias</label>
													<div class="controls">
													  <select class="input-xlarge focused" id="cesantias" name="cesantias" style="color:black;" required>
														<option></option>
														<option>PORVENIR</option>
														<option>PROTECCIÓN</option>
														<option>FNA</option>
														<option>NO APLICA</option>
													  </select>
													  <span class="obligatorio">*</span>
													</div>
									              </div>

										  		<div class="control-group">
													<label class="control-label" for="focusedInput">Valor cesantias</label>
													<div class="controls">
											  			<input class="input-xlarge focused" name="valor_cesantias" type="number" placeholder="Ingrese valor de cesantias">
													</div>
										  		</div>

										  		<div class="control-group">
													<label class="control-label" for="focusedInput">Causal de retiro</label>
													<div class="controls">
											  			<input class="input-xlarge focused" name="causal_retiro" type="text" placeholder="Ingrese causal de retiro" required>
														<span class='obligatorio'>*</span>
													</div>
										  		</div>

										  		<div class="control-group">
													<label class="control-label" for="focusedInput">Salario devengado</label>
													<div class="controls">
											  			<input class="input-xlarge focused" name="salario_devengado" type="number" placeholder="Ingrese salario devengado" required>
														<span class='obligatorio'>*</span>
													</div>
										  		</div>

											</div>
										</div>
									</div>
								</div>

								<?php 

								$vari .= '<div class="row-fluid">';
							        $vari .= '<div class="box-content" style="display: block;">';
										$vari .= '<div class="span12">';
											$vari .= '	<label style="color:#008fd1; font-size:18px;"><b>Vacaciones</b></label>';
										$vari .= '</div>';
									$vari .= '</div>';
								$vari .= '</div>';

								$vari .= '<div class="row-fluid">';
							        $vari .= '<div class="box-content" style="display: block;">';
										$vari .= '<div class="span12">';
											$vari .= '<div class="span6">';
												
												$añoActual = date("Y");
												$vari .= '<input type="hidden" name="añoActual" value='.$añoActual.'>';
												$añoPasado = date("Y")-1;
												$vari .= '<input type="hidden" name="añoPasado" value='.$añoPasado.'>';
												$añoAntepasado = date("Y")-2;
												$vari .= '<input type="hidden" name="añoAntepasado" value='.$añoAntepasado.'>';

		        								$vari .= '<div class="control-group">';
												$vari .= '	<label class="control-label">Vacaciones inicio '.$añoActual.'</label>';
												$vari .= '	<div class="controls">';
											  	$vari .= '		<input class="input-xlarge focused" name="añoActualInicio" type="date">';
												$vari .= '	</div>';
										  		$vari .= '</div>';

										  		$vari .= '<div class="control-group">';
												$vari .= '	<label class="control-label">Vacaciones inicio '.$añoPasado.'</label>';
												$vari .= '	<div class="controls">';
											  	$vari .= '		<input class="input-xlarge focused" name="añoPasadoInicio" type="date">';
												$vari .= '	</div>';
										  		$vari .= '</div>';

										  		$vari .= '<div class="control-group">';
												$vari .= '	<label class="control-label">Vacaciones inicio '.$añoAntepasado.'</label>';
												$vari .= '	<div class="controls">';
											  	$vari .= '		<input class="input-xlarge focused" name="añoAntepasadoInicio" type="date">';
												$vari .= '	</div>';
										  		$vari .= '</div>';

											$vari .= '</div>';

											$vari .= '<div class="span6">';

												$vari .= '<div class="control-group">';
												$vari .= '	<label class="control-label">Vacaciones fin '.$añoActual.'</label>';
												$vari .= '	<div class="controls">';
											  	$vari .= '		<input class="input-xlarge focused" name="añoActualFin" type="date">';
												$vari .= '	</div>';
										  		$vari .= '</div>';

										  		$vari .= '<div class="control-group">';
												$vari .= '	<label class="control-label">Vacaciones fin '.$añoPasado.'</label>';
												$vari .= '	<div class="controls">';
											  	$vari .= '		<input class="input-xlarge focused" name="añoPasadoFin" type="date">';
												$vari .= '	</div>';
										  		$vari .= '</div>';

										  		$vari .= '<div class="control-group">';
												$vari .= '	<label class="control-label">Vacaciones fin '.$añoAntepasado.'</label>';
												$vari .= '	<div class="controls">';
											  	$vari .= '		<input class="input-xlarge focused" name="añoAntepasadoFin" type="date">';
												$vari .= '	</div>';
										  		$vari .= '</div>';

											$vari .= '</div>';
										$vari .= '</div>';
									$vari .= '</div>';
								$vari .= '</div>';

								echo $vari;

								?>
		        								
								
							</fieldset>
							<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
							<button type="submit" class="btn btn-primary">Agregar historia laboral</button>
						</form>

	  					</div>
								
						
					</div>
				</div>

					<div class="row-fluid">	
					<div class="box span12"> 
					<div class="box-header" data-original-title="">
						<h2>
							<i class="halflings-icon th"></i>
							<span class="break"></span>
							Registros
						</h2>
						<div class="box-icon">
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-down"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div>
					</div>
					<div class="box-content" style="display: none;">
						<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper" role="grid">
							
							<table class="table table-striped table-bordered bootstrap-datatable datatable dataTable" id="DataTables_Table_0" aria-describedby="DataTables_Table_0_info">
							  <thead>
								  <tr role="row">
								  	<th>Nombre</th>
								  	<th>Cedula</th>
								  	<th>Codigo</th>
								  	<th>Cargo</th>
								  	<th>Area</th>
								  	<th>Acciones</th>
								  	
								  </tr>
							  </thead>   
							  
						  	  <tbody role="alert" aria-live="polite" aria-relevant="all">
						  	  	<?php 
						  	  		for($i=0; $i < count($data); $i++){
						  	  			
										
						  	  			echo "<tr class='even'>";
						  	  			echo "<td>".$data[$i]['nombreLaboral']."</td>";
						  	  			echo "<td>".$data[$i]['cedulaLaboral']."</td>";
						  	  			echo "<td>".$data[$i]['codigoLaboral']."</td>";
						  	  			echo "<td>".$data[$i]['cargoLaboral']."</td>";
						  	  			echo "<td>".$data[$i]['areaLaboral']."</td>";

						  	  			echo "<td><form class='form-gla' action='http://".$_SERVER['HTTP_HOST']."/prueba/index.php/formatos/formatos/get_datos' method='post' accept-charset='utf-8'><input type='hidden' name='cedula' value='".$cedula."'><input type='hidden' name='id' value='".$data[$i]['idHistorialLaboral']."'><button type='submit' class='btn btn-success'><i class='halflings-icon zoom-in'></i></button></form><form class='form-gla' action='http://".$_SERVER['HTTP_HOST']."/prueba/index.php/formatos/formatos/delete_formato' method='post' accept-charset='utf-8'><input type='hidden' name='cedula' value='".$cedula."'><input type='hidden' name='id' value='".$data[$i]['idHistorialLaboral']."'><button type='submit' class='btn btn-danger'><i class='halflings-icon trash'></i></button></form></td>";

						  	  			echo "</tr>";
						  	  		}
						  	  	 ?>
							  </tbody>
							</table>
					 	</div>            
					</div>
					</div>
					
				</div> 

				</div>

			
			</div>
			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->
		
	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Settings</h3>
		</div>
		<div class="modal-body">
			<p>Here settings can be configured...</p>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
			<a href="#" class="btn btn-primary">Save changes</a>
		</div>
	</div>
	
	<div class="clearfix"></div>
	<footer>
		<p>
			<span style="text-align:left;float:left">&copy; 2016 <a href="http://http://www.apuestasochoa.com.co/">Desarrollo interno - apuestasochoa</a></span>
		</p>
	</footer>
	<!-- start: JavaScript-->
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery-1.9.1.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery-migrate-1.0.0.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery-ui-1.10.0.custom.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.ui.touch-punch.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/modernizr.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/bootstrap.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.cookie.js"></script>
		<script src='http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/fullcalendar.min.js'></script>
		<script src='http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.dataTables.min.js'></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/excanvas.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.flot.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.flot.pie.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.flot.stack.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.flot.resize.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.chosen.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.uniform.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.cleditor.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.noty.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.elfinder.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.raty.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.iphone.toggle.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.uploadify-3.1.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.gritter.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.imagesloaded.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.masonry.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.knob.modified.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.sparkline.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/counter.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/retina.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/custom.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/empleados/fileinput.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/prueba/formato.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/apuestasochoa.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/prueba/evaluar_proveedor.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/empleados/nuevo.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/formatos/formatos.js"></script>
	<!-- end: JavaScript-->
	
</body>
</html>
