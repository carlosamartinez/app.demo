<?php 

	// Include the main TCPDF library (search for installation path).
	require_once($_SERVER['DOCUMENT_ROOT'].'\prueba\tcpdf\tcpdf_include.php');

	// create new PDF document
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	// set document information
	// echo PDF_CREATOR;
	// die();
	$pdf->SetCreator('Version 1.0');
	$pdf->SetAuthor('Version 1.0');
	$pdf->SetTitle('Apuestas Ochoa');
	$pdf->SetSubject('Apuestas Ochoa');
	$pdf->SetKeywords('Apuestas Ochoa');

	// echo PDF_HEADER_LOGO;
	// die();
	// set default header data
	$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, 'Software-Interno', 'Documentos en estado '.$view_state.'');

	// set header and footer fonts
	$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	// set margins
	$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		require_once(dirname(__FILE__).'/lang/eng.php');
		$pdf->setLanguageArray($l);
	}

	// ---------------------------------------------------------	
	// set font
	$pdf->SetFont('dejavusans', '', 10);



	$pdf->AddPage();

	// create some HTML content
	// $subtable = '<table border="1" cellspacing="6" cellpadding="4"><tr><td>a</td><td>b</td></tr><tr><td>c</td><td>d</td></tr></table>';

	for($i=0; $i < count($pdf_dates); $i++){
		if($pdf_dates[$i]->etapa_1){
			$pdf_dates[$i]->etapa_1 = 'Validada';
		}else{
			$pdf_dates[$i]->etapa_1 = 'Sin validar';
		}
		if($pdf_dates[$i]->etapa_2){
			$pdf_dates[$i]->etapa_2 = 'Validada';
		}else{
			$pdf_dates[$i]->etapa_2 = 'Sin validar';
		}
		if($pdf_dates[$i]->etapa_3){
			$pdf_dates[$i]->etapa_3 = 'Validada';
		}else{
			$pdf_dates[$i]->etapa_3 = 'Sin validar';
		}
		if($pdf_dates[$i]->etapa_4){
			$pdf_dates[$i]->etapa_4 = 'Validada';
		}else{
			$pdf_dates[$i]->etapa_4 = 'Sin validar';
		}
	}

	$html .= '<table border="1" cellspacing="1" style="border-color: blue;">';
		$html .= '<tr>';
			$html .= '<th><h3>Nro-factura</h3></th>';
			$html .= '<th><h3>Concepto</h3></th>';
			$html .= '<th><h3>Ubicacion</h3></th>';
			$html .= '<th><h3>Estado</h3></th>';
			$html .= '<th><h3>Etapa 1</h3></th>';
			$html .= '<th><h3>Etapa 2</h3></th>';
			$html .= '<th><h3>Etapa 3</h3></th>';
			$html .= '<th><h3>Etapa 4</h3></th>';
			
		$html .= '</tr>';

			for($i=0; $i < count($pdf_dates) ; $i++){
				$html .= '<tr>';
					$html .= '<td>'.$pdf_dates[$i]->nro_factura.'</td>';
					$html .= '<td>'.$pdf_dates[$i]->concepto.'</td>';
					$html .= '<td>'.$pdf_dates[$i]->ubicacion.'</td>';
					$html .= '<td>'.$pdf_dates[$i]->estado.'</td>';
					$html .= '<td>'.$pdf_dates[$i]->etapa_1.'</td>';
					$html .= '<td>'.$pdf_dates[$i]->etapa_2.'</td>';
					$html .= '<td>'.$pdf_dates[$i]->etapa_3.'</td>';
					$html .= '<td>'.$pdf_dates[$i]->etapa_4.'</td>';
					
				$html .= '</tr>';	

			}
	$html .= '</table>';

	// echo $html;
	// die();
	
	$pdf->SetFont('helvetica', 'BI', 10);
	// output the HTML content
	$pdf->writeHTML($html, true, false, true, false, '');



	// reset pointer to the last page
	$pdf->lastPage();


	// ---------------------------------------------------------

	//Close and output PDF document
	$pdf->Output('pendientes.pdf', 'I');

	//============================================================+
	// END OF FILE
	//============================================================+
 ?>