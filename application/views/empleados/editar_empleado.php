<?php 
	$id = $dates_consul['0']->id_empleado;
 ?>
<div id="content" class="span10">
	<ul class="breadcrumb">
		<li>
			<i class="icon-paste color_fla"></i>
			<a>Nueva hoja de vida</a> 
			<i class="icon-angle-right color_fla"></i>
		</li>
	</ul>
	
	<div class="alert alert-danger fade in sentasi">
		<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
		<strong>Info!</strong>El registro que usted indica ya existe en la base de datos.
	</div>

	<?php if($message == 'exito'){ ?>
		<div class="alert alert-success fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
			<strong>Info!</strong>Registro del empleado exitoso.
		</div>
	<?php } ?>	

	<div class="row-fluid">	
		<div class="bs-example">
		    <ul class="nav nav-tabs">
		        <li class="active">
		        	<a data-toggle="tab" id="datos_even" class="font_tabs" href="#datos_personales">Datos personales</a>
		        </li>
		        <li>
		        	<a data-toggle="tab" id="afiliaciones_even" class="font_tabs" href="#afiliaciones">Afiliaciones</a>
		        </li>
		        <li>
		        	<a data-toggle="tab" id="perfil_even" class="font_tabs" href="#perfil">Perfil</a>
		        </li>
		    </ul>
		    
		    <form class="form-horizontal" id="formulario_edit" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/empleados/Empleados/update_emple" method="post" enctype="multipart/form-data">
			   	<input type="hidden" name="cedula_user"  value="<?php echo $cedula_user; ?>">
			   	<div class="tab-content">
			        <div id="datos_personales" class="tab-pane fade in active">
			            <!-- PESTAÑAS DE DATOS PERSONALES -->
			            <div class="box span11">
			            	<div class="box-header" data-original-title="">
			            		<h2><i class="halflings-icon th"></i><span class="break"></span></h2>
			            	</div>

			            	<div class="box-content" style="display: block;">
			            		<div class="row-fluid">
					            	<div class="span6">
	            		              <div class="control-group ">
	            						<label class="control-label" for="focusedInput">Nombres</label>
	            						<div class="controls">
	            						  <input class="input-xlarge focused" id="nombres" name="nombres" type="text" placeholder="ingrese nombres" value="<?php echo $dates_consul[0]->nombres; ?>">
	            						  <span class="obligatorio">*</span>
	            						</div>
	            		              </div>

	            		              <div class="control-group ">
	            						<label class="control-label" for="focusedInput">Apellidos</label>
	            						<div class="controls">
	            						  <input class="input-xlarge focused" id="apellidos" name="apellidos" type="text" placeholder="ingrese apellidos" value="<?php echo $dates_consul[0]->apellidos; ?>">
	            						  <span class="obligatorio">*</span>
	            						</div>
	            		              </div>

	            		              <div class="control-group ">
	            						<label class="control-label" for="focusedInput">Cedula</label>
	            						<div class="controls">
	            						  <input class="input-xlarge focused" id="cedula" onchange="return_exis();" name="cedula" type="text" placeholder="ingrese cedula" value="<?php echo $dates_consul[0]->cedula; ?>">
	            						  <span class="obligatorio">*</span>
	            						</div>
	            		              </div>


	            		              <div class="control-group ">
	            						<label class="control-label" for="focusedInput">Dirección</label>
	            						<div class="controls">
	            						  <input class="input-xlarge focused" id="direccion" name="direccion" type="text" placeholder="ingrese dirección" value="<?php echo $dates_consul[0]->direccion; ?>">
	            						  <span class="obligatorio">*</span>
	            						</div>
	            		              </div>

	            		              <div class="control-group ">
	            						<label class="control-label" for="focusedInput">Telefono fijo</label>
	            						<div class="controls">
	            						  <input class="input-xlarge focused" id="telefono_fijo" name="telefono_fijo" type="text" placeholder="ingrese tel. fijo" value="<?php echo $dates_consul[0]->telefono_fijo; ?>">
	            						</div>
	            		              </div>

	            		              <div class="control-group ">
	            						<label class="control-label" for="focusedInput">Celular</label>
	            						<div class="controls">
	            						  <input class="input-xlarge focused" id="celular" name="celular" type="text" placeholder="ingrese # celular" value="<?php echo $dates_consul[0]->celular; ?>">
	            						  <span class="obligatorio">*</span>
	            						</div>
	            		              </div>

	            		              <div class="control-group ">
	            						<label class="control-label" for="focusedInput">Fecha de ingreso</label>
	            						<div class="controls">
	            						  <input id="fecha_ingreso" class="input-xlarge focused" id="fecha_ingreso" name="fecha_ingreso" type="text" placeholder="ingrese fecha de ingreso" value="<?php echo $dates_consul[0]->fecha_ingreso; ?>">
	            						  <span class="obligatorio">*</span>
	            						</div>
	            		              </div>

									<div class="control-group">
									<label class="control-label" for="focusedInput">Estado</label>
									<div class="controls">
									  <select name="estados" id="estado" class="input-xlarge focused">
									  	<?php 
									  		if($dates_consul[0]->estado === "Activo"){
									  			echo "<option selected>Activo</option>";
									  			echo "<option>Inactivo</option>";
									  		}else{
									  			echo "<option>Activo</option>";
									  			echo "<option selected>Inactivo</option>";
									  		}
									  		
									  	 ?>
									  </select>
									  <span class="obligatorio">*</span>
									</div>
								  </div>

					            	</div>
					            </div>          
			            	</div>					
			            </div>				            
			              <!-- PESTAÑAS DE DATOS PERSONALES -->
			        </div>

			        <div id="afiliaciones" class="tab-pane fade">
			            <!-- PESTAÑA DE AFILIACIONES -->
						<div class="box span11">
							<div class="box-header" data-original-title="">
								<h2><i class="halflings-icon th"></i><span class="break"></span></h2>
							</div>

							<div class="box-content" style="display: block;">
				              <div class="control-group ">
								<label class="control-label" for="focusedInput">Arl</label>
								<input type="hidden" id="arl_apr" value="<?php echo $dates_consul[0]->arl; ?>">
								<div class="controls">
								  <select id="arl" name="arl">
								  	<option>Seleccione...</option>
								  	<option>SURA</option>
								  	<option>POSITIVA</option>
								  </select>
								  <span class="obligatorio">*</span>
								</div>
				              </div>

				              <div class="control-group">
								<label class="control-label" for="focusedInput">Eps</label>
								<input type="hidden" id="eps_apr" value="<?php echo $dates_consul[0]->eps; ?>">
								<div class="controls">
								  <select id="eps" name="eps">
								  	<option>Seleccione...</option>
								  	<option>NUEVA EMPRESA PROMOTORA DE SALUD S.A.</option>
								  	<option>SURA EPS</option>
								  	<option>SALUD TOTAL</option>
								  	<option>CAFESALUD ENTIDAD PROMOTORA DE SALUD S.A.</option>
								  	<option >CONSORCIO SAYP 2011</option>
								  	<option >COOMEVA ENTIDAD PROMOTORA DE SALUD</option>
								  	<option>ENTIDAD PROMOTORA DE SALUD SERVICIO OCCIDENTAL</option>
								  	<option>ENTIDAD PROMOTORA DE SALUD SANITAS LTDA</option>
								  	<option>ASOCIACION MUTUAL LA ESPERANZA ASMET SALUD ES</option>
									<option>FOSYGA</option>
								  </select>
								  <span class="obligatorio">*</span>
								</div>
				              </div>

				              <div class="control-group ">
								<label class="control-label" for="focusedInput">Pensión</label>
								<input type="hidden" id="pension_apr" value="<?php echo $dates_consul[0]->pension; ?>">
								<div class="controls">
								  <select id="pension" name="pension">
								  	<option>Seleccione...</option>
								  	<option>PROTECCION</option>
								  	<option>PORVENIR S.A</option>
								  	<option>ADMINISTRADORA COLOMBIANA DE PENSIONES COLPEN</option>
								  	<option>COLFONDOS</option>
								  </select>
								  <span class="obligatorio">*</span>
								</div>
				              </div>

				              <div class="control-group">
								<label class="control-label" for="focusedInput">Cesantias</label>
								<input type="hidden" id="cesantias_apr" value="<?php echo $dates_consul[0]->cesantias; ?>"/>
								<div class="controls">
								  <select id="cesantias" name="cesantias">
									<option>Seleccione...</option>
									<option>PORVENIR</option>
									<option>PROTECCIÓN</option>
									<option>FNA</option>
								  </select>
								  <span class="obligatorio">*</span>
								</div>
				              </div>          
							</div>					
						</div>
			            <!-- PESTAÑA DE AFILIACIONES -->
			        </div>

			        <div id="perfil" class="tab-pane fade">
						<!-- PESTAÑA DE PERFIL EMPLO -->
						<div class="box span11">
							<div class="box-header" data-original-title="">
								<h2>
									<i class="halflings-icon th"></i><span class="break"></span>
									ROL ORGANIZACIONAL
								</h2>
								<div class="box-icon">
									<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
									<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
								</div>
							</div>

							<div class="box-content" style="display: block;">
					            <div class="control-group ">
									<label class="control-label" for="focusedInput">Cargo laboral</label>
									<div class="controls">
									  <input id="cargo" class="input-xlarge focused" name="cargo_laboral" type="text" placeholder="Ingrese cargo laboral" value="<?php echo $dates_consul[0]->cargo_laboral; ?>">
									  <span class="obligatorio">*</span>
									</div>
					            </div>

					            <!-- <div class="control-group ">
									<label class="control-label" for="focusedInput">Area organizacional</label>
									<div class="controls">
									  <input id="area" class="input-xlarge focused" name="departamento" type="text" placeholder="Ingrese area organizacional" value="<?php echo $dates_consul[0]->departamento; ?>">
									  <span class="obligatorio">*</span>
									</div>
					            </div> -->

					            <div class="control-group ">
									<label class="control-label" for="focusedInput">Area organizacional</label>
									<div class="controls">
									  <input id="area_f" type="hidden" value="<?php echo $dates_consul[0]->departamento; ?>">

									  	<select name="departamento" id="area">
									  		<option>Seleccione..</option>
									  		<?php 
									  			for ($i=0; $i < count($departamentos_organizacionales); $i++) { 
									  				echo "<option>".$departamentos_organizacionales[$i]."</option>";
									  			}
									  		 ?>
									  	</select>

									  <span class="obligatorio">*</span>
									</div>
					            </div>
								
								<input type="hidden" name="id_empleado" value="<?php echo $id; ?>">

								<?php if($rol == 'Administrador' or $rol == 'Jefe recursos'){ 
									$messa1 = 'Desea cambiar el carnet?';
									$messa2 = 'Desea agregar el carnet del empleado?';
								?>

									<div class="control-group span12">
										<div class="controls">
										<?php if(!empty($dates_consul[0]->carnet_img)){ ?>
											<input type="checkbox" id="check_carn" value="photo"><?php echo $messa1; ?>
										<?php }else{ ?>	
											<input type="checkbox" id="check_carn" value="photo"><?php echo $messa2; ?>
										<?php } ?>
										</div>
							        </div>

									<?php if(!empty($dates_consul[0]->carnet_img)){?>
						             	<div class="span11 photo_open alk">
						 					<div class="file-preview">
						 					   <div class="file-preview-status text-center text-success"></div>
						 					   <div class="file-preview-thumbnails">
						 							<div class="file-preview-frame">
						 								<img src="<?php echo $dates_consul[0]->carnet_img; ?>" class="file-preview-image" title="foto_hoja_de_vida_2.jpg">
						 							</div>
						 						</div>
						 					   <div class="clearfix"></div>
						 					</div>
						             	</div>
						            <?php } ?> 	


					            <div id="img_carnets" class="control-group span6">
									<label class="control-label" for="focusedInput">Carnet</label>
									<div class="controls">
									  <input type="file" class="image_carnet" name="carnet" id="carnet">
									</div>
					            </div>

					            <?php } ?>
							</div>					
						</div>

						<div class="box span11">
							<div class="box-header" data-original-title="">
								<h2>
									<i class="halflings-icon th"></i><span class="break"></span>
									DOCUMENTOS-ADJUNTOS
								</h2>
								<div class="box-icon">
									<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
									<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
								</div>
							</div>

							<div class="box-content" style="display: block;">
				            	<div class="box-content">            					
								<?php if($rol == 'Administrador' or $rol == 'Jefe recursos'  or $rol == 'Formatos'){ ?>
	            					 <a href="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/empleados/Empleados/generar_zip_hoja_vida/<?php echo $dates_consul[0]->id_empleado;?>">
	            					 	<img src="/prueba/theme/img/hoja.png" class="img-responsive zoom_i" alt="Responsive image">
	            					 	<span style="color: black;">Descargar hoja de vida</span>          					 	    
	            					 </a>
	            				<?php } ?>
	            		            <div class="control-group">
	            						<div class="controls">
	            							<input type="checkbox" id="check_doc" value="photo">Desea anexar documentos?
	            						</div>
	            		            </div>
	            				
	            		            <div id="img_content1" class="control-group span11 off-document">
	            						<label class="control-label" for="focusedInput">Anexos-documentos</label>
	            						<div class="controls">
	            						  <input type="file" class="image_hoja" name="upload[]" multiple="multiple" id="documentos_adjuntos">
	            						  <!-- <span class="obligatorio">*</span> -->
	            						</div>
	            		            </div>  
				            	</div>					
							</div>
						</div>
						

						<div class="box span11">
							<div class="box-header" data-original-title="">
								<h2>
									<i class="halflings-icon th"></i><span class="break"></span>
									MEDIDAS DICIPLINARIAS
								</h2>
								<div class="box-icon">
									<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
									<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
								</div>
							</div>

							<div class="box-content" style="display: block;">
				            	<div class="box-content">
				            	<?php if($rol == 'Administrador' or $rol == 'Jefe recursos' or $rol == 'Formatos'){ ?>			            		
									<?php if($dates_consul[0]->medidas_diciplinarias){ ?> 
					            		 <a href="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/empleados/Empleados/generar_zip_medidas/<?php echo $dates_consul[0]->id_empleado;?>">
					            		 	<img src="/prueba/theme/img/medidas.png" class="img-responsive zoom_i" alt="Responsive image">
					            		 	<span style="color: black;">Descargar medidas diciplinarias</span>
					            		 </a>
					            	<?php } ?>	
					            	<?php } ?> 

	            		            <div id="img_carnet" class="control-group span11">
	            						<label class="control-label" for="focusedInput">medidas diciplinarias</label>
	            						<div class="controls">
	            						  <input type="file" class="image_medidas" multiple="multiple" name="image_medidas[]" id="medidas">
	            						</div>
	            		            </div> 
				            	</div>					
							</div>
						</div>	
			             <!-- PESTAÑA DE PERFIL EMPLO --> 
			        </div>



			    </div>

			    <?php if($rol == 'Administrador' or $rol == 'Jefe recursos'){ ?>
			    	<button type="submit" class="btn btn-primary">Modificar hoja-vida</button>
			    <?php } ?>

			</form>
		</div>


	</div>
</div>
		
<!--// autocompletamos cargos de la organizacion-->
<script>
$(function(){
  var cargos   = <?php echo json_encode($cargos); ?>;
  var dept_org = <?php echo json_encode($departamentos_organizacionales); ?>;	
  $( "#cargo" ).autocomplete({
    source: cargos
  });
  $( "#area" ).autocomplete({
    source: dept_org
  });
});

</script>
	