	
	<div id="content" class="span10">
		<ul class="breadcrumb">
			<li>
				<i class="icon-paste color_fla"></i>
				<a>Hojas de vida</a> 
				<i class="icon-angle-right color_fla"></i>
			</li>
		</ul>
		
		<?php if($rol == 'Administrador' or $rol == 'Jefe recursos'){ ?>
			<div class="row-fluid">
				<div class="span3">
					<form class="form-gla" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/empleados/Empleados/nueva_hoja" method="post" accept-charset="utf-8">
						<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
						<input type="submit" class="btn btn-info" value="Registrar nueva hoja de vida" />
					</form>
				</div>		
			</div>	 
		<?php } ?>
		
		<div class="row-fluid">	
			<table id="example" class="display" cellspacing="0" width="100%">
			    <thead>
			        <tr>
			            <th>Nombres</th>
			            <th>Apellidos</th>
			            <th>Cedula</th>
			            <th>Estado</th>
			            <th>Fecha ingreso</th>
			            <th>cargo laboral</th>
			            <!-- <th>foto</th> -->
			            <th>Acciones</th>
			        </tr>
			    </thead>

			   
		
			    <tbody>
			    	<?php for ($i=0; $i < count($empleados); $i++) { ?>
			    		<tr>
							<td style="text-align: center;"><?php echo $empleados[$i]->nombres; ?></td>
							<td style="text-align: center;"><?php echo $empleados[$i]->apellidos; ?></td>
							<td style="text-align: center;"><?php echo $empleados[$i]->cedula; ?></td>
							<td style="text-align: center;"><?php echo $empleados[$i]->estado; 
								if($empleados[$i]->estado === "Activo"){
								echo '<br><button  class="btn btn-success btn-mini disabled"><span class="icon-ok icon-white"></span></button>';
							}else{
								echo '<br><button  class="btn btn-danger btn-mini disabled"><span class="icon-remove icon-white"></span></button>';
							}
							?></td>
							<td style="text-align: center;"><?php echo $empleados[$i]->fecha_ingreso; ?></td>
							<td style="text-align: center;"><?php echo $empleados[$i]->cargo_laboral; ?></td>
							<!-- <td style="text-align: center;"><img src="<?php echo $empleados[$i]->foto_perfil; ?>" class="img-responsive zoom" style="width:10%;" alt="Responsive image"></td> -->
							<td>
								<div class="icon">
									
									<form id="form_edt_<?php echo $i;?>" class="span3" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/empleados/Empleados/modificar_hoja" method="post" accept-charset="utf-8">	
										<a id="submit_edt_<?php echo $i;?>" onclick='get_func_edit(this);' style="color:black;">
											<i class="icon-pencil zoom_i" style="font-size: 25px;"></i>
										</a>
										<input type="hidden" name="cedula_user" value="<?php echo $cedula; ?>">
										<input type="hidden" name="id_empleado" value="<?php echo $empleados[$i]->id_empleado; ?>">
									</form>
								
								<?php if($rol == 'Administrador' or $rol == 'Jefe recursos' or $rol == 'Formatos'){ ?>
									<form id="form_hojaBasica_<?php echo $i;?>" class="span3" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/empleados/Empleados/generar_zip_hoja_vida" method="post" accept-charset="utf-8">	
										<a id="submit_hojaBasica_<?php echo $i;?>" onclick="get_func_hjo_basic(this);" title="Generar hoja de vida">
											<img src="/prueba/theme/img/hoja_basica.png" class="img-responsive zoom_i" alt="Responsive image">
										</a>
										<input type="hidden" name="cedula_user" value="<?php echo $cedula; ?>">
										<input type="hidden" name="id_empleado" value="<?php echo $empleados[$i]->id_empleado; ?>">
										<input type="hidden" name="type" value="hoja_basica">
									</form>	

									<form id="form_hojaCompleta_<?php echo $i;?>" class="span3" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/empleados/Empleados/generar_zip_medidas" method="post" accept-charset="utf-8">
										<a id="submit_hojaCompleta_<?php echo $i;?>" onclick="get_func_hjo_compl(this);" title="Generar medidas diciplinarias"><img src="/prueba/theme/img/hoja_completa.png" class="img-responsive zoom_i" alt="Responsive image"></a>
										<input type="hidden" name="cedula_user" value="<?php echo $cedula; ?>">
										<input type="hidden" name="id_empleado" value="<?php echo $empleados[$i]->id_empleado; ?>">
										<input type="hidden" name="type" value="hoja_completa">
									</form>	
								<?php } ?>
								</div>
							</td>
			    		</tr>
			    	<?php } ?>	
	            </tbody>
	        </table>
	        <!-- <div class="zoom">Efecto de zoom con CSS3</div> -->
			<!-- <div class="clearfix"></div>	 -->		
		</div>
	</div>
	<!-- end: Content -->
	


