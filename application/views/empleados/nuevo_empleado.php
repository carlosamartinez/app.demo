<	<div id="content" class="span10">
		<ul class="breadcrumb">
			<li>
				<i class="icon-paste color_fla"></i>
				<a>Nueva hoja de vida</a> 
				<i class="icon-angle-right color_fla"></i>
			</li>
		</ul>
		
		<div class="alert alert-danger fade in sentasi">
			<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
			<strong>Info!</strong>El registro que usted indica ya existe en la base de datos.
		</div>

		

		<div class="row-fluid">	
			<div class="bs-example">
			    <ul class="nav nav-tabs">
			        <li class="active">
			        	<a data-toggle="tab" id="datos_even" class="font_tabs" href="#datos_personales">Datos personales</a>
			        </li>
			        <li>
			        	<a data-toggle="tab" id="afiliaciones_even" class="font_tabs" href="#afiliaciones">Afiliaciones</a>
			        </li>
			        <li>
			        	<a data-toggle="tab" id="perfil_even" class="font_tabs" href="#perfil">Perfil</a>
			        </li>
			    </ul>
			    
			    <form class="form-horizontal" id="formulario" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/empleados/Empleados/insert_dates" method="post" enctype="multipart/form-data">
				   	<input type="hidden" name="cedula_user"  value="<?php echo $cedula; ?>">
				   	<div class="tab-content">
				        <div id="datos_personales" class="tab-pane fade in active">
				            <!-- PESTAÑAS DE DATOS PERSONALES -->
				            <div class="box span11">
				            	<div class="box-header" data-original-title="">
				            		<h2><i class="halflings-icon th"></i><span class="break"></span></h2>
				            	</div>

				            	<div class="box-content" style="display: block;">
				            		<div class="row-fluid">
						            	<div class="span6">
		            		              <div class="control-group ">
		            						<label class="control-label" for="focusedInput">Nombres</label>
		            						<div class="controls">
		            						  <input class="input-xlarge focused" id="nombres" name="nombres" type="text" placeholder="ingrese nombres">
		            						  <span class="obligatorio">*</span>
		            						</div>
		            		              </div>

		            		              <div class="control-group ">
		            						<label class="control-label" for="focusedInput">Apellidos</label>
		            						<div class="controls">
		            						  <input class="input-xlarge focused" id="apellidos" name="apellidos" type="text" placeholder="ingrese apellidos">
		            						  <span class="obligatorio">*</span>
		            						</div>
		            		              </div>

		            		              <div class="control-group ">
		            						<label class="control-label" for="focusedInput">Cedula</label>
		            						<div class="controls">
		            						  <input class="input-xlarge focused" id="cedula" name="cedula" onchange="return_exis();" type="text" placeholder="ingrese cedula">
		            						  <span class="obligatorio">*</span>
		            						</div>
		            		              </div>


		            		              <div class="control-group ">
		            						<label class="control-label" for="focusedInput">Dirección</label>
		            						<div class="controls">
		            						  <input class="input-xlarge focused" id="direccion" name="direccion" type="text" placeholder="ingrese dirección">
		            						  <span class="obligatorio">*</span>
		            						</div>
		            		              </div>

		            		              <div class="control-group ">
		            						<label class="control-label" for="focusedInput">Telefono fijo</label>
		            						<div class="controls">
		            						  <input class="input-xlarge focused" id="telefono_fijo" name="telefono_fijo" type="text" placeholder="ingrese tel. fijo">
		            						</div>
		            		              </div>

		            		              <div class="control-group ">
		            						<label class="control-label" for="focusedInput">Celular</label>
		            						<div class="controls">
		            						  <input class="input-xlarge focused" id="celular" name="celular" type="text" placeholder="ingrese # celular">
		            						  <span class="obligatorio">*</span>
		            						</div>
		            		              </div>

		            		              <div class="control-group ">
		            						<label class="control-label" for="focusedInput">Fecha de ingreso</label>
		            						<div class="controls">
		            						  <input id="fecha_ingreso" class="input-xlarge focused" id="fecha_ingreso" name="fecha_ingreso" type="text" placeholder="ingrese fecha de ingreso">
		            						  <span class="obligatorio">*</span>
		            						</div>
		            		              </div>
						            	</div>
						            	<!-- <div class="span2">
						            		<h3>Foto 3x4</h3>
						            		<div class="form-group">
						            		    <input id="foto_perfil_full" name="image_perfil" type="file" />
						            		</div>
						            	</div> -->
						            </div>          
				            	</div>					
				            </div>				            
				              <!-- PESTAÑAS DE DATOS PERSONALES -->
				        </div>

				        <div id="afiliaciones" class="tab-pane fade">
				            <!-- PESTAÑA DE AFILIACIONES -->
							<div class="box span11">
								<div class="box-header" data-original-title="">
									<h2><i class="halflings-icon th"></i><span class="break"></span></h2>
								</div>

								<div class="box-content" style="display: block;">
					              <div class="control-group ">
									<label class="control-label" for="focusedInput">Arl</label>
									<div class="controls">
									  <select id="arl" name="arl">
									  	<option>Seleccione...</option>
									  	<option>SURA</option>
									  	<option>POSITIVA</option>
									  </select>
									  <span class="obligatorio">*</span>
									</div>
					              </div>

					              <div class="control-group">
									<label class="control-label" for="focusedInput">Eps</label>
									<div class="controls">
									  <select id="eps" name="eps">
									  	<option>Seleccione...</option>
									  	<option>NUEVA EMPRESA PROMOTORA DE SALUD S.A.</option>
									  	<option>SURA EPS</option>
									  	<option>SALUD TOTAL</option>
									  	<option>CAFESALUD ENTIDAD PROMOTORA DE SALUD S.A.</option>
									  	<option>CONSORCIO SAYP 2011</option>
									  	<option >COOMEVA ENTIDAD PROMOTORA DE SALUD</option>
									  	<option>ENTIDAD PROMOTORA DE SALUD SERVICIO OCCIDENTAL</option>
									  	<option>ENTIDAD PROMOTORA DE SALUD SANITAS LTDA</option>
									  	<option>ASOCIACION MUTUAL LA ESPERANZA ASMET SALUD ES</option>
									  	<option>FOSYGA</option>
									  </select>
									  <span class="obligatorio">*</span>
									</div>
					              </div>

					              <div class="control-group ">
									<label class="control-label" for="focusedInput">Pensión</label>
									<div class="controls">
									  <select id="pension" name="pension">
									  	<option>Seleccione...</option>
									  	<option>PROTECCION</option>
									  	<option>PORVENIR S.A</option>
									  	<option>ADMINISTRADORA COLOMBIANA DE PENSIONES COLPEN</option>
									  	<option>COLFONDOS</option>
									  </select>
									  <span class="obligatorio">*</span>
									</div>
					              </div>

					              <div class="control-group ">
									<label class="control-label" for="focusedInput">Cesantias</label>
									<div class="controls">
									  <select id="cesantias" name="cesantias">
										<option>Seleccione...</option>
										<option>PORVENIR</option>
										<option>PROTECCIÓN</option>
										<option>FNA</option>
									  </select>
									  <span class="obligatorio">*</span>
									</div>
					              </div>          
								</div>					
							</div>
				            <!-- PESTAÑA DE AFILIACIONES -->
				        </div>

				        <div id="perfil" class="tab-pane fade">
							<!-- PESTAÑA DE PERFIL EMPLO -->

							<div class="box span11">
								<div class="box-header" data-original-title="">
									<h2><i class="halflings-icon th"></i><span class="break"></span></h2>
								</div>

								<div class="box-content" style="display: block;">
						            <div class="control-group ">
										<label class="control-label" for="focusedInput">Cargo laboral</label>
										<div class="controls">
										  <input id="cargo" class="input-xlarge focused" name="cargo_laboral" type="text" placeholder="Ingrese cargo laboral">
										  <span class="obligatorio">*</span>
										</div>
						            </div>
						            
						            <div class="control-group ">
										<label class="control-label" for="focusedInput">Area organizacional</label>
										<div class="controls">
										  <select name="departamento" id="area">
										  	<option>Seleccione..</option>
										  	<?php 
										  		for ($i=0; $i < count($departamentos_organizacionales); $i++) { 
										  			echo "<option>".$departamentos_organizacionales[$i]."</option>";
										  		}
										  	 ?>
										  </select>
										  <span class="obligatorio">*</span>
										</div>
						            </div>

						            <div id="img_content1" class="control-group span11">
										<label class="control-label" for="focusedInput">Anexos-documentos</label>
										<div class="controls">
										  <input type="file" class="image_hoja" name="upload[]" multiple="multiple" id="documentos_adjuntos">
										  <span class="obligatorio">*</span>
										</div>
						            </div>

						            <div id="img_medidas" class="control-group span11">
										<label class="control-label" for="focusedInput">Medidas-diciplinarias</label>
										<div class="controls">
										  <input type="file" class="image_medidas" name="medidas[]" multiple="multiple" id="medidas">
										</div>
						            </div>
								</div>					
							</div>
				             <!-- PESTAÑA DE PERFIL EMPLO --> 
				        </div>
				    </div>
				    <button type="submit" class="btn btn-primary">anexar hoja-vida</button>
				</form>
			</div>


		</div>
	</div>
			
	<!--// autocompletamos cargos de la organizacion-->
	<script>
	$(function(){
	  var cargos   = <?php echo json_encode($cargos); ?>;
	  $( "#cargo" ).autocomplete({
	    source: cargos
	  });
	 
	});
	</script>
		