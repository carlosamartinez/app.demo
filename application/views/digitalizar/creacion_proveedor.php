<div id="content" class="span10">
	<ul class="breadcrumb">
		<li>
			<i class="icon-folder-open color_fla"></i>
				<a>Proveedores</a> 
			<i class="icon-angle-right color_fla"></i>
		</li>
	</ul>
	<!-- <?php 
		if(isset($message)){
			if($message){?>
				<div class="alert alert-info fade in">
				    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
					<strong>Info!</strong> Usted debe de diligenciar los campos marcados con <b>*</b>.
				</div>
	<?php 
			}
		}
	?> -->
			
	<?php if($message == 'exito'){ ?>
		<div class="alert alert-success fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
			<strong>Info! </strong>Registro del proveedor exitoso.
		</div>
	<?php } ?>

	<?php if($message == 'nitIdentificacionExistente'){ ?>
		<div class="alert alert-warning fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
			<strong>Ojo! </strong>El registro no se ha podido realizar porque el Nit o la identificacion ya existe.
		</div>
	<?php } ?>						

	<div class="row-fluid">	
		<div class="box span12">
			<div class="box-header" data-original-title="">
				<h2><i class="halflings-icon edit"></i><span class="break"></span>Informacion del proveedor</h2>
					<!-- <div class="box-icon">
						<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
						<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
					</div> -->
			</div>
			
			<div class="box-content" style="display: block;">
					<fieldset> 
						<div class="row-fluid">	
							<div class="bs-example">
							    <ul class="nav nav-tabs">
							        <li class="active">
							        	<a data-toggle="tab" id="informacion_even" class="font_tabs" href="#informacion_general">Información general</a>
							        </li>
							        <li>
							        	<a data-toggle="tab" id="afiliaciones_even" class="font_tabs" href="#referencias_comerciales">Referencias comerciales</a>
							        </li>
							        <li>
							        	<a data-toggle="tab" id="perfil_even" class="font_tabs" href="#forma_pago">Pago y documentos</a>
							        </li>
								</ul>

								<form class="form-horizontal" id="formulario_p" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Digital/datos_proveedor" method="post" enctype="multipart/form-data">
				   				<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
				   				<div class="tab-content">
				        			<div id="informacion_general" class="tab-pane fade in active">
				            		<!-- PESTAÑAS DE INFORMACIÓN GENERAL -->
				            			<div class="box span12">

							            	<div class="box-content" style="display: block;">
							            		<div class="row-fluid">
									            	<div class="span6">
					            		              <div class="control-group ">
					            						<label class="control-label" for="focusedInput">Nombre o Razón social</label>
					            						<div class="controls">
					            						  <input class="input-xlarge focused" id="nombre" name="nombre" type="text" placeholder="Ingrese nombre o razón social">
					            						  <span class="obligatorio">*</span>
					            						</div>
					            		              </div>

					            		              <div class="control-group" style="margin-left:200px">
					            		              	<input type="button" id="btnMostrarIdentificacion" name="btnMostrarIdentificacion" value="Añadir identificacion" class="btn btn-primary" onclick="mostrarIdentificacion()">

													  	<input type="button" id="btnMostrarNit" name="btnMostrarNit" value="Añadir Nit" class="btn btn-primary" onclick="mostrarNit()">
													  </div>

					            		              <div class="control-group" style="display:none" id="campoIdentificacion">
					            						<label class="control-label" for="focusedInput">Nro de identificación</label>
					            						<div class="controls">
					            						  <input class="input-xlarge focused" id="identificacion" name="identificacion" type="text" placeholder="Ingrese identificación">
					            						  <span class="obligatorio">*</span>
					            						</div>
					            		              </div>

					            		              <div class="control-group" style="display:none" id="campoNit">
					            						<label class="control-label" for="focusedInput">NIT</label>
					            						<div class="controls">
					            						  <input class="input-xlarge focused" id="nit" name="nit" type="text" placeholder="Ingrese NIT">
					            						  <span class="obligatorio">*</span>
					            						</div>
					            		              </div>


					            		              <div class="control-group ">
					            						<label class="control-label" for="focusedInput">Dirección</label>
					            						<div class="controls">
					            						  <input class="input-xlarge focused" id="direccion" name="direccion" type="text" placeholder="Ingrese dirección">
					            						  <span class="obligatorio">*</span>
					            						</div>
					            		              </div>

					            		              <div class="control-group ">
					            						<label class="control-label" for="focusedInput">Ciudad</label>
					            						<div class="controls">
					            						  <input class="input-xlarge focused" id="ciudad" name="ciudad" type="text" placeholder="Ingrese ciudad">
					            						  <span class="obligatorio">*</span>
					            						</div>
					            		              </div>

					            		              <div class="control-group ">
					            						<label class="control-label" for="focusedInput">Pais</label>
					            						<div class="controls">
					            						  <input class="input-xlarge focused" id="pais" name="pais" type="text" placeholder="Ingrese país">
					            						  <span class="obligatorio">*</span>
					            						</div>
					            		              </div>

					            		              <div class="control-group ">
					            						<label class="control-label" for="focusedInput">Teléfono</label>
					            						<div class="controls">
					            						  <input class="input-xlarge focused" id="telefono" name="telefono" type="text" placeholder="Ingrese número de teléfono">
					            						  <span class="obligatorio">*</span>
					            						</div>
					            		              </div>

					            		              <div class="control-group ">
					            						<label class="control-label" for="focusedInput">E-mail</label>
					            						<div class="controls">
					            						  <input class="input-xlarge focused" id="email" name="email" type="text" placeholder="Ingrese el e-mail">
					            						  <span class="obligatorio">*</span>
					            						</div>
					            		              </div>

					            		              <div class="control-group ">
					            						<label class="control-label" for="focusedInput">Nombre de contacto</label>
					            						<div class="controls">
					            						  <input class="input-xlarge focused" id="contacto" name="contacto" type="text" placeholder="Ingrese contacto">
					            						  <span class="obligatorio">*</span>
					            						</div>
					            		              </div>

									            	</div>

									            	<div class="form-horizontal span6">
														<fieldset>

															<div class="control-group">
																<label>Responsabilidad tributaria <span class="obligatorio">*</span>
																</label>
																<div style="clear:both"></div>
																  <label class="radio">
																	<div class="radio" id="uniform-optionsRadios1">
																			<input type="radio" name="resp_tributaria" id="resp_tributaria" value="Gran contribuyente">
																	</div>
																	<span style="margin-left:20px;">Gran contribuyente</span>
																  </label>
																  <div style="clear:both"></div>
																  <label class="radio">
																	<div class="radio" id="uniform-optionsRadios1">
																			<input type="radio" name="resp_tributaria" id="resp_tributaria" value="Auto retenedor">
																	</div>
																	<span style="margin-left:20px;">Auto retenedor</span>
																  </label>
																  <div style="clear:both"></div>
																  <label class="radio">
																	<div class="radio" id="uniform-optionsRadios1">
																			<input type="radio" name="resp_tributaria" id="resp_tributaria" value="Regimen comun">
																	</div>
																	<span style="margin-left:20px;">Régimen común</span>
																  </label>
																<div style="clear:both"></div>
																  <label class="radio">
																	<div class="radio" id="uniform-optionsRadios1">
																			<input type="radio" name="resp_tributaria" id="resp_tributaria" value="Regimen simplificado">
																	</div>
																	<span style="margin-left:20px;">Régimen simplificado</span>
																  </label>

																
															</div>

															<div class="control-group">
																<label class="control-label">Tipo de empresa <span class="obligatorio">*</span>
																</label>
																<div style="clear:both"></div>
																  <label class="radio">
																	<div class="radio" id="uniform-optionsRadios1">
																			<input type="radio" name="tipo_empresa" id="tipo_empresa" value="Industrial">
																	</div>
																	<span style="margin-left:20px;">Industrial</span>
																  </label>
																  <div style="clear:both"></div>
																  <label class="radio">
																	<div class="radio" id="uniform-optionsRadios1">
																			<input type="radio" name="tipo_empresa" id="tipo_empresa" value="Comercial">
																	</div>
																	<span style="margin-left:20px;">Comercial</span>
																  </label>
																  <div style="clear:both"></div>
																  <label class="radio">
																	<div class="radio" id="uniform-optionsRadios1">
																			<input type="radio" name="tipo_empresa" id="tipo_empresa" value="Servicios">
																	</div>
																	<span style="margin-left:20px;">Servicios</span>
																  </label>
																  <div style="clear:both"></div>
																  <label class="radio">
																	<div class="radio" id="uniform-optionsRadios1">
																			<input type="radio" name="tipo_empresa" id="tipo_empresa" value="Mixto">
																	</div>
																	<span style="margin-left:20px;">Mixto</span>
																  </label>

															</div>

															<div class="control-group">
														  	   <label class="control-label" for="focusedInput">Área del proveedor <span class='obligatorio'>*</span></label>
														  	  	<div class="controls">
														  	  	   <select id="ubicacion" name="ubicacion"></select>
														  	  	</div>
														    </div>
		
								  						</fieldset>						  	
								  					</div>

									            </div>          
							            	</div>					
							            </div>

							              <!-- PESTAÑAS DE INFORMACIÓN GENERAL -->
							        </div>

				        			<div id="referencias_comerciales" class="tab-pane fade">
				            		<!-- PESTAÑA DE REFERENCIAS COMERCIALES -->
				        				<div class="box span12">
				        	
											<div class="row-fluid">

					            				<div class="span6">

													<div class="box-content" style="display: block;">
											            <div class="control-group ">
															<label class="control-label" for="focusedInput">Empresa 1</label>
															<div class="controls">
															  <input id="empresa1" class="input-xlarge focused" name="empresa1" type="text" placeholder="Ingrese empresa">
															  
															</div>
											            </div>

											            <div class="control-group ">
															<label class="control-label" for="focusedInput">Ciudad</label>
															<div class="controls">
															  <input id="ciudad1" class="input-xlarge focused" name="ciudad1" type="text" placeholder="Ingrese ciudad">
															  
															</div>
											            </div>

											            <div class="control-group ">
															<label class="control-label" for="focusedInput">Teléfono</label>
															<div class="controls">
															  <input id="telefono1" class="input-xlarge focused" name="telefono1" type="text" placeholder="Ingrese telefono">
															  
															</div>
											            </div>
										            </div>
												</div>

										        <div class="span6">

													<div class="box-content" style="display: block;">
											            <div class="control-group ">
															<label class="control-label" for="focusedInput">Empresa 2</label>
															<div class="controls">
															  <input id="empresa2" class="input-xlarge focused" name="empresa2" type="text" placeholder="Ingrese empresa">
															  
															</div>
											            </div>

											            <div class="control-group ">
															<label class="control-label" for="focusedInput">Ciudad</label>
															<div class="controls">
															  <input id="ciudad2" class="input-xlarge focused" name="ciudad2" type="text" placeholder="Ingrese ciudad">
															  
															</div>
											            </div>

											            <div class="control-group ">
															<label class="control-label" for="focusedInput">Teléfono</label>
															<div class="controls">
															  <input id="telefono2" class="input-xlarge focused" name="telefono2" type="text" placeholder="Ingrese telefono">
															  
															</div>
											            </div>
										            </div>

										        </div>

											</div>					
						
										</div>

				            			<!-- PESTAÑA DE REFERENCIAS COMERCIALES -->
				        			</div>

							        <div id="forma_pago" class="tab-pane fade">
										<!-- PESTAÑA DE FORMA DE PAGO -->
										<div class="box span12">
				        	
											<div class="row-fluid">
												<div class="span6">
													<div class="control-group">
														<label class="control-label">Forma de pago <span class="obligatorio">*</span>
														</label>

														<div style="clear:both"></div>
														<label class="radio">
															<div class="radio" id="uniform-optionsRadios1">
																<input type="radio" name="forma_pago" id="forma_pago" value="Contado">
															</div>
															<span style="margin-left:20px;">Contado</span>
														</label>

														<div style="clear:both"></div>
														<label class="radio">
															<div class="radio" id="uniform-optionsRadios1">
																<input type="radio" name="forma_pago" id="forma_pago" value="Plazo 30 dias">
															</div>
															<span style="margin-left:20px;">Plazo 30 días</span>
														</label>

														<div style="clear:both"></div>
														<label class="radio">
															<div class="radio" id="uniform-optionsRadios1">
																<input type="radio" name="forma_pago" id="forma_pago" value="Plazo 60 dias">
															</div>
															<span style="margin-left:20px;">Plazo 60 días</span>
														</label>

														<div style="clear:both"></div>
														<label class="radio">
															<div class="radio" id="uniform-optionsRadios1">
																<input type="radio" name="forma_pago" id="forma_pago" value="Plazo 90 dias">
															</div>
															<span style="margin-left:20px;">Plazo 90 días</span>
														</label>

														<div style="clear:both"></div>
														<label class="radio">
															<div class="radio" id="uniform-optionsRadios1">
																<input type="radio" name="forma_pago" id="forma_pago" value="otro">
															</div>
															<span style="margin-left:20px;">Otro</span>
														</label>
														 <!-- style="display:none" -->
														<div id="otra_forma_pago">
															<label style="margin-left:15px;" class="control-label">¿Cuál?</label>&nbsp
															<input id="otro_tipo" name="otro_tipo" type="text" placeholder="Ingrese forma de pago">
															<!-- <span class="obligatorio">*</span> -->
														</div><br>

													</div>

													<div class="control-group ">
				            						<label class="control-label" for="focusedInput">Fecha de diligenciamiento</label>
				            						<div class="controls">
				            						  	<input id="fecha_diligenciamiento" class="input-xlarge focused" name="fecha_diligenciamiento" type="text" placeholder="ingrese fecha de diligenciamiento">
				            						  <span class="obligatorio">*</span>
				            						</div>
				            		              </div>

												</div>

												<div class="span6">

													<div class="box-content" style="display: block;"">
														<label for="focusedInput">Certificado de existencia y representación legal vigente</label>
															<input name="file_certificado" type="file" />
												  	</div>

												  	<div class="box-content" style="display: block;"">
														<label for="focusedInput">Copia del RUT</label>
															<input name="file_rut" type="file" />
												  	</div>

												  	<div class="box-content" style="display: block;"">
														<label for="focusedInput">Copia del Nit o cédula de ciudadanía</label>
															<input name="file_nit_cedula" type="file" />
												  	</div>

												  	<div class="box-content" style="display: block;"">
														<label for="focusedInput">Listado de precios</label>
															<input name="file_precios" type="file" />
												  	</div>

												</div>
											</div>
										</div>	  	
								  	</div>
										
							             <!-- PESTAÑA DE FORMA DE PAGO --> 
							    </div>
				    			</div>
				    			<button type="submit" class="btn btn-primary">Agregar proveedor</button>
								</form>

							</div>
						</div>	  							 
					</fieldset>
			</div>

		</div>			
	</div>

</div>
<!-- end: Content -->
		