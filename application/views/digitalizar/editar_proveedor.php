<!DOCTYPE html>
<html lang="en">
<head>
	
	<!-- start: Meta -->
	<meta charset="utf-8">
	<title>Digitalización</title>
	<meta name="description" content="Bootstrap Metro Dashboard">
	<meta name="author" content="Dennis Ji">
	<meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	<!-- end: Meta -->
	
	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->
	
	<!-- start: CSS kjkjkjk-->
	<link id="bootstrap-style" href="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/css/bootstrap.min.css" rel="stylesheet">
	<link href="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style" href="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/css/style-responsive.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
	<!-- end: CSS -->
	
	<link rel="shortcut icon" href="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/img/favicon.ico">
	<!-- end: Favicon -->
</head>

<body>
	<!-- start: Header -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>

				<a class="brand" href="index.html">
					<img src="../../theme/img/ochoa_logo_blanco.png" class="img-responsive" alt="Responsive image">
					<span class="style_title_nav">Digitalización de archivos</span>
				</a>
								
				<!-- start: Header Menu -->
				<div class="nav-no-collapse header-nav">
					<ul class="nav pull-right">
						<h3 style="text-align: center;">version 1.1</h3>
						<!-- start: User Dropdown -->
						<li class="dropdown">
							<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="halflings-icon white user"></i> 
								<input id="cedula" type="hidden" value="<?php echo $cedula; ?>">
									<?php 
										echo $nombres;
									?>
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li class="dropdown-menu-title">
								</li>
								<form class="form-gla" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Welcome/cerrar_sesion" method="post" accept-charset="utf-8">
									<li class="aline-menu">
										<i class="halflings-icon remove color_ico"></i>
										<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
										<input type="submit" value="Cerrar" class="btn-menu-lg">
									</li>	
								</form>	
								<!-- <li><a href="http://localhost/prueba/index.php/Welcome/form_profile"><i class="halflings-icon user"></i> Perfil</a></li> -->
								<!-- <li><a href="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/"><i class="halflings-icon off"></i> cerrar sesion</a></li> -->
							</ul>
						</li>
						<!-- end: User Dropdown -->
					</ul>
				</div>
				<!-- end: Header Menu -->
				
			</div>
		</div>
	</div>

	<!-- start: Header -->
		<div class="container-fluid-full">
		<div class="row-fluid">
				
				<!-- start: Main Menu -->
				<div id="sidebar-left" class="span2">
					<div class="nav-collapse sidebar-nav">
						<ul class="nav nav-tabs nav-stacked main-menu">
							
							<form class="form-gla" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Welcome/re_inicio" method="post" accept-charset="utf-8">
								<li class="aline-menu">
									<i class="icon-home color_ico"></i>
									<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
									<input type="submit" value="Inicio" class="btn-menu-lg">
								</li>	
							</form>

							<li class="aline-menu">
								<a class="dropmenu" href="#"><i class="icon-folder-close-alt"></i><span class="hidden-tablet btn-menu-lg">Digitalización</span></a>
								<ul style="display: none;">
									<?php if($rol == 'Administrador' or $rol == 'Registro' or $rol == 'Calificador 1-etapa y 4-etapa' or $rol == 'Calificador 2-etapa' or $rol == 'Calificador 3-etapa' or $rol == 'Pagador' or $rol == 'Contabilidad' or $rol == 'Jefe recursos'){ ?>
										<form class="form-gla" id="digitalizacion_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Digital/index" method="post" accept-charset="utf-8">
											<li>
												<a class="submenu subMen" id="digitalizacion_submit"><i class="icon-check"></i>
													<span class="hidden-tablet"> Digitalización</span>
												</a>
											</li>
											<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
											<input type="hidden" name="Digitalización" value="Digitalización" class="btn-menu-lg">
										</form>	
									<?php } ?>

									<?php if($rol == 'Administrador' or $rol == 'Calificador 3-etapa'){ ?>
										<form class="form-gla" id="formato_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Digital/index" method="post" accept-charset="utf-8">
											<li>
												<a class="submenu subMen" id="formato_submit"><i class="icon-check"></i>
													<span class="hidden-tablet"> Creación formatos</span>
												</a>
											</li>
											<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
											<input type="hidden" name="Creación_formatos" value="Creación_formatos" class="btn-menu-lg">
										</form>	
									<?php } ?>
								</ul>
							</li>	

							<?php if($rol == 'Administrador' or $rol == 'Registro' or $rol == 'Calificador 1-etapa y 4-etapa' or $rol == 'Recursos humanos'){ ?>
							<form class="form-gla" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Conceptos/index" method="post" accept-charset="utf-8">
								<li class="aline-menu">
									<i class="icon-pencil color_ico"></i>
									<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
									<input type="submit" value="Conceptos" class="btn-menu-lg">
								</li>	
							</form>
							<?php } ?>

							<li class="aline-menu">
								<a class="dropmenu" href="#"><i class="icon-folder-close-alt"></i><span class="hidden-tablet btn-menu-lg">Aprobaciones</span></a>
								<ul style="display: none;">
									<?php if($rol == 'Administrador' or $rol == 'Calificador 1-etapa y 4-etapa'){ ?>
										<form class="form-gla" id="compras_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Aprobaciones/index" method="post" accept-charset="utf-8">
											<li>
												<a class="submenu subMen" id="compras_submit"><i class="icon-check"></i>
													<span class="hidden-tablet"> Compras</span>
												</a>
											</li>
											<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
											<input type="hidden" name="etapa-1-4" value="Compras" class="btn-menu-lg">
										</form>	
									<?php } ?>	

									<?php if($rol == 'Administrador' or $rol == 'Calificador 2-etapa' or $rol == 'Jefe recursos'){ ?>
										<form class="form-gla" id="director_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Aprobaciones/index" method="post" accept-charset="utf-8">
											<li>
												<a class="submenu subMen" id="director_submit"><i class="icon-signin"></i>
													<span class="hidden-tablet"> Director de area</span>
												</a>
											</li>
											<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
											<input type="hidden" name="etapa-2" value="Director de área" class="btn-menu-lg">
										</form>	
									<?php } ?>
									
									<?php if($rol == 'Administrador' or $rol == 'Calificador 3-etapa'){ ?>
							    	<form class="form-gla" id="control_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Aprobaciones/index" method="post" accept-charset="utf-8">
										<li>
											<a class="submenu subMen" id="control_submit"><i class="icon-download-alt"></i>
												<span class="hidden-tablet"> Control interno</span>
											</a>
										</li>
										<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
										<input type="hidden" name="etapa-3" value="Control interno" class="btn-menu-lg">
									</form>	
									<?php } ?>
									
									<?php if($rol == 'Administrador' or $rol == 'Contabilidad'){ ?>
							    	<form class="form-gla" id="contabilidad_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Aprobaciones/view_contabilidad" method="post" accept-charset="utf-8">
										<li>
											<a class="submenu subMen" id="contabilidad_submit"><i class="icon-tasks"></i>
												<span class="hidden-tablet"> Contabilidad</span>
											</a>
										</li>
										<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
										<input type="hidden" value="Contabilidad" class="btn-menu-lg">
									</form>	
									<?php } ?>
									
									<?php if($rol == 'Administrador' or $rol == 'Pagador'){ ?>
									<form class="form-gla" id="pagos_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Aprobaciones/view_pagos" method="post" accept-charset="utf-8">
										<li>
											<a class="submenu subMen" id="pagos_submit"><i class="icon-money"></i>
												<span class="hidden-tablet"> Pagos</span>
											</a>
										</li>
										<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
										<input type="hidden" value="Pagos" class="btn-menu-lg">
									</form>	
									<?php } ?>

								</ul>	
							</li>
							
					
							<li class="aline-menu">
								<a class="dropmenu" href="#"><i class="icon-group color_ico"></i><span class="hidden-tablet btn-menu-lg">Empleados</span></a>
								<ul style="display: none;">
									<?php if($rol == 'Administrador' or $rol == 'Jefe recursos' or $rol == 'Recursos humanos' or $rol == 'Formatos'){ ?>
										<form class="form-gla" id="empleados_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/empleados/Empleados/index" method="post" accept-charset="utf-8">
											<li>
												<a class="submenu subMen" id="empleados_submit"><i class="icon-group color_ico"></i>
													<span class="hidden-tablet"> Registros</span>
												</a>
											</li>	
											<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
										</form>
									<?php } ?>	

									<?php if($rol == 'Administrador' or $rol == 'Jefe recursos'){ ?>
										<form class="form-gla" id="reportes_empleados_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/empleados/Empleados/reportes" method="post" accept-charset="utf-8">
											<li>
												<a class="submenu subMen" id="reportes_empleados_submit"><i class="icon-check"></i>
													<span class="hidden-tablet"> Reportes</span>
												</a>
											</li>
											<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>										
										</form>	
									<?php } ?>
								</ul>
							</li>
							

							<?php if($rol == 'Administrador'){ ?>
							<form class="form-gla" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Usuarios/index" method="post" accept-charset="utf-8">
								<li class="aline-menu">
									<i class="icon-user-md color_ico"></i>
									<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
									<input type="submit" value="Usuarios" class="btn-menu-lg">
								</li>	
							</form>
							<?php } ?>

							<?php if($rol == 'Administrador' or $rol == 'Jefe recursos'){ ?>
							<form class="form-gla" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/evaluacion/Evaluacion/index" method="post" accept-charset="utf-8">
								<li class="aline-menu">
									<i class="icon-user-md color_ico"></i>
									<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
									<input type="submit" value="Evaluacion" class="btn-menu-lg">
								</li>	
							</form>
							<?php } ?>


							<li class="aline-menu">
								<a class="dropmenu" href="#"><i class="icon-list-alt"></i><span class="hidden-tablet btn-menu-lg">Formatos</span></a>
								<ul style="display: none;">
									<?php if($rol == 'Administrador' or $rol == 'Formatos'){ ?>
										<form class="form-gla" id="datosFormatos_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/formatos/formatos/index" method="post" accept-charset="utf-8">
											<li>
												<a class="submenu subMen" id="datosFormatos_submit"><i class="icon-pencil"></i>
													<span class="hidden-tablet"> Ingresar datos</span>
												</a>
											</li>
											<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
											<input type="hidden" name="Ingresar_datos" value="Ingresar_datos" class="btn-menu-lg">
										</form>	
									<?php } ?>

									<?php if($rol == 'Administrador' or $rol == 'Formatos'){ ?>
										<form class="form-gla" id="gestionFormatos_form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/formatos/formatos/index" method="post" accept-charset="utf-8">
											<li>
												<a class="submenu subMen" id="gestionFormatos_submit"><i class="icon-list-alt"></i>
													<span class="hidden-tablet"> Generar formatos</span>
												</a>
											</li>
											<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
											<input type="hidden" name="Gestión_formatos" value="Gestión_formatos" class="btn-menu-lg">
										</form>	
									<?php } ?>
								</ul>
							</li>

							<?php if($rol == 'Administrador' or $rol == 'Jefe recursos'){ ?>
							<form class="form-gla" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/cuestionario/cuestionario/registros" method="post" accept-charset="utf-8">
								<li class="aline-menu">
									<i class="icon-user-md color_ico"></i>
									<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
									<input type="submit" value="Cuestionario" class="btn-menu-lg">
								</li>	
							</form>
							<?php } ?>

						</ul>
					</div>
				</div>
			<!-- end: Main Menu -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<div id="content" class="span10">
				<ul class="breadcrumb">
					<li>
						<i class="icon-folder-open color_fla"></i>
						<a>Digitalización</a> 
						<i class="icon-angle-right color_fla"></i>
					</li>
				</ul>
				<?php 
					if(isset($message)){
						if($message){?>
						<div class="alert alert-info fade in">
						    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
						    <strong>Info!</strong> Usted debe de diligenciar los campos marcados con <b>*</b>.
						</div>
				<?php 
						}
					}
				 ?>		
				
				 <div class="row-fluid">	
					<div class="box span12">
						<div class="box-header" data-original-title="">
							<h2><i class="halflings-icon edit"></i><span class="break"></span>Informacion del proveedor</h2>
								<!-- <div class="box-icon">
									<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
									<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
								</div> -->
						</div>
						
						<div class="box-content" style="display: block;">
								<fieldset> 
									<div class="row-fluid">	
										<div class="bs-example">
										    <ul class="nav nav-tabs">
										        <li class="active">
										        	<a data-toggle="tab" id="informacion_even" class="font_tabs" href="#informacion_general">Información general</a>
										        </li>
										        <li>
										        	<a data-toggle="tab" id="afiliaciones_even" class="font_tabs" href="#referencias_comerciales">Referencias comerciales</a>
										        </li>
										        <li>
										        	<a data-toggle="tab" id="perfil_even" class="font_tabs" href="#forma_pago">Pago y documentos</a>
										        </li>
											</ul>

											<form class="form-horizontal" id="formulario_p" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/index.php/Digital/update_info_proveedor" method="post" enctype="multipart/form-data">
							   				<input type="hidden" name="cedula" value='<?php echo $cedula; ?>'>
							   				<?php  
							   				$idProveedor = $datos_editar['0']->idProveedor;
							   				?>
							   				<div class="tab-content">
							        			<div id="informacion_general" class="tab-pane fade in active">
							            		<!-- PESTAÑAS DE INFORMACIÓN GENERAL -->
							            			<div class="box span12">
							            			<?php 
											  		$nombreProveedor = $datos_editar['0']->nombreProveedor;
											  		$identificacionProveedor = $datos_editar['0']->identificacionProveedor;
											  		$nitProveedor 	 = $datos_editar['0']->nitProveedor;
											  		$direccionProveedor = $datos_editar['0']->direccionProveedor;
											  		$ciudadProveedor 	 = $datos_editar['0']->ciudadProveedor;
											  		$paisProveedor = $datos_editar['0']->paisProveedor;
											  		$telefonoProveedor 	 = $datos_editar['0']->telefonoProveedor;
											  		$contactoProveedor = $datos_editar['0']->contactoProveedor;
											  		$emailProveedor = $datos_editar['0']->emailProveedor;
											  		$responsabilidadTributaria 	 = $datos_editar['0']->responsabilidadTributaria;
											  		$tipoEmpresa = $datos_editar['0']->tipoEmpresa;
											  		$areaProveedor = $datos_editar['0']->areaProveedor;
											  		?>

										            	<div class="box-content" style="display: block;">
										            		<div class="row-fluid">
												            	<div class="span6">
								            		              <div class="control-group ">
								            						<label class="control-label" for="focusedInput">Nombre o Razón social</label>
								            						<div class="controls">
								            						  <input class="input-xlarge focused" id="nombre" name="nombre" type="text" value="<?php echo $nombreProveedor; ?>">
								            						  <span class="obligatorio">*</span>
								            						</div>
								            		              </div>

								            		              <div class="control-group" style="margin-left:200px">
								            		              	<input type="button" id="btnMostrarIdentificacion" name="btnMostrarIdentificacion" value="Añadir identificacion" class="btn btn-primary" onclick="mostrarIdentificacion()">

																  	<input type="button" id="btnMostrarNit" name="btnMostrarNit" value="Añadir Nit" class="btn btn-primary" onclick="mostrarNit()">
																  </div>

								            		              <div class="control-group" style="display:none" id="campoIdentificacion">
								            						<label class="control-label" for="focusedInput">Nro de identificación</label>
								            						<div class="controls">
								            						  <input class="input-xlarge focused" id="identificacion" name="identificacion" type="text" value="<?php echo $identificacionProveedor; ?>">
								            						  <span class="obligatorio">*</span>
								            						</div>
								            		              </div>

								            		              <div class="control-group" style="display:none" id="campoNit">
								            						<label class="control-label" for="focusedInput">NIT</label>
								            						<div class="controls">
								            						  <input class="input-xlarge focused" id="nit" name="nit" type="text" value="<?php echo $nitProveedor; ?>">
								            						  <span class="obligatorio">*</span>
								            						</div>
								            		              </div>


								            		              <div class="control-group ">
								            						<label class="control-label" for="focusedInput">Dirección</label>
								            						<div class="controls">
								            						  <input class="input-xlarge focused" id="direccion" name="direccion" type="text" value="<?php echo $direccionProveedor; ?>">
								            						  <span class="obligatorio">*</span>
								            						</div>
								            		              </div>

								            		              <div class="control-group ">
								            						<label class="control-label" for="focusedInput">Ciudad</label>
								            						<div class="controls">
								            						  <input class="input-xlarge focused" id="ciudad" name="ciudad" type="text" value="<?php echo $ciudadProveedor; ?>">
								            						  <span class="obligatorio">*</span>
								            						</div>
								            		              </div>

								            		              <div class="control-group ">
								            						<label class="control-label" for="focusedInput">Pais</label>
								            						<div class="controls">
								            						  <input class="input-xlarge focused" id="pais" name="pais" type="text" value="<?php echo $paisProveedor; ?>">
								            						  <span class="obligatorio">*</span>
								            						</div>
								            		              </div>

								            		              <div class="control-group ">
								            						<label class="control-label" for="focusedInput">Teléfono</label>
								            						<div class="controls">
								            						  <input class="input-xlarge focused" id="telefono" name="telefono" type="text" value="<?php echo $telefonoProveedor; ?>">
								            						  <span class="obligatorio">*</span>
								            						</div>
								            		              </div>

								            		              <div class="control-group ">
								            						<label class="control-label" for="focusedInput">E-mail</label>
								            						<div class="controls">
								            						  <input class="input-xlarge focused" id="email" name="email" type="text" value="<?php echo $emailProveedor; ?>">
								            						  <span class="obligatorio">*</span>
								            						</div>
								            		              </div>

								            		              <div class="control-group ">
								            						<label class="control-label" for="focusedInput">Nombre de contacto</label>
								            						<div class="controls">
								            						  <input class="input-xlarge focused" id="contacto" name="contacto" type="text" value="<?php echo $contactoProveedor; ?>">
								            						  <span class="obligatorio">*</span>
								            						</div>
								            		              </div>

												            	</div>

												            	<div class="form-horizontal span6">
																	<fieldset>

																		<div class="control-group">
																			<label>Responsabilidad tributaria <span class="obligatorio">*</span>
																			</label>
																			<div style="clear:both"></div>
																			  <label class="radio">
																				<div class="radio" id="uniform-optionsRadios1">
																					<input type="radio" name="resp_tributaria" id="resp_tributaria" value="Gran contribuyente" <?php if($responsabilidadTributaria=='Gran contribuyente') echo "checked=true"?>>
																				</div>
																				<span style="margin-left:20px;">Gran contribuyente</span>
																			  </label>

																			  <div style="clear:both"></div>
																			  <label class="radio">
																				<div class="radio" id="uniform-optionsRadios1">
																					<input type="radio" name="resp_tributaria" id="resp_tributaria" value="Auto retenedor" <?php if($responsabilidadTributaria=='Auto retenedor') echo "checked=true"?>>
																				</div>
																				<span style="margin-left:20px;">Auto retenedor</span>
																			  </label>

																			  <div style="clear:both"></div>
																			  <label class="radio">
																				<div class="radio" id="uniform-optionsRadios1">
																					<input type="radio" name="resp_tributaria" id="resp_tributaria" value="Regimen comun" <?php if($responsabilidadTributaria=='Regimen comun') echo "checked=true"?>>
																				</div>
																				<span style="margin-left:20px;">Régimen común</span>
																			  </label>

																			<div style="clear:both"></div>
																			  <label class="radio">
																				<div class="radio" id="uniform-optionsRadios1">
																					<input type="radio" name="resp_tributaria" id="resp_tributaria" value="Regimen simplificado" <?php if($responsabilidadTributaria=='Regimen simplificado') echo "checked=true"?>>
																				</div>
																				<span style="margin-left:20px;">Régimen simplificado</span>
																			  </label>
																			
																		</div>

																		<div class="control-group">
																			<label class="control-label">Tipo de empresa <span class="obligatorio">*</span>
																			</label>
																			<div style="clear:both"></div>
																			  <label class="radio">
																				<div class="radio" id="uniform-optionsRadios1">
																					<input type="radio" name="tipo_empresa" id="tipo_empresa" value="Industrial" <?php if($tipoEmpresa=='Industrial') echo "checked=true"?>>
																				</div>
																				<span style="margin-left:20px;">Industrial</span>
																			  </label>

																			  <div style="clear:both"></div>
																			  <label class="radio">
																				<div class="radio" id="uniform-optionsRadios1">
																					<input type="radio" name="tipo_empresa" id="tipo_empresa" value="Comercial" <?php if($tipoEmpresa=='Comercial') echo "checked=true"?>>
																				</div>
																				<span style="margin-left:20px;">Comercial</span>
																			  </label>

																			  <div style="clear:both"></div>
																			  <label class="radio">
																				<div class="radio" id="uniform-optionsRadios1">
																					<input type="radio" name="tipo_empresa" id="tipo_empresa" value="Servicios" <?php if($tipoEmpresa=='Servicios') echo "checked=true"?>>
																				</div>
																				<span style="margin-left:20px;">Servicios</span>
																			  </label>

																			  <div style="clear:both"></div>
																			  <label class="radio">
																				<div class="radio" id="uniform-optionsRadios1">
																					<input type="radio" name="tipo_empresa" id="tipo_empresa" value="Mixto" <?php if($tipoEmpresa=='Mixto') echo "checked=true"?>>
																				</div>
																				<span style="margin-left:20px;">Mixto</span>
																			  </label>

																			  <div class="control-group">
																		  	   <label class="control-label" for="focusedInput">Área del proveedor <span class='obligatorio'>*</span></label>
																		  	   <input type="hidden" id="ubicacion_apr" value="<?php echo $areaProveedor; ?>">
																		  	  	<div class="controls">
																		  	  	   <select id="ubicacion" name="ubicacion"></select>
																		  	  	</div>
																		      </div>

																		</div>
					
											  						</fieldset>						  	
											  					</div>

												            </div>          
										            	</div>					
										            </div>

										              <!-- PESTAÑAS DE INFORMACIÓN GENERAL -->
										        </div>

							        			<div id="referencias_comerciales" class="tab-pane fade">
							            		<!-- PESTAÑA DE REFERENCIAS COMERCIALES -->
							        				<div class="box span12">
							        				<?php  
							        				$nombreReferencia1 	 = $datos_editar['0']->nombreReferencia1;
											  		$ciudadReferencia1 = $datos_editar['0']->ciudadReferencia1;
											  		$telefonoReferencia1 = $datos_editar['0']->telefonoReferencia1;
											  		$nombreReferencia2 	 = $datos_editar['0']->nombreReferencia2;
											  		$ciudadReferencia2 = $datos_editar['0']->ciudadReferencia2;
											  		$telefonoReferencia2 = $datos_editar['0']->telefonoReferencia2;
											  		?>
							        	
														<div class="row-fluid">

								            				<div class="span6">

																<div class="box-content" style="display: block;">
														            <div class="control-group ">
																		<label class="control-label" for="focusedInput">Empresa 1</label>
																		<div class="controls">
																		  <input id="empresa1" class="input-xlarge focused" name="empresa1" type="text" value="<?php echo $nombreReferencia1; ?>">
																		  </div>
														            </div>

														            <div class="control-group ">
																		<label class="control-label" for="focusedInput">Ciudad</label>
																		<div class="controls">
																		  <input id="ciudad1" class="input-xlarge focused" name="ciudad1" type="text" value="<?php echo $ciudadReferencia1; ?>">
																		  </div>
														            </div>

														            <div class="control-group ">
																		<label class="control-label" for="focusedInput">Teléfono</label>
																		<div class="controls">
																		  <input id="telefono1" class="input-xlarge focused" name="telefono1" type="text" value="<?php echo $telefonoReferencia1; ?>">
																		  
																		</div>
														            </div>
													            </div>
															</div>

													        <div class="span6">

																<div class="box-content" style="display: block;">
														            <div class="control-group ">
																		<label class="control-label" for="focusedInput">Empresa 2</label>
																		<div class="controls">
																		  <input id="empresa2" class="input-xlarge focused" name="empresa2" type="text" value="<?php echo $nombreReferencia2; ?>">
																		  
																		</div>
														            </div>

														            <div class="control-group ">
																		<label class="control-label" for="focusedInput">Ciudad</label>
																		<div class="controls">
																		  <input id="ciudad2" class="input-xlarge focused" name="ciudad2" type="text" value="<?php echo $ciudadReferencia2; ?>">
																		  
																		</div>
														            </div>

														            <div class="control-group ">
																		<label class="control-label" for="focusedInput">Teléfono</label>
																		<div class="controls">
																		  <input id="telefono2" class="input-xlarge focused" name="telefono2" type="text" value="<?php echo $telefonoReferencia2; ?>">
																		  
																		</div>
														            </div>
													            </div>

													        </div>

														</div>					
									
													</div>

							            			<!-- PESTAÑA DE REFERENCIAS COMERCIALES -->
							        			</div>

										        <div id="forma_pago" class="tab-pane fade">
													<!-- PESTAÑA DE FORMA DE PAGO -->
													<div class="box span12">
													<?php  
							        				$fechaDiligenciamiento 	 = $datos_editar['0']->fechaDiligenciamiento;
							        				$formaPago = $datos_editar['0']->formaPago;
							        				?>
							        	
														<div class="row-fluid">
															<div class="span6">
																<div class="control-group">
																	<label class="control-label">Forma de pago <span class="obligatorio">*</span>
																	</label>

																	<div style="clear:both"></div>
																	<label class="radio">
																		<div class="radio" id="uniform-optionsRadios1">
																			<input type="radio" name="forma_pago" id="forma_pago" value="Contado" <?php if($formaPago=='Contado') echo "checked=true"?>>
																		</div>
																		<span style="margin-left:20px;">Contado</span>
																	</label>

																	<div style="clear:both"></div>
																	<label class="radio">
																		<div class="radio" id="uniform-optionsRadios1">
																			<input type="radio" name="forma_pago" id="forma_pago" value="Plazo 30 dias" <?php if($formaPago=='Plazo 30 dias') echo "checked=true"?>>
																		</div>
																		<span style="margin-left:20px;">Plazo 30 días</span>
																	</label>

																	<div style="clear:both"></div>
																	<label class="radio">
																		<div class="radio" id="uniform-optionsRadios1">
																			<input type="radio" name="forma_pago" id="forma_pago" value="Plazo 60 dias" <?php if($formaPago=='Plazo 60 dias') echo "checked=true"?>>
																		</div>
																		<span style="margin-left:20px;">Plazo 60 días</span>
																	</label>

																	<div style="clear:both"></div>
																	<label class="radio">
																		<div class="radio" id="uniform-optionsRadios1">
																			<input type="radio" name="forma_pago" id="forma_pago" value="Plazo 90 dias" <?php if($formaPago=='Plazo 90 dias') echo "checked=true"?>>
																		</div>
																		<span style="margin-left:20px;">Plazo 90 días</span>
																	</label>

																	<div style="clear:both"></div>
																	<label class="radio">
																		<div class="radio" id="uniform-optionsRadios1">
																		<input type="radio" name="forma_pago" id="forma_pago" value="otro" <?php if(($formaPago!='Contado') && ($formaPago!='Plazo 30 dias') && ($formaPago!='Plazo 60 dias') && ($formaPago!='Plazo 90 dias')) echo "checked=true"?>>
																		</div>
																		<span style="margin-left:20px;">Otro</span>
																	</label>
																	 <!-- style="display:none" -->
																	<div id="otra_forma_pago">
																		<label style="margin-left:15px;" class="control-label">¿Cuál?</label>&nbsp
																		<input id="otro_tipo" name="otro_tipo" type="text" value="<?php echo $formaPago; ?>">
																
																		<!-- <span class="obligatorio">*</span> -->
																	</div><br>

																</div>

																<div class="control-group ">
							            						<label class="control-label" for="focusedInput">Fecha de diligenciamiento</label>
							            						<div class="controls">
							            						  	<input id="fecha_diligenciamiento" class="input-xlarge focused" name="fecha_diligenciamiento" type="text" value="<?php echo $fechaDiligenciamiento; ?>">
							            						  <span class="obligatorio">*</span>
							            						</div>
							            		              </div>



															</div>

															<div class="span6">

																<div class="box-content" style="display: block;"">
																	<label for="focusedInput">Certificado de existencia y representación legal vigente</label>
																		<input name="file_certificado" type="file" />
															  	</div>

															  	<div class="box-content" style="display: block;"">
																	<label for="focusedInput">Copia del RUT</label>
																		<input name="file_rut" type="file" />
															  	</div>

															  	<div class="box-content" style="display: block;"">
																	<label for="focusedInput">Copia del Nit o cédula de ciudadanía</label>
																		<input name="file_nit_cedula" type="file" />
															  	</div>

															  	<div class="box-content" style="display: block;"">
																	<label for="focusedInput">Listado de precios</label>
																		<input name="file_precios" type="file" />
															  	</div>

															</div>
														</div>
													</div>

													<div class="span12" style="margin-left: 0px;">
													<?php  
													$rutaCertificadoRepresentacion = $datos_editar['0']->rutaCertificadoRepresentacion;
										        	$rutaRut = $datos_editar['0']->rutaRut;
										        	$rutaNitCedula = $datos_editar['0']->rutaNitCedula;
										        	$rutaPrecios = $datos_editar['0']->rutaPrecios;
										        	?>

														<div class="row-fluid">
															
																<div class="span6" style="float:left; margin-left: 5px;">
																	<div class="control-group">
																	<?php 
																	$xt = explode('.', $rutaCertificadoRepresentacion);
																	if($xt[1] == 'pdf'){
																		echo "<h4>Certificado de existencia y representación legal vigente</h4><br>". '<object type="application/pdf" data="'.$rutaCertificadoRepresentacion.'"width="500" height="450"></object>';
																	}elseif($xt[1] == 'jpg'){
																		echo"<h4>Certificado de existencia y representación legal vigente</h4><br>". '<img src="'.$rutaCertificadoRepresentacion.'" class="img-responsive" alt="Responsive image">';
																	}elseif($xt[1] == 'png'){
																		echo"<h4>Certificado de existencia y representación legal vigente</h4><br>". '<img src="'.$rutaCertificadoRepresentacion.'" class="img-responsive" alt="Responsive image">';
																	}
																	?>
		            												</div>
		            											</div>

		            											<div class="span6" style="float:left; margin-left: 5px;">
		            												<div class="control-group">
																	<?php 
																	$xt = explode('.', $rutaRut);
																	if($xt[1] == 'pdf'){
																		echo"<h4>Rut</h4><br>". '<object type="application/pdf" data="'.$rutaRut.'"width="500" height="450"></object>';
																	}elseif($xt[1] == 'jpg'){
																		echo "<h4>Rut</h4><br>".'<img src="'.$rutaRut.'" class="img-responsive" alt="Responsive image">';
																	}elseif($xt[1] == 'png'){
																		echo"<h4>Rut</h4><br>". '<img src="'.$rutaRut.'" class="img-responsive" alt="Responsive image">';
																	}
																	?>
		            												</div>
		            											</div>
															
		            											<div class="span6" style="float:left; margin-left: 5px;">
																	<div class="control-group">
																	<?php 
																	$xt = explode('.', $rutaNitCedula);
																	if($xt[1] == 'pdf'){
																		echo"<h4>Identificacion / Nit</h4><br>". '<object type="application/pdf" data="'.$rutaNitCedula.'"width="500" height="450"></object>';
																	}elseif($xt[1] == 'jpg'){
																		echo"<h4>Identificacion / Nit</h4><br>". '<img src="'.$rutaNitCedula.'" class="img-responsive" alt="Responsive image">';
																	}elseif($xt[1] == 'png'){
																		echo"<h4>Identificacion / Nit</h4><br>". '<img src="'.$rutaNitCedula.'" class="img-responsive" alt="Responsive image">';
																	}
																	?>
		            												</div>
		            											</div>

		            											<div class="span6" style="float:left; margin-left: 5px;">
		            												<div class="control-group">
																	<?php 
																	$xt = explode('.', $rutaPrecios);
																	if($xt[1] == 'pdf'){
																		echo"<h4>Listado de precios</h4><br>". '<object type="application/pdf" data="'.$rutaPrecios.'"width="500" height="450"></object>';
																	}elseif($xt[1] == 'jpg'){
																		echo"<h4>Listado de precios</h4><br>". '<img src="'.$rutaPrecios.'" class="img-responsive" alt="Responsive image">';
																	}elseif($xt[1] == 'png'){
																		echo"<h4>Listado de precios</h4><br>". '<img src="'.$rutaPrecios.'" class="img-responsive" alt="Responsive image">';
																	}
																	?>
	            													</div>
	            												</div>

        													
        												</div>					
							        	
							        					<input type="hidden" name="file_dlt_certificado" value="<?php echo $rutaCertificadoRepresentacion; ?>" />
							        					<input type="hidden" name="file_dlt_rut" value="<?php echo $rutaRut; ?>" />
							        					<input type="hidden" name="file_dlt_nitCedula" value="<?php echo $rutaNitCedula; ?>" />
							        					<input type="hidden" name="file_dlt_precios" value="<?php echo $rutaPrecios; ?>" />
														

													</div>

											  	</div>
													
										             <!-- PESTAÑA DE FORMA DE PAGO --> 
										    </div>
										    <input type="hidden" name="idProveedor" value="<?php echo $idProveedor; ?>">
							    		</div>


							    		<button type="submit" class="btn btn-primary">Actualizar proveedor</button>
										</form>

										</div>
									</div>	  							 
								</fieldset>
						</div>

					</div>			
				</div>
			
				
			</div>
	
			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->
		
	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Settings</h3>
		</div>
		<div class="modal-body">
			<p>Here settings can be configured...</p>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
			<a href="#" class="btn btn-primary">Save changes</a>
		</div>
	</div>
	
	<div class="clearfix"></div>
	<footer>
		<p>
			<span style="text-align:left;float:left">&copy; 2016 <a href="http://http://www.apuestasochoa.com.co/">Desarrollo interno - apuestasochoa</a></span>
		</p>
	</footer>
	<!-- start: JavaScript-->
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery-1.9.1.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery-migrate-1.0.0.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery-ui-1.10.0.custom.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.ui.touch-punch.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/modernizr.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/bootstrap.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.cookie.js"></script>
		<script src='http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/fullcalendar.min.js'></script>
		<script src='http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.dataTables.min.js'></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/excanvas.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.flot.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.flot.pie.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.flot.stack.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.flot.resize.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.chosen.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.uniform.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.cleditor.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.noty.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.elfinder.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.raty.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.iphone.toggle.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.uploadify-3.1.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.gritter.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.imagesloaded.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.masonry.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.knob.modified.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/jquery.sparkline.min.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/counter.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/retina.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/custom.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/apuestasochoa.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/prueba/proveedor.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/prueba/evaluar_proveedor.js"></script>
		<script src="http://<?php echo $_SERVER['HTTP_HOST'];?>/prueba/theme/js/empleados/nuevo.js"></script>
	<!-- end: JavaScript-->
	
</body>
</html>
