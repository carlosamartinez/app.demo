<?php
require_once(APPPATH.'libraries/jpgraph/src/jpgraph.php');
require_once(APPPATH.'libraries/jpgraph/src/jpgraph_bar.php');
require_once(APPPATH.'libraries/jpgraph/src/themes/OceanTheme.class.php');
require_once(APPPATH.'libraries/jpgraph/src/jpgraph_pie.php');
require_once(APPPATH.'libraries/jpgraph/src/jpgraph_pie3d.php');
require_once(APPPATH.'libraries/fpdf/fpdf.php');
class Empleados_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function get_empleados()
    {        
        $this->db->select('nombres, apellidos, cedula, estado, fecha_ingreso, cargo_laboral, empleados.id_empleado');
        $this->db->from('empleados');
        $this->db->join('perfiles', 'perfiles.id_empleado = empleados.id_empleado', 'left');

        $query_employee = $this->db->get();
        $result_employee = array();
        $result_employee = $query_employee->result();

        return $result_employee;
    }

    function get_cargos()
    {
        $this->db->select('nombre_cargo');
        $this->db->from('cargos');
        $this->db->group_by("nombre_cargo"); 
        
        $query = $this->db->get();
        $result_consul = array();
        $result_consul = $query->result();         
        return $result_consul;
    }

    // validamos si el usuario existe
    function cons_exit($cedula)
    {
        $this->db->select('cedula');
        $this->db->from('empleados');
        $this->db->where('cedula', $cedula); 
        $query = $this->db->get();
        $retu = $query->result_id->num_rows;
       
        return $retu;
    }

    // por medio de esta funcion guardamos los datos del empleado y guardamos las imagenes cargadas por el usuario
    // donde se anexa constancias de la hoja de vida
    function insert_empleados($empleado)
    {
        $this->db->select('cedula');
        $this->db->from('empleados');
        $this->db->where('cedula', $empleado['cedula']);
        $query = $this->db->get();

        if($query->result_id->num_rows){
            return false;
        }else{
            // iteracion para guardar todas las imagenes que cargen 
            $imagenes_hoja_vida = array();
            $medidas = array();

            $prefijo = substr(md5(uniqid(rand())),0,6);
            $zip = new ZipArchive;
            ///////////////////////////////----------------------------------------------------------------/////////////////////////////
            if($zip->open($carpeta = $_SERVER['DOCUMENT_ROOT'].'/prueba/img_empleados/documents_resumes/'.$prefijo.'.zip', ZipArchive::CREATE) === TRUE){
                for($i=0; $i < count($_FILES["upload"]["name"]); $i++){
                    $archivo = $_FILES["upload"]['name'][$i];                  
                    $zip->addFile($_FILES['upload']['tmp_name'][$i], substr(md5(uniqid(rand())),0,6).'_'.$archivo);
                }    
                    $zip->close();
                    // echo 'ok';
                    // echo "<br>";
                    // echo $carpeta;
            }else{
                    echo 'failed';
                    die('problemas al cargar pdfs');
            }


            if(!empty($_FILES["medidas"]['name'][0])){
                if($zip->open($carpeta_medi = $_SERVER['DOCUMENT_ROOT'].'/prueba/img_empleados/medidas_diciplinarias/'.$prefijo.'.zip', ZipArchive::CREATE) === TRUE){
                    for($i=0; $i < count($_FILES["medidas"]["name"]); $i++){
                        $archivo = $_FILES["medidas"]['name'][$i];                  
                        $zip->addFile($_FILES['medidas']['tmp_name'][$i], substr(md5(uniqid(rand())),0,6).'_'.$archivo);
                    }    
                        $zip->close();
                        // echo 'ok';
                        // echo "<br>";
                        // echo $carpeta_medi;
                }else{
                        echo 'failed';
                        die('problemas al cargar pdfs');
                }
            }
            
            // echo "<pre>";
            //     print_r($_FILES["upload"]["name"]);
            // echo "</pre>";
            // die('here');
            /////////////////////////////--------------------------------------------------------------////////////////////////////////
        
            // for($i=0; $i < count($_FILES["upload"]["name"]) ; $i++){ 
            //     $archivo = $_FILES["upload"]['name'][$i];
            //     $prefijo = substr(md5(uniqid(rand())),0,6);
            //     $destino = "../prueba/img_empleados/documents_resumes/".$prefijo."_".$archivo;
            //     if(copy($_FILES['upload']['tmp_name'][$i],$destino)){
            //         $status = "Archivo subido: <b>".$archivo."</b>";
            //     }else{
            //         die('comuniquese con el desarrollador el sistema tubo problemas con el o los pdf que esta cargando');
            //     }
            //     $imagenes_hoja_vida[] = substr($destino, 2);
            // }

            // if(!empty($_FILES["medidas"]['name'][0])){
            //     for($i=0; $i < count($_FILES["medidas"]["name"]) ; $i++){ 
            //         $archivo = $_FILES["medidas"]['name'][$i];
            //         $prefijo = substr(md5(uniqid(rand())),0,6);
            //         $destino = "../prueba/img_empleados/medidas_diciplinarias/".$prefijo."_".$archivo;
            //         if(copy($_FILES['medidas']['tmp_name'][$i],$destino)){
            //             $status = "Archivo subido: <b>".$archivo."</b>";
            //         }else{
            //             die('comuniquese con el desarrollador el sistema tubo problemas con el o los pdf que esta cargando');
            //         }
            //         $medidas[$i] = substr($destino, 2);
            //     }
            // }

            // get destinos o rutas de las imagnes
            // $foto_perfil = substr($destino, 2);
            // $img_adjun = json_encode($imagenes_hoja_vida);
            // $medidas_m = json_encode($medidas);

            // $empleado['foto_perfil'] = $foto_perfil;
            $this->db->insert('empleados', $empleado);

            $this->db->select_max('id_empleado');
            $this->db->from('empleados');
            $query = $this->db->get();
            $id_max = array();
            $id_max = $query->result();

            if($return['medidas_diciplinarias']){
                $return = array('id_empleado' => $id_max[0]->id_empleado, 'imagenes_hoja_vida' => $carpeta, 'medidas_diciplinarias' => $carpeta_medi);
            }else{
                $return = array('id_empleado' => $id_max[0]->id_empleado, 'imagenes_hoja_vida' => $carpeta);
            }
            return $return;
        }
    }

    function insert_empleados_perfil($perfil_emple)
    {
        $this->db->insert('perfiles', $perfil_emple);
    }

    function insert_empleados_afiliaci($afiliaciones)
    {
        $this->db->insert('afiliaciones', $afiliaciones);
    }

    function info_selec($id)
    {
        $this->db->select('nombres, apellidos, cedula, direccion, telefono_fijo, celular, estado, fecha_ingreso, arl, eps, pension,cesantias, cargo_laboral, departamento, imagenes_hoja_vida, medidas_diciplinarias, carnet_img, empleados.id_empleado');
        $this->db->from('empleados');
        $this->db->join('afiliaciones', 'empleados.id_empleado = afiliaciones.id_empleado', 'left');
        $this->db->join('perfiles', 'afiliaciones.id_empleado = perfiles.id_empleado', 'left');
        $this->db->where('empleados.id_empleado', $id);

        $query = $this->db->get();
        $info_retun = array();
        $info_retun = $query->result();

        return $info_retun;
    }

    // funcion encargada de eliiminar documentos adjuntos hoja de vida
    function delete_doc_espe($dele_doc)
    {
        $this->db->select('imagenes_hoja_vida');
        $this->db->from('perfiles');
        $this->db->where('id_empleado', $dele_doc['id_empleado']);

        $query = $this->db->get();
        $info_retun = array();
        $info_retun = $query->result();

        $docs = json_decode($info_retun[0]->imagenes_hoja_vida);

        for($i=0; $i < count($docs); $i++){ 
            if($dele_doc['doc'] == $docs[$i])
            {
                unset($docs[$i]);
            }
        }

        $dat = array('imagenes_hoja_vida' => json_encode($docs));

        $respu = $this->db->where('id_empleado', $dele_doc['id_empleado']);
        $respu = $this->db->update('perfiles', $dat);

        unlink('..'.$dele_doc['doc']);
        echo $respu;
        die();  
    }

    // funcion encargada de eliiminar documentos adjuntos hoja de vida
    function delete_doc_espe_med($dele_doc)
    {
        $this->db->select('medidas_diciplinarias');
        $this->db->from('perfiles');
        $this->db->where('id_empleado', $dele_doc['id_empleado']);

        $query = $this->db->get();
        $info_retun = array();
        $info_retun = $query->result();

        $docs = json_decode($info_retun[0]->medidas_diciplinarias);

        for($i=0; $i < count($docs); $i++){ 
            if($dele_doc['doc'] == $docs[$i])
            {
                unset($docs[$i]);
            }
        }

        $dat = array('medidas_diciplinarias' => json_encode($docs));

        $respu = $this->db->where('id_empleado', $dele_doc['id_empleado']);
        $respu = $this->db->update('perfiles', $dat);

        unlink('..'.$dele_doc['doc']);
        echo $respu;
        die();
       
    }

    // actualizamos la informacion del empleado en la entidad (EMPLEADOS)
    // teniendo en cuenta que se hizo logica para cuando se cambien las imagene de perfil
    function update_emple($empledos_modif)
    {
        $this->db->where('id_empleado', $empledos_modif['id_empleado']);
        $this->db->update('empleados', $empledos_modif);
    }

    // funcion encargada de actualizar cambios de afiliaciones en los empleados (AFILICACIONES)
    function update_afili($afiliaciones_modif)
    {
        $this->db->where('id_empleado', $afiliaciones_modif['id_empleado']);
        $this->db->update('afiliaciones', $afiliaciones_modif);
    }

    function update_perfil($perfiles_modif)
    {
        // $this->db->select('imagenes_hoja_vida, medidas_diciplinarias, carnet_img');
        // $this->db->from('perfiles');
        // $this->db->where('id_empleado', $perfiles_modif['id_empleado']);
        // $query = $this->db->get();
        // $info_retun = array();
        // $info_retun = $query->result();


        // $image_exist = array();
        // $medidas_exist = array();
        // $image_exist = json_decode($info_retun[0]->imagenes_hoja_vida);
        // $medidas_exist = json_decode($info_retun[0]->medidas_diciplinarias);
        // $del_carnet_old = $info_retun[0]->carnet_img;

        //------------------------------------------------------------------------------------------------------------------------///
        $id = $this->input->post('id_empleado');
        $this->db->select('imagenes_hoja_vida, medidas_diciplinarias');
        $this->db->from('perfiles');
        $this->db->where('id_empleado', $id);
        $query = $this->db->get();
        $info_retun = array();
        $info_retun = $query->result();

        $zip = new ZipArchive;
        if(!empty($_FILES["upload"]['name'][0])){
            if($zip->open($info_retun[0]->imagenes_hoja_vida) === TRUE){
                for($i=0; $i < count($_FILES["upload"]["name"]) ; $i++){
                    $zip->addFile($_FILES['upload']['tmp_name'][$i], substr(md5(uniqid(rand())),0,6).'_'.$_FILES["upload"]['name'][$i]);
                }
            }else{
                echo 'failed';
                die('problemas al importar archivos pdf');
            }
            $perfiles_modif['imagenes_hoja_vida'] = $info_retun[0]->imagenes_hoja_vida;
        }   

        if(!empty($_FILES["image_medidas"]['name'][0])){
            if(!$info_retun[0]->medidas_diciplinarias){
                $prefijo = substr(md5(uniqid(rand())),0,6);
                if($zip->open($carpeta = $_SERVER['DOCUMENT_ROOT'].'/prueba/img_empleados/medidas_diciplinarias/'.$prefijo.'.zip', ZipArchive::CREATE) === TRUE){
                    for($i=0; $i < count($_FILES["image_medidas"]["name"]); $i++){
                        $archivo = $_FILES["image_medidas"]['name'][$i];
                        $zip->addFile($_FILES['image_medidas']['tmp_name'][$i], substr(md5(uniqid(rand())),0,6).'_'.$archivo);
                    }    
                }
                $perfiles_modif['medidas_diciplinarias'] = $carpeta;
            }else if($zip->open($info_retun[0]->medidas_diciplinarias) === TRUE){
                for($i=0; $i < count($_FILES["image_medidas"]["name"]) ; $i++){ 
                    $zip->addFile($_FILES['image_medidas']['tmp_name'][$i], substr(md5(uniqid(rand())),0,6).'_'.$_FILES["image_medidas"]['name'][$i]);                 
                } 
                $perfiles_modif['medidas_diciplinarias'] = $info_retun[0]->medidas_diciplinarias;
            }else{
                die('problemas al importar archivos pdf');
            }
            $zip->close();
        }        
        ///-----------------------------------------------------------------------------------------------------------------------//
  
        $respues = $this->db->where('id_empleado', $perfiles_modif['id_empleado']);
        $respues = $this->db->update('perfiles', $perfiles_modif);

        return $respues;

    }

    // por medio de esta funcion podemos realizar la consulta de la hoja de vida basica 
    function get_report($id)
    {
        // select bd of join dates
        $this->db->select('nombres, apellidos, cedula, direccion, telefono_fijo, celular, fecha_ingreso, arl, eps, pension,cesantias, cargo_laboral, departamento,imagenes_hoja_vida');
        $this->db->from('empleados');
        $this->db->join('afiliaciones', 'empleados.id_empleado = afiliaciones.id_empleado', 'left');
        $this->db->join('perfiles', 'afiliaciones.id_empleado = perfiles.id_empleado', 'left');
        $this->db->where('empleados.id_empleado', $id);

        $query = $this->db->get();
        $info_retun = array();
        $info_retun = $query->result();

        return $info_retun;

    }


    // por medio de esta funcion podemos realizar la consulta de la hoja de vida basica 
    function get_report_complet($id)
    {
        // select bd of join dates
        $this->db->select('nombres, apellidos, cedula, imagenes_hoja_vida, medidas_diciplinarias');
        $this->db->from('empleados');
        $this->db->join('afiliaciones', 'empleados.id_empleado = afiliaciones.id_empleado', 'left');
        $this->db->join('perfiles', 'afiliaciones.id_empleado = perfiles.id_empleado', 'left');
        $this->db->where('empleados.id_empleado', $id);

        $query = $this->db->get();
        $info_retun = array();
        $info_retun = $query->result();

        $da = json_decode($info_retun['0']->imagenes_hoja_vida);
        $de = json_decode($info_retun['0']->medidas_diciplinarias);

        unset($info_retun['0']->imagenes_hoja_vida);
        unset($info_retun['0']->medidas_diciplinarias);

        $hojacompl = array();
        $hojacompl = array_merge($da,$de);

        $info_retun[0]->hoja_vida_compl = json_encode($hojacompl);
        return $info_retun;

    }


    function get_report_medida($id)
    {
        // select bd of join dates
        $this->db->select('nombres, apellidos, cedula, direccion, telefono_fijo, celular, fecha_ingreso, arl, eps, pension,cesantias, cargo_laboral, medidas_diciplinarias');
        $this->db->from('empleados');
        $this->db->join('afiliaciones', 'empleados.id_empleado = afiliaciones.id_empleado', 'left');
        $this->db->join('perfiles', 'afiliaciones.id_empleado = perfiles.id_empleado', 'left');
        $this->db->where('empleados.id_empleado', $id);

        $query = $this->db->get();
        $info_retun = array();
        $info_retun = $query->result();

        return $info_retun;
    }

    // por medio de esta funcion podemos obtener los
    // empleados proximos a salir a vacaciones el presente mes
    function get_empleados_proximos_vacaciones($dependencia)
    {
        // obternmos la fecha de 15 dias mas tarde en metodo orientado
         // a objetos (parametro para la consulra en la iteracion)
        $fecha = new DateTime();
        $fecha->add(new DateInterval('P15D'));
        $fecha_hasta = $fecha->format('m-d');

        // obtenemos la informacion de la fecha actual para pasar 
        // como parametro a la consulta que se itera en el foreach
        $hoy = getdate();

        $fecha_desde = $hoy['mon']."-".$hoy['mday'];
    
        //por medio de esta consulta extraemos las fechas que 
        // tenemos para realizar consultas en la iteracion en base a la fecha
        $this->db->select('empleados.fecha_ingreso');
        $this->db->from('empleados');
        $this->db->join('perfiles', 'perfiles.id_empleado = empleados.id_empleado', 'left');
        $this->db->where('perfiles.departamento', $dependencia);
        $this->db->group_by("empleados.fecha_ingreso");
        $this->db->order_by("empleados.fecha_ingreso", "asc"); 
        $query = $this->db->get();

        $info_retun = array();
        $info_retun = $query->result();

        // inicializamos variable que guarda los años cuando estos sean diferentes
        $year_cache = 0;
        $empleados_vacaciones = array();

        foreach($info_retun as $key => $value){ 
            
            $porciones = explode("-", $info_retun[$key]->fecha_ingreso);            
            $year = $porciones[0];

            if($year != $year_cache){

                $year_cache = $year;
                $this->db->select('empleados.id_empleado,empleados.nombres,empleados.apellidos,empleados.fecha_ingreso,perfiles.cargo_laboral,perfiles.departamento');
                $this->db->from('empleados');
                $this->db->join('perfiles', 'perfiles.id_empleado = empleados.id_empleado', 'left');
                $this->db->where('perfiles.departamento', $dependencia);
                $this->db->where('empleados.fecha_ingreso >=', $year_cache.'-'.$fecha_desde);
                $this->db->where('empleados.fecha_ingreso <=', $year_cache.'-'.$fecha_hasta);
                $this->db->where('empleados.bandera_view_vacaciones', 0);

                $query_vaca = $this->db->get();

                if(!empty($query_vaca->result()[0])){
                    $empleados_vacaciones[] = $query_vaca->result();
                }                
            }            
        }

        $empleados_full_vaca = array();
        for($i=0; $i < count($empleados_vacaciones); $i++){
            for ($x=0; $x < count($empleados_vacaciones[$i]) ; $x++){ 
                $empleados_full_vaca[] = $empleados_vacaciones[$i][$x];
            }
        }

        if(!empty($_POST['notificacion'])){

            $nombres = array();           

            for($i=0; $i < count($empleados_full_vaca); $i++){ 
                $nombres[] = $empleados_full_vaca[$i]->nombres;
            } 

            $empleados_proximos_vaca = json_encode($nombres);
            echo $empleados_proximos_vaca;
            die();  
        }

        //retornamos los empleados que estan proximos a cumplirse su fecha
        //de vacaciones para que el los jefes de areas puedan determinar si aprueban las vacaciones de los mismos
        return $empleados_full_vaca;
    }

    // insertamos la infomacion de los empleados en vacaciones
    function inser_vaca_emple($employee_registre_vaca)
    {        
        $fecha_arr_inicial = explode("/", $employee_registre_vaca['fecha_inicial']);
        $employee_registre_vaca['fecha_inicial'] = $fecha_arr_inicial[2].'-'.$fecha_arr_inicial[1].'-'.$fecha_arr_inicial[0];

        $fecha_arr_final = explode("/", $employee_registre_vaca['fecha_final']);
        $employee_registre_vaca['fecha_final'] = $fecha_arr_final[2].'-'.$fecha_arr_final[1].'-'.$fecha_arr_final[0];

        $resul = $this->db->insert('empleados_vacaciones', $employee_registre_vaca);
        return $resul;
    }

    // actualizamos el estado del empleado ---> (vacaciones)
    function inser_vaca_estado($update_esta_vaca)
    {   
        $data = array();
    
        $data['bandera_view_vacaciones'] = $update_esta_vaca['bandera_view_bacaciones'];
        $data['fecha_establece_bandera'] = $update_esta_vaca['fecha_establece_bandera'];
        $id = $update_esta_vaca['id_empleado'];
    
        $respu = $this->db->where('id_empleado', $id);
        $respu = $this->db->update('empleados', $data);
        return $respu;
    }

    // funcion encargada de retornar empleados de area 
    function employeee_area($dependencia)
    {
        $this->db->select('empleados.nombres, empleados.apellidos, empleados.fecha_ingreso, perfiles.cargo_laboral, perfiles.departamento');
        $this->db->from('empleados');
        $this->db->join('perfiles', 'perfiles.id_empleado = empleados.id_empleado', 'left');
        $this->db->where('perfiles.departamento', $dependencia);
        $query = $this->db->get();
        $empleados_area = array();
        $empleados_area = $query->result();
        return $empleados_area;       
    }


    // funcion encargada de retornar empleados de area con su estado activo
    function employeee_area_activos($dependencia)
    {
        $this->db->select('empleados.nombres, empleados.apellidos, empleados.fecha_ingreso, perfiles.cargo_laboral, perfiles.departamento');
        $this->db->from('empleados');
        $this->db->join('perfiles', 'perfiles.id_empleado = empleados.id_empleado', 'left');
        $this->db->where('perfiles.departamento', $dependencia);
        $this->db->where('empleados.bandera_view_vacaciones', 0);
        $query = $this->db->get();
        $empleados_area_activos = array();
        $empleados_area_activos = $query->result();
        return $empleados_area_activos;       
    }

    function employeee_area_vacaciones($dependencia)
    {
        $this->db->select('empleados.nombres, empleados.apellidos, empleados.fecha_ingreso, perfiles.cargo_laboral, perfiles.departamento');
        $this->db->from('empleados');
        $this->db->join('perfiles', 'perfiles.id_empleado = empleados.id_empleado', 'left');
        $this->db->where('perfiles.departamento', $dependencia);
        $this->db->where('empleados.bandera_view_vacaciones', 1);
        $query = $this->db->get();
        $empleados_area_vacaciones = array();
        $empleados_area_vacaciones = $query->result();
        return $empleados_area_vacaciones;       
    }

    function get_zip_hoja_vida($id)
    {
        $this->db->select('imagenes_hoja_vida');
        $this->db->from('perfiles');
        $this->db->where('id_empleado', $id);
        $query = $this->db->get();
        $ok = array();
        $ok = $query->result();

        header($_SERVER['SERVER_PROTOCOL'].' 200 OK');
        header("Content-Type: application/zip");
        header("Content-Transfer-Encoding: Binary");
        header("Content-Length: ".filesize($ok[0]->imagenes_hoja_vida));
        header("Content-Disposition: attachment; filename=\"".basename($ok[0]->imagenes_hoja_vida)."\"");
        readfile($ok[0]->imagenes_hoja_vida);
    }

    function get_zip_medidias($id)
    {
        $this->db->select('medidas_diciplinarias');
        $this->db->from('perfiles');
        $this->db->where('id_empleado', $id);
        $query = $this->db->get();
        $ok = array();
        $ok = $query->result();

        header($_SERVER['SERVER_PROTOCOL'].' 200 OK');
        header("Content-Type: application/zip");
        header("Content-Transfer-Encoding: Binary");
        header("Content-Length: ".filesize($ok[0]->medidas_diciplinarias));
        header("Content-Disposition: attachment; filename=\"".basename($ok[0]->medidas_diciplinarias)."\"");
        readfile($ok[0]->medidas_diciplinarias);
    }
  
    function get_info_reportes_empleados(){        

        // empleados
        $this->db->select('COUNT(*) FROM `empleados` where `estado` LIKE "Activo"');        
        $query = $this->db->get();
        $empleados_activos = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `empleados` where `estado` LIKE "Inactivo"');        
        $query = $this->db->get();
        $empleados_inactivos = json_decode(json_encode($query->result()), True);

        $empleados = array('empleados_activos' => $empleados_activos[0]['COUNT(*)'], 'empleados_inactivos' => $empleados_inactivos[0]['COUNT(*)']);

        // grafica empleados

        $datay=array($empleados['empleados_activos'],$empleados['empleados_inactivos']);

        // Create the graph. These two calls are always required
        $graph = new Graph(800,720,'auto');
        
        $graph->SetScale("textlin");
        $theme_class = new OceanTheme();
        $graph->SetTheme($theme_class);
        //$theme_class="DefaultTheme";
        //$graph->SetTheme(new $theme_class());

        // set major and minor tick positions manually
        $graph->yaxis->SetTickPositions(array(0,100,200,300,400,500,600,700,800,900,1000), array(50,150,250,350,450,550,650,750,850,950));
        $graph->SetBox(false);

        //$graph->ygrid->SetColor('gray');
        $graph->ygrid->SetFill(false);
        $graph->xaxis->SetTickLabels(array('Activos '.$empleados['empleados_activos'],'Inactivos '.$empleados['empleados_inactivos']));
        $graph->xaxis->SetFont(FF_ARIAL,FS_NORMAL,15);
        $graph->yaxis->HideLine(false);
        $graph->yaxis->HideTicks(false,false);

        // Create the bar plots
        $b1plot = new BarPlot($datay);

        // ...and add it to the graPH
        $graph->Add($b1plot);


        $b1plot->SetColor("white");
        $b1plot->SetFillGradient("#188EAF","white",GRAD_LEFT_REFLECTION);
        $b1plot->SetWidth(45);
        // $graph->title->Set("Reporte(estado empleados)");

        // Display the graph
        $graph->Stroke(_IMG_HANDLER);

        $fileName = APPPATH.'img\empleados1.png';
        $graph->img->Stream($fileName);

        // // grafica de pastl

        // Some data
        $data = array($empleados['empleados_activos'],$empleados['empleados_inactivos']);

        // Create the Pie Graph. 
        $graph = new PieGraph(450,350);

        $theme_class = new OceanTheme();
        $graph->SetTheme($theme_class);

        // Set A title for the plot
        // $graph->title->Set("Reporte(estado empleados)");

        // Create
        $p1 = new PiePlot3D($data);
        $graph->Add($p1);
        $p1->ShowBorder();
        $p1->SetColor('black');
        $p1->ExplodeSlice(1);
        $p1->SetLegends(array('Activos '.$empleados['empleados_activos'],'Inactivos '.$empleados['empleados_inactivos']));
        $graph->Stroke(_IMG_HANDLER);

        $fileName = APPPATH.'img\empleados2.png';
        $graph->img->Stream($fileName);

        // Creación del objeto de la clase heredada
        $pdf = new PDF();
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetFont('Arial','B',15);
        $pdf->Ln(20);
        $pdf->Cell(40,20);   
        $pdf->Write(5,'                    Estado empleados');
        $pdf->Image(APPPATH.'img\empleados1.png', 10 ,90,190,'png');
        $pdf->AddPage();
        $pdf->Ln(30);
        $pdf->Cell(40,20);
        $pdf->Write(5,'                    Estado empleados');
        $pdf->Image(APPPATH.'img\empleados2.png', 10 ,70,190,'png');        
        $pdf->Output();
        unlink(APPPATH.'img\empleados1.png');
        unlink(APPPATH.'img\empleados2.png');
        

    }

    public function get_info_reportes_afiliaciones(){

        // eps
        $this->db->select('COUNT(*) FROM `afiliaciones` where `eps` LIKE "NUEVA EMPRESA PROMOTORA DE SALUD S.A."');      
        $query = $this->db->get();
        $eps_nueva = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `afiliaciones` where `eps` LIKE "SURA EPS"');      
        $query = $this->db->get();
        $eps_sura = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `afiliaciones` where `eps` LIKE "SALUD TOTAL"');      
        $query = $this->db->get();
        $eps_salud_total = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `afiliaciones` where `eps` LIKE "CAFESALUD ENTIDAD PROMOTORA DE SALUD S.A."');      
        $query = $this->db->get();
        $eps_cafesalud = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `afiliaciones` where `eps` LIKE "CONSORCIO SAYP 2011"');      
        $query = $this->db->get();
        $eps_sayp = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `afiliaciones` where `eps` LIKE "COOMEVA ENTIDAD PROMOTORA DE SALUD"');      
        $query = $this->db->get();
        $eps_coomeva = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `afiliaciones` where `eps` LIKE "ENTIDAD PROMOTORA DE SALUD SERVICIO OCCIDENTAL"');     
        $query = $this->db->get();
        $eps_servicio_occidental = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `afiliaciones` where `eps` LIKE "ENTIDAD PROMOTORA DE SALUD SANITAS LTDA"');     
        $query = $this->db->get();
        $eps_sanitas = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `afiliaciones` where `eps` LIKE "ASOCIACION MUTUAL LA ESPERANZA ASMET SALUD ES"');      
        $query = $this->db->get();
        $eps_asociacion = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `afiliaciones` where `eps` LIKE "FOSYGA"');      
        $query = $this->db->get();
        $eps_fosyga = json_decode(json_encode($query->result()), True);
        
        $eps = array('eps_promotora' => $eps_nueva[0]['COUNT(*)'], 'eps_sura' => $eps_sura[0]['COUNT(*)'], 
            'eps_salud_total' => $eps_salud_total[0]['COUNT(*)'], 'eps_cafesalud' => $eps_cafesalud[0]['COUNT(*)'], 'eps_sayp' => $eps_sayp[0]['COUNT(*)'], 'eps_coomeva' => $eps_coomeva[0]['COUNT(*)'], 'eps_servicio_occidental' => $eps_servicio_occidental[0]['COUNT(*)'], 'eps_sanitas' => $eps_sanitas[0]['COUNT(*)'], 'eps_asociacion' => $eps_asociacion[0]['COUNT(*)'], 'eps_fosyga' => $eps_fosyga[0]['COUNT(*)']);        


        // pension

        $this->db->select('COUNT(*) FROM `afiliaciones` where `pension` LIKE "PROTECCION"');     
        $query = $this->db->get();
        $pension_proteccion = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `afiliaciones` where `pension` LIKE "PORVENIR S.A"');     
        $query = $this->db->get();
        $pension_porvenir = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `afiliaciones` where `pension` LIKE "ADMINISTRADORA COLOMBIANA DE PENSIONES COLPEN"');  
        $query = $this->db->get();
        $pension_colpen = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `afiliaciones` where `pension` LIKE "COLFONDOS"');      
        $query = $this->db->get();
        $pension_colfondos = json_decode(json_encode($query->result()), True);

        $pension = array('pension_proteccion' => $pension_proteccion[0]['COUNT(*)'], 'pension_porvenir' => $pension_porvenir[0]['COUNT(*)'], 'pension_colpen' => $pension_colpen[0]['COUNT(*)'], 'pension_colfondos' => $pension_colfondos[0]['COUNT(*)']);


        // cesantias

        $this->db->select('COUNT(*) FROM `afiliaciones` where `cesantias` LIKE "PORVENIR"');     
        $query = $this->db->get();
        $cesantias_porvenir = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `afiliaciones` where `cesantias` LIKE "PROTECCIÓN"');
        $query = $this->db->get();
        $cesantias_proteccion = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `afiliaciones` where `cesantias` LIKE "FNA"');      
        $query = $this->db->get();
        $cesantias_fna = json_decode(json_encode($query->result()), True);

        $cesantias = array('cesantias_porvenir' => $cesantias_porvenir[0]['COUNT(*)'], 'cesantias_proteccion' => $cesantias_proteccion[0]['COUNT(*)'], 'cesantias_fna' => $cesantias_fna[0]['COUNT(*)']);


        // arl

        $this->db->select('COUNT(*) FROM `afiliaciones` where `arl` LIKE "SURA"');
        $query = $this->db->get();
        $arl_sura = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `afiliaciones` where `arl` LIKE "POSITIVA"');      
        $query = $this->db->get();
        $arl_positiva = json_decode(json_encode($query->result()), True);

        $arl = array('arl_sura' => $arl_sura[0]['COUNT(*)'], 'arl_positiva' => $arl_positiva[0]['COUNT(*)']);

        $resultado = array('eps' => $eps, 'pension' => $pension, 'cesantias' => $cesantias, 'arl' => $arl);

        // graficas eps

        $datay=array($resultado['eps']['eps_promotora'], $resultado['eps']['eps_sura'], $resultado['eps']['eps_salud_total'], $resultado['eps']['eps_cafesalud'], $resultado['eps']['eps_sayp'], $resultado['eps']['eps_coomeva'], $resultado['eps']['eps_servicio_occidental'], $resultado['eps']['eps_sanitas'], $resultado['eps']['eps_asociacion'], $resultado['eps']['eps_fosyga']);

        // Create the graph. These two calls are always required
        $graph = new Graph(1100,720,'auto');
        
        $graph->SetScale("textlin");
        $theme_class = new OceanTheme();
        $graph->SetTheme($theme_class);
        //$theme_class="DefaultTheme";
        //$graph->SetTheme(new $theme_class());

        // set major and minor tick positions manually
        $graph->yaxis->SetTickPositions(array(0,100,200,300,400,500,600,700,800,900,1000), array(50,150,250,350,450,550,650,750,850,950));
        $graph->SetBox(false);

        //$graph->ygrid->SetColor('gray');
        $graph->ygrid->SetFill(false);
        $graph->xaxis->SetTickLabels(array('Nueva eps '.$resultado['eps']['eps_promotora'],'Sura '.$resultado['eps']['eps_sura'], 'Salud total '.$resultado['eps']['eps_salud_total'], 'Cafesalud '.$resultado['eps']['eps_cafesalud'], 'Sayp '.$resultado['eps']['eps_sayp'], 'Coomeva '.$resultado['eps']['eps_coomeva'], 'Sos '.$resultado['eps']['eps_servicio_occidental'], 'Sanitas '.$resultado['eps']['eps_sanitas'], 'Asmet salud '.$resultado['eps']['eps_asociacion'], 'Fosyga '.$resultado['eps']['eps_fosyga']));
        $graph->xaxis->SetFont(FF_ARIAL,FS_NORMAL,10);
        $graph->yaxis->HideLine(false);
        $graph->yaxis->HideTicks(false,false);

        // Create the bar plots
        $b1plot = new BarPlot($datay);

        // ...and add it to the graPH
        $graph->Add($b1plot);


        $b1plot->SetColor("white");
        $b1plot->SetFillGradient("#188EAF","white",GRAD_LEFT_REFLECTION);
        $b1plot->SetWidth(45);
        // $graph->title->Set("Reporte(estado empleados)");

        // Display the graph
        $graph->Stroke(_IMG_HANDLER);

        $fileName = APPPATH.'img\eps1.png';
        $graph->img->Stream($fileName);

        // // grafica de pastl

        // Some data
        $data = array($resultado['eps']['eps_promotora'], $resultado['eps']['eps_sura'], $resultado['eps']['eps_salud_total'], $resultado['eps']['eps_cafesalud'], $resultado['eps']['eps_sayp'], $resultado['eps']['eps_coomeva'], $resultado['eps']['eps_servicio_occidental'], $resultado['eps']['eps_sanitas'], $resultado['eps']['eps_asociacion'], $resultado['eps']['eps_fosyga']);

        // Create the Pie Graph. 
        $graph = new PieGraph(800,950);

        $theme_class = new OceanTheme();
        $graph->SetTheme($theme_class);

        // Set A title for the plot
        // $graph->title->Set("Reporte(estado empleados)");
        // Create
        $p1 = new PiePlot3D($data);
        $graph->Add($p1);
        $p1->ShowBorder();
        $p1->SetColor('black');
        $p1->ExplodeSlice(1);
        $p1->SetLegends(array('Nueva eps '.$resultado['eps']['eps_promotora'],'Sura '.$resultado['eps']['eps_sura'], 'Salud total '.$resultado['eps']['eps_salud_total'], 'Cafesalud '.$resultado['eps']['eps_cafesalud'], 'Sayp '.$resultado['eps']['eps_sayp'], 'Coomeva '.$resultado['eps']['eps_coomeva'], 'Sos '.$resultado['eps']['eps_servicio_occidental'], 'Sanitas '.$resultado['eps']['eps_sanitas'], 'Asmet salud '.$resultado['eps']['eps_asociacion'], 'Fosyga '.$resultado['eps']['eps_fosyga']));
        $graph->Stroke(_IMG_HANDLER);

        $fileName = APPPATH.'img\eps2.png';
        $graph->img->Stream($fileName);


        // graficas pension


        $datay=array($resultado['pension']['pension_proteccion'], $resultado['pension']['pension_porvenir'], $resultado['pension']['pension_colpen'], $resultado['pension']['pension_colfondos']);

        // Create the graph. These two calls are always required
        $graph = new Graph(800,520,'auto');
        
        $graph->SetScale("textlin");
        $theme_class = new OceanTheme();
        $graph->SetTheme($theme_class);
        //$theme_class="DefaultTheme";
        //$graph->SetTheme(new $theme_class());

        // set major and minor tick positions manually
        $graph->yaxis->SetTickPositions(array(0,100,200,300,400,500,600,700,800,900,1000), array(50,150,250,350,450,550,650,750,850,950));
        $graph->SetBox(false);

        //$graph->ygrid->SetColor('gray');
        $graph->ygrid->SetFill(false);
        $graph->xaxis->SetTickLabels(array('Proteccion '.$resultado['pension']['pension_proteccion'],'Porvenir '.$resultado['pension']['pension_porvenir'], 'Colpenciones '.$resultado['pension']['pension_colpen'], 'Colfondos '.$resultado['pension']['pension_colfondos']));
        $graph->xaxis->SetFont(FF_ARIAL,FS_NORMAL,10);
        $graph->yaxis->HideLine(false);
        $graph->yaxis->HideTicks(false,false);

        // Create the bar plots
        $b1plot = new BarPlot($datay);

        // ...and add it to the graPH
        $graph->Add($b1plot);


        $b1plot->SetColor("white");
        $b1plot->SetFillGradient("#188EAF","white",GRAD_LEFT_REFLECTION);
        $b1plot->SetWidth(45);
        // $graph->title->Set("Reporte(estado empleados)");

        // Display the graph
        $graph->Stroke(_IMG_HANDLER);

        $fileName = APPPATH.'img\pension1.png';
        $graph->img->Stream($fileName);

        // // grafica de pastl

        // Some data
        $data=array($resultado['pension']['pension_proteccion'], $resultado['pension']['pension_porvenir'], $resultado['pension']['pension_colpen'], $resultado['pension']['pension_colfondos']);

        // Create the Pie Graph. 
        $graph = new PieGraph(600,750);

        $theme_class = new OceanTheme();
        $graph->SetTheme($theme_class);

        // Set A title for the plot
        // $graph->title->Set("Reporte(estado empleados)");
        // Create
        $p1 = new PiePlot3D($data);
        $graph->Add($p1);
        $p1->ShowBorder();
        $p1->SetColor('black');
        $p1->ExplodeSlice(1);
        $p1->SetLegends(array('Proteccion '.$resultado['pension']['pension_proteccion'],'Porvenir '.$resultado['pension']['pension_porvenir'], 'Colpenciones '.$resultado['pension']['pension_colpen'], 'Colfondos '.$resultado['pension']['pension_colfondos']));
        $graph->Stroke(_IMG_HANDLER);

        $fileName = APPPATH.'img\pension2.png';
        $graph->img->Stream($fileName);


        // grafica cesdantias


        $datay=array($resultado['cesantias']['cesantias_porvenir'], $resultado['cesantias']['cesantias_proteccion'], $resultado['cesantias']['cesantias_fna']);

        // Create the graph. These two calls are always required
        $graph = new Graph(600,420,'auto');
        
        $graph->SetScale("textlin");
        $theme_class = new OceanTheme();
        $graph->SetTheme($theme_class);
        //$theme_class="DefaultTheme";
        //$graph->SetTheme(new $theme_class());

        // set major and minor tick positions manually
        $graph->yaxis->SetTickPositions(array(0,100,200,300,400,500,600,700,800,900,1000), array(50,150,250,350,450,550,650,750,850,950));
        $graph->SetBox(false);

        //$graph->ygrid->SetColor('gray');
        $graph->ygrid->SetFill(false);
        $graph->xaxis->SetTickLabels(array('Porvenir '.$resultado['cesantias']['cesantias_porvenir'],'Proteccion '.$resultado['cesantias']['cesantias_proteccion'], 'Fna '.$resultado['cesantias']['cesantias_fna']));
        $graph->xaxis->SetFont(FF_ARIAL,FS_NORMAL,10);
        $graph->yaxis->HideLine(false);
        $graph->yaxis->HideTicks(false,false);

        // Create the bar plots
        $b1plot = new BarPlot($datay);

        // ...and add it to the graPH
        $graph->Add($b1plot);


        $b1plot->SetColor("white");
        $b1plot->SetFillGradient("#188EAF","white",GRAD_LEFT_REFLECTION);
        $b1plot->SetWidth(45);
        // $graph->title->Set("Reporte(estado empleados)");

        // Display the graph
        $graph->Stroke(_IMG_HANDLER);

        $fileName = APPPATH.'img\cesantias1.png';
        $graph->img->Stream($fileName);

        // // grafica de pastl

        // Some data
        $data=array($resultado['cesantias']['cesantias_porvenir'], $resultado['cesantias']['cesantias_proteccion'], $resultado['cesantias']['cesantias_fna']);

        // Create the Pie Graph. 
        $graph = new PieGraph(600,750);

        $theme_class = new OceanTheme();
        $graph->SetTheme($theme_class);

        // Set A title for the plot
        // $graph->title->Set("Reporte(estado empleados)");
        // Create
        $p1 = new PiePlot3D($data);
        $graph->Add($p1);
        $p1->ShowBorder();
        $p1->SetColor('black');
        $p1->ExplodeSlice(1);
        $p1->SetLegends(array('Porvenir '.$resultado['cesantias']['cesantias_porvenir'],'Proteccion '.$resultado['cesantias']['cesantias_proteccion'], 'Fna '.$resultado['cesantias']['cesantias_fna']));
        $graph->Stroke(_IMG_HANDLER);

        $fileName = APPPATH.'img\cesantias2.png';
        $graph->img->Stream($fileName);


        // grafica arl


        $datay=array($resultado['arl']['arl_sura'], $resultado['arl']['arl_positiva']);

        // Create the graph. These two calls are always required
        $graph = new Graph(600,420,'auto');
        
        $graph->SetScale("textlin");
        $theme_class = new OceanTheme();
        $graph->SetTheme($theme_class);
        //$theme_class="DefaultTheme";
        //$graph->SetTheme(new $theme_class());

        // set major and minor tick positions manually
        $graph->yaxis->SetTickPositions(array(0,100,200,300,400,500,600,700,800,900,1000), array(50,150,250,350,450,550,650,750,850,950));
        $graph->SetBox(false);

        //$graph->ygrid->SetColor('gray');
        $graph->ygrid->SetFill(false);
        $graph->xaxis->SetTickLabels(array('Sura '.$resultado['arl']['arl_sura'],'Positiva '.$resultado['arl']['arl_positiva']));
        $graph->xaxis->SetFont(FF_ARIAL,FS_NORMAL,10);
        $graph->yaxis->HideLine(false);
        $graph->yaxis->HideTicks(false,false);

        // Create the bar plots
        $b1plot = new BarPlot($datay);

        // ...and add it to the graPH
        $graph->Add($b1plot);


        $b1plot->SetColor("white");
        $b1plot->SetFillGradient("#188EAF","white",GRAD_LEFT_REFLECTION);
        $b1plot->SetWidth(45);
        // $graph->title->Set("Reporte(estado empleados)");

        // Display the graph
        $graph->Stroke(_IMG_HANDLER);

        $fileName = APPPATH.'img\arl1.png';
        $graph->img->Stream($fileName);

        // // grafica de pastl

        // Some data
        $data=array($resultado['arl']['arl_sura'], $resultado['arl']['arl_positiva']);

        // Create the Pie Graph. 
        $graph = new PieGraph(650,550);

        $theme_class = new OceanTheme();
        $graph->SetTheme($theme_class);

        // Set A title for the plot
        // $graph->title->Set("Reporte(estado empleados)");
        // Create
        $p1 = new PiePlot3D($data);
        $graph->Add($p1);
        $p1->ShowBorder();
        $p1->SetColor('black');
        $p1->ExplodeSlice(1);
        $p1->SetLegends(array('Sura '.$resultado['arl']['arl_sura'],'Positiva '.$resultado['arl']['arl_positiva']));
        $graph->Stroke(_IMG_HANDLER);

        $fileName = APPPATH.'img\arl2.png';
        $graph->img->Stream($fileName);


        // Creación del objeto de la clase heredada
        $pdf = new PDF();
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetFont('Arial','B',15);
        $pdf->Ln(30);
        $pdf->Cell(40,20);
        $pdf->Write(5,'                    Empleados y eps');
        $pdf->Image(APPPATH.'img\eps1.png', 10,90,190,'png');
        $pdf->AddPage();
        $pdf->Ln(30);
        $pdf->Cell(40,20);
        $pdf->Write(5,'                    Empleados y eps');
        $pdf->Image(APPPATH.'img\eps2.png', 10 ,70,190,'png');
        $pdf->AddPage();
        $pdf->Ln(30);
        $pdf->Cell(40,20);
        $pdf->Write(5,'                    Empleados y pension');
        $pdf->Image(APPPATH.'img\pension1.png', 10 ,90,190,'png');
        $pdf->AddPage();
        $pdf->Ln(30);
        $pdf->Cell(40,20);
        $pdf->Write(5,'                    Empleados y pension');
        $pdf->Image(APPPATH.'img\pension2.png', 10 ,70,190,'png');
        $pdf->AddPage();
        $pdf->Ln(30);
        $pdf->Cell(40,20);
        $pdf->Write(5,'                    Empleados y cesantias');
        $pdf->Image(APPPATH.'img\cesantias1.png', 10 ,90,190,'png');
        $pdf->AddPage();
        $pdf->Ln(30);
        $pdf->Cell(40,20);
        $pdf->Write(5,'                    Empleados y cesantias');
        $pdf->Image(APPPATH.'img\cesantias2.png', 10 ,70,190,'png');
        $pdf->AddPage();
        $pdf->Ln(30);
        $pdf->Cell(40,20);
        $pdf->Write(5,'                    Empleados y arl');
        $pdf->Image(APPPATH.'img\arl1.png', 10 ,90,190,'png');
        $pdf->AddPage();
        $pdf->Ln(30);
        $pdf->Cell(40,20);
        $pdf->Write(5,'                    Empleados y arl');
        $pdf->Image(APPPATH.'img\arl2.png', 10 ,70,190,'png');
        $pdf->Output();

        unlink(APPPATH.'img\eps1.png');
        unlink(APPPATH.'img\eps2.png');
        unlink(APPPATH.'img\pension1.png');
        unlink(APPPATH.'img\pension2.png');
        unlink(APPPATH.'img\cesantias1.png');
        unlink(APPPATH.'img\cesantias2.png');
        unlink(APPPATH.'img\arl1.png');
        unlink(APPPATH.'img\arl2.png');

    }

    public function get_info_reportes_perfiles(){

        // cargos laborales

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "ADMINISTRADOR OFICINA"');
        $query = $this->db->get();
        $cargo_admin_oficina = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "ANALISTA DE DATOS"');
        $query = $this->db->get();
        $cargo_analista_datos = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "APRENDIZ SENA"');
        $query = $this->db->get();
        $cargo_aprendiz_sena = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "ASESOR CALL CENTER"');
        $query = $this->db->get();
        $cargo_asesor_call_center = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "ASESOR DE VENTA SERVICIOS Y R"');
        $query = $this->db->get();
        $cargo_asesor_venta_servicios = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "ASISTENTE CONTABILIDAD"');
        $query = $this->db->get();
        $cargo_asistente_contabilidad = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "ASISTENTE DE SISTEMAS Y COMUNI"');
        $query = $this->db->get();
        $cargo_asistente_sistemas = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "ASISTENTE GERENCIA"');
        $query = $this->db->get();
        $cargo_asistente_gerencia = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "ASISTENTE NOMINA"');
        $query = $this->db->get();
        $cargo_asistente_nomina = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "AUXILIAR CALL CENTER"');
        $query = $this->db->get();
        $cargo_auxiliar_call_center = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "AUXILIAR COMERCIAL"');
        $query = $this->db->get();
        $cargo_auxiliar_comercial = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "AUXILIAR CONSULTORIO JURIDICO"');
        $query = $this->db->get();
        $cargo_auxiliar_consultorio_juridico = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "AUXILIAR DE CAPACITACION"');
        $query = $this->db->get();
        $cargo_auxiliar_capacitacion = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "AUXILIAR DE ENFERMERIA"');
        $query = $this->db->get();
        $cargo_auxiliar_enfermeria = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "AUXILIAR DE GIROS"');
        $query = $this->db->get();
        $cargo_auxiliar_giros = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "AUXILIAR DE MANTENIMIENTO"');
        $query = $this->db->get();
        $cargo_auxiliar_mantenimiento = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "AUXILIAR DE PREMIOS"');
        $query = $this->db->get();
        $cargo_auxiliar_premios = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "AUXILIAR DE SEGURIDAD OCUPACIO"');
        $query = $this->db->get();
        $cargo_auxiliar_seguridad_ocupacional = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "AUXILIAR DE SERVICIOS GENERALE"');
        $query = $this->db->get();
        $cargo_auxiliar_servicios_generales = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "AUXILIAR PLAN TIENDA"');
        $query = $this->db->get();
        $cargo_auxiliar_plan_tienda = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "CAJERA SUPERGIROS"');
        $query = $this->db->get();
        $cargo_cajera_supergiros = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "CONDUCTOR/ ESCOLTA"');
        $query = $this->db->get();
        $cargo_conductor_escolta = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "COORDINADOR ADMINISTRATIVO"');
        $query = $this->db->get();
        $cargo_coordinador_administrativo = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "COORDINADOR CONTABLE"');
        $query = $this->db->get();
        $cargo_coordinador_contable = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "COORDINADOR DE ZONA"');
        $query = $this->db->get();
        $cargo_coordinador_zona = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "COORDINADOR JUEGOS LOCALIZADOS"');
        $query = $this->db->get();
        $cargo_coordinador_juegos_localizados = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "DIRECTOR COMERCIAL"');
        $query = $this->db->get();
        $cargo_director_comercial = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "DIRECTOR TALENTO HUMANO Y GEST"');
        $query = $this->db->get();
        $cargo_director_talento_humano = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "DIRECTORA FINANCIERA"');
        $query = $this->db->get();
        $cargo_director_financiero = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "GERENTE GENERAL"');
        $query = $this->db->get();
        $cargo_gerente = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "INGENIERO DE DESARROLLO"');
        $query = $this->db->get();
        $cargo_ingeniero_desarrollo = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "LIDER COMERCIAL"');
        $query = $this->db->get();
        $cargo_lider_comercial = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "LIDER DE CALIDAD"');
        $query = $this->db->get();
        $cargo_lider_calidad = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "LIDER DE SEGURIDAD"');
        $query = $this->db->get();
        $cargo_lider_seguridad = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "LIDER DE SERVICIO AL CLIENTE"');
        $query = $this->db->get();
        $cargo_lider_servicio_cliente = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "LIDER MERCADEO COMUNI Y PUBLIC"');
        $query = $this->db->get();
        $cargo_lider_mercadeo_publicidad = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "MENSAJERO SERVICIOS EXTERNOS"');
        $query = $this->db->get();
        $cargo_mensajero_servicios_externos = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "OFICIAL DE CUMPLIMIENTO"');
        $query = $this->db->get();
        $cargo_oficial_cumplimiento = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "PSICOLOGA"');
        $query = $this->db->get();
        $cargo_psicologa = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "RECEPCIONISTA"');
        $query = $this->db->get();
        $cargo_recepcionista = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "SUPERVISOR DE MAQUINAS"');
        $query = $this->db->get();
        $cargo_supervisor_maquinitas = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "SUPERVISOR DE MOVILES"');
        $query = $this->db->get();
        $cargo_supervisor_moviles = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "SUPERVISOR DE PLAN TIENDA"');
        $query = $this->db->get();
        $cargo_supervisor_plan_tiendas = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "SUPERVISOR DE SUPERGIROS"');
        $query = $this->db->get();
        $cargo_supervisor_super_giros = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "TECNICO DE SISTEMAS"');
        $query = $this->db->get();
        $cargo_tecnico_sistemas = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `cargo_laboral` LIKE "TESORERA"');
        $query = $this->db->get();
        $cargo_tesorera = json_decode(json_encode($query->result()), True);

        $cargos = array('cargo_admin_oficina' => $cargo_admin_oficina[0]['COUNT(*)'], 'cargo_analista_datos' => $cargo_analista_datos[0]['COUNT(*)'], 'cargo_aprendiz_sena' => $cargo_aprendiz_sena[0]['COUNT(*)'], 'cargo_asesor_call_center' => $cargo_asesor_call_center[0]['COUNT(*)'],  'cargo_asesor_venta_servicios' => $cargo_asesor_venta_servicios[0]['COUNT(*)'],  'cargo_asistente_contabilidad' => $cargo_asistente_contabilidad[0]['COUNT(*)'],  'cargo_asistente_sistemas' => $cargo_asistente_sistemas[0]['COUNT(*)'],  'cargo_asistente_gerencia' => $cargo_asistente_gerencia[0]['COUNT(*)'],  'cargo_asistente_nomina' => $cargo_asistente_nomina[0]['COUNT(*)'],  'cargo_auxiliar_call_center' => $cargo_auxiliar_call_center[0]['COUNT(*)'],  'cargo_auxiliar_comercial' => $cargo_auxiliar_comercial[0]['COUNT(*)'],  'cargo_auxiliar_consultorio_juridico' => $cargo_auxiliar_consultorio_juridico[0]['COUNT(*)'],  'cargo_auxiliar_capacitacion' => $cargo_auxiliar_capacitacion[0]['COUNT(*)'],  'cargo_auxiliar_enfermeria' => $cargo_auxiliar_enfermeria[0]['COUNT(*)'],  'cargo_auxiliar_giros' => $cargo_auxiliar_giros[0]['COUNT(*)'],  'cargo_auxiliar_mantenimiento' => $cargo_auxiliar_mantenimiento[0]['COUNT(*)'],  'cargo_auxiliar_premios' => $cargo_auxiliar_premios[0]['COUNT(*)'],  'cargo_auxiliar_seguridad_ocupacional' => $cargo_auxiliar_seguridad_ocupacional[0]['COUNT(*)'],  'cargo_auxiliar_servicios_generales' => $cargo_auxiliar_servicios_generales[0]['COUNT(*)'],  'cargo_auxiliar_plan_tienda' => $cargo_auxiliar_plan_tienda[0]['COUNT(*)'],  'cargo_cajera_supergiros' => $cargo_cajera_supergiros[0]['COUNT(*)'],  'cargo_conductor_escolta' => $cargo_conductor_escolta[0]['COUNT(*)'],  'cargo_coordinador_administrativo' => $cargo_coordinador_administrativo[0]['COUNT(*)'],  'cargo_coordinador_contable' => $cargo_coordinador_contable[0]['COUNT(*)'],  'cargo_coordinador_zona' => $cargo_coordinador_zona[0]['COUNT(*)'],  'cargo_coordinador_juegos_localizados' => $cargo_coordinador_juegos_localizados[0]['COUNT(*)'],  'cargo_director_comercial' => $cargo_director_comercial[0]['COUNT(*)'],  'cargo_director_talento_humano' => $cargo_director_talento_humano[0]['COUNT(*)'],  'cargo_director_financiero' => $cargo_director_financiero[0]['COUNT(*)'],  'cargo_gerente' => $cargo_gerente[0]['COUNT(*)'],  'cargo_ingeniero_desarrollo' => $cargo_ingeniero_desarrollo[0]['COUNT(*)'],  'cargo_lider_comercial' => $cargo_lider_comercial[0]['COUNT(*)'],  'cargo_lider_calidad' => $cargo_lider_calidad[0]['COUNT(*)'],  'cargo_lider_seguridad' => $cargo_lider_seguridad[0]['COUNT(*)'],  'cargo_lider_servicio_cliente' => $cargo_lider_servicio_cliente[0]['COUNT(*)'],  'cargo_lider_mercadeo_publicidad' => $cargo_lider_mercadeo_publicidad[0]['COUNT(*)'],  'cargo_mensajero_servicios_externos' => $cargo_mensajero_servicios_externos[0]['COUNT(*)'],  'cargo_oficial_cumplimiento' => $cargo_oficial_cumplimiento[0]['COUNT(*)'],  'cargo_psicologa' => $cargo_psicologa[0]['COUNT(*)'],  'cargo_recepcionista' => $cargo_recepcionista[0]['COUNT(*)'],  'cargo_supervisor_maquinitas' => $cargo_supervisor_maquinitas[0]['COUNT(*)'],  'cargo_supervisor_moviles' => $cargo_supervisor_moviles[0]['COUNT(*)'],  'cargo_supervisor_plan_tiendas' => $cargo_supervisor_plan_tiendas[0]['COUNT(*)'],  'cargo_supervisor_super_giros' => $cargo_supervisor_super_giros[0]['COUNT(*)'],  'cargo_tecnico_sistemas' => $cargo_tecnico_sistemas[0]['COUNT(*)'],  'cargo_tesorera' => $cargo_tesorera[0]['COUNT(*)']);


        // departamentos

        $this->db->select('COUNT(*) FROM `perfiles` where `departamento` LIKE "COMERCIAL"');
        $query = $this->db->get();
        $departamento_comercial = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `departamento` LIKE "CONTABILIDAD"');
        $query = $this->db->get();
        $departamento_contabilidad = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `departamento` LIKE "CONTROL INTERNO"');
        $query = $this->db->get();
        $departamento_control_interno = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `departamento` LIKE "DIRECCION FINANCIERA"');
        $query = $this->db->get();
        $departamento_directora_financiera = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `departamento` LIKE "GERENCIA"');
        $query = $this->db->get();
        $departamento_gerencia = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `departamento` LIKE "MANTENIMIENTO"');
        $query = $this->db->get();
        $departamento_mantenimiento = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `departamento` LIKE "RECURSOS HUMANOS"');
        $query = $this->db->get();
        $departamento_recursos_humanos = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `departamento` LIKE "SISTEMAS"');
        $query = $this->db->get();
        $departamento_sistemas = json_decode(json_encode($query->result()), True);

        $this->db->select('COUNT(*) FROM `perfiles` where `departamento` LIKE "TESORERIA"');
        $query = $this->db->get();
        $departamento_tesoreria = json_decode(json_encode($query->result()), True);


        $departamentos = array('departamento_comercial' => $departamento_comercial[0]['COUNT(*)'], 'departamento_contabilidad' => $departamento_contabilidad[0]['COUNT(*)'], 'departamento_control_interno' => $departamento_control_interno[0]['COUNT(*)'], 'departamento_directora_financiera' => $departamento_directora_financiera[0]['COUNT(*)'], 'departamento_gerencia' => $departamento_gerencia[0]['COUNT(*)'], 'departamento_mantenimiento' => $departamento_mantenimiento[0]['COUNT(*)'], 'departamento_recursos_humanos' => $departamento_recursos_humanos[0]['COUNT(*)'], 'departamento_sistemas' => $departamento_sistemas[0]['COUNT(*)'], 'departamento_tesoreria' => $departamento_tesoreria[0]['COUNT(*)']);

        $resultado = array('cargos' => $cargos, 'departamentos' => $departamentos);


        // grafica cargos laborales
        

        $datay=array($resultado['cargos']['cargo_admin_oficina'], $resultado['cargos']['cargo_analista_datos'], $resultado['cargos']['cargo_aprendiz_sena'], $resultado['cargos']['cargo_asesor_call_center'], $resultado['cargos']['cargo_asesor_venta_servicios'], $resultado['cargos']['cargo_asistente_contabilidad'], $resultado['cargos']['cargo_asistente_sistemas'], $resultado['cargos']['cargo_asistente_gerencia'], $resultado['cargos']['cargo_asistente_nomina'], $resultado['cargos']['cargo_auxiliar_call_center'], $resultado['cargos']['cargo_auxiliar_comercial'], $resultado['cargos']['cargo_auxiliar_consultorio_juridico'], $resultado['cargos']['cargo_auxiliar_capacitacion'], $resultado['cargos']['cargo_auxiliar_enfermeria'], $resultado['cargos']['cargo_auxiliar_giros'], $resultado['cargos']['cargo_auxiliar_mantenimiento'], $resultado['cargos']['cargo_auxiliar_premios'], $resultado['cargos']['cargo_auxiliar_seguridad_ocupacional'], $resultado['cargos']['cargo_auxiliar_servicios_generales'], $resultado['cargos']['cargo_auxiliar_plan_tienda'], $resultado['cargos']['cargo_cajera_supergiros'], $resultado['cargos']['cargo_conductor_escolta'], $resultado['cargos']['cargo_coordinador_administrativo'], $resultado['cargos']['cargo_coordinador_contable'], $resultado['cargos']['cargo_coordinador_zona'], $resultado['cargos']['cargo_coordinador_juegos_localizados'], $resultado['cargos']['cargo_director_comercial'], $resultado['cargos']['cargo_director_talento_humano'], $resultado['cargos']['cargo_director_financiero'], $resultado['cargos']['cargo_gerente'], $resultado['cargos']['cargo_ingeniero_desarrollo'], $resultado['cargos']['cargo_lider_comercial'], $resultado['cargos']['cargo_lider_calidad'], $resultado['cargos']['cargo_lider_seguridad'], $resultado['cargos']['cargo_lider_servicio_cliente'], $resultado['cargos']['cargo_lider_mercadeo_publicidad'], $resultado['cargos']['cargo_mensajero_servicios_externos'], $resultado['cargos']['cargo_oficial_cumplimiento'], $resultado['cargos']['cargo_psicologa'], $resultado['cargos']['cargo_recepcionista'], $resultado['cargos']['cargo_supervisor_maquinitas'], $resultado['cargos']['cargo_supervisor_moviles'], $resultado['cargos']['cargo_supervisor_plan_tiendas'], $resultado['cargos']['cargo_supervisor_super_giros'], $resultado['cargos']['cargo_tecnico_sistemas'], $resultado['cargos']['cargo_tesorera']);


        // Create the graph. These two calls are always required
        $graph = new Graph(700,700);
        $graph->SetScale("textlin");

        $theme_class=new UniversalTheme;
        $graph->SetTheme($theme_class);

        $graph->Set90AndMargin(250,40,40,40);
        $graph->img->SetAngle(90); 

        // set major and minor tick positions manually
        $graph->SetBox(false);

        //$graph->ygrid->SetColor('gray');
        $graph->ygrid->Show(false);
        $graph->ygrid->SetFill(false);
        $graph->xaxis->SetTickLabels(array('Administador oficina       '.$resultado['cargos']['cargo_admin_oficina'], 'Analista de datos       '.$resultado['cargos']['cargo_analista_datos'], 'Aprendiz sena     '.$resultado['cargos']['cargo_aprendiz_sena'], 'Asesor call center       '.$resultado['cargos']['cargo_asesor_call_center'], 'Asesor venta de servicios   '.$resultado['cargos']['cargo_asesor_venta_servicios'], 'Asistente contabilidad       '.$resultado['cargos']['cargo_asistente_contabilidad'], 'Asistente sistemas       '.$resultado['cargos']['cargo_asistente_sistemas'], 'Asistente gerencia       '.$resultado['cargos']['cargo_asistente_gerencia'], 'Asistente nomina       '.$resultado['cargos']['cargo_asistente_nomina'], 'Auxiliar call center       '.$resultado['cargos']['cargo_auxiliar_call_center'], 'Auxiliar comercial       '.$resultado['cargos']['cargo_auxiliar_comercial'], 'Auxiliar consultorio juridico       '.$resultado['cargos']['cargo_auxiliar_consultorio_juridico'], 'Auxiliar de capacitacion       '.$resultado['cargos']['cargo_auxiliar_capacitacion'], 'Auxiliar de enfermeria       '.$resultado['cargos']['cargo_auxiliar_enfermeria'], 'Auxiliar de giros       '.$resultado['cargos']['cargo_auxiliar_giros'], 'Auxiliar de mantenimiento       '.$resultado['cargos']['cargo_auxiliar_mantenimiento'], 'Auxiliar de premios       '.$resultado['cargos']['cargo_auxiliar_premios'], 'Auxiliar de seguridad ocupacional       '.$resultado['cargos']['cargo_auxiliar_seguridad_ocupacional'], 'Auxiliar de servicios generales       '.$resultado['cargos']['cargo_auxiliar_servicios_generales'], 'Auxiliar plan tienda       '.$resultado['cargos']['cargo_auxiliar_plan_tienda'], 'Cajera supergiros       '.$resultado['cargos']['cargo_cajera_supergiros'], 'Conductor / escolta       '.$resultado['cargos']['cargo_conductor_escolta'], 'Coordinador administrativo       '.$resultado['cargos']['cargo_coordinador_administrativo'], 'Coordinador contable       '.$resultado['cargos']['cargo_coordinador_contable'], 'Coordinador de zona       '.$resultado['cargos']['cargo_coordinador_zona'], 'Coordinador juegos localizados       '.$resultado['cargos']['cargo_coordinador_juegos_localizados'], 'Director comercial       '.$resultado['cargos']['cargo_director_comercial'], 'Director talento humano       '.$resultado['cargos']['cargo_director_talento_humano'], 'Director financiero       '.$resultado['cargos']['cargo_director_financiero'], 'Gerente general       '.$resultado['cargos']['cargo_gerente'], 'Ingeniero de desarrollo       '.$resultado['cargos']['cargo_ingeniero_desarrollo'], 'Lider comercial       '.$resultado['cargos']['cargo_lider_comercial'], 'Lider de calidad       '.$resultado['cargos']['cargo_lider_calidad'], 'Lider de seguridad       '.$resultado['cargos']['cargo_lider_seguridad'], 'Lider servicio al cliente       '.$resultado['cargos']['cargo_lider_servicio_cliente'], 'Lider de mercadeo y publicidad       '.$resultado['cargos']['cargo_lider_mercadeo_publicidad'], 'Mensajero servicios externos       '.$resultado['cargos']['cargo_mensajero_servicios_externos'], 'Oficial del cumplimiento       '.$resultado['cargos']['cargo_oficial_cumplimiento'], 'Psicologa       '.$resultado['cargos']['cargo_psicologa'], 'Recepcionista       '.$resultado['cargos']['cargo_recepcionista'], 'Supervisor de maquinitas       '.$resultado['cargos']['cargo_supervisor_maquinitas'], 'Supervisor de moviles       '.$resultado['cargos']['cargo_supervisor_moviles'], 'Supervisor plan tienda       '.$resultado['cargos']['cargo_supervisor_plan_tiendas'], 'Supervisor supergiros       '.$resultado['cargos']['cargo_supervisor_super_giros'], 'Tecnico de sistemas       '.$resultado['cargos']['cargo_tecnico_sistemas'], 'Tesorera       '.$resultado['cargos']['cargo_tesorera']));
        $graph->yaxis->HideLine(false);
        $graph->yaxis->HideTicks(false,false);

        // For background to be gradient, setfill is needed first.
        $graph->SetBackgroundGradient('#00CED1', '#FFFFFF', GRAD_HOR, BGRAD_PLOT);

        // Create the bar plots
        $b1plot = new BarPlot($datay);

        // ...and add it to the graPH
        $graph->Add($b1plot);


        $b1plot->SetWeight(0);
        $b1plot->SetFillGradient("#808000","#90EE90",GRAD_HOR);
        $b1plot->SetWidth(17);

        // Display the graph
        $graph->Stroke(_IMG_HANDLER);

        $fileName = APPPATH.'img\cargos1.png';
        $graph->img->Stream($fileName);        



        // grafica de departamentos


        $datay=array($resultado['departamentos']['departamento_comercial'], $resultado['departamentos']['departamento_contabilidad'], $resultado['departamentos']['departamento_control_interno'], $resultado['departamentos']['departamento_directora_financiera'], $resultado['departamentos']['departamento_gerencia'], $resultado['departamentos']['departamento_mantenimiento'], $resultado['departamentos']['departamento_recursos_humanos'], $resultado['departamentos']['departamento_sistemas'], $resultado['departamentos']['departamento_tesoreria']);

        // Create the graph. These two calls are always required
        $graph = new Graph(700,500);
        $graph->SetScale("textlin");

        $theme_class=new UniversalTheme;
        $graph->SetTheme($theme_class);

        $graph->Set90AndMargin(300,40,40,40);
        $graph->img->SetAngle(90); 

        // set major and minor tick positions manually
        $graph->SetBox(false);

        //$graph->ygrid->SetColor('gray');
        $graph->ygrid->Show(false);
        $graph->ygrid->SetFill(false);
        $graph->xaxis->SetTickLabels(array('Departamento comercial    '.$resultado['departamentos']['departamento_comercial'],'Departamento de contabilidad        '.$resultado['departamentos']['departamento_contabilidad'],'Departamento de control interno        '.$resultado['departamentos']['departamento_control_interno'],'Departamento financiero        '.$resultado['departamentos']['departamento_directora_financiera'],'Departamento de gerencia        '.$resultado['departamentos']['departamento_gerencia'],'Departamento de mantenimiento        '.$resultado['departamentos']['departamento_mantenimiento'],'Departamento de recursos humanos       '.$resultado['departamentos']['departamento_recursos_humanos'],'Departamento de sistemas       '.$resultado['departamentos']['departamento_sistemas'],'Departamento de tesoreria        '.$resultado['departamentos']['departamento_tesoreria']));
        $graph->yaxis->HideLine(false);
        $graph->yaxis->HideTicks(false,false);

        // For background to be gradient, setfill is needed first.
        $graph->SetBackgroundGradient('#00CED1', '#FFFFFF', GRAD_HOR, BGRAD_PLOT);

        // Create the bar plots
        $b1plot = new BarPlot($datay);

        // ...and add it to the graPH
        $graph->Add($b1plot);


        $b1plot->SetWeight(0);
        $b1plot->SetFillGradient("#808000","#90EE90",GRAD_HOR);
        $b1plot->SetWidth(17);

        // Display the graph
        $graph->Stroke(_IMG_HANDLER);

        $fileName = APPPATH.'img\departamentos1.png';
        $graph->img->Stream($fileName);

        $pdf = new PDF();
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetFont('Arial','B',15);
        $pdf->Ln(30);
        $pdf->Cell(40,20);
        $pdf->Write(5,'                    Empleados por cargos');
        $pdf->Image(APPPATH.'img\cargos1.png', 10,80,190,'png');
        $pdf->AddPage();
        $pdf->Ln(30);
        $pdf->Cell(40,20);
        $pdf->Write(5,'                 Empleados por departamentos');
        $pdf->Image(APPPATH.'img\departamentos1.png', 10 ,90,190,'png');        
        $pdf->Output();

        unlink(APPPATH.'img\cargos1.png');
        unlink(APPPATH.'img\departamentos1.png');
        
    }
  
}

class PDF extends FPDF
{
// Cabecera de página
function Header()
{
    // Logo
    $this->Image(APPPATH.'img\image001.png',10,8,200);
    // Arial bold 15
    $this->SetFont('Arial','B',15);
    // Movernos a la derecha
    // $this->Cell(80);
    // Título
    // $this->Cell(30,10,'Title',1,0,'C');
    // Salto de línea
    $this->Ln(40);
}

// Pie de página
function Footer()
{
    // Posición: a 1,5 cm del final
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Número de página
    $this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
}
}

