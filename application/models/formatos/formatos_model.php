<?php
require_once(APPPATH.'libraries/fpdf/fpdf.php');
require_once(APPPATH.'libraries/Classes/PHPExcel.php');

class formatos_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function insertFormato($datos){

    	$mensaje;
        $this->db->select('cedulaLaboral');
        $this->db->from('historial_laboral');
        $this->db->where('cedulaLaboral', $datos['cedulaLaboral']);
        $consultaCedula = $this->db->get();
        
        if($consultaCedula->result_id->num_rows){
            $mensaje='cedulaExistente';
            return $mensaje;
        }
        else{
            $this->db->insert('historial_laboral', $datos);
            $mensaje='exito';
            return $mensaje;
        }
    }

    function updateFormato($formato){

    	$mensaje;
        $this->db->select('idHistorialLaboral, cedulaLaboral');
        $this->db->from('historial_laboral');
        $this->db->where('idHistorialLaboral', $formato['idHistorialLaboral']);
        $consulta = $this->db->get();
        $consultaResult = $consulta->result();

        if ($consultaResult['0']->cedulaLaboral != $formato['cedulaLaboral']) {
        	
        	$this->db->select('cedulaLaboral');
	        $this->db->from('historial_laboral');
	        $this->db->where('cedulaLaboral', $formato['cedulaLaboral']);
	        $consulta = $this->db->get();

	        if($consulta->result_id->num_rows){
	            $mensaje='cedulaExistente';
	            return $mensaje;
	        }
	        else{
	           $this->db->where('idHistorialLaboral', $formato['idHistorialLaboral']);
	           $this->db->update('historial_laboral', $formato);
	           $mensaje = 'actualizado';
	           return $mensaje;
	        }
        }else{
           $this->db->where('idHistorialLaboral', $formato['idHistorialLaboral']);
           $this->db->update('historial_laboral', $formato);
           $mensaje = 'actualizado';
           return $mensaje;
        }

    }

    function delete_formato($id){

    	$this->db->where('idHistorialLaboral', $id);
        $this->db->delete('historial_laboral');
        $mensaje = 'eliminado';
        return $mensaje;
    }

    function get_historial($id){

    	$this->db->select('idHistorialLaboral, nombreLaboral, cedulaLaboral, codigoLaboral, telefonoLaboral, cargoLaboral, areaLaboral, fechaIngreso, fechaRetiro, tipoContrato, comparendosEducativos, llamadosAtención, suspensiones, diasAusentismos, diasLaborados, eps, afp, cesantias, valorCesantias, causalRetiro, salarioDevengado, vacacionesJson');
    	$this->db->from('historial_laboral');
    	$this->db->where('idHistorialLaboral',$id);
    	$consulta = $this->db->get();
    	$resultado = $consulta->result();
    	
    	return $resultado;
    }

    function get_information(){
    	$this->db->select('idHistorialLaboral, nombreLaboral, cedulaLaboral, codigoLaboral, cargoLaboral, areaLaboral');
    	$this->db->from('historial_laboral');
    	$query = $this->db->get();
        $result = json_decode(json_encode($query->result()), True);
    	return $result;

    }

    function historiaLaboral($cedulaIngresada){

    	$this->db->select('nombreLaboral, cedulaLaboral, codigoLaboral, telefonoLaboral, cargoLaboral, areaLaboral, fechaIngreso, fechaRetiro, tipoContrato, comparendosEducativos, llamadosAtención, suspensiones, diasAusentismos, diasLaborados, eps, afp, cesantias, valorCesantias, causalRetiro, salarioDevengado, vacacionesJson');
    	$this->db->from('historial_laboral');
    	$this->db->where('cedulaLaboral', $cedulaIngresada);
    	$consul = $this->db->get();
    	$consulta = $consul->result();

    	if(!$consul->result_id->num_rows){
    		return $mensaje='noExiste';
    	}else{
    	
	    	$objXLS = new PHPExcel();
	        $objSheet = $objXLS->setActiveSheetIndex(0);

	        /*Agregamos la imágen*/
	        $gdImage = imagecreatefromjpeg(APPPATH.'img\logoExcel.jpg');
	        $objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
	        $objDrawing->setName('Sample image');
	        $objDrawing->setDescription('Sample image');
	        $objDrawing->setImageResource($gdImage);
	        $objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG);
	        $objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
	        $objDrawing->setHeight(100);
	        $objDrawing->setWorksheet($objXLS->getActiveSheet());
	        $objDrawing->setCoordinates('A1');

	        /*Empezamos a llenar las celdas del documento*/
	        $objSheet->setCellValue('C8', 'HISTORIA LABORAL');
	        $objSheet->setCellValue('B10', 'CONCEPTO');

	        $objSheet->setCellValue('B12', 'NOMBRE:');
	        $objSheet->setCellValue('D12', $consulta['0']->nombreLaboral);
	        $objSheet->setCellValue('B13', 'CÉDULA:');
	        $objSheet->setCellValue('D13', $consulta['0']->cedulaLaboral);
	        $objSheet->setCellValue('B14', 'CÓDIGO:');
	        $objSheet->setCellValue('D14', $consulta['0']->codigoLaboral);
	        $objSheet->setCellValue('B15', 'TELÉFONO:');
	        $objSheet->setCellValue('D15', $consulta['0']->telefonoLaboral);
	        $objSheet->setCellValue('B16', 'CARGO:');
	        $objSheet->setCellValue('D16', $consulta['0']->cargoLaboral);
	        $objSheet->setCellValue('B17', 'ÁREA:');
	        $objSheet->setCellValue('D17', $consulta['0']->areaLaboral);
	        $objSheet->setCellValue('B18', 'FECHA DE INGRESO:');
	        $objSheet->setCellValue('D18', $consulta['0']->fechaIngreso);
	        $objSheet->setCellValue('B19', 'FECHA DE RETIRO:');
	        $objSheet->setCellValue('D19', $consulta['0']->fechaRetiro);
	        $objSheet->setCellValue('B20', 'TIPO DE CONTRATO:');
	        $objSheet->setCellValue('D20', $consulta['0']->tipoContrato);
	        $objSheet->setCellValue('B21', 'COMPARENDOS EDUCATIVOS:');
	        $objSheet->setCellValue('D21', $consulta['0']->comparendosEducativos);
	        $objSheet->setCellValue('B22', 'LLAMADOS DE ATENCIÓN:');
	        $objSheet->setCellValue('D22', $consulta['0']->llamadosAtención);
	        $objSheet->setCellValue('B23', 'SUSPENSIONES:');
	        $objSheet->setCellValue('D23', $consulta['0']->suspensiones);
	        $objSheet->setCellValue('B24', 'DÍAS AUSENTISMOS:');
	        $objSheet->setCellValue('D24', $consulta['0']->diasAusentismos);
	        $objSheet->setCellValue('B25', 'DÍAS LABORADOS:');
	        $objSheet->setCellValue('D25', $consulta['0']->diasLaborados);
	        $objSheet->setCellValue('B26', 'EPS:');
	        $objSheet->setCellValue('D26', $consulta['0']->eps);
	        $objSheet->setCellValue('B27', 'AFP:');
	        $objSheet->setCellValue('D27', $consulta['0']->afp);
	        $objSheet->setCellValue('B28', 'CESANTÍAS:');
	        if ($consulta['0']->cesantias == "NO APLICA") {
	        	$objSheet->setCellValue('D28', $consulta['0']->cesantias);
	        }else{
	        	$objSheet->setCellValue('D28', $consulta['0']->cesantias." $".$consulta['0']->valorCesantias);
	        }
	        $objSheet->setCellValue('B29', 'CAUSAL DE RETIRO:');
	        $objSheet->setCellValue('D29', $consulta['0']->causalRetiro);
	        $objSheet->setCellValue('B30', 'SALARIO DEVENGADO:');
	        $objSheet->setCellValue('D30', $consulta['0']->salarioDevengado);

	        $cont = 31;
	        $arrayVacaciones = json_decode($consulta['0']->vacacionesJson);
	        /*Llenamos las celdas de las vacaciones*/
	        foreach ($arrayVacaciones as $arrayVacaciones) {
	        	
	        	$año = $arrayVacaciones->año;
	        	$inicio = $arrayVacaciones->inicio;
	        	$fin = $arrayVacaciones->fin;

	        	$objSheet->setCellValue("B".$cont, "VACACIONES ".$año);//Asignamos las preguntas a las celdas
	        	$objSheet->setCellValue("D".$cont, $inicio."     ".$fin);//Asignamos las preguntas a las celdas
	        	$cont++;
	        }	
	        
	        
	        $objXLS->getActiveSheet()->getStyle('C8')->getFont()->setBold(true);//Aplicamos tipo de letra negrita

	        $objXLS->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);//Aplicamos tamaño automático a las celdas de la fila B
	        $objXLS->getActiveSheet()->getStyle('B10:B33')->getFont()->setBold(true);//Aplicamos tipo de letra negrita

	        $objXLS->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);//Aplicamos tamaño automático a las celdas de la fila D
	        $objXLS->getActiveSheet()
	            ->getStyle('D10:D30')
	            ->getAlignment()
	            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);//Alineamos el texto de las celdas en la fila D a la izquierda

	    	$objXLS->getActiveSheet()->setCellValueExplicit('D14', $consulta['0']->codigoLaboral, PHPExcel_Cell_DataType::TYPE_STRING);//Ponemos el código como valor String para que tome los ceros "0"

	        $objXLS->getActiveSheet()->setTitle('Historia');
	        $objXLS->getActiveSheetIndex(0);

	        $objWriter = PHPExcel_IOFactory::createWriter($objXLS, 'Excel2007');
	        ob_end_clean();
	        // We'll be outputting an excel file
	        header('Content-type: application/vnd.ms-excel');
	        // It will be called file.xls
	        header('Content-Disposition: attachment; filename="Historia Laboral '.$consulta['0']->cedulaLaboral.'.xlsx"');
	        $objWriter->save('php://output');
	        return $mensaje='exito';
	    }
    }

    function pazSalvo($cedulaIngresada){

    	$this->db->select('idHistorialLaboral, nombreLaboral, cedulaLaboral, codigoLaboral, telefonoLaboral, fechaIngreso, fechaRetiro, causalRetiro');
    	$this->db->from('historial_laboral');
    	$this->db->where('cedulaLaboral', $cedulaIngresada);
    	$consul = $this->db->get();
    	$consulta = $consul->result();

    	if(!$consul->result_id->num_rows){
    		return $mensaje='noExiste';
    	}else{

	    	$consecutivo = $consulta['0']->idHistorialLaboral;
	    	$nombre = $consulta['0']->nombreLaboral;
	    	$cedula = $consulta['0']->cedulaLaboral;
	    	$codigo = $consulta['0']->codigoLaboral;
	    	$telefono = $consulta['0']->telefonoLaboral;
	    	$fechaIngreso = $consulta['0']->fechaIngreso;
	    	$fechaRetiro = $consulta['0']->fechaRetiro;
	    	$causalRetiro = $consulta['0']->causalRetiro;

	    	$objXLS = new PHPExcel();
	        $objSheet = $objXLS->setActiveSheetIndex(0);

	        //Imagen
	        $gdImage = imagecreatefromjpeg(APPPATH.'img\logoExcel.jpg');
	        $objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
	        $objDrawing->setName('Sample image');
	        $objDrawing->setDescription('Sample image');
	        $objDrawing->setImageResource($gdImage);
	        $objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG);
	        $objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
	        $objDrawing->setHeight(79);
	        $objDrawing->setWorksheet($objXLS->getActiveSheet());
	        $objDrawing->setCoordinates('A1');
			$objXLS->getActiveSheet()->mergeCells('A1:G4');

			//NIT
			$objSheet->setCellValue('H1', 'NIT 890.002.142-6');
			$objXLS->getActiveSheet()->getStyle("H1")->getFont()->setSize(20);
	        $objXLS->getActiveSheet()->getStyle('H1')->getFont()->setBold(true);
	        $objXLS->getActiveSheet()->mergeCells('H1:K4');
	        $objXLS->getActiveSheet()->getStyle('H1:K4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	        $objXLS->getActiveSheet()->getStyle('H1:K4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	        $styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('H1:K4')->applyFromArray($styleBorder);

	        //Versión
	        $objXLS->getActiveSheet()->setCellValue('L1', "FC-FO-5\nVersion 2");
			$objXLS->getActiveSheet()->getStyle('L1')->getAlignment()->setWrapText(true);
			$objXLS->getActiveSheet()->mergeCells('L1:M4');
			$objXLS->getActiveSheet()->getStyle("L1")->getFont()->setSize(12);
	        $objXLS->getActiveSheet()->getStyle('L1')->getFont()->setBold(true);
	        $objXLS->getActiveSheet()->getStyle('L1:M4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	        $objXLS->getActiveSheet()->getStyle('L1:M4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	        $styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('L1:M4')->applyFromArray($styleBorder);

	        //Título Paz y salvo 
	        $objXLS->getActiveSheet()->getCell('A5')->setValue('PAZ Y SALVO No. '.$consecutivo);
    		$objXLS->getActiveSheet()->mergeCells('A5:M5');
    		$objXLS->getActiveSheet()->getStyle('A5:M5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objSheet->getStyle('A5:M5')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => '222a34'))));
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => 'FFFFFF'),'size'  => 12,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('A5')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('A5:M5')->applyFromArray($styleBorder);

    		//Titulo Nombre
    		$objSheet->setCellValue('A6', 'Nombre:');
    		$objXLS->getActiveSheet()->mergeCells('A6:B6');
    		$objXLS->getActiveSheet()->getStyle('A6:B6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objSheet->getStyle('A6:B6')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'ddebf7'))));
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('A6:B6')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('A6:B6')->applyFromArray($styleBorder);

    		//Nombre de persona
    		$objSheet->setCellValue('C6', $nombre);
    		$objXLS->getActiveSheet()->mergeCells('C6:I6');
    		$objXLS->getActiveSheet()->getStyle('C6:I8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objSheet->getStyle('C6:I6')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'ffff00'))));
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('C6:I6')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('C6:I6')->applyFromArray($styleBorder);

    		//Titulo Cedula
    		$objSheet->setCellValue('J6', 'Cédula:');
    		$objXLS->getActiveSheet()->getStyle('J6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objSheet->getStyle('J6')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'ddebf7'))));
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('J6')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('J6')->applyFromArray($styleBorder);

    		//Cedula de persona
    		$objSheet->setCellValue('K6', $cedula);
    		$objXLS->getActiveSheet()->mergeCells('K6:M6');
    		$objXLS->getActiveSheet()->getStyle('K6:M6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objSheet->getStyle('K6:M6')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'ffff00'))));
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('K6:M6')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('K6:M6')->applyFromArray($styleBorder);

    		//Titulo Codigo
    		$objSheet->setCellValue('A7', 'Código:');
    		$objXLS->getActiveSheet()->mergeCells('A7:B7');
    		$objXLS->getActiveSheet()->getStyle('A7:B7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objSheet->getStyle('A7:B7')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'ddebf7'))));
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('A7:B7')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('A7:B7')->applyFromArray($styleBorder);

    		//Codigo de persona
    		$objXLS->getActiveSheet()->setCellValueExplicit('C7', $codigo, PHPExcel_Cell_DataType::TYPE_STRING);
    		$objXLS->getActiveSheet()->mergeCells('C7:D7');
    		$objXLS->getActiveSheet()->getStyle('C7:D7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objSheet->getStyle('C7:D7')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'ffff00'))));
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('C7:D7')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('C7:D7')->applyFromArray($styleBorder);

    		//Titulo oficina
    		$objSheet->setCellValue('E7', 'Oficina:');
    		$objXLS->getActiveSheet()->mergeCells('E7:F7');
    		$objXLS->getActiveSheet()->getStyle('E7:F7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objSheet->getStyle('E7:F7')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'ddebf7'))));
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('E7:F7')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('E7:F7')->applyFromArray($styleBorder);

    		//Oficina de persona
    		$objSheet->setCellValue('G7', "Armenia");
    		$objXLS->getActiveSheet()->mergeCells('G7:H7');
    		$objXLS->getActiveSheet()->getStyle('G7:H7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objSheet->getStyle('G7:H7')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'ffff00'))));
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('G7:H7')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('G7:H7')->applyFromArray($styleBorder);

    		//Titulo Telefono
    		$objSheet->setCellValue('I7', 'Teléfono:');
    		$objXLS->getActiveSheet()->mergeCells('I7:J7');
    		$objXLS->getActiveSheet()->getStyle('I7:J7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objSheet->getStyle('I7')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'ddebf7'))));
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('I7')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('I7:J7')->applyFromArray($styleBorder);

    		//Telefono de persona
    		$objSheet->setCellValue('K7', $telefono);
    		$objXLS->getActiveSheet()->mergeCells('K7:M7');
    		$objXLS->getActiveSheet()->getStyle('K7:M7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objSheet->getStyle('K7:M7')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'ffff00'))));
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('K7:M7')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('K7:M7')->applyFromArray($styleBorder);

    		//Titulo Fecha ingreso
    		$objSheet->setCellValue('A8', 'Fecha de ingreso:');
    		$objXLS->getActiveSheet()->mergeCells('A8:B8');
    		$objXLS->getActiveSheet()->getStyle('A8:B8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objSheet->getStyle('A8:B8')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'ddebf7'))));
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('A8:B8')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('A8:B8')->applyFromArray($styleBorder);

    		//fecha ingreso de persona
    		$objSheet->setCellValue('C8', $fechaIngreso);
    		$objXLS->getActiveSheet()->mergeCells('C8:D8');
    		$objXLS->getActiveSheet()->getStyle('C8:D8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objSheet->getStyle('C8:D8')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'ffff00'))));
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('C8:D8')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('C8:D8')->applyFromArray($styleBorder);

    		//Titulo fecha retiro
    		$objSheet->setCellValue('E8', 'Fecha de retiro:');
    		$objXLS->getActiveSheet()->mergeCells('E8:F8');
    		$objXLS->getActiveSheet()->getStyle('E8:F8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objSheet->getStyle('E8:F8')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'ddebf7'))));
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('E8:F8')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('E8:F8')->applyFromArray($styleBorder);

    		//fecha retiro de persona
    		$objSheet->setCellValue('G8', $fechaRetiro);
    		$objXLS->getActiveSheet()->mergeCells('G8:H8');
    		$objXLS->getActiveSheet()->getStyle('G8:H8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objSheet->getStyle('G8:H8')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'ffff00'))));
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('G8:H8')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('G8:H8')->applyFromArray($styleBorder);

    		//Titulo Causa
    		$objSheet->setCellValue('I8', 'Causa:');
    		$objXLS->getActiveSheet()->mergeCells('I8:J8');
    		$objXLS->getActiveSheet()->getStyle('I8:J8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objSheet->getStyle('I8')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'ddebf7'))));
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('I8')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('I8:J8')->applyFromArray($styleBorder);

    		//Causa de persona
    		$objSheet->setCellValue('K8', $causalRetiro);
    		$objXLS->getActiveSheet()->mergeCells('K8:M8');
    		$objXLS->getActiveSheet()->getStyle('K8:M8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objSheet->getStyle('K8:M8')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'ffff00'))));
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('K8:M8')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('K8:M8')->applyFromArray($styleBorder);

    		
    		//Tabla Oficina comercial
	        $objXLS->getActiveSheet()->getCell('A9')->setValue('OFICINA COMERCIAL');
    		$objXLS->getActiveSheet()->mergeCells('A9:M9');
    		$objXLS->getActiveSheet()->getStyle('A9:M9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objSheet->getStyle('A9:M9')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => '2F75B5'))));
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => 'FFFFFF'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('A9')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('A9:M9')->applyFromArray($styleBorder);

	        $objXLS->getActiveSheet()->getCell('A10')->setValue('Devolución completa elementos de trabajo (Marque x si aplica)');
    		$objXLS->getActiveSheet()->mergeCells('A10:M10');
    		$objXLS->getActiveSheet()->getStyle('A10:M10')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('A10')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('A10:M10')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('A11', '');
    		$objXLS->getActiveSheet()->mergeCells('A11:A12');
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('A11:A12')->applyFromArray($styleBorder);

    		$objXLS->getActiveSheet()->setCellValue('B11', "Papelería\ntipo Bond");
			$objXLS->getActiveSheet()->getStyle('B11')->getAlignment()->setWrapText(true);
    		$objXLS->getActiveSheet()->mergeCells('B11:C12');
    		$objXLS->getActiveSheet()->getStyle('B11:C12')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('B11')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('B11:C12')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('D11', '');
    		$objXLS->getActiveSheet()->mergeCells('D11:D12');
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('D11:D12')->applyFromArray($styleBorder);

    		$objXLS->getActiveSheet()->setCellValue('E11', "Papelería\ntipo Térmica");
			$objXLS->getActiveSheet()->getStyle('E11')->getAlignment()->setWrapText(true);
    		$objXLS->getActiveSheet()->mergeCells('E11:F12');
    		$objXLS->getActiveSheet()->getStyle('E11:F12')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('E11')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('E11:F12')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('G11', '');
    		$objXLS->getActiveSheet()->mergeCells('G11:G12');
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('G11:G12')->applyFromArray($styleBorder);

    		$objXLS->getActiveSheet()->setCellValue('H11', "Máquina\nMóvil");
			$objXLS->getActiveSheet()->getStyle('H11')->getAlignment()->setWrapText(true);
    		$objXLS->getActiveSheet()->mergeCells('H11:I12');
    		$objXLS->getActiveSheet()->getStyle('H11:I12')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('H11')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('H11:I12')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('J11', '');
    		$objXLS->getActiveSheet()->mergeCells('J11:J12');
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('J11:J12')->applyFromArray($styleBorder);

    		$objXLS->getActiveSheet()->setCellValue('K11', "Devolución de\nloterías");
			$objXLS->getActiveSheet()->getStyle('K11')->getAlignment()->setWrapText(true);
    		$objXLS->getActiveSheet()->mergeCells('K11:M12');
    		$objXLS->getActiveSheet()->getStyle('K11:M12')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('K11')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('K11:M12')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('A13', '');
    		$objXLS->getActiveSheet()->mergeCells('A13:A14');
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('A13:A14')->applyFromArray($styleBorder);

    		$objXLS->getActiveSheet()->setCellValue('B13', "Precintos o bolsas\nde seguridad");
			$objXLS->getActiveSheet()->getStyle('B13')->getAlignment()->setWrapText(true);
    		$objXLS->getActiveSheet()->mergeCells('B13:C14');
    		$objXLS->getActiveSheet()->getStyle('B13:C14')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('B13')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('B13:C14')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('D13', '');
    		$objXLS->getActiveSheet()->mergeCells('D13:D14');
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('D13:D14')->applyFromArray($styleBorder);

    		$objXLS->getActiveSheet()->setCellValue('E13', "");
			$objXLS->getActiveSheet()->getStyle('E13')->getAlignment()->setWrapText(true);
    		$objXLS->getActiveSheet()->mergeCells('E13:F14');
    		$objXLS->getActiveSheet()->getStyle('E13:F14')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('E13')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('E13:F14')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('G13', '');
    		$objXLS->getActiveSheet()->mergeCells('G13:G14');
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('G13:G14')->applyFromArray($styleBorder);

    		$objXLS->getActiveSheet()->setCellValue('H13', "");
			$objXLS->getActiveSheet()->getStyle('H13')->getAlignment()->setWrapText(true);
    		$objXLS->getActiveSheet()->mergeCells('H13:I14');
    		$objXLS->getActiveSheet()->getStyle('H13:I14')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('H13')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('H13:I14')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('J13', '');
    		$objXLS->getActiveSheet()->mergeCells('J13:J14');
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('J13:J14')->applyFromArray($styleBorder);

    		$objXLS->getActiveSheet()->setCellValue('K13', "");
			$objXLS->getActiveSheet()->getStyle('K13')->getAlignment()->setWrapText(true);
    		$objXLS->getActiveSheet()->mergeCells('K13:M14');
    		$objXLS->getActiveSheet()->getStyle('K13:M14')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('K13')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('K13:M14')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('A15', "Observaciones:");
    		$objXLS->getActiveSheet()->mergeCells('A15:G18');
    		$objXLS->getActiveSheet()->getStyle('A15:G18')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    		$objXLS->getActiveSheet()->getStyle('A15:G18')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('A15')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('A15:G18')->applyFromArray($styleBorder);

    		$objXLS->getActiveSheet()->setCellValue('H15', "Nombre y Firma\nquien verifica");
			$objXLS->getActiveSheet()->getStyle('H15')->getAlignment()->setWrapText(true);
    		$objXLS->getActiveSheet()->mergeCells('H15:I18');
    		$objXLS->getActiveSheet()->getStyle('H15:I18')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    		$objXLS->getActiveSheet()->getStyle('H15:I18')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objSheet->getStyle('H15:I18')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => '2F75B5'))));
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => 'FFFFFF'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('H15')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('H15:I18')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('J15', '');
    		$objXLS->getActiveSheet()->mergeCells('J15:M15');
			$objSheet->getStyle('J15:M15')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'ffff'))));
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('J15:M15')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('J16', 'Cargo: Administrador de Oficina');
    		$objXLS->getActiveSheet()->mergeCells('J16:M16');
			$objSheet->getStyle('J16:M16')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'ddebf7'))));
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('J16:M16')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('J16:M16')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('J17', '');
    		$objXLS->getActiveSheet()->mergeCells('J17:M17');
			$objSheet->getStyle('J17:M17')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'ffffff'))));
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('J17:M17')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('J18', 'Cargo: Auxiliar premios');
    		$objXLS->getActiveSheet()->mergeCells('J18:M18');
			$objSheet->getStyle('J18:M18')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'ddebf7'))));
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('J18:M18')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('J18:M18')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('A19', '');
    		$objXLS->getActiveSheet()->mergeCells('A19:M19');
			$objSheet->getStyle('A19:M19')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'ddebf7'))));
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('A19:M19')->applyFromArray($styleBorder);


    		//Tabla Ahorro y crédito
	        $objXLS->getActiveSheet()->getCell('A20')->setValue('AHORRO Y CREDITO');
    		$objXLS->getActiveSheet()->mergeCells('A20:M20');
    		$objXLS->getActiveSheet()->getStyle('A20:M20')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objSheet->getStyle('A20:M20')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => '2F75B5'))));
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => 'FFFFFF'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('A20')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('A20:M20')->applyFromArray($styleBorder);

	        $objXLS->getActiveSheet()->getCell('A21')->setValue('Devolución completa elementos de trabajo (Marque x si aplica)');
    		$objXLS->getActiveSheet()->mergeCells('A21:M21');
    		$objXLS->getActiveSheet()->getStyle('A21:M21')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('A21')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('A21:M21')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('A22', '');
    		$objXLS->getActiveSheet()->mergeCells('A22:A23');
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('A22:A23')->applyFromArray($styleBorder);

    		$objXLS->getActiveSheet()->setCellValue('B22', "");
    		$objXLS->getActiveSheet()->mergeCells('B22:C23');
    		$objXLS->getActiveSheet()->getStyle('B22:C23')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('B22')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('B22:C23')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('D22', '');
    		$objXLS->getActiveSheet()->mergeCells('D22:D23');
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('D22:D23')->applyFromArray($styleBorder);

    		$objXLS->getActiveSheet()->setCellValue('E22', "");
			$objXLS->getActiveSheet()->getStyle('E22')->getAlignment()->setWrapText(true);
    		$objXLS->getActiveSheet()->mergeCells('E22:F23');
    		$objXLS->getActiveSheet()->getStyle('E22:F23')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('E22')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('E22:F23')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('G22', '');
    		$objXLS->getActiveSheet()->mergeCells('G22:G23');
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('G22:G23')->applyFromArray($styleBorder);

    		$objXLS->getActiveSheet()->setCellValue('H22', "");
			$objXLS->getActiveSheet()->getStyle('H22')->getAlignment()->setWrapText(true);
    		$objXLS->getActiveSheet()->mergeCells('H22:I23');
    		$objXLS->getActiveSheet()->getStyle('H22:I23')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('H22')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('H22:I23')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('J22', '');
    		$objXLS->getActiveSheet()->mergeCells('J22:J23');
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('J22:J23')->applyFromArray($styleBorder);

    		$objXLS->getActiveSheet()->setCellValue('K22', "");
			$objXLS->getActiveSheet()->getStyle('K22')->getAlignment()->setWrapText(true);
    		$objXLS->getActiveSheet()->mergeCells('K22:M23');
    		$objXLS->getActiveSheet()->getStyle('K22:M23')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('K22')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('K22:M23')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('A24', "Observaciones:");
    		$objXLS->getActiveSheet()->mergeCells('A24:G27');
    		$objXLS->getActiveSheet()->getStyle('A24:G27')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    		$objXLS->getActiveSheet()->getStyle('A24:G27')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('A24')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('A24:G27')->applyFromArray($styleBorder);

    		$objXLS->getActiveSheet()->setCellValue('H24', "Nombre y Firma\nquien verifica");
			$objXLS->getActiveSheet()->getStyle('H24')->getAlignment()->setWrapText(true);
    		$objXLS->getActiveSheet()->mergeCells('H24:I27');
    		$objXLS->getActiveSheet()->getStyle('H24:I27')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    		$objXLS->getActiveSheet()->getStyle('H24:I27')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objSheet->getStyle('H24:I27')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => '2F75B5'))));
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => 'FFFFFF'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('H24')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('H24:I27')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('J24', '');
    		$objXLS->getActiveSheet()->mergeCells('J24:M24');
			$objSheet->getStyle('J24:M24')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'ffff'))));
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('J24:M24')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('J25', 'Cargo:');
    		$objXLS->getActiveSheet()->mergeCells('J25:M25');
			$objSheet->getStyle('J25:M25')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'ddebf7'))));
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('J25:M25')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('J25:M25')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('J26', '');
    		$objXLS->getActiveSheet()->mergeCells('J26:M26');
			$objSheet->getStyle('J26:M26')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'ffffff'))));
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('J26:M26')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('J27', 'Cargo:');
    		$objXLS->getActiveSheet()->mergeCells('J27:M27');
			$objSheet->getStyle('J27:M27')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'ddebf7'))));
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('J27:M27')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('J27:M27')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('A28', '');
    		$objXLS->getActiveSheet()->mergeCells('A28:M28');
			$objSheet->getStyle('A28:M28')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'ddebf7'))));
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('A28:M28')->applyFromArray($styleBorder);


    		//Tabla Sistemas
	        $objXLS->getActiveSheet()->getCell('A29')->setValue('SISTEMAS');
    		$objXLS->getActiveSheet()->mergeCells('A29:M29');
    		$objXLS->getActiveSheet()->getStyle('A29:M29')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objSheet->getStyle('A29:M29')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => '2F75B5'))));
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => 'FFFFFF'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('A29')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('A29:M29')->applyFromArray($styleBorder);

	        $objXLS->getActiveSheet()->getCell('A30')->setValue('Devolución completa elementos de trabajo (Marque x si aplica)');
    		$objXLS->getActiveSheet()->mergeCells('A30:M30');
    		$objXLS->getActiveSheet()->getStyle('A30:M30')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('A30')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('A30:M30')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('A31', '');
    		$objXLS->getActiveSheet()->mergeCells('A31:A32');
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('A31:A32')->applyFromArray($styleBorder);

    		$objXLS->getActiveSheet()->setCellValue('B31', "Usuarios de\nventas");
			$objXLS->getActiveSheet()->getStyle('B31')->getAlignment()->setWrapText(true);
    		$objXLS->getActiveSheet()->mergeCells('B31:C32');
    		$objXLS->getActiveSheet()->getStyle('B31:C32')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('B31')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('B31:C32')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('D31', '');
    		$objXLS->getActiveSheet()->mergeCells('D31:D32');
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('D31:D32')->applyFromArray($styleBorder);

    		$objXLS->getActiveSheet()->setCellValue('E31', "Usuarios\nadministrativos");
			$objXLS->getActiveSheet()->getStyle('E31')->getAlignment()->setWrapText(true);
    		$objXLS->getActiveSheet()->mergeCells('E31:F32');
    		$objXLS->getActiveSheet()->getStyle('E31:F32')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('E31')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('E31:F32')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('G31', '');
    		$objXLS->getActiveSheet()->mergeCells('G31:G32');
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('G31:G32')->applyFromArray($styleBorder);

    		$objXLS->getActiveSheet()->setCellValue('H31', "Inactiva cuenta\nde correo");
			$objXLS->getActiveSheet()->getStyle('H31')->getAlignment()->setWrapText(true);
    		$objXLS->getActiveSheet()->mergeCells('H31:I32');
    		$objXLS->getActiveSheet()->getStyle('H31:I32')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('H31')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('H31:I32')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('J31', '');
    		$objXLS->getActiveSheet()->mergeCells('J31:J32');
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('J31:J32')->applyFromArray($styleBorder);

    		$objXLS->getActiveSheet()->setCellValue('K31', "Inactiva Terminal Server,\n VPN, entre otros");
			$objXLS->getActiveSheet()->getStyle('K31')->getAlignment()->setWrapText(true);
    		$objXLS->getActiveSheet()->mergeCells('K31:M32');
    		$objXLS->getActiveSheet()->getStyle('K31:M32')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('K31')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('K31:M32')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('A33', '');
    		$objXLS->getActiveSheet()->mergeCells('A33:A34');
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('A33:A34')->applyFromArray($styleBorder);

    		$objXLS->getActiveSheet()->setCellValue('B33', "Aplicaciones\nexternas");
			$objXLS->getActiveSheet()->getStyle('B33')->getAlignment()->setWrapText(true);
    		$objXLS->getActiveSheet()->mergeCells('B33:C34');
    		$objXLS->getActiveSheet()->getStyle('B33:C34')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('B33')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('B33:C34')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('D33', '');
    		$objXLS->getActiveSheet()->mergeCells('D33:D34');
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('D33:D34')->applyFromArray($styleBorder);

    		$objXLS->getActiveSheet()->setCellValue('E33', "Inactiva\nServicios)");
			$objXLS->getActiveSheet()->getStyle('E33')->getAlignment()->setWrapText(true);
			$objXLS->getActiveSheet()->getStyle('E33')->getAlignment()->setWrapText(true);
    		$objXLS->getActiveSheet()->mergeCells('E33:F34');
    		$objXLS->getActiveSheet()->getStyle('E33:F34')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('E33')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('E33:F34')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('G33', '');
    		$objXLS->getActiveSheet()->mergeCells('G33:G34');
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('G33:G34')->applyFromArray($styleBorder);

    		$objXLS->getActiveSheet()->setCellValue('H33', "Otros");
			$objXLS->getActiveSheet()->getStyle('H33')->getAlignment()->setWrapText(true);
    		$objXLS->getActiveSheet()->mergeCells('H33:I34');
    		$objXLS->getActiveSheet()->getStyle('H33:I34')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    		$objXLS->getActiveSheet()->getStyle('H33:I34')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('H33')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('H33:I34')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('J33', '');
    		$objXLS->getActiveSheet()->mergeCells('J33:J34');
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('J33:J34')->applyFromArray($styleBorder);

    		$objXLS->getActiveSheet()->setCellValue('K33', "");
			$objXLS->getActiveSheet()->getStyle('K33')->getAlignment()->setWrapText(true);
    		$objXLS->getActiveSheet()->mergeCells('K33:M34');
    		$objXLS->getActiveSheet()->getStyle('K33:M34')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('K33')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('K33:M34')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('A35', "Observaciones:");
    		$objXLS->getActiveSheet()->mergeCells('A35:G38');
    		$objXLS->getActiveSheet()->getStyle('A35:G38')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    		$objXLS->getActiveSheet()->getStyle('A35:G38')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('A35')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('A35:G38')->applyFromArray($styleBorder);

    		$objXLS->getActiveSheet()->setCellValue('H35', "Nombre y Firma\nquien verifica");
			$objXLS->getActiveSheet()->getStyle('H35')->getAlignment()->setWrapText(true);
    		$objXLS->getActiveSheet()->mergeCells('H35:I38');
    		$objXLS->getActiveSheet()->getStyle('H35:I38')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    		$objXLS->getActiveSheet()->getStyle('H35:I38')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objSheet->getStyle('H35:I38')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => '2F75B5'))));
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => 'FFFFFF'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('H35')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('H35:I38')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('J35', '');
    		$objXLS->getActiveSheet()->mergeCells('J35:M35');
			$objSheet->getStyle('J35:M35')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'ffff'))));
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('J35:M35')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('J36', 'Cargo:');
    		$objXLS->getActiveSheet()->mergeCells('J36:M36');
			$objSheet->getStyle('J36:M36')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'ddebf7'))));
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('J36:M36')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('J36:M36')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('J37', '');
    		$objXLS->getActiveSheet()->mergeCells('J37:M37');
			$objSheet->getStyle('J37:M37')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'ffffff'))));
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('J37:M37')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('J38', 'Cargo:');
    		$objXLS->getActiveSheet()->mergeCells('J38:M38');
			$objSheet->getStyle('J38:M38')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'ddebf7'))));
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('J38:M38')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('J38:M38')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('A39', '');
    		$objXLS->getActiveSheet()->mergeCells('A39:M39');
			$objSheet->getStyle('A39:M39')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'ddebf7'))));
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('A39:M39')->applyFromArray($styleBorder);


    		//Tabla gestión humana
	        $objXLS->getActiveSheet()->getCell('A40')->setValue('GESTIÓN HUMANA');
    		$objXLS->getActiveSheet()->mergeCells('A40:M40');
    		$objXLS->getActiveSheet()->getStyle('A40:M40')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objSheet->getStyle('A40:M40')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => '2F75B5'))));
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => 'FFFFFF'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('A40')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('A40:M40')->applyFromArray($styleBorder);

	        $objXLS->getActiveSheet()->getCell('A41')->setValue('Devolución completa elementos de trabajo (Marque x si aplica)');
    		$objXLS->getActiveSheet()->mergeCells('A41:M41');
    		$objXLS->getActiveSheet()->getStyle('A41:M41')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('A41')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('A41:M41')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('A42', '');
    		$objXLS->getActiveSheet()->mergeCells('A42:A43');
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('A42:A43')->applyFromArray($styleBorder);

    		$objXLS->getActiveSheet()->setCellValue('B42', "Dotación con\ndistintivo");
			$objXLS->getActiveSheet()->getStyle('B42')->getAlignment()->setWrapText(true);
    		$objXLS->getActiveSheet()->mergeCells('B42:C43');
    		$objXLS->getActiveSheet()->getStyle('B42:C43')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('B42')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('B42:C43')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('D42', '');
    		$objXLS->getActiveSheet()->mergeCells('D42:D43');
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('D42:D43')->applyFromArray($styleBorder);

    		$objXLS->getActiveSheet()->setCellValue('E42', "Exámen médico\nde retiro");
			$objXLS->getActiveSheet()->getStyle('E42')->getAlignment()->setWrapText(true);
    		$objXLS->getActiveSheet()->mergeCells('E42:F43');
    		$objXLS->getActiveSheet()->getStyle('E42:F43')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('E42')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('E42:F43')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('G42', '');
    		$objXLS->getActiveSheet()->mergeCells('G42:G43');
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('G42:G43')->applyFromArray($styleBorder);

    		$objXLS->getActiveSheet()->setCellValue('H42', "Carnet");
			$objXLS->getActiveSheet()->getStyle('H42')->getAlignment()->setWrapText(true);
    		$objXLS->getActiveSheet()->mergeCells('H42:I43');
    		$objXLS->getActiveSheet()->getStyle('H42:I43')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    		$objXLS->getActiveSheet()->getStyle('H42:I43')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('H42')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('H42:I43')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('J42', '');
    		$objXLS->getActiveSheet()->mergeCells('J42:J43');
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('J42:J43')->applyFromArray($styleBorder);

    		$objXLS->getActiveSheet()->setCellValue('K42', "Otros");
			$objXLS->getActiveSheet()->getStyle('K42')->getAlignment()->setWrapText(true);
    		$objXLS->getActiveSheet()->mergeCells('K42:M43');
    		$objXLS->getActiveSheet()->getStyle('K42:M43')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    		$objXLS->getActiveSheet()->getStyle('K42:M43')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('K42')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('K42:M43')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('A44', "Observaciones:");
    		$objXLS->getActiveSheet()->mergeCells('A44:G47');
    		$objXLS->getActiveSheet()->getStyle('A44:G47')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    		$objXLS->getActiveSheet()->getStyle('A44:G47')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('A44')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('A44:G47')->applyFromArray($styleBorder);

    		$objXLS->getActiveSheet()->setCellValue('H44', "Nombre y Firma\nquien verifica");
			$objXLS->getActiveSheet()->getStyle('H44')->getAlignment()->setWrapText(true);
    		$objXLS->getActiveSheet()->mergeCells('H44:I47');
    		$objXLS->getActiveSheet()->getStyle('H44:I47')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    		$objXLS->getActiveSheet()->getStyle('H44:I47')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objSheet->getStyle('H44:I47')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => '2F75B5'))));
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => 'FFFFFF'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('H44')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('H44:I47')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('J44', '');
    		$objXLS->getActiveSheet()->mergeCells('J44:M44');
			$objSheet->getStyle('J44:M44')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'ffff'))));
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('J44:M44')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('J45', 'Cargo:');
    		$objXLS->getActiveSheet()->mergeCells('J45:M45');
			$objSheet->getStyle('J45:M45')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'ddebf7'))));
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('J45:M45')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('J45:M45')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('J46', '');
    		$objXLS->getActiveSheet()->mergeCells('J46:M46');
			$objSheet->getStyle('J46:M46')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'ffffff'))));
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('J46:M46')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('J47', 'Cargo:');
    		$objXLS->getActiveSheet()->mergeCells('J47:M47');
			$objSheet->getStyle('J47:M47')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'ddebf7'))));
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('J47:M47')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('J47:M47')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('A48', '');
    		$objXLS->getActiveSheet()->mergeCells('A48:M48');
			$objSheet->getStyle('A48:M48')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'ddebf7'))));
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('A48:M48')->applyFromArray($styleBorder);


    		//Tabla control interno
	        $objXLS->getActiveSheet()->getCell('A49')->setValue('CONTROL INTERNO');
    		$objXLS->getActiveSheet()->mergeCells('A49:M49');
    		$objXLS->getActiveSheet()->getStyle('A49:M49')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objSheet->getStyle('A49:M49')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => '2F75B5'))));
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => 'FFFFFF'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('A49')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('A49:M49')->applyFromArray($styleBorder);

	        $objXLS->getActiveSheet()->getCell('A50')->setValue('Devolución completa elementos de trabajo (Marque x si aplica)');
    		$objXLS->getActiveSheet()->mergeCells('A50:M50');
    		$objXLS->getActiveSheet()->getStyle('A50:M50')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('A50')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('A50:M50')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('A51', '');
    		$objXLS->getActiveSheet()->mergeCells('A51:A52');
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('A51:A52')->applyFromArray($styleBorder);

    		$objXLS->getActiveSheet()->setCellValue('B51', "Llaves de\nacceso oficinas");
			$objXLS->getActiveSheet()->getStyle('B51')->getAlignment()->setWrapText(true);
    		$objXLS->getActiveSheet()->mergeCells('B51:C52');
    		$objXLS->getActiveSheet()->getStyle('B51:C52')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('B51')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('B51:C52')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('D51', '');
    		$objXLS->getActiveSheet()->mergeCells('D51:D52');
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('D51:D52')->applyFromArray($styleBorder);

    		$objXLS->getActiveSheet()->setCellValue('E51', "Teléfono y/o\nSim Card");
			$objXLS->getActiveSheet()->getStyle('E51')->getAlignment()->setWrapText(true);
    		$objXLS->getActiveSheet()->mergeCells('E51:F52');
    		$objXLS->getActiveSheet()->getStyle('E51:F52')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('E51')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('E51:F52')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('G51', '');
    		$objXLS->getActiveSheet()->mergeCells('G51:G52');
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('G51:G52')->applyFromArray($styleBorder);

    		$objXLS->getActiveSheet()->setCellValue('H51', "Llaves de \nacceso oficinas");
			$objXLS->getActiveSheet()->getStyle('H51')->getAlignment()->setWrapText(true);
    		$objXLS->getActiveSheet()->mergeCells('H51:I52');
    		$objXLS->getActiveSheet()->getStyle('H51:I52')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    		$objXLS->getActiveSheet()->getStyle('H51:I52')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('H51')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('H51:I52')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('J51', '');
    		$objXLS->getActiveSheet()->mergeCells('J51:J52');
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('J51:J52')->applyFromArray($styleBorder);

    		$objXLS->getActiveSheet()->setCellValue('K51', "Inactivacion Password\nalarmas o cajas de seguridad");
			$objXLS->getActiveSheet()->getStyle('K51')->getAlignment()->setWrapText(true);
    		$objXLS->getActiveSheet()->mergeCells('K51:M52');
    		$objXLS->getActiveSheet()->getStyle('K51:M52')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    		$objXLS->getActiveSheet()->getStyle('K51:M52')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('K51')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('K51:M52')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('A53', "Observaciones:");
    		$objXLS->getActiveSheet()->mergeCells('A53:G56');
    		$objXLS->getActiveSheet()->getStyle('A53:G56')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    		$objXLS->getActiveSheet()->getStyle('A53:G56')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('A53')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('A53:G56')->applyFromArray($styleBorder);

    		$objXLS->getActiveSheet()->setCellValue('H53', "Nombre y Firma\nquien verifica");
			$objXLS->getActiveSheet()->getStyle('H53')->getAlignment()->setWrapText(true);
    		$objXLS->getActiveSheet()->mergeCells('H53:I56');
    		$objXLS->getActiveSheet()->getStyle('H53:I56')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    		$objXLS->getActiveSheet()->getStyle('H53:I56')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objSheet->getStyle('H53:I56')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => '2F75B5'))));
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => 'FFFFFF'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('H53')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('H53:I56')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('J53', '');
    		$objXLS->getActiveSheet()->mergeCells('J53:M53');
			$objSheet->getStyle('J53:M53')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'ffff'))));
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('J53:M53')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('J54', 'Cargo:');
    		$objXLS->getActiveSheet()->mergeCells('J54:M54');
			$objSheet->getStyle('J54:M54')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'ddebf7'))));
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('J54:M54')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('J54:M54')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('J55', '');
    		$objXLS->getActiveSheet()->mergeCells('J55:M55');
			$objSheet->getStyle('J55:M55')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'ffffff'))));
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('J55:M55')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('J56', 'Cargo:');
    		$objXLS->getActiveSheet()->mergeCells('J56:M56');
			$objSheet->getStyle('J56:M56')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'ddebf7'))));
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('J56:M56')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('J56:M56')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('A57', '');
    		$objXLS->getActiveSheet()->mergeCells('A57:M57');
			$objSheet->getStyle('A57:M57')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'ddebf7'))));
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('A57:M57')->applyFromArray($styleBorder);


    		//Tabla contabilidad
	        $objXLS->getActiveSheet()->getCell('A58')->setValue('CONTABILIDAD');
    		$objXLS->getActiveSheet()->mergeCells('A58:M58');
    		$objXLS->getActiveSheet()->getStyle('A58:M58')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objSheet->getStyle('A58:M58')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => '2F75B5'))));
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => 'FFFFFF'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('A58')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('A58:M58')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('A59', "Observaciones:");
    		$objXLS->getActiveSheet()->mergeCells('A59:G62');
    		$objXLS->getActiveSheet()->getStyle('A59:G62')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    		$objXLS->getActiveSheet()->getStyle('A59:G62')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('A59')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('A59:G62')->applyFromArray($styleBorder);

    		$objXLS->getActiveSheet()->setCellValue('H59', "Nombre y Firma\nquien verifica");
			$objXLS->getActiveSheet()->getStyle('H59')->getAlignment()->setWrapText(true);
    		$objXLS->getActiveSheet()->mergeCells('H59:I62');
    		$objXLS->getActiveSheet()->getStyle('H59:I62')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    		$objXLS->getActiveSheet()->getStyle('H59:I62')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objSheet->getStyle('H59:I62')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => '2F75B5'))));
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => 'FFFFFF'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('H59')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('H59:I62')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('J59', '');
    		$objXLS->getActiveSheet()->mergeCells('J59:M61');
			$objSheet->getStyle('J59:M61')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'ffff'))));
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('J59:M61')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('J62', 'Cargo:');
    		$objXLS->getActiveSheet()->mergeCells('J62:M62');
			$objSheet->getStyle('J62:M62')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'ddebf7'))));
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('J62:M62')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('J62:M62')->applyFromArray($styleBorder);

    		$objSheet->setCellValue('A63', '');
    		$objXLS->getActiveSheet()->mergeCells('A63:M63');
			$objSheet->getStyle('A63:M63')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'ddebf7'))));
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('A63:M63')->applyFromArray($styleBorder);

    		$objXLS->getActiveSheet()->getCell('A64')->setValue('IMPORTANTE: Este documento es prerequisito para la entrega de la liquidación en el departamento de Tesorería');
    		$objXLS->getActiveSheet()->mergeCells('A64:M64');
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('A64')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('A64:M64')->applyFromArray($styleBorder);

    		$objXLS->getActiveSheet()->getCell('A65')->setValue('Original: liquidación');
    		$objXLS->getActiveSheet()->mergeCells('A65:M65');
    		$objXLS->getActiveSheet()->getStyle('A65:M65')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
    		$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 10,'name'  => 'Arial'));
    		$objXLS->getActiveSheet()->getStyle('A65')->applyFromArray($styleArray);
    		$styleBorder = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
    		$objXLS->getActiveSheet()->getStyle('A65:M65')->applyFromArray($styleBorder);

    		








    		
    		


	        

	        $objXLS->getActiveSheet()->setTitle($consecutivo);
	        $objXLS->getActiveSheetIndex(0);

	        $objWriter = PHPExcel_IOFactory::createWriter($objXLS, 'Excel2007');
	        ob_end_clean();
	        // We'll be outputting an excel file
	        header('Content-type: application/vnd.ms-excel');
	        // It will be called file.xls
	        header('Content-Disposition: attachment; filename="Paz y salvo '.$consulta['0']->cedulaLaboral.'.xlsx"');
	        $objWriter->save('php://output');
	        return $mensaje='exito';
	    }
    }

    function retiroCesantias($cedulaIngresada){

    	$this->db->select('nombreLaboral, cedulaLaboral, cesantias, fechaRetiro');
    	$this->db->from('historial_laboral');
    	$this->db->where('cedulaLaboral', $cedulaIngresada);
    	$consul = $this->db->get();
    	$consulta = $consul->result();

    	if(!$consul->result_id->num_rows){
    		return $mensaje='noExiste';
    	}else{

    		$nombre = $consulta['0']->nombreLaboral;
	    	$cedula = $consulta['0']->cedulaLaboral;
	    	$cesantias = $consulta['0']->cesantias;
	    	$fechaRetiro = $consulta['0']->fechaRetiro;

	    	$fechaSeparada = explode("-", $fechaRetiro);
	    	
	    	$pdf = new retiroDeCesantiasWord();

	        $diaRetiroLetras = $pdf->obtenerDia($fechaSeparada['2']);
	        $mesRetiroLetras = $pdf->obtenerMes($fechaSeparada['1']);

	    	$pdf->AliasNbPages();
	        $pdf->SetTitle("Retiro de cesantias");
	        $pdf->SetMargins(25,0);
	        $pdf->AddPage();
	        $pdf->Ln(70);
	        $pdf->SetFont('Arial','',14);
	        $pdf->Cell(160,5,utf8_decode('Armenia, '.$mesRetiroLetras.' '.$fechaSeparada['2'].' de '.$fechaSeparada['0']), 0);
	        $pdf->Ln(30);
	        $pdf->Cell(160,5,utf8_decode('Señores'), 0);
	        $pdf->Ln();
	        $pdf->SetFont('Arial','B',14);
	        $pdf->Cell(160,5,utf8_decode($cesantias), 0);
	        $pdf->Ln();
	        $pdf->Cell(160,5,utf8_decode('Cesantías'), 0);
	        $pdf->Ln();
	        $pdf->SetFont('Arial','',14);
	        $pdf->Cell(160,5,utf8_decode('Armenia'), 0);
	        $pdf->Ln(20);
	        $pdf->Cell(160,5,utf8_decode('Asunto: Autorización retiro de cesantías'), 0,0,'R');
	        $pdf->Ln(20);
	        $pdf->MultiCell(160,5,utf8_decode('Por medio de la presente le informo que el(la) señor(a) '.$nombre.' identificado(a) con la cedula de ciudadanía numero '.$cedula.' laboró en nuestra empresa hasta el '.$diaRetiroLetras.' ('.$fechaSeparada['2'].') de '.$mesRetiroLetras.' del año '.$fechaSeparada['0'].'.'), 0,'J');
	        $pdf->Ln(20);
	        $pdf->Cell(160,5,utf8_decode('Por lo anterior autorizo el retiro de cesantías.'), 0,'J');
	        $pdf->Ln(40);
	        $pdf->SetFont('Arial','B',14);
	        $pdf->Cell(160,5,utf8_decode('Luis Alfonso Martínez Martínez'), 0);
	        $pdf->Ln();
	        $pdf->Cell(160,5,utf8_decode('Director de Gestión Humana'), 0);
	        $pdf->Output('i','Retiro de cesantias C.C. '.$cedula.'.pdf');
	        return $mensaje='exito';
	    }
    }

    function seguridadSocial($cedulaIngresada){


    	$this->db->select('nombreLaboral, cedulaLaboral, cargoLaboral, eps, afp, fechaRetiro');
    	$this->db->from('historial_laboral');
    	$this->db->where('cedulaLaboral', $cedulaIngresada);
    	$consul = $this->db->get();
    	$consulta = $consul->result();

    	if(!$consul->result_id->num_rows){
    		return $mensaje='noExiste';
    	}else{

    		$nombre = $consulta['0']->nombreLaboral;
	    	$cedula = $consulta['0']->cedulaLaboral;
	    	$cargo = $consulta['0']->cargoLaboral;
	    	$eps = $consulta['0']->eps;
	    	$afp = $consulta['0']->afp;
	    	$fechaRetiro = $consulta['0']->fechaRetiro;

	    	$fechaSeparada = explode("-", $fechaRetiro);
    		
    		$pdf = new seguridadSocialWord();

	        $diaRetiroLetras = $pdf->obtenerDia($fechaSeparada['2']);
	        $mesRetiroLetras = $pdf->obtenerMes($fechaSeparada['1']);
    		
	    	$pdf->AliasNbPages();
	        $pdf->SetTitle("Seguridad social");
	        $pdf->SetMargins(25,0);
	        $pdf->AddFont('Calibri','','Calibri.php');
	        $pdf->AddFont('Calibri Bold','','Calibri Bold.php');
	        $pdf->AddFont('Calibri Italic','','Calibri Italic.php');
	        $pdf->AddFont('Calibri Bold Italic','','Calibri Bold Italic.php');
	        $pdf->AddPage();
	        $pdf->Ln(60);
	        $pdf->SetFont('Calibri Italic','',14);
	        $pdf->Cell(160,5,utf8_decode('Armenia, '.$mesRetiroLetras.' '.$fechaSeparada['2'].' de '.$fechaSeparada['0']), 0);
	        $pdf->Ln(20);
	        $pdf->Cell(160,5,utf8_decode('Señor (a):'), 0);
	        $pdf->Ln();
	        $pdf->SetFont('Calibri Bold Italic','',14);
	        $pdf->Cell(160,5,utf8_decode($nombre), 0);
	        $pdf->Ln();
	        $pdf->Cell(160,5,utf8_decode('C.C. '.$cedula), 0);
	        $pdf->Ln();
	        $pdf->SetFont('Calibri Italic','',14);
	        $pdf->Cell(160,5,utf8_decode('Armenia'), 0);
	        $pdf->Ln(15);
	        $pdf->SetFont('Calibri','',14);
	        $pdf->Cell(160,5,utf8_decode('Asunto: Aceptación  Renuncia'), 0,0,'R');
	        $pdf->Ln(10);
	        $pdf->Cell(160,5,utf8_decode('Respetado señor (a):'), 0);
	        $pdf->Ln(10);
	        $pdf->MultiCell(160,5,utf8_decode('Nos permitimos comunicarle que la Empresa acepta la renuncia presentada por usted al cargo que venía desempeñando como: '.$cargo.' hasta el '.$diaRetiroLetras.' ('.$fechaSeparada['2'].') de '.$mesRetiroLetras.' de '.$fechaSeparada['0'].'.'), 0,'J');
	        $pdf->Ln();

	        if ($afp == "NO APLICA") {
	        	$pdf->MultiCell(160,5,utf8_decode('Así mismo, le estamos entregando una constancia laboral por el tiempo ejercido en la Empresa y copia del pago a la Seguridad Social efectuado en '.$eps.', Administradora de Riesgos Profesionales Sura y Parafiscales de los tres últimos periodos, de acuerdo a la disposición contenida en el artículo 29 de la Ley 789 de 2002.'), 0,'J');
	        }else{
	        	$pdf->MultiCell(160,5,utf8_decode('Así mismo, le estamos entregando una constancia laboral por el tiempo ejercido en la Empresa y copia del pago a la Seguridad Social efectuado en '.$eps.', fondo de pensiones '.$afp.', Administradora de Riesgos Profesionales Sura y Parafiscales de los tres últimos periodos, de acuerdo a la disposición contenida en el artículo 29 de la Ley 789 de 2002.'), 0,'J');
	        }

	        $pdf->Ln();
	        $pdf->Cell(160,5,utf8_decode('Agradecemos el tiempo servido.'), 0);
	        $pdf->Ln(10);
	        $pdf->Cell(160,5,utf8_decode('Atentamente,'), 0);
	        $pdf->Ln(25);
	        $pdf->SetFont('Calibri Bold Italic','',14);
	        $pdf->Cell(160,5,utf8_decode('______________________________'), 0);
	        $pdf->Ln();
	        $pdf->Cell(160,5,utf8_decode('Luis Alfonso Martínez Martínez'), 0);
	        $pdf->Ln();
	        $pdf->Cell(160,5,utf8_decode('Director de Gestión Humana'), 0);
	        $pdf->Output('i','Seguridad social C.C. '.$cedula.'.pdf');
	        return $mensaje='exito';
	    }
    }

    function certificadoLaboral($cedulaIngresada){

    	$pdf = new certificadoLaboralWord();

    	$this->db->select('nombreLaboral, cedulaLaboral, cargoLaboral, fechaIngreso, fechaRetiro, tipoContrato, salarioDevengado');
    	$this->db->from('historial_laboral');
    	$this->db->where('cedulaLaboral', $cedulaIngresada);
    	$consul = $this->db->get();
    	$consulta = $consul->result();

    	if(!$consul->result_id->num_rows){
    		return $mensaje='noExiste';
    	}else{

    		$nombre = $consulta['0']->nombreLaboral;
	    	$cedula = $consulta['0']->cedulaLaboral;
	    	$cargo = $consulta['0']->cargoLaboral;
	    	$fechaIngreso = $consulta['0']->fechaIngreso;
	    	$fechaRetiro = $consulta['0']->fechaRetiro;
	    	$tipoContrato = $consulta['0']->tipoContrato;
	    	$salarioDevengado = $consulta['0']->salarioDevengado;

	    	$fechaIngresoSeparada = explode("-", $fechaIngreso);
	    	$diaFechaIngreso = $pdf->obtenerDia($fechaIngresoSeparada['2']);
	    	$mesFechaIngreso = $pdf->obtenerMes($fechaIngresoSeparada['1']);
	    	$añoFechaIngreso = $fechaIngresoSeparada['0'];

	    	$fecharetiroSeparada = explode("-", $fechaRetiro);
	    	$diaFechaRetiro = $pdf->obtenerDia($fecharetiroSeparada['2']); 
	    	$mesFechaRetiro = $pdf->obtenerMes($fecharetiroSeparada['1']);
	    	$añoFechaRetiro = $fecharetiroSeparada['0'];

	    	$pdf->AliasNbPages();
	        $pdf->SetTitle("Certificado laboral");
	        $pdf->SetMargins(25,0);
	        $pdf->AddPage();
	        $pdf->Ln(70);
	        $pdf->SetFont('Arial','BI',24);
	        $pdf->Cell(160,5,utf8_decode('El Director de Gestión Humana  de'), 0,0,'C');
	        $pdf->Ln(10);
	        $pdf->Cell(160,5,utf8_decode('Apuestas Ochoa S.A. NIT 890.002.142-6'), 0,0,'C');
	        $pdf->Ln(30);
	        $pdf->SetFont('Arial','B',34);
	        $pdf->Cell(160,5,utf8_decode('CERTIFICA'), 0,0,'C');
	        $pdf->SetFont('Arial','',15);
	        $pdf->Ln(30);
	        $salarioLetras = $this->numtoletras($salarioDevengado);
	        $pdf->MultiCell(160,5,utf8_decode('Que  el(la) señor(a) '.$nombre.', identificado(a) con Cédula de Ciudadanía número '.$cedula.', laboró en nuestra empresa en el cargo de '.$cargo.', desde el '.$diaFechaIngreso.' de '.$mesFechaIngreso.' de '.$añoFechaIngreso.' hasta el '.$diaFechaRetiro.' de '.$mesFechaRetiro.' de '.$añoFechaRetiro.', con un contrato a término '.$tipoContrato.', y cuyo salario mensual era de '.$salarioLetras.' ($'.$salarioDevengado.').'), 0,'J');
	        $pdf->Ln();
	        $pdf->MultiCell(160,5,utf8_decode('Se expide a solicitud del interesado, y se firma en Armenia, Quindío, a los '.$fecharetiroSeparada['2'].' días del mes de '.$mesFechaRetiro.' '.$fecharetiroSeparada['0'].'.'), 0,'J');
	        $pdf->Ln(40);
	        $pdf->SetFont('Arial','BI',24);
	        $pdf->Cell(160,5,utf8_decode('Luis Alfonso Martínez Martínez'), 0,0,"C");
	        
	        $pdf->Output('i','Certificado laboral C.C. '.$cedula.'.pdf');
	        return $mensaje='exito';
	    }
    }



    function numtoletras($xcifra)
    {
    	// die('khkjhkjhkhkh '.$xcifra);
        $xarray = array(0 => "Cero",
            1 => "UN", "DOS", "TRES", "CUATRO", "CINCO", "SEIS", "SIETE", "OCHO", "NUEVE",
            "DIEZ", "ONCE", "DOCE", "TRECE", "CATORCE", "QUINCE", "DIECISEIS", "DIECISIETE", "DIECIOCHO", "DIECINUEVE",
            "VEINTI", 30 => "TREINTA", 40 => "CUARENTA", 50 => "CINCUENTA", 60 => "SESENTA", 70 => "SETENTA", 80 => "OCHENTA", 90 => "NOVENTA",
            100 => "CIENTO", 200 => "DOSCIENTOS", 300 => "TRESCIENTOS", 400 => "CUATROCIENTOS", 500 => "QUINIENTOS", 600 => "SEISCIENTOS", 700 => "SETECIENTOS", 800 => "OCHOCIENTOS", 900 => "NOVECIENTOS"
        );
    //
        $xcifra = trim($xcifra);
        $xlength = strlen($xcifra);
        $xpos_punto = strpos($xcifra, ".");
        $xaux_int = $xcifra;
        $xdecimales = "00";
        if (!($xpos_punto === false)) {
            if ($xpos_punto == 0) {
                $xcifra = "0" . $xcifra;
                $xpos_punto = strpos($xcifra, ".");
            }
            $xaux_int = substr($xcifra, 0, $xpos_punto); // obtengo el entero de la cifra a covertir
            $xdecimales = substr($xcifra . "00", $xpos_punto + 1, 2); // obtengo los valores decimales
        }

        $XAUX = str_pad($xaux_int, 18, " ", STR_PAD_LEFT); // ajusto la longitud de la cifra, para que sea divisible por centenas de miles (grupos de 6)
        $xcadena = "";
        for ($xz = 0; $xz < 3; $xz++) {
            $xaux = substr($XAUX, $xz * 6, 6);
            $xi = 0;
            $xlimite = 6; // inicializo el contador de centenas xi y establezco el límite a 6 dígitos en la parte entera
            $xexit = true; // bandera para controlar el ciclo del While
            while ($xexit) {
                if ($xi == $xlimite) { // si ya llegó al límite máximo de enteros
                    break; // termina el ciclo
                }

                $x3digitos = ($xlimite - $xi) * -1; // comienzo con los tres primeros digitos de la cifra, comenzando por la izquierda
                $xaux = substr($xaux, $x3digitos, abs($x3digitos)); // obtengo la centena (los tres dígitos)
                for ($xy = 1; $xy < 4; $xy++) { // ciclo para revisar centenas, decenas y unidades, en ese orden
                    switch ($xy) {
                        case 1: // checa las centenas
                            if (substr($xaux, 0, 3) < 100) { // si el grupo de tres dígitos es menor a una centena ( < 99) no hace nada y pasa a revisar las decenas
                                
                            } else {
                                $key = (int) substr($xaux, 0, 3);
                                if (TRUE === array_key_exists($key, $xarray)){  // busco si la centena es número redondo (100, 200, 300, 400, etc..)
                                    $xseek = $xarray[$key];
                                    // $xsub = subfijo($xaux); // devuelve el subfijo correspondiente (Millón, Millones, Mil o nada)

                                    $xaux = trim($xaux);
                                    $xstrlen = strlen($xaux);
                                    if ($xstrlen == 1 || $xstrlen == 2 || $xstrlen == 3)
                                        $xsub = "";
                                    //
                                    if ($xstrlen == 4 || $xstrlen == 5 || $xstrlen == 6)
                                        $xsub = "MIL";


                                    if (substr($xaux, 0, 3) == 100)
                                        $xcadena = " " . $xcadena . " CIEN " . $xsub;
                                    else
                                        $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
                                    $xy = 3; // la centena fue redonda, entonces termino el ciclo del for y ya no reviso decenas ni unidades
                                }
                                else { // entra aquí si la centena no fue numero redondo (101, 253, 120, 980, etc.)
                                    $key = (int) substr($xaux, 0, 1) * 100;
                                    $xseek = $xarray[$key]; // toma el primer caracter de la centena y lo multiplica por cien y lo busca en el arreglo (para que busque 100,200,300, etc)
                                    $xcadena = " " . $xcadena . " " . $xseek;
                                } // ENDIF ($xseek)
                            } // ENDIF (substr($xaux, 0, 3) < 100)
                            break;
                        case 2: // checa las decenas (con la misma lógica que las centenas)
                            if (substr($xaux, 1, 2) < 10) {
                                
                            } else {
                                $key = (int) substr($xaux, 1, 2);
                                if (TRUE === array_key_exists($key, $xarray)) {
                                    $xseek = $xarray[$key];
                                    // $xsub = subfijo($xaux);

                                    $xaux = trim($xaux);
                                    $xstrlen = strlen($xaux);
                                    if ($xstrlen == 1 || $xstrlen == 2 || $xstrlen == 3)
                                        $xsub = "";
                                    //
                                    if ($xstrlen == 4 || $xstrlen == 5 || $xstrlen == 6)
                                        $xsub = "MIL";
                                    //
                                    // return $xsub;


                                    if (substr($xaux, 1, 2) == 20)
                                        $xcadena = " " . $xcadena . " VEINTE " . $xsub;
                                    else
                                        $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
                                    $xy = 3;
                                }
                                else {
                                    $key = (int) substr($xaux, 1, 1) * 10;
                                    $xseek = $xarray[$key];
                                    if (20 == substr($xaux, 1, 1) * 10)
                                        $xcadena = " " . $xcadena . " " . $xseek;
                                    else
                                        $xcadena = " " . $xcadena . " " . $xseek . " Y ";
                                } // ENDIF ($xseek)
                            } // ENDIF (substr($xaux, 1, 2) < 10)
                            break;
                        case 3: // checa las unidades
                            if (substr($xaux, 2, 1) < 1) { // si la unidad es cero, ya no hace nada
                                
                            } else {
                                $key = (int) substr($xaux, 2, 1);
                                $xseek = $xarray[$key]; // obtengo directamente el valor de la unidad (del uno al nueve)
                                // $xsub = $this->subfijo($xaux);		


                                $xaux = trim($xaux);
                                $xstrlen = strlen($xaux);
                                if ($xstrlen == 1 || $xstrlen == 2 || $xstrlen == 3)
                                    $xsub = "";
                                //
                                if ($xstrlen == 4 || $xstrlen == 5 || $xstrlen == 6)
                                    $xsub = "MIL";
                                //
                                // return $xsub;


                                $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
                            } // ENDIF (substr($xaux, 2, 1) < 1)
                            break;
                    } // END SWITCH
                } // END FOR
                $xi = $xi + 3;
            } // ENDDO

            if (substr(trim($xcadena), -5, 5) == "ILLON") // si la cadena obtenida termina en MILLON o BILLON, entonces le agrega al final la conjuncion DE
                $xcadena.= " DE";

            if (substr(trim($xcadena), -7, 7) == "ILLONES") // si la cadena obtenida en MILLONES o BILLONES, entoncea le agrega al final la conjuncion DE
                $xcadena.= " DE";

            // ----------- esta línea la puedes cambiar de acuerdo a tus necesidades o a tu país -------
            if (trim($xaux) != "") {
                switch ($xz) {
                    case 0:
                        if (trim(substr($XAUX, $xz * 6, 6)) == "1")
                            $xcadena.= "UN BILLON ";
                        else
                            $xcadena.= " BILLONES ";
                        break;
                    case 1:
                        if (trim(substr($XAUX, $xz * 6, 6)) == "1")
                            $xcadena.= "UN MILLON ";
                        else
                            $xcadena.= " MILLONES ";
                        break;
                    case 2:
                        if ($xcifra < 1) {
                            $xcadena = "CERO PESOS";
                        }
                        if ($xcifra >= 1 && $xcifra < 2) {
                            $xcadena = "UN PESO ";
                        }
                        if ($xcifra >= 2) {
                            $xcadena.= " PESOS "; //
                        }
                        break;
                } // endswitch ($xz)
            } // ENDIF (trim($xaux) != "")
            // ------------------      en este caso, para México se usa esta leyenda     ----------------
            $xcadena = str_replace("VEINTI ", "VEINTI", $xcadena); // quito el espacio para el VEINTI, para que quede: VEINTICUATRO, VEINTIUN, VEINTIDOS, etc
            $xcadena = str_replace("  ", " ", $xcadena); // quito espacios dobles
            $xcadena = str_replace("UN UN", "UN", $xcadena); // quito la duplicidad
            $xcadena = str_replace("  ", " ", $xcadena); // quito espacios dobles
            $xcadena = str_replace("BILLON DE MILLONES", "BILLON DE", $xcadena); // corrigo la leyenda
            $xcadena = str_replace("BILLONES DE MILLONES", "BILLONES DE", $xcadena); // corrigo la leyenda
            $xcadena = str_replace("DE UN", "UN", $xcadena); // corrigo la leyenda
        } // ENDFOR ($xz)
        return trim($xcadena);
    }



    function subfijo($xx)
    { // esta función regresa un subfijo para la cifra
        $xx = trim($xx);
        $xstrlen = strlen($xx);
        if ($xstrlen == 1 || $xstrlen == 2 || $xstrlen == 3)
            $xsub = "";
        //
        if ($xstrlen == 4 || $xstrlen == 5 || $xstrlen == 6)
            $xsub = "MIL";
        //
        return $xsub;
    }





}

class retiroDeCesantiasWord extends FPDF
{
	// Cabecera de página
	function Header()
	{
	    // Logo210
	    $this->Image(APPPATH.'img\logoTransparente.png',25,25,160,25,'png');
	}

	function obtenerMes($numMes){
		
		$mes;
		if ($numMes == '01') {
			$mes = "enero";
		}elseif ($numMes == '02') {
			$mes = 'febrero';
		}elseif ($numMes == '03') {
			$mes = 'marzo';
		}elseif ($numMes == '04') {
			$mes = 'abril';
		}elseif ($numMes == '05') {
			$mes = 'mayo';
		}elseif ($numMes == '06') {
			$mes = 'junio';
		}elseif ($numMes == '07') {
			$mes = 'julio';
		}elseif ($numMes == '08') {
			$mes = 'agosto';
		}elseif ($numMes == '09') {
			$mes = 'septiembre';
		}elseif ($numMes == '10') {
			$mes = 'octubre';
		}elseif ($numMes == '11') {
			$mes = 'noviembre';
		}elseif ($numMes == '12') {
			$mes = 'diciembre';
		}
		return $mes;
	}

	function obtenerDia($numDia){
		$diaLetras;
		if ($numDia == '01') {
			$diaLetras = "primero";
		}elseif ($numDia == '02') {
			$diaLetras = 'dos';
		}elseif ($numDia == '03') {
			$diaLetras = 'tres';
		}elseif ($numDia == '04') {
			$diaLetras = 'cuatro';
		}elseif ($numDia == '05') {
			$diaLetras = 'cinco';
		}elseif ($numDia == '06') {
			$diaLetras = 'seis';
		}elseif ($numDia == '07') {
			$diaLetras = 'siete';
		}elseif ($numDia == '08') {
			$diaLetras = 'ocho';
		}elseif ($numDia == '09') {
			$diaLetras = 'nueve';
		}elseif ($numDia == '10') {
			$diaLetras = 'diez';
		}elseif ($numDia == '11') {
			$diaLetras = 'once';
		}elseif ($numDia == '12') {
			$diaLetras = 'doce';
		}elseif ($numDia == '13') {
			$diaLetras = 'trece';
		}elseif ($numDia == '14') {
			$diaLetras = 'catorce';
		}elseif ($numDia == '15') {
			$diaLetras = 'quince';
		}elseif ($numDia == '16') {
			$diaLetras = 'dieciséis';
		}elseif ($numDia == '17') {
			$diaLetras = 'diecisiete';
		}elseif ($numDia == '18') {
			$diaLetras = 'dieciocho';
		}elseif ($numDia == '19') {
			$diaLetras = 'diecinueve';
		}elseif ($numDia == '20') {
			$diaLetras = 'veinte';
		}elseif ($numDia == '21') {
			$diaLetras = 'veintiuno';
		}elseif ($numDia == '22') {
			$diaLetras = 'veintidós';
		}elseif ($numDia == '23') {
			$diaLetras = 'veintitrés';
		}elseif ($numDia == '24') {
			$diaLetras = 'veinticuatro';
		}elseif ($numDia == '25') {
			$diaLetras = 'veinticinco';
		}elseif ($numDia == '26') {
			$diaLetras = 'veintiséis';
		}elseif ($numDia == '27') {
			$diaLetras = 'veintisiete';
		}elseif ($numDia == '28') {
			$diaLetras = 'veintiocho';
		}elseif ($numDia == '29') {
			$diaLetras = 'veintinueve';
		}elseif ($numDia == '30') {
			$diaLetras = 'treinta';
		}elseif ($numDia == '31') {
			$diaLetras = 'treinta y uno';
		}
		return $diaLetras;
	}
}

class seguridadSocialWord extends retiroDeCesantiasWord
{

	// Pie de página
	function Footer()
	{
	    $this->SetY(-30);
	    $this->SetFont('Calibri','',12);
	    $this->setTextColor(130,130,130);
	    $this->Cell(160,5,utf8_decode('Proyectado por: Margarita Arias Diaz'), 0);
        $this->Ln();
        $this->Cell(160,5,utf8_decode('Revisado por: MAD'), 0);
	}
}

class certificadoLaboralWord extends retiroDeCesantiasWord
{

	function Footer(){
		$this->SetY(-30);
	    $this->SetFont('Arial','',11);
	    $this->setTextColor(130,130,130);
	    $this->Cell(160,5,utf8_decode('Proyectado por: Margarita Arias Diaz'), 0);
        $this->Ln();
        $this->Cell(160,5,utf8_decode('Revisado por: MAD'), 0);
        $this->Ln(10);
        $this->SetFont('Arial','B',11);
        $this->Cell(160,5,utf8_decode('Carrera 14 # 23 - 07 Carrera 14 # 21 - 26   PBX: 7412266   Armenia - Quindío'), 0,0,"C");
	}
}





