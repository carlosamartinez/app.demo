<?php
class Welcome_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function get_exist_login_user($correo, $password)
    {

        $this->db->select('correo, password, nombres, cedula, dependencia, rol');
        $this->db->from('usuarios');
        $this->db->where('correo', $correo);
        $this->db->where('password', $password);

        $query = $this->db->get();
        $result_consul = array();
        $result_consul = $query->result();
        $corre_retur = $result_consul[0]->correo;
        $passw_retur = $result_consul[0]->password;

        $ban_user = array();
        
        if(empty($corre_retur) || empty($passw_retur))
        {
            $ban_user['1'] = FALSE;
        }else{
            $ban_user['1'] = TRUE; 
            $ban_user['2'] = $result_consul[0]->nombres;  
            $ban_user['3'] = $result_consul[0]->cedula;
            $ban_user['4'] = $result_consul[0]->dependencia;
            $ban_user['5'] = $result_consul[0]->rol;
        }    
        return $ban_user;
    }
    
    function get_info_users($cedula){

        $this->db->select('nombres, cedula, dependencia, rol');
        $this->db->from('usuarios');
        $this->db->where('cedula', $cedula);

        $query = $this->db->get();
        $info_user = array();
        $info_user = $query->result();

        return $info_user;
    }


    function get_cantidad_estados()
    { 
        $this->db->select('count(estado)');
        $this->db->from('documentos_digitalizados');
        $this->db->where('estado', 'Pagado');
        $query_estado_pagado = $this->db->get();
        $count_pagado = array();
        $count_pagado = $query_estado_pagado->result();
        foreach ($count_pagado as $key) {
           foreach ($key as $k => $value) {
                $cant_pagado = $value;
           }
        }

        $this->db->select('count(estado)');
        $this->db->from('documentos_digitalizados');
        $this->db->where('estado', 'Aprobado');
        $query_estado_aprobado = $this->db->get();
        $count_aprobado = array();
        $count_aprobado = $query_estado_aprobado->result();
        foreach ($count_aprobado as $key_y) {
           foreach ($key_y as $k_y => $valu) {
                $cant_aprobado = $valu;
           }
        }


        $this->db->select('count(estado)');
        $this->db->from('documentos_digitalizados');
        $this->db->where('estado', 'Pendiente');
        $query_estado_pendiente = $this->db->get();
        $count_pendiente = array();
        $count_pendiente = $query_estado_pendiente->result();
        foreach ($count_pendiente as $k_e_y) {
           foreach ($k_e_y as $k_y_y => $val) {
                $cant_pendiente = $val;
           }
        }


        $this->db->select('count(estado)');
        $this->db->from('documentos_digitalizados');
        $this->db->where('estado', 'listo para pago');
        $query_estado_listo = $this->db->get();
        $count_listo = array();
        $count_listo = $query_estado_listo->result();
        foreach ($count_listo as $k_e_y_o) {
           foreach ($k_e_y_o as $k_y_y_y => $va) {
                $cant_listo_para_pago = $va;
           }
        }

        $cantidades_estados = array(
                                     'cant_pagado' => $cant_pagado,
                                     'cant_aprobado' => $cant_aprobado,
                                     'cant_pendiente' => $cant_pendiente,
                                     'cant_listo_para_pago' => $cant_listo_para_pago
                                    );

        // truco para determinar si estamos haciendo la peticion
         // por medio de ajax ya que por medio de este evento generamos los valores de los graficos
        if(!empty($_POST['past_charts'])){
            $cant_est = json_encode($cantidades_estados);
            echo $cant_est;
            die();
        }

        return $cantidades_estados;

    }


    function update_estados_empleados_in_vacaciones()
    {
        $hoy = getdate();
        $fecha = $hoy['year']."-".$hoy['mon']."-".$hoy['mday'];

        $this->db->select('fecha_establece_bandera');
        $this->db->from('empleados');
        $this->db->where('fecha_establece_bandera', $fecha);
        $query = $this->db->get();

        if($query->result_id->num_rows >= 1){

            $data = array('bandera_view_vacaciones' => 0, 'fecha_establece_bandera' => '0000-00-00');
            $this->db->where('fecha_establece_bandera', $fecha);
            $this->db->update('empleados', $data);

        }

    }

    function agregar_registro($datos){
        $this->db->insert('datos', $datos);
    }

}