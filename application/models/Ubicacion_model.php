<?php
class Ubicacion_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function get_ubicaciones()
    {

        $this->db->select('nombre_ubicacion');
        $this->db->from('ubicaciones');
        
        $query = $this->db->get();
        $result_consul = array();
        $result_consul = $query->result();
         
        return $result_consul;
    }

    function get_ubicaciones_all()
    {

        $this->db->select('id_ubicacion, nombre_ubicacion, observaciones');
        $this->db->from('ubicaciones');
        
        $query = $this->db->get();
        $result_consul = array();
        $result_consul = $query->result();
         
        return $result_consul;
    }

    function save_info_ubicaciones($info_insert_ubicaciones)
    {  
        $this->db->select('nombre_ubicacion');
        $this->db->from('ubicaciones');
        $this->db->where('nombre_ubicacion', $info_insert_ubicaciones['nombre_ubicacion']);
        $query = $this->db->get();
        if($query->result_id->num_rows)
        {
            return 0;
        }else{
            $this->db->insert('ubicaciones', $info_insert_ubicaciones);
            return 1;
        }
    }

    function delete_registros($id)
    {
        $this->db->delete('ubicaciones', array('id_ubicacion' => $id)); 
    }

    function loading_unico_ubicacion($id)
    {
        $this->db->select('nombre_ubicacion, observaciones');
        $this->db->from('ubicaciones');
        $this->db->where('id_ubicacion', $id);
        $query = $this->db->get();
        $result_unico_concept = array();
        $result_unico_concept = $query->result();

        return $result_unico_concept;        

    }

    function update_ubicacions($info_update)
    {
        $datos_update = array(
                              'nombre_ubicacion'      => $info_update['nombre_ubicacion'], 
                              'observaciones' => $info_update['observaciones']
                             );
        $this->db->where('id_ubicacion', $info_update['id_ubicacion']);
        $this->db->update('ubicaciones', $datos_update); 
    }

}