<?php
class Inicio_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function get_cumplimiento()
    {

       $query = $this->db_b->query("SELECT cumplimiento.Porcentaje_chance, cumplimiento.Porcentaje_chance_diario, cumplimiento.Porcentaje_astro, cumplimiento.Porcentaje_recargas, cumplimiento.Porcentaje_giros, cumplimiento.Porcentaje_loteria, cumplimiento.Porcentaje_recaudos, cumplimiento.Lider, cumplimiento.Cedula, asesores.Nombre FROM cumplimiento INNER JOIN asesores ON cumplimiento.Cedula = asesores.Cedula ORDER BY RAND() LIMIT 1");

        $result_consul = array();
        $result_consul = $query->result();

        $porcentajes = array();
        $porcentajes = $result_consul[0]; 

        $porcent_json = json_encode($porcentajes);
        echo $porcent_json;
        die();
    }

}