<?php
require_once(APPPATH.'libraries/jpgraph/src/jpgraph.php');
require_once(APPPATH.'libraries/jpgraph/src/jpgraph_bar.php');
require_once(APPPATH.'libraries/jpgraph/src/themes/OceanTheme.class.php');
require_once(APPPATH.'libraries/fpdf/fpdf.php');
class Evaluacion_model extends CI_Model {

    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }

    function insert_evaluacion(){
        unset($_POST['array'][0]);
        
        $this->input->post('nombre_evaluacion');
        $this->input->post('descripcion');
        $this->db->select('id_evaluacion, descripcion');
        $this->db->from('evaluaciones_cuestionario');
        $this->db->where('nombre_evaluacion', $this->input->post('nombre_evaluacion'));
        $respuest = $this->db->get();
        
        if(!$respuest->result_id->num_rows){  

              $json_cue = json_encode($_POST['array']);
              $data = array();

              $data['nombre_evaluacion'] = $this->input->post('nombre_evaluacion');
              $data['json_cuestionario'] = $json_cue;
              $data['descripcion']       = $this->input->post('descripcion');
              $data['calificaciones']    = json_encode($this->input->post('calificaciones'));

              $retorna = $this->db->insert('evaluaciones_cuestionario', $data);
              echo $retorna;

        }else{
             $retorna = 0;
             echo $retorna;
        }
   
    }

    public function get_info_seleccionada_act($id){
        $this->db->select('id_evaluacion, nombre_evaluacion, json_cuestionario, descripcion,calificaciones ');
        $this->db->from('evaluaciones_cuestionario'); 
        $this->db->where('id_evaluacion',$id);

        $query = $this->db->get();
        $result_consul_info = $query->result();
        return $result_consul_info;
    }

    public function get_info_seleccionada_realizacion_evaluacion($nombre){
        $this->db->select('id_evaluacion, nombre_evaluacion, json_cuestionario, descripcion,calificaciones ');
        $this->db->from('evaluaciones_cuestionario'); 
        $this->db->where('nombre_evaluacion',$nombre);

        $query = $this->db->get();
        $result_consul_info = $query->result();

        return $result_consul_info;
    }

    function validarDatosEvaluacion($datos){

        $mensaje;
        $this->db->select('cedulaEvaluado, nombreEvaluacion');
        $this->db->from('calificar_evaluaciones');
        $this->db->where('cedulaEvaluado', $datos['cedula_evaluado']);
        $this->db->where('nombreEvaluacion', $datos['nombre_evaluacion']);
        $consulta = $this->db->get();
        
        if($consulta->result_id->num_rows){
            $mensaje='existente';
            return $mensaje;
        }
    }

    function get_evaluaciones(){
        $this->db->select('nombre_evaluacion');
        $this->db->from('evaluaciones_cuestionario');      

        $return_nombres_evaluaciones = $this->db->get();
        $nombres_evaluaciones = array();
        $nombres_evaluaciones = $return_nombres_evaluaciones->result();

        return $nombres_evaluaciones;
    }

    function get_user_presentar_evaluacion(){
       $cedula = $_POST['cedula'];

       $this->db->select('empleados.id_empleado, empleados.nombres, empleados.apellidos, perfiles.cargo_laboral, perfiles.departamento');
       $this->db->from('empleados');
       $this->db->join('perfiles', 'perfiles.id_empleado = empleados.id_empleado', 'left');
       $this->db->where('empleados.cedula', $cedula);

       $query = $this->db->get();
       $empleados_area_presenta_eval = array();
       $empleados_area_presenta_eval = $query->result();

       $employe_dates = array();
       $employe_dates = $empleados_area_presenta_eval[0];
       echo json_encode($employe_dates);  
       die();

    }

    function print_evaluacion(){        
        $this->db->select('json_cuestionario');
        $this->db->from('evaluaciones_cuestionario');
        $this->db->where('nombre_evaluacion', $_POST['evaluacion']);

        $query = $this->db->get();
        $cuestionario = array();
        $cuestionario = $query->result();

        $obj = json_decode($cuestionario[0]->json_cuestionario);

        $cuestions =  (array) $obj;

        echo $cuestions;

        $html = '';
        for($i=1; $i <= count($cuestions); $i++){ 

            $pregunta = $obj->{$i}->{'pregunta'};             
            $options_respuestas = explode("_", $obj->{$i}->{'opciones_respuestas'});
            unset($options_respuestas[0]);
            $html .= '<div class="card pregunta'.$i.'" style="margin-bottom: 2.75rem !important; background-color: #91BFDA !important; border-radius: 1.25rem !important; padding: 0px 8px 8px 8px; ">';
            $html .= '<div class="row">';
            $html .=   '<div class="form-group">';
            $html .=     '<div class="col-xs-12">';
            $html .=       '<label class="form-control-label" >Pregunta #'.$i.'</label>';
            $html .=       '<input type="text" class="form-control" value="'.$pregunta.'" disabled="disabled">';
            $html .=     '</div>';
            $html .=   '</div>';
            $html .= '</div>';
            $html .= '<div class="row">';
            $html .=   '<div class="form-group">';
              $html  .= $this->evaluacion_model->retri($options_respuestas, $abecedario);
            $html .=   '</div>';
            $html .= '</div>';
            $html .= '</div>';

        }
          echo $html;
          die();     
    }


    public function retri($options_respuestas, $abecedario)
    {
        $htm = '';
        for($i=1; $i <= count($options_respuestas); $i++){
          $htm .=     '<div class="col-xs-12">';
          $htm .=       '<label class="form-control-label" >Opcion '.$abecedario[$i].'</label>';
          $htm .=       '<input type="text" class="form-control" value="'.$options_respuestas[$i].'" name="">';
          $htm .=     '</div>';
        }
        return $htm;
    }

    public function consultar_evaluaciones(){

        $this->db->select('nombre_evaluacion');
        $this->db->from('evaluaciones_cuestionario');      

        $return_nombres_evaluaciones = $this->db->get();
        $nombres_evaluaciones = array();
        $nombres_evaluaciones = $return_nombres_evaluaciones->result();

        return $nombres_evaluaciones;

    }

        public function consultar_evaluaciones_a_realizar(){

        $this->db->select('id_evaluacion, nombre_evaluacion, descripcion');
        $this->db->from('evaluaciones_cuestionario');      

        $return_nombres_evaluaciones = $this->db->get();
        $nombres_evaluaciones = array();
        $nombres_evaluaciones = $return_nombres_evaluaciones->result();

        return $nombres_evaluaciones;

    }

   public function consultar_tabla(){

      $this->db->select('id_evaluacion, nombre_evaluacion, descripcion');
      $this->db->from('evaluaciones_cuestionario');
      $query = $this->db->get();

      $info_table_cons = array();
      $info_table_cons = $query->result();

      return $info_table_cons;
    }

    public function delete_evaluacion($id){

        $this->db->select('nombre_evaluacion');
        $this->db->from('evaluaciones_cuestionario');
        $this->db->where('id_evaluacion', $id);
        $query = $this->db->get();

        $retur_respues = $this->db->where('id_evaluacion', $id);
        $retur_respues = $this->db->delete('evaluaciones_cuestionario');
        
        return $retur_respues;
    }

    function borrarEvaluado($id){
        $this->db->select('nombreEvaluado');
        $this->db->from('calificar_evaluaciones');
        $this->db->where('idCalificacion', $id);
        $query = $this->db->get();

        $retur_respues = $this->db->where('idCalificacion', $id);
        $retur_respues = $this->db->delete('calificar_evaluaciones');
        
        return $retur_respues;
    }

    function visualizarBoton($dato){

        $this->db->select('visualizarBoton');
        $this->db->from('boton_evaluacion');
        $consulta = $this->db->get();
        $resultadoConsulta = $consulta->result();

        $this->db->update('boton_evaluacion', $dato);
    }

    function get_table_evaluacion(){
        $this->db->select('id_evaluacion, nombre_evaluacion, json_cuestionario, calificaciones, descripcion,');
        $this->db->from('evaluaciones_cuestionario'); 
        $query = $this->db->get();
        $result_consul_table_eval = array();
        $result_consul_table_eval = $query->result();
        
        return $result_consul_table_eval;
    }

    function get_calificaciones(){
        $this->db->select('idCalificacion, nombreEvaluado, cedulaEvaluado, nombreEvaluacion, descripcion, puntaje, calificacion');
        $this->db->from('calificar_evaluaciones');
        /*$this->db->order_by("puntaje", "desc");*/
        $consultaCalifica = $this->db->get();
        $calificaciones = array();
        $calificaciones = $consultaCalifica->result();
        return $calificaciones;
    }

    function ver_calificaciones($id){
        $this->db->select('idCalificacion, nombreEvaluado, cedulaEvaluado, nombreEvaluacion, jsonCuestionario, descripcion, puntaje, calificacion');
        $this->db->from('calificar_evaluaciones');
        $this->db->where('idCalificacion', $id);
        /*$this->db->order_by("puntaje", "desc");*/
        $consultaCalifica = $this->db->get();
        $calificaciones = array();
        $calificaciones = $consultaCalifica->result();

        return $calificaciones;
    }

    function editar_evaluacion($id){


        $this->db->select('id_evaluacion, descripcion');
        $this->db->from('evaluaciones_cuestionario');
        $this->db->where('id_evaluacion', $id);
        $consulta = $this->db->get();


          function __construct($nombre,$url){
              $this->json= $nombre;
              $this->url = $url;
              }


        if($consulta->result_id->num_rows){  
            
              $data = array();

              /*$data['nombre_evaluacion'] = $this->input->post('nombre_evaluacion');
              $data['descripcion']       = $this->input->post('descripcion');

              foreach ($this->input->post('json_cuestionario') as $dataCuest ) {
                  $data['json_cuestionario'] = $data['json_cuestionario'] . $dataCuest.", ";
              }

              foreach ($this->input->post('calificaciones') as $dataCalificaciones ) {
                  $data['calificaciones'] = $data['calificaciones'] . $dataCalificaciones.", ";
              }*/

            $data['json_cuestionario'] = json_encode($this->input->post('json_cuestionario'));
            $data['calificaciones']       = json_encode($this->input->post('calificaciones'));

            echo "<pre>";
                print_r($data);
            echo "</pre>";

            die();

              /*$this->db->where('id_evaluacion', $id);
              $retorna = $this->db->update('evaluaciones_cuestionario', $data);*/

        }

        else {

            $retorna = 0;
            echo $retorna;
        }
    }

    function calificarEvaluacion($info){

        $mensaje='exito';

        $this->db->insert('calificar_evaluaciones', $info);

        return $mensaje;
    }

    public function validarBoton(){
        $this->db->select('visualizarBoton');
        $this->db->from('boton_evaluacion');
        $consulta = $this->db->get();
        $result = $consulta->result();

        return $result;
    }

    public function get_evaluacion_por_empleado(){

      $this->db->select('idCalificacion, nombreEvaluado, cedulaEvaluado, nombreEvaluacion, jsonCuestionario, descripcion, puntaje, calificacion');
      $this->db->from('calificar_evaluaciones');
      $this->db->where('idCalificacion', $_POST['id']);      
      $query = $this->db->get();
      $info = json_decode(json_encode($query->result()), True);

      $this->db->select('json_cuestionario,calificaciones');
      $this->db->from('evaluaciones_cuestionario');
      $this->db->where('nombre_evaluacion', $info[0]['nombreEvaluacion']); 
      $query = $this->db->get();
      $evaluacion = json_decode(json_encode($query->result()), True);       

      $array_preguntas    = json_decode($evaluacion[0]['json_cuestionario']);
      $array_porcentaje = json_decode($evaluacion[0]['calificaciones']);
      $array_calificaciones   = json_decode($info[0]['jsonCuestionario']);
      
      

      $preguntas = json_decode(json_encode($array_preguntas), True);
      $resultados = json_decode(json_encode($array_calificaciones), True);
      $valor = json_decode(json_encode($array_porcentaje), True);
      $abecedario = ["A: ", "B: ", "C: ", "D: ", "E: "];
      $abecedarioRespuestas = ["A", "B", "C", "D", "E"];
      

      // echo "<pre>";
      //   print_r($preguntas);
      // echo "</pre><br><br>";
      // die();
      // echo "<pre>";
      //   print_r($resultados);
      // echo "</pre><br><br>";
      // die();
      // echo "<pre>";
      //   print_r($valor);
      // echo "</pre><br><br>";
      // die();
      



      $pdf = new PDF();
      $pdf->AliasNbPages();
      $pdf->AddPage();
      $pdf->SetFont('Arial','B',15);
      $pdf->SetFont('Arial','B',14);
      $pdf->setTextColor(255,255,255);
      $pdf->SetFillColor(37, 67, 125);
      $pdf->Cell(190,10,utf8_decode($info[0]['nombreEvaluacion']),1,0,'C',1);
      $pdf->Ln();
      $pdf->SetFillColor(2, 141, 210);
      $pdf->setTextColor(0,0,0);
      $pdf->SetFont('Arial','',10);
      $pdf->MultiCell(190,8,utf8_decode($info[0]['descripcion']),1,'C');
      $pdf->setTextColor(255,255,255);
      $pdf->Cell(50,8,utf8_decode('Nombre empleado'),1,0,'C',1);
      $pdf->setTextColor(0,0,0);
      $pdf->Cell(140,8,utf8_decode($info[0]['nombreEvaluado']),1,0,'C');
      $pdf->Ln();       
      $pdf->setTextColor(255,255,255);
      $pdf->Cell(31,8,utf8_decode('Cedula empleado'),1,0,'C',1);
      $pdf->setTextColor(0,0,0);
      $pdf->Cell(35,8,utf8_decode($info[0]['cedulaEvaluado']),1,0,'C');      
      $pdf->setTextColor(255,255,255);
      $pdf->Cell(31,8,utf8_decode('Puntaje'),1,0,'C',1);
      $pdf->setTextColor(0,0,0);
      $pdf->Cell(21,8,utf8_decode($info[0]['puntaje']),1,0,'C');
      $pdf->setTextColor(255,255,255);
      $pdf->Cell(31,8,utf8_decode('Calificación'),1,0,'C',1);
      $pdf->setTextColor(0,0,0);
      $pdf->Cell(41,8,utf8_decode($info[0]['calificacion']),1,0,'C');
      $pdf->Ln();
      $pdf->Ln();
      $pdf->Ln();
      $pdf->Ln();
      $pdf->SetFont('Arial','B',14);
      $pdf->setTextColor(255,255,255);
      $pdf->SetFillColor(37, 67, 125);      
      $pdf->Cell(190,10,utf8_decode('RESULTADOS'),1,0,'C',1);      
      $pdf->Ln();

      for ($i=1; $i <= count($preguntas) ; $i++) {     
        
        $pdf->setTextColor(255,255,255);
        $pdf->SetFillColor(2, 141, 210);
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(30,10,utf8_decode('Pregunta N° '.$i),1,0,'C',1);
        $pdf->setTextColor(0,0,0);
        $pdf->SetFont('Arial','',10);
        $pdf->MultiCell(160,10,utf8_decode($preguntas[$i]['pregunta']),1,'C',0);
        $pdf->setTextColor(255,255,255);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(190,10,utf8_decode('Posibles respuestas'),1,0,'C',1); 
        $pdf->Ln();      
        $options_posibles = substr($preguntas[$i]['opciones_respuestas'],10);
        $array_posibles_preguntas    = explode("_", $options_posibles);
        $pos = array();
        
        for ($j=0; $j < count($array_posibles_preguntas); $j++) {  

          $pdf->SetFont('Arial','',10);
          $pdf->setTextColor(0,0,0);
          $pdf->MultiCell(190,10,utf8_decode($abecedarioRespuestas[$j].'.   '.$array_posibles_preguntas[$j]),1,'L',0);
          $pos[$abecedarioRespuestas[$j]] =$abecedarioRespuestas[$j].'.   '.$array_posibles_preguntas[$j]; 
        }
        $cor = substr($preguntas[$i]['respuestas_verdaderas'],10);
        $pdf->setTextColor(255,255,255);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(190,10,utf8_decode('Respuesta correcta'),1,0,'C',1); 
        $pdf->Ln();
        $pdf->SetFont('Arial','',10);
        $pdf->setTextColor(0,0,0);        
        $pdf->MultiCell(190,10,utf8_decode($pos[$cor]),1,'L',0);
        $pdf->setTextColor(255,255,255);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(190,10,utf8_decode('Respuesta seleccionada'),1,0,'C',1); 
        $pdf->Ln();
        $pdf->SetFont('Arial','',10);
        $pdf->setTextColor(0,0,0);        
        $pdf->MultiCell(190,10,utf8_decode($pos[$resultados[$i]['respuestas_seleccionadas']]),1,'L',0);
        $pdf->setTextColor(255,255,255);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(100,10,utf8_decode('Porcentaje ganado'),1,0,'C',1);
        $pdf->setTextColor(0,0,0);
        $letra = substr($pos[$cor],0,1);
        if($i<51){
          if($letra == $resultados[$i]['respuestas_seleccionadas']){
            $pdf->Cell(90,10,utf8_decode('1.6'),1,0,'C',0);
          }else{
            $pdf->Cell(90,10,utf8_decode('0'),1,0,'C',0);
          } 
        }else{
          if($letra == $resultados[$i]['respuestas_seleccionadas']){
            $pdf->Cell(90,10,utf8_decode('2'),1,0,'C',0);
          }else{
            $pdf->Cell(90,10,utf8_decode('0'),1,0,'C',0);
          } 
        }                
        $pdf->Ln();
        $pdf->Ln();
        $pdf->Ln();
        $pdf->Ln();
        if($i == 60){

        }else{
        $pdf->AddPage();
      }
        $pdf->Ln();
      $pdf->Ln();
      $pdf->Ln();
      }
      $pdf->Output();
      
    }

    public function get_reporte_evaluacion(){

      // echo "<pre>";
      //   print_r($_POST);
      // echo "</pre>";
      // die();

      if($_POST['tipo']==1){
        $inicio = 1;
        $fin = 31;
      }else{
        $inicio = 31;
        $fin = 61;
      }

      // set_time_limit(300);

      $this->db->select('idCalificacion, nombreEvaluado, cedulaEvaluado, nombreEvaluacion, jsonCuestionario, descripcion, puntaje, calificacion');
      $this->db->from('calificar_evaluaciones');
      $query = $this->db->get();
      $info = json_decode(json_encode($query->result()), True);

      $this->db->select('json_cuestionario,calificaciones');
      $this->db->from('evaluaciones_cuestionario');
      $query = $this->db->get();
      $evaluacion = json_decode(json_encode($query->result()), True);
      $array_preguntas    = json_decode($evaluacion[0]['json_cuestionario']);
      $preguntas = json_decode(json_encode($array_preguntas), True);
      $abecedario = ["A: ", "B: ", "C: ", "D: ", "E: "];
      $abecedarioRespuestas = ["A", "B", "C", "D", "E"];

       $pdf = new PDF();
      $pdf->AliasNbPages();
      $pdf->AddPage();
      $pdf->SetFont('Arial','B',15);
      $pdf->SetFont('Arial','B',14);
      $pdf->setTextColor(255,255,255);
      $pdf->SetFillColor(37, 67, 125);
      $pdf->Cell(190,10,utf8_decode('REPORTE '.$info[0]['nombreEvaluacion']),1,0,'C',1);
      $pdf->Ln(); 
      $pdf->Ln();          
   
        // $array_calificaciones   = json_decode($info[0]['jsonCuestionario']);
        //   $resultados = json_decode(json_encode($array_calificaciones), True);
        //   echo "<pre>";
        //     print_r($resultados);
        //   echo "</pre>";
        //   die();
        $a = array();
        $b = array();
        $c = array();
        $d = array();
        $e = array();
        $f = array();
        $cant = array();
        for ($i=0; $i < count($info); $i++) {

          $array_calificaciones   = json_decode($info[$i]['jsonCuestionario']);
          $resultados = json_decode(json_encode($array_calificaciones), True);

          for ($j=1; $j <= count($resultados); $j++) {
            

            $options_posibles = substr($preguntas[$j]['opciones_respuestas'],10);
            $array_posibles_preguntas    = explode("_", $options_posibles);             

              switch ($resultados[$j]['respuestas_seleccionadas']) {
                case 'A':
                  $a[$j]['A'] = $a[$j]['A']+1;
                  break;
                case 'B':
                  $b[$j]['B'] = $b[$j]['B']+1;
                  break;
                case 'C':
                  $c[$j]['C'] = $c[$j]['C']+1;
                  break;
                case 'D':
                  $d[$j]['D'] = $d[$j]['D']+1;
                  break;
                case 'E':
                  $e[$j]['E'] = $e[$j]['E']+1;
                  break;
                case '':
                  $f[$j]['F'] = $f[$j]['F']+1;
                  break;              
              }
          }

        }


      for ($i=$inicio; $i < $fin; $i++) {
          $options_posibles = substr($preguntas[$i]['opciones_respuestas'],10);
          $array_posibles_preguntas    = explode("_", $options_posibles);

          switch (count($array_posibles_preguntas)) {
            case '3':
              $cant[$i][1]=$a[$i]['A'];
              $cant[$i][2]=$b[$i]['B'];
              $cant[$i][3]=$c[$i]['C'];
              $cant[$i][4]=$f[$i]['F'];
              break;
            case '4':
              $cant[$i][1]=$a[$i]['A'];
              $cant[$i][2]=$b[$i]['B'];
              $cant[$i][3]=$c[$i]['C'];
              $cant[$i][4]=$d[$i]['D'];
              $cant[$i][5]=$f[$i]['F'];
              break;
            case '5':
              $cant[$i][1]=$a[$i]['A'];
              $cant[$i][2]=$b[$i]['B'];
              $cant[$i][3]=$c[$i]['C'];
              $cant[$i][4]=$d[$i]['D'];
              $cant[$i][5]=$e[$i]['E'];
              $cant[$i][6]=$f[$i]['F'];
              break;
          }
                 
             

        $data = array();
        
        
          for ($j=1; $j <= count($cant[$i]); $j++) { 
            if(empty($cant[$i][$j])){
              $cant[$i][$j]=0;
            }

            $data[$j-1] = $cant[$i][$j];
          }
      
      // echo "<script> console.log('va en la vuelta numero ".$i."');</script>";
        // echo "<pre>";
        //     print_r($data);
        //   echo "</pre>";
        //   die(); 

      $pdf->setTextColor(255,255,255);
      $pdf->SetFillColor(2, 141, 210);
      $pdf->SetFont('Arial','',12);
      $pdf->Cell(30,10,utf8_decode('Pregunta N° '.$i),1,0,'C',1);
      $pdf->setTextColor(0,0,0);
      $pdf->SetFont('Arial','',10);
      $pdf->MultiCell(160,10,utf8_decode($preguntas[$i]['pregunta']),1,'C',0);
      $pdf->setTextColor(255,255,255);
      $pdf->SetFont('Arial','B',12);
      $pdf->Cell(190,10,utf8_decode('Posibles respuestas'),1,0,'C',1); 
      $pdf->Ln();      
      $options_posibles = substr($preguntas[$i]['opciones_respuestas'],10);
      $array_posibles_preguntas    = explode("_", $options_posibles);
      $pos = array();


      
      for ($j=0; $j < count($array_posibles_preguntas); $j++) {  

        $pdf->SetFont('Arial','',10);
        $pdf->setTextColor(0,0,0);
        $pdf->MultiCell(190,10,utf8_decode($abecedarioRespuestas[$j].'.   '.$array_posibles_preguntas[$j]),1,'L',0);
        $pos[$abecedarioRespuestas[$j]] =$abecedarioRespuestas[$j].'.   '.$array_posibles_preguntas[$j];

      }
      $cor = substr($preguntas[$i]['respuestas_verdaderas'],10);
      $pdf->setTextColor(255,255,255);
      $pdf->SetFont('Arial','B',12);
      $pdf->Cell(190,10,utf8_decode('Respuesta correcta'),1,0,'C',1); 
      $pdf->Ln();
      $pdf->SetFont('Arial','',10);
      $pdf->setTextColor(0,0,0);        
      $pdf->MultiCell(190,10,utf8_decode($pos[$cor]),1,'L',0);
      $pdf->setTextColor(255,255,255);
      $pdf->SetFont('Arial','B',12);
      $pdf->Ln(); 
      $pdf->Ln(); 
      $pdf->Ln();
      
      // Create the graph. These two calls are always required
      $graph = new Graph(800,500,'auto');
      
      $graph->SetScale("textlin");
      $theme_class = new OceanTheme();
      $graph->SetTheme($theme_class);
      //$theme_class="DefaultTheme";
      //$graph->SetTheme(new $theme_class());

      // set major and minor tick positions manually
      $graph->yaxis->SetTickPositions(array(100,200,300,400,500), array(50,150,250,350,450,550));
      $graph->SetBox(false);

      //$graph->ygrid->SetColor('gray');
      $graph->ygrid->SetFill(false);
      if(count($data) == 4){
        $label = ["A - ".$data[0],"B - ".$data[1],"C - ".$data[2],"NO - ".$data[3]];
      }elseif (count($data) == 5) {
        $label = ["A - ".$data[0],"B - ".$data[1],"C - ".$data[2],"D - ".$data[3],"NO - ".$data[4]];
      }elseif (count($data) == 6){
        $label = ["A - ".$data[0],"B - ".$data[1],"C - ".$data[2],"D - ".$data[3],"E - ".$data[4],"NO - ".$data[5]];
      }
      $graph->xaxis->SetTickLabels($label);
      $graph->xaxis->SetFont(FF_ARIAL,FS_NORMAL,15);
      $graph->yaxis->HideLine(false);
      $graph->yaxis->HideTicks(false,false);

      // Create the bar plots
      $b1plot = new BarPlot($data);

      // ...and add it to the graPH
      $graph->Add($b1plot);


      $b1plot->SetColor("white");
      $b1plot->SetFillGradient("#188EAF","white",GRAD_LEFT_REFLECTION);
      $b1plot->SetWidth(45);
      // $graph->title->Set("Reporte(estado empleados)");

      // Display the graph
      $graph->Stroke(_IMG_HANDLER);

      $fileName = APPPATH.'img\grafica'.$i.'.png';      
      $graph->img->Stream($fileName);      
      $pdf->Image(APPPATH.'img\grafica'.$i.'.png', 10,175,190,'png');
      unlink(APPPATH.'img\grafica'.$i.'.png');
      if($i == 30 || $i == 60){
         
      }else{
        $pdf->AddPage();
      }
           
      
     }
      $pdf->Output();
                   
          
      // die();
    }
      

      
    

}

