<?php
class Usuarios_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function get_usuarios()
    {
        $this->db->select('id_usuario, nombres');
        $this->db->from('usuarios');
        $query = $this->db->get();
        $result_consul = array();
        $result_consul = $query->result();
         
        return $result_consul;
    }

    function insert_new_user($dates)
    {
        $value_return = $this->db->insert('usuarios', $dates);  
        return $value_return;

    }

    function load_unic_user_consul($id)
    {

        $this->db->select('id_usuario, nombres, cedula, dependencia, correo, password, rol');
        $this->db->from('usuarios');
        $this->db->where('id_usuario', $id);
        $query = $this->db->get();
        $result_consul = array();
        $result_consul = $query->result();

        $retorna = array(
                           'id_usuario' => $result_consul['0']->id_usuario,
                           'nombres' => $result_consul['0']->nombres,
                           'cedula' => $result_consul['0']->cedula,
                           'dependencia' => $result_consul['0']->dependencia,
                           'correo' => $result_consul['0']->correo,
                           'password' => $result_consul['0']->password,
                           'rol' => $result_consul['0']->rol,
                          );
        return $retorna;
    }

    function update_user($dates)
    {   
        $data = array(
                        'nombres'     => $dates['nombres'],
                        'cedula'      => $dates['cedula'],
                        'dependencia' => $dates['dependencia'],
                        'correo'      => $dates['correo'],
                        'password'    => $dates['password'],
                        'rol'         => $dates['rol']
                     );

        $this->db->where('id_usuario', $dates['id_usuario']);
        $this->db->update('usuarios', $data);
    }


    function delete_user($id_delete)
    {
       $this->db->where('id_usuario', $id_delete);
       $this->db->delete('usuarios');
    }


    function get_area_user($cedula)
    {
        $this->db->select('dependencia');
        $this->db->from('usuarios');
        $this->db->where('cedula', $cedula);
        $query = $this->db->get();
        $resul = array();
        $resul = $query->result();

        return $resul[0]->dependencia;
    }

}