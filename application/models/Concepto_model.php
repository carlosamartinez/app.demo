<?php
class Concepto_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function get_conceptos()
    {
        $this->db->select('nombre_concepto');
        $this->db->from('conceptos');
        
        $query = $this->db->get();
        $result_consul = array();
        $result_consul = $query->result();
         
        return $result_consul;
    }

    function get_conceptos_all()
    {

        $this->db->select('id_concepto, nombre_concepto, observaciones');
        $this->db->from('conceptos');
        
        $query = $this->db->get();
        $result_consul = array();
        $result_consul = $query->result();
         
        return $result_consul;
    }

    function save_info_conceptos($info_insert_conceptos)
    {  
        $this->db->select('nombre_concepto');
        $this->db->from('conceptos');
        $this->db->where('nombre_concepto', $info_insert_conceptos['nombre_concepto']);
        $query = $this->db->get();
        if($query->result_id->num_rows)
        {
            return 0;
        }else{
            $this->db->insert('conceptos', $info_insert_conceptos);
            return 1;
        }
    }

    function delete_registros($id)
    {
        $this->db->delete('conceptos', array('id_concepto' => $id)); 
    }

    function loading_unico_concept($id)
    {
        $this->db->select('nombre_concepto, observaciones');
        $this->db->from('conceptos');
        $this->db->where('id_concepto', $id);
        $query = $this->db->get();
        $result_unico_concept = array();
        $result_unico_concept = $query->result();

        return $result_unico_concept;        

    }

    function update_concepts($info_update)
    {
        $datos_update = array(
                              'nombre_concepto'      => $info_update['concepto'], 
                              'observaciones' => $info_update['observaciones']
                             );
        $this->db->where('id_concepto', $info_update['id_concepto']);
        $this->db->update('conceptos', $datos_update); 
    }

}