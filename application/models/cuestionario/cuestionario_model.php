<?php

require_once(APPPATH.'libraries/Classes/PHPExcel.php');

class cuestionario_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function viewArray(){

		$arrayCuestionario['1']['pregunta'] = $_POST['nombre_pregunta'];
		$arrayCuestionario['1']['respuesta'] = $_POST['nombre'];
		$arrayCuestionario['2']['pregunta'] = $_POST['apellidos_pregunta'];
		$arrayCuestionario['2']['respuesta'] = $_POST['apellidos'];
		$arrayCuestionario['3']['pregunta'] = $_POST['cedula_evaluado_pregunta'];
		$arrayCuestionario['3']['respuesta'] = $_POST['cedula_vendedor'];
		$arrayCuestionario['4']['pregunta'] = $_POST['fecha_expedicion_pregunta'];
		$arrayCuestionario['4']['respuesta'] = $_POST['fecha_expedicion'];
		$arrayCuestionario['5']['pregunta'] = $_POST['fecha_nacimiento_pregunta'];
		$arrayCuestionario['5']['respuesta'] = $_POST['fecha_nacimiento'];
		$arrayCuestionario['6']['pregunta'] = $_POST['lugar_nacimiento_pregunta'];
		$arrayCuestionario['6']['respuesta'] = $_POST['lugar_nacimiento'];
		$arrayCuestionario['7']['pregunta'] = $_POST['depto_nacimiento_pregunta'];
		$arrayCuestionario['7']['respuesta'] = $_POST['depto_nacimiento'];
		$arrayCuestionario['8']['pregunta'] = $_POST['nacionalidad_pregunta'];
		$arrayCuestionario['8']['respuesta'] = $_POST['nacionalidad'];
		$arrayCuestionario['9']['pregunta'] = $_POST['libreta_militar_pregunta'];
		$arrayCuestionario['9']['respuesta'] = $_POST['libreta_militar'];
		$arrayCuestionario['10']['pregunta'] = $_POST['genero_pregunta'];
		$arrayCuestionario['10']['respuesta'] = $_POST['genero'];
		$arrayCuestionario['11']['pregunta'] = $_POST['edad_pregunta'];
		$arrayCuestionario['11']['respuesta'] = $_POST['edad'];
		$arrayCuestionario['12']['pregunta'] = $_POST['email_pregunta'];
		$arrayCuestionario['12']['respuesta'] = $_POST['email'];
		$arrayCuestionario['13']['pregunta'] = $_POST['tipo_sangre_pregunta'];
		$arrayCuestionario['13']['respuesta'] = $_POST['tipo_sangre'];
		$arrayCuestionario['14']['pregunta'] = $_POST['estado_civil_pregunta'];
		$arrayCuestionario['14']['respuesta'] = $_POST['estado_civil'];
		$arrayCuestionario['15']['pregunta'] = $_POST['numero_celular_pregunta'];
		$arrayCuestionario['15']['respuesta'] = $_POST['numero_celular'];
		$arrayCuestionario['16']['pregunta'] = $_POST['talla_camisa_pregunta'];
		$arrayCuestionario['16']['respuesta'] = $_POST['talla_camisa'];
		$arrayCuestionario['17']['pregunta'] = $_POST['talla_pantalon_pregunta'];
		$arrayCuestionario['17']['respuesta'] = $_POST['talla_pantalon'];
		$arrayCuestionario['18']['pregunta'] = $_POST['talla_zapatos_pregunta'];
		$arrayCuestionario['18']['respuesta'] = $_POST['talla_zapatos'];

		$arrayCuestionario['19']['pregunta'] = $_POST['cotizante_salud_pregunta'];
		$arrayCuestionario['19']['respuesta'] = $_POST['cotizante_salud'];
		$arrayCuestionario['20']['pregunta'] = $_POST['tipo_afiliado_pregunta'];
		$arrayCuestionario['20']['respuesta'] = $_POST['tipo_afiliado'];
		$arrayCuestionario['21']['pregunta'] = $_POST['entidad_salud_pregunta'];
		$arrayCuestionario['21']['respuesta'] = $_POST['entidad_salud'];
		$arrayCuestionario['22']['pregunta'] = $_POST['parentesco_cotizante_pregunta'];
		$arrayCuestionario['22']['respuesta'] = $_POST['parentesco_cotizante'];
		$arrayCuestionario['23']['pregunta'] = $_POST['afp_pregunta'];
		$arrayCuestionario['23']['respuesta'] = $_POST['afp'];
		$arrayCuestionario['24']['pregunta'] = $_POST['cesantias_pregunta'];
		$arrayCuestionario['24']['respuesta'] = $_POST['cesantias'];

		$arrayCuestionario['25']['pregunta'] = $_POST['tipo_de_vivienda_pregunta'];
		$arrayCuestionario['25']['respuesta'] = $_POST['tipo_de_vivienda'];
		$arrayCuestionario['26']['pregunta'] = $_POST['tiempo_sector_pregunta'];
		$arrayCuestionario['26']['respuesta'] = $_POST['tiempo_sector'];
		$arrayCuestionario['27']['pregunta'] = $_POST['barrio_pregunta'];
		$arrayCuestionario['27']['respuesta'] = $_POST['barrio'];
		$arrayCuestionario['28']['pregunta'] = $_POST['direccion_residencia_pregunta'];
		$arrayCuestionario['28']['respuesta'] = $_POST['direccion_residencia'];
		$arrayCuestionario['29']['pregunta'] = $_POST['telefono_residencia_pregunta'];
		$arrayCuestionario['29']['respuesta'] = $_POST['telefono_residencia'];
		$arrayCuestionario['30']['pregunta'] = $_POST['estrato_pregunta'];
		$arrayCuestionario['30']['respuesta'] = $_POST['estrato'];

		$arrayCuestionario['31']['pregunta'] = $_POST['hijos_cargo_pregunta'];
		$arrayCuestionario['31']['respuesta'] = $_POST['hijos_cargo'];
		$arrayCuestionario['32']['pregunta'] = $_POST['padres_cargo_pregunta'];
		$arrayCuestionario['32']['respuesta'] = $_POST['padres_cargo'];
		$arrayCuestionario['33']['pregunta'] = $_POST['edad_hijos_cargo_pregunta'];
		$arrayCuestionario['33']['respuesta'] = $_POST['edad_hijos_cargo'];
		$arrayCuestionario['34']['pregunta'] = $_POST['edad_padres_cargo_pregunta'];
		$arrayCuestionario['34']['respuesta'] = $_POST['edad_padres_cargo'];
		$arrayCuestionario['35']['pregunta'] = $_POST['conyuge_pregunta'];
		$arrayCuestionario['35']['respuesta'] = $_POST['conyuge'];
		$arrayCuestionario['36']['pregunta'] = $_POST['nombre_conyuge_pregunta'];
		$arrayCuestionario['36']['respuesta'] = $_POST['nombre_conyuge'];
		$arrayCuestionario['37']['pregunta'] = $_POST['cedula_conyuge_pregunta'];
		$arrayCuestionario['37']['respuesta'] = $_POST['cedula_conyuge'];
		$arrayCuestionario['38']['pregunta'] = $_POST['eps_conyuge_pregunta'];
		$arrayCuestionario['38']['respuesta'] = $_POST['eps_conyuge'];
		$arrayCuestionario['39']['pregunta'] = $_POST['labor_conyuge_pregunta'];
		$arrayCuestionario['39']['respuesta'] = $_POST['labor_conyuge'];
		$arrayCuestionario['40']['pregunta'] = $_POST['estado_conyuge_pregunta'];
		$arrayCuestionario['40']['respuesta'] = $_POST['estado_conyuge'];
		$arrayCuestionario['41']['pregunta'] = $_POST['composicion_familiar_pregunta'];
		$arrayCuestionario['41']['respuesta'] = $_POST['composicion_familiar'];
		$arrayCuestionario['42']['pregunta'] = $_POST['total_personas_pregunta'];
		$arrayCuestionario['42']['respuesta'] = $_POST['total_personas'];

		$arrayCuestionario['43']['pregunta'] = $_POST['sector_residencial_pregunta'];
		$arrayCuestionario['43']['respuesta'] = $_POST['sector_residencial'];
		$arrayCuestionario['44']['pregunta'] = $_POST['estado_vivienda_pregunta'];
		$arrayCuestionario['44']['respuesta'] = $_POST['estado_vivienda'];
		$arrayCuestionario['45']['pregunta'] = $_POST['tipo_vivienda_pregunta'];
		$arrayCuestionario['45']['respuesta'] = $_POST['tipo_vivienda'];
		$arrayCuestionario['46']['pregunta'] = $_POST['tenencia_vivienda_pregunta'];
		$arrayCuestionario['46']['respuesta'] = $_POST['tenencia_vivienda'];
		$arrayCuestionario['47']['pregunta'] = $_POST['aspectos_fisicos_pregunta'];
		$arrayCuestionario['47']['respuesta'] = $_POST['aspectos_fisicos'];
		$arrayCuestionario['48']['pregunta'] = $_POST['tipo_estructura_pregunta'];
		$arrayCuestionario['48']['respuesta'] = $_POST['tipo_estructura'];
		$arrayCuestionario['49']['pregunta'] = $_POST['vivienda_social_pregunta'];
		$arrayCuestionario['49']['respuesta'] = $_POST['vivienda_social'];
		
		$contadorPosee = count($_POST['posee']);
		for ($i=0; $i <$contadorPosee ; $i++) { 
			$StringPosee = implode(", ", $_POST['posee']);
		}
		$arrayCuestionario['50']['pregunta'] = $_POST['posee_pregunta'];
		$arrayCuestionario['50']['respuesta'] = $StringPosee;

		$contadorCondiciones = count($_POST['condiciones']);
		for ($i=0; $i <$contadorCondiciones ; $i++) { 
			$StringCondiciones = implode(", ", $_POST['condiciones']);
		}
		$arrayCuestionario['51']['pregunta'] = $_POST['condiciones_pregunta'];
		$arrayCuestionario['51']['respuesta'] = $StringCondiciones;

		$contadorEquipamiento = count($_POST['equipamento']);
		for ($i=0; $i <$contadorEquipamiento ; $i++) { 
			$StringEquipamento = implode(", ", $_POST['equipamento']);
		}
		$arrayCuestionario['52']['pregunta'] = $_POST['equipamento_pregunta'];
		$arrayCuestionario['52']['respuesta'] = $StringEquipamento;

		$arrayCuestionario['53']['pregunta'] = $_POST['grado_escolaridad_pregunta'];
		$arrayCuestionario['53']['respuesta'] = $_POST['grado_escolaridad'];
		$arrayCuestionario['54']['pregunta'] = $_POST['institucion_bachillerato_pregunta'];
		$arrayCuestionario['54']['respuesta'] = $_POST['institucion_bachillerato'];
		$arrayCuestionario['55']['pregunta'] = $_POST['profesion_pregunta'];
		$arrayCuestionario['55']['respuesta'] = $_POST['profesion'];
		$arrayCuestionario['56']['pregunta'] = $_POST['nombre_formacion_pregunta'];
		$arrayCuestionario['56']['respuesta'] = $_POST['nombre_formacion'];
		$arrayCuestionario['57']['pregunta'] = $_POST['nombre_titulo_pregunta'];
		$arrayCuestionario['57']['respuesta'] = $_POST['nombre_titulo'];
		$arrayCuestionario['58']['pregunta'] = $_POST['lugar_formacion_pregunta'];
		$arrayCuestionario['58']['respuesta'] = $_POST['lugar_formacion'];
		$arrayCuestionario['59']['pregunta'] = $_POST['duracion_formacion_pregunta'];
		$arrayCuestionario['59']['respuesta'] = $_POST['duracion_formacion'];
		$arrayCuestionario['60']['pregunta'] = $_POST['gustaria_estudiar_pregunta'];
		$arrayCuestionario['60']['respuesta'] = $_POST['gustaria_estudiar'];
		$arrayCuestionario['61']['pregunta'] = $_POST['cursa_actualmente_pregunta'];
		$arrayCuestionario['61']['respuesta'] = $_POST['cursa_actualmente'];
		$arrayCuestionario['62']['pregunta'] = $_POST['centro_estudios_actual_pregunta'];
		$arrayCuestionario['62']['respuesta'] = $_POST['centro_estudios_actual'];
		$arrayCuestionario['63']['pregunta'] = $_POST['tipo_estudio_actual_pregunta'];
		$arrayCuestionario['63']['respuesta'] = $_POST['tipo_estudio_actual'];
		$arrayCuestionario['64']['pregunta'] = $_POST['horario_clases_pregunta'];
		$arrayCuestionario['64']['respuesta'] = $_POST['horario_clases'];
		$arrayCuestionario['65']['pregunta'] = $_POST['conoce_ingles_pregunta'];
		$arrayCuestionario['65']['respuesta'] = $_POST['conoce_ingles'];
		$arrayCuestionario['66']['pregunta'] = $_POST['nivel_ingles_pregunta'];
		$arrayCuestionario['66']['respuesta'] = $_POST['nivel_ingles'];
		
		$arrayCuestionario['67']['pregunta'] = $_POST['otros_idiomas_pregunta'];
		$arrayCuestionario['67']['respuesta'] = $_POST['otros_idiomas'];
		$arrayCuestionario['68']['pregunta'] = $_POST['cual_idioma_pregunta'];
		$arrayCuestionario['68']['respuesta'] = $_POST['cual_idioma'];

		$arrayCuestionario['69']['pregunta'] = $_POST['experiencia_laboral_pregunta'];
		$arrayCuestionario['69']['respuesta'] = $_POST['experiencia_laboral'];
		$arrayCuestionario['70']['pregunta'] = $_POST['empresa1_pregunta'];
		$arrayCuestionario['70']['respuesta'] = $_POST['empresa1'];
		$arrayCuestionario['71']['pregunta'] = $_POST['cargo1_pregunta'];
		$arrayCuestionario['71']['respuesta'] = $_POST['cargo1'];
		$arrayCuestionario['72']['pregunta'] = $_POST['año_inicio1_pregunta'];
		$arrayCuestionario['72']['respuesta'] = $_POST['año_inicio1'];
		$arrayCuestionario['73']['pregunta'] = $_POST['año_fin1_pregunta'];
		$arrayCuestionario['73']['respuesta'] = $_POST['año_fin1'];
		$arrayCuestionario['74']['pregunta'] = $_POST['empresa2_pregunta'];
		$arrayCuestionario['74']['respuesta'] = $_POST['empresa2'];
		$arrayCuestionario['75']['pregunta'] = $_POST['cargo2_pregunta'];
		$arrayCuestionario['75']['respuesta'] = $_POST['cargo2'];
		$arrayCuestionario['76']['pregunta'] = $_POST['año_inicio2_pregunta'];
		$arrayCuestionario['76']['respuesta'] = $_POST['año_inicio2'];
		$arrayCuestionario['77']['pregunta'] = $_POST['año_fin2_pregunta'];
		$arrayCuestionario['77']['respuesta'] = $_POST['año_fin2'];

		$arrayCuestionario['78']['pregunta'] = $_POST['jefe_inmediato_actual_pregunta'];
		$arrayCuestionario['78']['respuesta'] = $_POST['jefe_inmediato_actual'];
		$arrayCuestionario['79']['pregunta'] = $_POST['cargo_actual_pregunta'];
		$arrayCuestionario['79']['respuesta'] = $_POST['cargo_actual'];
		$arrayCuestionario['80']['pregunta'] = $_POST['estado_laboral_pregunta'];
		$arrayCuestionario['80']['respuesta'] = $_POST['estado_laboral'];
		$arrayCuestionario['81']['pregunta'] = $_POST['tipo_contrato_pregunta'];
		$arrayCuestionario['81']['respuesta'] = $_POST['tipo_contrato'];
		$arrayCuestionario['82']['pregunta'] = $_POST['fecha_ingreso_pregunta'];
		$arrayCuestionario['82']['respuesta'] = $_POST['fecha_ingreso'];
		$arrayCuestionario['83']['pregunta'] = $_POST['fecha_retiro_pregunta'];
		$arrayCuestionario['83']['respuesta'] = $_POST['fecha_retiro'];
		$arrayCuestionario['84']['pregunta'] = $_POST['reingreso_pregunta'];
		$arrayCuestionario['84']['respuesta'] = $_POST['reingreso'];
		$arrayCuestionario['85']['pregunta'] = $_POST['codigo_vendedor_pregunta'];
		$arrayCuestionario['85']['respuesta'] = $_POST['codigo_vendedor'];
		$arrayCuestionario['86']['pregunta'] = $_POST['centro_costos_pregunta'];
		$arrayCuestionario['86']['respuesta'] = $_POST['centro_costos'];
		$arrayCuestionario['87']['pregunta'] = $_POST['comision_pregunta'];
		$arrayCuestionario['87']['respuesta'] = $_POST['comision'];
		$arrayCuestionario['88']['pregunta'] = $_POST['zona_venta_pregunta'];
		$arrayCuestionario['88']['respuesta'] = $_POST['zona_venta'];
		$arrayCuestionario['89']['pregunta'] = $_POST['tipo_punto_pregunta'];
		$arrayCuestionario['89']['respuesta'] = $_POST['tipo_punto'];
		$arrayCuestionario['90']['pregunta'] = $_POST['municipio_punto_pregunta'];
		$arrayCuestionario['90']['respuesta'] = $_POST['municipio_punto'];
		$arrayCuestionario['91']['pregunta'] = $_POST['direccion_punto_pregunta'];
		$arrayCuestionario['91']['respuesta'] = $_POST['direccion_punto'];
		$arrayCuestionario['92']['pregunta'] = $_POST['tipologia_semana_inicio_pregunta'];
		$arrayCuestionario['92']['respuesta'] = $_POST['tipologia_semana_inicio'];
		$arrayCuestionario['93']['pregunta'] = $_POST['tipologia_semana_fin_pregunta'];
		$arrayCuestionario['93']['respuesta'] = $_POST['tipologia_semana_fin'];
		$arrayCuestionario['94']['pregunta'] = $_POST['tipologia_dominical_inicio_pregunta'];
		$arrayCuestionario['94']['respuesta'] = $_POST['tipologia_dominical_inicio'];
		$arrayCuestionario['95']['pregunta'] = $_POST['tipologia_dominical_fin_pregunta'];
		$arrayCuestionario['95']['respuesta'] = $_POST['tipologia_dominical_fin'];
		$arrayCuestionario['96']['pregunta'] = $_POST['tecnologia_pregunta'];
		$arrayCuestionario['96']['respuesta'] = $_POST['tecnologia'];
		$arrayCuestionario['97']['pregunta'] = $_POST['serie_pregunta'];
		$arrayCuestionario['97']['respuesta'] = $_POST['serie'];
		$arrayCuestionario['98']['pregunta'] = $_POST['tipo_cuestionario_pregunta'];
		$arrayCuestionario['98']['respuesta'] = $_POST['tipo_cuestionario'];
    	
		$this->db->select('cedulaVendedor');
        $this->db->from('cuestionario');
        $this->db->where('cedulaVendedor', $_POST['cedula_vendedor']);
        
        $consultaCedula = $this->db->get();
        $consultaResultado = $consultaCedula->result();

        if ($consultaCedula->result_id->num_rows) {
        	$mensaje = "error";
        	return $mensaje;
        }else{

			$datosCuestionario = array(
	            'jsonCuestionario'   => json_encode($arrayCuestionario),
	            'nombreVendedor' => $_POST['nombre']." ".$_POST['apellidos'],
	            'cedulaVendedor' => $_POST['cedula_vendedor'],
	            'tipoCuestionario' => $_POST['tipo_cuestionario'],
	            'textoHabeasData' => $_POST['textoHabeas'],
	            'habeasData' => $_POST['habeas']
	            );

			$this->db->insert('cuestionario', $datosCuestionario);

			$mensaje = "exito";
			return $mensaje;
		}

    }

    function validarDatosCuestionario($datos){

    	$mensaje;
        $this->db->select('cedulaVendedor');
        $this->db->from('cuestionario');
        $this->db->where('cedulaVendedor', $datos['cedula_persona']);
        $consulta = $this->db->get();
        
        if($consulta->result_id->num_rows){
            $mensaje='existente';
            return $mensaje;
        }
    }

    function visualizarBoton($dato){

        $this->db->select('verBotonCuestionario');
        $this->db->from('boton_cuestionario');
        $consulta = $this->db->get();
        $resultadoConsulta = $consulta->result();

        $this->db->update('boton_cuestionario', $dato);
    }

    public function validarBoton(){
        $this->db->select('verBotonCuestionario');
        $this->db->from('boton_cuestionario');
        $consulta = $this->db->get();
        $result = $consulta->result();

        return $result;
    }

    function get_cuestionarios(){
    	$this->db->select('idCuestionario, nombreVendedor, cedulaVendedor, tipoCuestionario, jsonCuestionario');
        $this->db->from('cuestionario');
        $query =$this->db->get();
        $datosCuestionarios = array();
        $datosCuestionarios = $query->result();
        return $datosCuestionarios;
    }

    function verCuestionarios($id){
    	$this->db->select('idCuestionario, nombreVendedor, cedulaVendedor, jsonCuestionario');
        $this->db->from('cuestionario');
        $this->db->where('idCuestionario', $id);
        
        $consultaCalifica = $this->db->get();
        $calificaciones = array();
        $calificaciones = $consultaCalifica->result();

        return $calificaciones;
    }

    function deleteDatosVendedor($id){
    	$this->db->where('idCuestionario', $id);
        $this->db->delete('cuestionario');
    }

    function exportarDatos(){
    	$this->db->select('idCuestionario, nombreVendedor, cedulaVendedor, jsonCuestionario');
        $this->db->from('cuestionario');
        
        $consultaCalifica = $this->db->get();
        $calificaciones = array();
        $calificaciones = $consultaCalifica->result();

        if ($consultaCalifica->result_id->num_rows) {
        
	        $jsonCuestionario = json_decode($calificaciones['0']->jsonCuestionario);

	        $arrayLetras = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","AA","AB","AC","AD","AE","AF","AG","AH","AI","AJ","AK","AL","AM","AN","AO","AP","AQ","AR","AS","AT","AU","AV","AW","AX","AY","AZ","BA","BB","BC","BD","BE","BF","BG","BH","BI","BJ","BK","BL","BM","BN","BO","BP","BQ","BR","BS","BT","BU","BV","BW","BX","BY","BZ","CA","CB","CC","CD","CE","CF","CG","CH","CI","CJ","CK","CL","CM","CN","CO","CP","CQ","CR","CS","CT"];

	        $objXLS = new reporteExcel();
	        $objSheet = $objXLS->setActiveSheetIndex(0);
	        
	        $posicionTitulo = 0;
	        $uno = 1;
	        
	        foreach ($jsonCuestionario as $jsonCuestionario) {

	        	/*Asignamos diferentes colores a las celdas*/
	        	if ($posicionTitulo>=0 && $posicionTitulo<=29) {
	        		$color='ffff00';
	        	}else if ($posicionTitulo>=30 && $posicionTitulo<=41) {
	        		$color='66ff00';
	        	}else if ($posicionTitulo>=42 && $posicionTitulo<=51) {
	        		$color='cc9999';
	        	}else if ($posicionTitulo>=52 && $posicionTitulo<=67) {
	        		$color='0099ff';
	        	}else if ($posicionTitulo>=68 && $posicionTitulo<=76) {
	        		$color='ff0000';
	        	}else if ($posicionTitulo>=77 && $posicionTitulo<=96) {
	        		$color='ffcc00';
	        	}else{
	        		$color='AF41FF';
	        	}
	        	
	        	$preguntas = $jsonCuestionario->pregunta;//Traemos las preguntas

	        	$objXLS->getActiveSheet()->getColumnDimension($arrayLetras[$posicionTitulo])->setAutoSize(true);//Celdas con ancho automático
	        	
	        	/*Pintamos los colores anteriormente asignados */
	        	$objSheet->getStyle($arrayLetras[$posicionTitulo].$uno)->applyFromArray(
	        	    array(
	        	        'fill' => array(
	        	            'type' => PHPExcel_Style_Fill::FILL_SOLID,
	        	            'color' => array('rgb' => $color)
	        	        )
	        	    )
	        	);

	        	$objSheet->setCellValue($arrayLetras[$posicionTitulo].$uno, $preguntas);//Asignamos las preguntas a las celdas
	        	$posicionTitulo = $posicionTitulo+1;//Incrementamos una posición en el array para pintar la siguiente pregunta

	        }

	        $contArray = 0;
	        $cont = 2;
	        
	        for ($i=0; $i <count($calificaciones) ; $i++) { 
	        	
	        	$jsonRespuestas = json_decode($calificaciones[$i]->jsonCuestionario);

	        	foreach ($jsonRespuestas as $jsonRespuestas) {

	        		$respuestas[$i] = $jsonRespuestas->respuesta;

	        		$objXLS->getActiveSheet()->setCellValueExplicit($arrayLetras[$contArray].$cont, $respuestas[$i], PHPExcel_Cell_DataType::TYPE_STRING);
	        		$objXLS->getActiveSheet()->getStyle($arrayLetras[$contArray].$cont)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
	        		/*$objSheet->setCellValue($arrayLetras[$contArray].$cont, $respuestas[$i]);*/
	        		$contArray = $contArray+1;
	        		
	        	}

	        	$contArray = 0;
	        	$cont++;
	        	
	        }
	        
	        $objXLS->getActiveSheet()->setTitle('Reporte');
	        $objXLS->getActiveSheetIndex(0);
	        // We'll be outputting an excel file
	        header('Content-type: application/vnd.ms-excel');
	        // It will be called file.xls
	        header('Content-Disposition: attachment; filename="reporte.xls"');
	        $objWriter = PHPExcel_IOFactory::createWriter($objXLS, 'Excel5');
	        $objWriter->save('php://output');
	        /*$objWriter->save(APPPATH.'\..\..\..\..\reporte.xls');*/

	    }else{

	    	$objXLS = new reporteExcel();
	    	$objSheet = $objXLS->setActiveSheetIndex(0);
	    	
    		$objSheet->getStyle('A1')->applyFromArray(
    		    array(
    		        'fill' => array(
    		            'type' => PHPExcel_Style_Fill::FILL_SOLID,
    		            'color' => array('rgb' => 'ff0000')
    		        )
    		    )
    		);

    		$objSheet->setCellValue('A1', 'No hay registros');
    		
    		$objXLS->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	    	$objXLS->getActiveSheet()->setTitle('Reporte');
	    	$objXLS->getActiveSheetIndex(0);
	    	header('Content-type: application/vnd.ms-excel');
	    	header('Content-Disposition: attachment; filename="reporte.xls"');
	    	$objWriter = PHPExcel_IOFactory::createWriter($objXLS, 'Excel5');
	    	$objWriter->save('php://output');
	    }
    }

}

class reporteExcel extends PHPExcel{

}