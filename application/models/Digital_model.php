<?php
class Digital_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function get_date_user($cedula)
    {
        $this->db->select('cedula, dependencia, nombres, rol');
        $this->db->from('usuarios');
        $this->db->where('cedula', $cedula);
        $query = $this->db->get();
        $result_consul = array();
        $result_consul = $query->result();
        return $result_consul;
    }

    function insert_proveedor($proveedor)
    {
        $mensaje;
        $this->db->select('nitProveedor, identificacionProveedor');
        $this->db->from('proveedores');
        $this->db->where('nitProveedor', $proveedor['nitProveedor']);
        $this->db->where('identificacionProveedor', $proveedor['identificacionProveedor']);
        $consultaNitIdentificacion = $this->db->get();
        
        if($consultaNitIdentificacion->result_id->num_rows){
            $mensaje='nitIdentificacionExistente';
            return $mensaje;
        }

        else{
            // Añadimos el archivo de Certificado de existencia y representación legal vigente
            $archivo_certificado = $_FILES["file_certificado"]['name'];
            if (!empty($archivo_certificado)) {
                $array_archivo_certificado = explode('.', $archivo_certificado);
                $prefijo_certificado = substr(md5(uniqid(rand())),0,6);
                $destino_certificado = "../prueba/files_proveedores/".$prefijo_certificado.".".$array_archivo_certificado[1];
                (copy($_FILES['file_certificado']['tmp_name'],$destino_certificado));
                $dest_certificado = substr($destino_certificado, 2);
                $proveedor['rutaCertificadoRepresentacion'] = $dest_certificado;
            }
            

            // Añadimos el archivo de la copia del Rut
            $archivo_rut = $_FILES["file_rut"]['name'];
            if (!empty($archivo_rut)) {
                $array_archivo_rut = explode('.', $archivo_rut);
                $prefijo_rut = substr(md5(uniqid(rand())),0,6);
                $destino_rut = "../prueba/files_proveedores/".$prefijo_rut.".".$array_archivo_rut[1];
                (copy($_FILES['file_rut']['tmp_name'],$destino_rut));
                $dest_rut = substr($destino_rut, 2);
                $proveedor['rutaRut'] = $dest_rut;
            }
            

            // Añadimos el archivo de la copia del Nit o Cédula de ciudadanía
            $archivo_nit_cedula = $_FILES["file_nit_cedula"]['name'];
            if (!empty($archivo_nit_cedula)) {
                $array_archivo_nit_cedula = explode('.', $archivo_nit_cedula);
                $prefijo_nit_cedula = substr(md5(uniqid(rand())),0,6);
                $destino_nit_cedula = "../prueba/files_proveedores/".$prefijo_nit_cedula.".".$array_archivo_nit_cedula[1];
                (copy($_FILES['file_nit_cedula']['tmp_name'],$destino_nit_cedula));
                $dest_nit_cedula = substr($destino_nit_cedula, 2);
                $proveedor['rutaNitCedula'] = $dest_nit_cedula;
            }


            // Añadimos el archivo de los listados de precios
            $archivo_precios = $_FILES["file_precios"]['name'];
            if (!empty($archivo_precios)) {
                $array_archivo_precios = explode('.', $archivo_precios);
                $prefijo_archivo_precios = substr(md5(uniqid(rand())),0,6);
                $destino_archivo_precios = "../prueba/files_proveedores/".$prefijo_archivo_precios.".".$array_archivo_precios[1];
                (copy($_FILES['file_precios']['tmp_name'],$destino_archivo_precios));
                $dest_archivo_precios = substr($destino_archivo_precios, 2);
                $proveedor['rutaPrecios'] = $dest_archivo_precios;
            }
            
            $this->db->insert('proveedores', $proveedor);
            $mensaje='exito';
            return $mensaje;
        }
        
    }

    function insert_formato($formato){

        $mensaje='';

        // echo "<pre>";
        //     print_r($formato);
        // echo "</pre>";
        // die();

        $this->db->select('tipoEvaluacion, nombreFormato');
        $this->db->from('formatos');
        $this->db->where('tipoEvaluacion', $formato['tipoEvaluacion']);
        $this->db->where('nombreFormato', $formato['nombreFormato']);
        $query = $this->db->get();

        $mensaje = '';

        if($query->result_id->num_rows){
            $mensaje = 'existente';
            return $mensaje;
        }elseif(empty($mensaje)){      

           $areas_probar = json_decode($formato['areas']);
  
           $this->db->select('areas');
           $this->db->from('formatos');
           $this->db->where('tipoEvaluacion', $formato['tipoEvaluacion']);               
           $query_y = $this->db->get();
           $respu = $query_y->result();

           if(empty($respu)){
            $this->db->insert('formatos', $formato);
            $mensaje = 'exito';
            return $mensaje;
           }

            $areas_com = json_decode($respu[0]->areas);

           for($i=0; $i < count($areas_probar); $i++){               
              
              for($x=0; $x < count($areas_com) ; $x++){ 
                 if($areas_probar[$i] == $areas_com[$x]){
                    $mensaje = 'existente';
                    return $mensaje;
                 }
              } 

           }

        }else{
            $this->db->insert('formatos', $formato);
            $mensaje = 'exito';
            return $mensaje;
        }

    }

    function get_table_formatos(){
        $this->db->select('idFormato, nombreFormato, tipoEvaluacion, areas, preguntas');
        $this->db->from('formatos');
        $query =$this->db->get();
        $datos_formatos = array();
        $datos_formatos = $query->result();
        return $datos_formatos;
    }

    function get_table_calificaciones(){

        $this->db->select('idEvaluacionProveedor, nombreProveedorEvaluado, nombreEvaluacion, areaEvaluacion, tipoEvaluacion, respuestas, total_calificacion');
        $this->db->from('evaluar_proveedor');
        $this->db->order_by("total_calificacion", "desc");
        $consultaCalifica = $this->db->get();
        $calificaciones = array();
        $calificaciones = $consultaCalifica->result();
        return $calificaciones;
    }

    function get_info_formatos($id){
        $this->db->select('idFormato, nombreFormato, tipoEvaluacion, areas, preguntas');
        $this->db->from('formatos');
        $this->db->where('idFormato',$id);
        $query = $this->db->get();
        $result_consul_info = $query->result();
        return $result_consul_info;
    }

    function get_table_proveedores(){
        $this->db->select('idProveedor, nombreProveedor, identificacionProveedor,  nitProveedor, ciudadProveedor, paisProveedor, telefonoProveedor, contactoProveedor, tipoEmpresa, areaProveedor');
        $this->db->from('proveedores'); 
        $query = $this->db->get();
        $result_consul_table = array();
        $result_consul_table = $query->result();

        $this->db->select('tipoEvaluacion, areas');
        $this->db->from('formatos');
        $que = $this->db->get();
        $rs = array();
        $rs = $que->result();

        for($a=0; $a <count($rs) ; $a++){ 
            $rs[$a]->areas = json_decode($rs[$a]->areas);
        }

        for($i=0; $i < count($result_consul_table); $i++){ 
            $array = array();

            for($x=0; $x <count($rs); $x++){ 
                for($c=0; $c <count($rs[$x]->areas); $c++){ 
                    if($result_consul_table[$i]->areaProveedor == $rs[$x]->areas[$c]){
                        $array[] = $rs[$x]->tipoEvaluacion;
                    }
                }
            }
            $result_consul_table[$i]->evaluaciones = $array;            
        }

        return $result_consul_table;
    }

    function get_table_digitalizados()
    {
        $this->db->select('id_documento, dependencia, fecha_ingreso, nro_factura, concepto, ruta_imagen, ubicacion, usuario_aprueba, estado');
        $this->db->from('documentos_digitalizados'); 
        $query = $this->db->get();
        $result_consul_table = array();
        $result_consul_table = $query->result();
        return $result_consul_table;
    }

    function get_table_digitalizados_one_and_four()
    {

        $where = "etapa_1 = 0 OR etapa_4 = 0";

        $this->db->select('id_documento, dependencia, fecha_ingreso, nro_factura, concepto, ruta_imagen, ubicacion, usuario_aprueba, estado, etapa_1, etapa_4');
        $this->db->from('documentos_digitalizados');
        $this->db->where($where);
        $query = $this->db->get();
        $result_consul_table = array();
        $result_consul_table = $query->result();

        return $result_consul_table;
    }

    function get_table_digitalizados_two($area)
    {
        $this->db->select('id_documento, dependencia, fecha_ingreso, nro_factura, concepto, ruta_imagen, ubicacion, usuario_aprueba, estado, etapa_1, etapa_4');
        $this->db->from('documentos_digitalizados');
        $this->db->where('etapa_2', 0);
        $this->db->where('ubicacion', $area);
        $query = $this->db->get();
        $result_consul_table = array();
        $result_consul_table = $query->result();
        return $result_consul_table;
    }

    function get_table_digitalizados_tree()
    {
        $this->db->select('id_documento, dependencia, fecha_ingreso, nro_factura, concepto, ruta_imagen, ubicacion, usuario_aprueba, estado, etapa_1, etapa_4');
        $this->db->from('documentos_digitalizados');
        $this->db->where('etapa_3', 0);
        $query = $this->db->get();
        $result_consul_table = array();
        $result_consul_table = $query->result();
        return $result_consul_table;
    }

    function get_table_digitalizados_aprobados()
    {
        $this->db->select('id_documento, dependencia, fecha_ingreso, nro_factura, concepto, ruta_imagen, ubicacion, usuario_aprueba, estado');
        $this->db->from('documentos_digitalizados');
        $this->db->where('estado', 'Aprobado'); 
        $query = $this->db->get();
        $result_consul_table = array();
        $result_consul_table = $query->result();
        return $result_consul_table;
    }

    function get_table_digitalizados_listo_pago()
    {
        $this->db->select('id_documento, dependencia, fecha_ingreso, nro_factura, concepto, ruta_imagen, ubicacion, usuario_aprueba, estado');
        $this->db->from('documentos_digitalizados');
        $this->db->where('estado', 'listo para pago'); 
        $query = $this->db->get();
        $result_consul_table = array();
        $result_consul_table = $query->result();
        return $result_consul_table;
    }

    function get_info_seleccionada($id)
    {
        $this->db->select('id_documento, nro_factura, concepto, ruta_imagen, ubicacion, usuario_aprueba');
        $this->db->from('documentos_digitalizados'); 
        $this->db->where('id_documento',$id);
        $query = $this->db->get();
        $result_consul_info = $query->result();
        return $result_consul_info;
    }

    function get_info_proveedor($id){
        $this->db->select('idProveedor, nombreProveedor, identificacionProveedor,  nitProveedor, direccionProveedor, ciudadProveedor, paisProveedor, telefonoProveedor, contactoProveedor, emailProveedor, responsabilidadTributaria, tipoEmpresa, areaProveedor, nombreReferencia1, ciudadReferencia1, telefonoReferencia1, nombreReferencia2, ciudadReferencia2, telefonoReferencia2, formaPago, fechaDiligenciamiento, rutaCertificadoRepresentacion, rutaRut, rutaNitCedula, rutaPrecios');
        $this->db->from('proveedores');
        $this->db->where('idProveedor',$id);
        $query = $this->db->get();
        $result_consul_info = $query->result();
        return $result_consul_info;
    }

    function insert_values($data)
    {
        $this->db->select('nro_factura');
        $this->db->from('documentos_digitalizados');
        $this->db->where('nro_factura', $data['nro_factura']);
        $query = $this->db->get();
       
        if($query->result_id->num_rows){
            return 0;
        }else{
            // upload imagen en folder
            $archivo = $_FILES["uploadedfile"]['name'];
            $array_name_docume = explode('.', $archivo);
            
            $prefijo = substr(md5(uniqid(rand())),0,6);
            $destino = "../prueba/img_digitalizados/".$prefijo.".".$array_name_docume[1];
            if (copy($_FILES['uploadedfile']['tmp_name'],$destino)){
                $status = "Archivo subido: <b>".$prefijo."</b>";
            }else{
                $status = "Error al subir el archivo";
            }

            $dest = substr($destino, 2);
            $data['ruta_imagen'] = $dest;
            $value_return = $this->db->insert('documentos_digitalizados', $data);  

            $this->db->select_max('id_documento');
            $this->db->from('documentos_digitalizados');
            $quer_y = $this->db->get();
            $res = array();
            $res = $quer_y->result();
            
            $historico_data = array(
                                    'cedula_user_cambios' => $data['cedula_user_digitaliza'],
                                    'dependencia'         => $data['dependencia'],
                                    'fecha_movimiento'    => $data['fecha_ingreso'],
                                    'nro_factura'         => $data['nro_factura'],
                                    'concepto'            => $data['concepto'],
                                    'ruta_imagen'         => $data['ruta_imagen'],
                                    'ubicacion'           => $data['ubicacion'],
                                    'estado'              => $data['estado'],
                                    'id_documento'        => $res['0']->id_documento,
                                    'movimiento'          => 'Ingreso'
                                    );
            $this->db->insert('historico_digitalizados', $historico_data);
            return $value_return;
        }
        
    }

    function update_info($info_bandera)
    {
        $retur_respues = '';
        if($info_bandera['bandera']){

            $ruta_imagen = substr($info_bandera['ruta_imagen'], 2); 
            $id = $info_bandera['id_documento'];
            $data = array(
                    'cedula_user_digitaliza' => $info_bandera['cedula_user_digitaliza'],
                    'dependencia'            => $info_bandera['dependencia'],
                    'fecha_ingreso'          => $info_bandera['fecha_ingreso'],
                    'nro_factura'            => $info_bandera['nro_factura'],
                    'concepto'               => $info_bandera['concepto'],
                    'ruta_imagen'            => $ruta_imagen,
                    'ubicacion'              => $info_bandera['ubicacion'],
                    'usuario_aprueba'        => $info_bandera['usuario_aprueba']
            );

            $retur_respues = $this->db->where('id_documento', $id);
            $retur_respues = $this->db->update('documentos_digitalizados', $data);

            $this->db->select_max('estado');
            $this->db->from('documentos_digitalizados');
            $this->db->where('id_documento', $id);
            $quer_y = $this->db->get();
            $res = array();
            $res = $quer_y->result();

            $historico_data = array(
                                    'cedula_user_cambios' => $info_bandera['cedula_user_digitaliza'],
                                    'dependencia'         => $info_bandera['dependencia'],
                                    'fecha_movimiento'    => $info_bandera['fecha_ingreso'],
                                    'nro_factura'         => $info_bandera['nro_factura'],
                                    'concepto'            => $info_bandera['concepto'],
                                    'ruta_imagen'         => $ruta_imagen,
                                    'ubicacion'           => $info_bandera['ubicacion'],
                                    'estado'              => $res['0']->estado,
                                    'id_documento'        => $id,
                                    'movimiento'          => 'Modificacion'
                                    );
            $this->db->insert('historico_digitalizados', $historico_data);

        }else{

            $id = $info_bandera['id_documento'];
            $data = array(
                    'cedula_user_digitaliza' => $info_bandera['cedula_user_digitaliza'],
                    'dependencia'            => $info_bandera['dependencia'],
                    'fecha_ingreso'          => $info_bandera['fecha_ingreso'],
                    'nro_factura'            => $info_bandera['nro_factura'],
                    'concepto'               => $info_bandera['concepto'],
                    'ubicacion'              => $info_bandera['ubicacion'],
                    'usuario_aprueba'        => $info_bandera['usuario_aprueba']
            );

            $retur_respues = $this->db->where('id_documento', $id);
            $retur_respues = $this->db->update('documentos_digitalizados', $data);


            $this->db->select_max('estado');
            $this->db->from('documentos_digitalizados');
            $this->db->where('id_documento', $id);
            $quer_y = $this->db->get();
            $res = array();
            $res = $quer_y->result();

            $historico_data = array(
                                    'cedula_user_cambios' => $info_bandera['cedula_user_digitaliza'],
                                    'dependencia'         => $info_bandera['dependencia'],
                                    'fecha_movimiento'    => $info_bandera['fecha_ingreso'],
                                    'nro_factura'         => $info_bandera['nro_factura'],
                                    'concepto'            => $info_bandera['concepto'],
                                    'ruta_imagen'         => 'no cambio la imagen',
                                    'ubicacion'           => $info_bandera['ubicacion'],
                                    'estado'              => $res['0']->estado,
                                    'id_documento'        => $id,
                                    'movimiento'          => 'Modificacion'
                                    );

            $this->db->insert('historico_digitalizados', $historico_data);


        }
       
        return $retur_respues;

    }

    function update_proveedor($info){

        $id = $info['idProveedor'];
        $retur_respues = '';

        $this->db->select('idProveedor, identificacionProveedor, nitProveedor');
        $this->db->from('proveedores');
        $this->db->where('idProveedor', $id);
        $consulta = $this->db->get();
        $consultaNitIden = $consulta->result();

        if (($consultaNitIden['0']->identificacionProveedor != $info['identificacionProveedor']) || 
            ($consultaNitIden['0']->nitProveedor != $info['nitProveedor'])) {
            
            $this->db->select('nitProveedor, identificacionProveedor');
            $this->db->from('proveedores');
            $this->db->where('nitProveedor', $info['nitProveedor']);
            $this->db->where('identificacionProveedor', $info['identificacionProveedor']);
            $consultaNitIdentificacion = $this->db->get();
            
            if($consultaNitIdentificacion->result_id->num_rows){
                $retur_respues='Proveedor no actualizado';
                return $retur_respues;
            }else{
                $rutaCertificadoRepresentacion = substr($info['rutaCertificadoRepresentacion'], 2); 
                $rutaRut = substr($info['rutaRut'], 2); 
                $rutaNitCedula = substr($info['rutaNitCedula'], 2); 
                $rutaPrecios = substr($info['rutaPrecios'], 2); 

                $this->db->select('rutaCertificadoRepresentacion, rutaRut, rutaNitCedula, rutaPrecios');
                $this->db->from('proveedores');
                $this->db->where('idProveedor', $id);
                $query = $this->db->get();
                $image_delet = $query->result();
                $bd_certificado = $image_delet['0']->rutaCertificadoRepresentacion;
                $bd_rut = $image_delet['0']->rutaRut;
                $bd_nitCedula = $image_delet['0']->rutaNitCedula;
                $bd_precios = $image_delet['0']->rutaPrecios;

                if (empty($bd_certificado)) {
                    $rutaCertificadoRepresentacion = $rutaCertificadoRepresentacion;
                }
                else if ($rutaCertificadoRepresentacion != $bd_certificado) {
                    if (!empty($rutaCertificadoRepresentacion)) {
                        $rutaCertificadoRepresentacion = $rutaCertificadoRepresentacion;
                        $bd_certificado = $image_delet['0']->rutaCertificadoRepresentacion;
                        unlink('..'.$bd_certificado);
                    }
                    else{
                        $rutaCertificadoRepresentacion = $bd_certificado;
                    }
                }

                if (empty($bd_rut)) {
                    $rutaRut = $rutaRut;
                }
                else if ($rutaRut != $bd_rut) {
                    if (!empty($rutaRut)) {
                        $rutaRut = $rutaRut;
                        $bd_rut = $image_delet['0']->rutaRut;
                        unlink('..'.$bd_rut);
                    }
                    else{
                        $rutaRut = $bd_rut;
                    }
                }

                if (empty($bd_nitCedula)) {
                    $rutaNitCedula = $rutaNitCedula;
                }
                else if ($rutaNitCedula != $bd_nitCedula) {
                    if (!empty($rutaNitCedula)) {
                        $rutaNitCedula = $rutaNitCedula;
                        $bd_nitCedula = $image_delet['0']->rutaNitCedula;
                        unlink('..'.$bd_nitCedula);
                    }
                    else{
                        $rutaNitCedula = $bd_nitCedula;
                    }
                }

                if (empty($bd_precios)) {
                    $rutaPrecios = $rutaPrecios;
                }
                else if ($rutaPrecios != $bd_precios) {
                    if (!empty($rutaPrecios)) {
                        $rutaPrecios = $rutaPrecios;
                        $bd_precios = $image_delet['0']->rutaPrecios;
                        unlink('..'.$bd_precios);
                    }
                    else{
                        $rutaPrecios = $bd_precios;
                    }
                }

                $data = array(
                        'nombreProveedor'               => $info['nombreProveedor'],
                        'identificacionProveedor'       => $info['identificacionProveedor'],
                        'nitProveedor'                  => $info['nitProveedor'],
                        'direccionProveedor'            => $info['direccionProveedor'],
                        'ciudadProveedor'               => $info['ciudadProveedor'],
                        'paisProveedor'                 => $info['paisProveedor'],
                        'telefonoProveedor'             => $info['telefonoProveedor'],
                        'contactoProveedor'             => $info['contactoProveedor'],
                        'emailProveedor'                => $info['emailProveedor'],
                        'responsabilidadTributaria'     => $info['responsabilidadTributaria'],
                        'tipoEmpresa'                   => $info['tipoEmpresa'],
                        'areaProveedor'                 => $info['areaProveedor'],
                        'nombreReferencia1'             => $info['nombreReferencia1'],
                        'ciudadReferencia1'             => $info['ciudadReferencia1'],
                        'telefonoReferencia1'           => $info['telefonoReferencia1'],
                        'nombreReferencia2'             => $info['nombreReferencia2'],
                        'ciudadReferencia2'             => $info['ciudadReferencia2'],
                        'telefonoReferencia2'           => $info['telefonoReferencia2'],
                        'formaPago'                     => $info['formaPago'],
                        'fechaDiligenciamiento'         => $info['fechaDiligenciamiento'],
                        'rutaCertificadoRepresentacion' => $rutaCertificadoRepresentacion,
                        'rutaRut'                       => $rutaRut,
                        'rutaNitCedula'                 => $rutaNitCedula,
                        'rutaPrecios'                   => $rutaPrecios
                );

                $this->db->where('idProveedor', $id);
                $this->db->update('proveedores', $data);            
                
                $retur_respues = 'Proveedor actualizado';
                return $retur_respues;

            }
        }else{
                $rutaCertificadoRepresentacion = substr($info['rutaCertificadoRepresentacion'], 2); 
                $rutaRut = substr($info['rutaRut'], 2); 
                $rutaNitCedula = substr($info['rutaNitCedula'], 2); 
                $rutaPrecios = substr($info['rutaPrecios'], 2); 

                $this->db->select('rutaCertificadoRepresentacion, rutaRut, rutaNitCedula, rutaPrecios');
                $this->db->from('proveedores');
                $this->db->where('idProveedor', $id);
                $query = $this->db->get();
                $image_delet = $query->result();
                $bd_certificado = $image_delet['0']->rutaCertificadoRepresentacion;
                $bd_rut = $image_delet['0']->rutaRut;
                $bd_nitCedula = $image_delet['0']->rutaNitCedula;
                $bd_precios = $image_delet['0']->rutaPrecios;

                if (empty($bd_certificado)) {
                    $rutaCertificadoRepresentacion = $rutaCertificadoRepresentacion;
                }
                else if ($rutaCertificadoRepresentacion != $bd_certificado) {
                    if (!empty($rutaCertificadoRepresentacion)) {
                        $rutaCertificadoRepresentacion = $rutaCertificadoRepresentacion;
                        $bd_certificado = $image_delet['0']->rutaCertificadoRepresentacion;
                        unlink('..'.$bd_certificado);
                    }
                    else{
                        $rutaCertificadoRepresentacion = $bd_certificado;
                    }
                }

                if (empty($bd_rut)) {
                    $rutaRut = $rutaRut;
                }
                else if ($rutaRut != $bd_rut) {
                    if (!empty($rutaRut)) {
                        $rutaRut = $rutaRut;
                        $bd_rut = $image_delet['0']->rutaRut;
                        unlink('..'.$bd_rut);
                    }
                    else{
                        $rutaRut = $bd_rut;
                    }
                }

                if (empty($bd_nitCedula)) {
                    $rutaNitCedula = $rutaNitCedula;
                }
                else if ($rutaNitCedula != $bd_nitCedula) {
                    if (!empty($rutaNitCedula)) {
                        $rutaNitCedula = $rutaNitCedula;
                        $bd_nitCedula = $image_delet['0']->rutaNitCedula;
                        unlink('..'.$bd_nitCedula);
                    }
                    else{
                        $rutaNitCedula = $bd_nitCedula;
                    }
                }

                if (empty($bd_precios)) {
                    $rutaPrecios = $rutaPrecios;
                }
                else if ($rutaPrecios != $bd_precios) {
                    if (!empty($rutaPrecios)) {
                        $rutaPrecios = $rutaPrecios;
                        $bd_precios = $image_delet['0']->rutaPrecios;
                        unlink('..'.$bd_precios);
                    }
                    else{
                        $rutaPrecios = $bd_precios;
                    }
                }

                $data = array(
                        'nombreProveedor'               => $info['nombreProveedor'],
                        'identificacionProveedor'       => $info['identificacionProveedor'],
                        'nitProveedor'                  => $info['nitProveedor'],
                        'direccionProveedor'            => $info['direccionProveedor'],
                        'ciudadProveedor'               => $info['ciudadProveedor'],
                        'paisProveedor'                 => $info['paisProveedor'],
                        'telefonoProveedor'             => $info['telefonoProveedor'],
                        'contactoProveedor'             => $info['contactoProveedor'],
                        'emailProveedor'                => $info['emailProveedor'],
                        'responsabilidadTributaria'     => $info['responsabilidadTributaria'],
                        'tipoEmpresa'                   => $info['tipoEmpresa'],
                        'nombreReferencia1'             => $info['nombreReferencia1'],
                        'ciudadReferencia1'             => $info['ciudadReferencia1'],
                        'telefonoReferencia1'           => $info['telefonoReferencia1'],
                        'nombreReferencia2'             => $info['nombreReferencia2'],
                        'ciudadReferencia2'             => $info['ciudadReferencia2'],
                        'telefonoReferencia2'           => $info['telefonoReferencia2'],
                        'formaPago'                     => $info['formaPago'],
                        'fechaDiligenciamiento'         => $info['fechaDiligenciamiento'],
                        'rutaCertificadoRepresentacion' => $rutaCertificadoRepresentacion,
                        'rutaRut'                       => $rutaRut,
                        'rutaNitCedula'                 => $rutaNitCedula,
                        'rutaPrecios'                   => $rutaPrecios
                );

                $this->db->where('idProveedor', $id);
                $this->db->update('proveedores', $data);            
                
                $retur_respues = 'Proveedor actualizado';
                return $retur_respues;

            }

    }

    function update_formato($datos){

        $mensaje='';

        $id = $datos['idFormato'];
        $tipoEntrante = $datos['tipoEvaluacion'];
        $nombreEntrante = $datos['nombre'];

        $this->db->select('tipoEvaluacion, nombreFormato');
        $this->db->from('formatos');
        $this->db->where('idFormato', $id);
        $consulta = $this->db->get();
        $resultadoConsulta = $consulta->result();

        $tipo_bd = $resultadoConsulta['0']->tipoEvaluacion;
        $nombre_bd = $resultadoConsulta['0']->nombreFormato;

        if (($nombre_bd == $nombreEntrante) && ($tipo_bd == $tipoEntrante)) {

            $actualizacionFormato = array(
                'nombreFormato'     => $datos['nombreFormato'],
                'tipoEvaluacion'    => $datos['tipoEvaluacion'],
                'areas'             => $datos['areas'],
                'preguntas'         => $datos['preguntas']                
            );

            $this->db->where('idFormato', $id);
            $this->db->update('formatos', $actualizacionFormato);
            $mensaje = 'exito';
            return $mensaje;
        }else{

            $this->db->select('tipoEvaluacion, nombreFormato');
            $this->db->from('formatos');
            $this->db->where('tipoEvaluacion', $tipoEntrante);
            $this->db->where('nombreFormato', $nombreEntrante);
            $query = $this->db->get();

            if($query->result_id->num_rows){
                $mensaje = 'existente';
                return $mensaje;
            }else{
                $actualizacionFormato = array(
                'nombreFormato'     => $datos['nombreFormato'],
                'tipoEvaluacion'    => $datos['tipoEvaluacion'],
                'areas'             => $datos['areas'],
                'preguntas'         => $datos['preguntas']                
                );

                $this->db->where('idFormato', $id);
                $this->db->update('formatos', $actualizacionFormato);
                $mensaje = 'exito';
                return $mensaje;   
            }
        }
    }

    function delete_digitalizacion($id)
    {
        $this->db->select('ruta_imagen');
        $this->db->from('documentos_digitalizados');
        $this->db->where('id_documento', $id);
        $query = $this->db->get();
        $image_delet = $query->result();

        $img_delete = $image_delet['0']->ruta_imagen;
        unlink('..'.$img_delete);
        $retur_respues = $this->db->where('id_documento', $id);
        $retur_respues = $this->db->delete('documentos_digitalizados');
        
        return $retur_respues;
    }

    function delete_proveedor($id)
    {
        $this->db->select('rutaCertificadoRepresentacion, rutaRut, rutaNitCedula, rutaPrecios');
        $this->db->from('proveedores');
        $this->db->where('idProveedor', $id);
        $query = $this->db->get();
        $image_delet = $query->result();

        if (!empty($image_delet['0']->rutaCertificadoRepresentacion)) {
            $img_delete = $image_delet['0']->rutaCertificadoRepresentacion;
            unlink('..'.$img_delete);    
        }
        if (!empty($image_delet['0']->rutaRut)) {
            $img_delete = $image_delet['0']->rutaRut;
            unlink('..'.$img_delete);
        }
        if (!empty($image_delet['0']->rutaNitCedula)) {
            $img_delete = $image_delet['0']->rutaNitCedula;
            unlink('..'.$img_delete);
        }
        if (!empty($image_delet['0']->rutaPrecios)) {
            $img_delete = $image_delet['0']->rutaPrecios;
            unlink('..'.$img_delete);
        }
        
        $retur_respues = $this->db->where('idProveedor', $id);
        $retur_respues = $this->db->delete('proveedores');
        
        return $retur_respues;
    }

    function delete_formato($id)
    {
        $this->db->where('idFormato', $id);
        $this->db->delete('formatos');
        $mensaje = 'eliminado';
        return $mensaje;
    }

    function get_document_state_pendiente_pdf()
    {
        $this->db->select('nro_factura, concepto, ruta_imagen, ubicacion, estado, etapa_1, etapa_2, etapa_3, etapa_4');
        $this->db->from('documentos_digitalizados'); 
        $this->db->where('estado','Pendiente');
        $query = $this->db->get();
        $result_pediente = $query->result();

        foreach ($result_pediente as $key => $value){

            $vec_rute = explode('/', $result_pediente[$key]->ruta_imagen);
            unset($result_pediente[$key]->ruta_imagen);
            $result_pediente[$key]->name_imagen = $vec_rute['3'];

        }
        return $result_pediente;
    }

    function get_document_state_aprobado_pdf()
    {
        $this->db->select('nro_factura, concepto, ruta_imagen, ubicacion, estado, etapa_1, etapa_2, etapa_3, etapa_4');
        $this->db->from('documentos_digitalizados'); 
        $this->db->where('estado','Aprobado');
        $query = $this->db->get();
        $result_aprobado = $query->result();

        foreach ($result_aprobado as $key => $value){

            $vec_rute = explode('/', $result_aprobado[$key]->ruta_imagen);
            unset($result_aprobado[$key]->ruta_imagen);
            $result_aprobado[$key]->name_imagen = $vec_rute['3'];

        }
        return $result_aprobado;
    }


    function get_document_state_espera_pdf()
    {
        $this->db->select('nro_factura, concepto, ruta_imagen, ubicacion, estado, etapa_1, etapa_2, etapa_3, etapa_4');
        $this->db->from('documentos_digitalizados'); 
        $this->db->where('estado','listo para pago');
        $query = $this->db->get();
        $result_espera = $query->result();

        foreach ($result_espera as $key => $value){

            $vec_rute = explode('/', $result_espera[$key]->ruta_imagen);
            unset($result_espera[$key]->ruta_imagen);
            $result_espera[$key]->name_imagen = $vec_rute['3'];

        }
        return $result_espera;
    }

    function get_document_state_pagado_pdf()
    {
        $this->db->select('nro_factura, concepto, ruta_imagen, ubicacion, estado, etapa_1, etapa_2, etapa_3, etapa_4');
        $this->db->from('documentos_digitalizados'); 
        $this->db->where('estado','Pagado');
        $query = $this->db->get();
        $result_pagado = $query->result();

        foreach ($result_pagado as $key => $value){
            $vec_rute = explode('/', $result_pagado[$key]->ruta_imagen);
            unset($result_pagado[$key]->ruta_imagen);
            $result_pagado[$key]->name_imagen = $vec_rute['3'];
        }
        return $result_pagado;
    }

    function get_evalua($get_date)
    {
        $area = str_replace("%20"," ",$get_date[1]); 

        $this->db->select('nombreFormato, tipoEvaluacion, areas, preguntas');
        $this->db->from('formatos'); 
        // $this->db->where('area', $area);
        $this->db->where('tipoEvaluacion', $get_date[0]);
        $query = $this->db->get();
        $evalu = $query->result(); 

        $evalu[0]->preguntas = json_decode($evalu[0]->preguntas);   

        return $evalu;

    }

    function insertarEvaluacion($datos){

        $mensaje='';

        $cn = count($_POST['pregunta']);
        $regla = 100 / $cn;
        $mitad = $regla / 2;

        $cns = 1;
        for($i=0; $i < count($datos['respuestas']); $i++){ 
            if($datos['respuestas'][$cns] == 'Malo'){
                $datos['respuestas'][$cns] = 0;
            }elseif($datos['respuestas'][$cns] == 'Normal'){
                $datos['respuestas'][$cns] = $mitad;
            }elseif($datos['respuestas'][$cns] == 'Bueno'){
                $datos['respuestas'][$cns] = $regla;
            }
            $cns++;
        }

        $total_calificacion = array_sum($datos['respuestas']);
        
        $evaluacion = array(
            'nombreProveedorEvaluado'   => $datos['nombreProveedorEvaluado'],
            'nombreEvaluacion'          => $datos['nombreEvaluacion'],
            'areaEvaluacion'            => $datos['areaEvaluacion'],
            'tipoEvaluacion'            => $datos['tipoEvaluacion'],
            'preguntas'                 => $datos['preguntas'],
            'respuestas'                => json_encode($datos['respuestas']),
            'total_calificacion'        => $total_calificacion          
            );

        $registroEvaluacion = $this->db->insert('evaluar_proveedor', $evaluacion);

        $mensaje='evaluación registrada';
        return $mensaje;

    }

    public function get_areas()
    {
        $this->db->select('nombre_ubicacion');
        $this->db->from('ubicaciones');
        $query = $this->db->get();
        $areas = $query->result();
        return $areas;
    }

}
