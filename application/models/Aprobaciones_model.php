<?php
class Aprobaciones_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function update_estado($update_estad)
    {
        if(!empty($update_estad['etapa_1'])){

            $data = array(
                            'cedula_user_digitaliza' => $update_estad['cedula_user_digitaliza'],
                            'dependencia'            => $update_estad['dependencia'],
                            'fecha_ingreso'          => $update_estad['fecha_ingreso'],
                            'etapa_1'                => $update_estad['etapa_1'] 
                          );

            $result = $this->db->where('id_documento', $update_estad['id_documento']);
            $result = $this->db->update('documentos_digitalizados', $data); 
         
            $this->db->select('nro_factura, concepto, ruta_imagen, ubicacion, estado, etapa_1, etapa_2, etapa_3, etapa_4');
            $this->db->from('documentos_digitalizados');
            $this->db->where('id_documento', $update_estad['id_documento']);
            $query = $this->db->get();
            $result_consul = array();
            $result_consul = $query->result();

            if($result_consul['0']->etapa_1 == '1' && $result_consul['0']->etapa_2 == '1' && $result_consul['0']->etapa_3 == '1' && $result_consul['0']->etapa_4 == '1'){
                $datas = array(
                                'estado' => 'Aprobado',
                              );
                $result = $this->db->where('id_documento', $update_estad['id_documento']);
                $result = $this->db->update('documentos_digitalizados', $datas); 
            }


            $this->db->select('nro_factura, concepto, ruta_imagen, ubicacion, estado, etapa_1, etapa_2, etapa_3, etapa_4');
            $this->db->from('documentos_digitalizados');
            $this->db->where('id_documento', $update_estad['id_documento']);
            $que = $this->db->get();
            $e = array();
            $e = $query->result();


            $historico_data = array(
                                    'cedula_user_cambios' => $update_estad['cedula_user_digitaliza'],
                                    'dependencia'         => $update_estad['dependencia'],
                                    'fecha_movimiento'    => $update_estad['fecha_ingreso'],
                                    'nro_factura'         => $e['0']->nro_factura,
                                    'concepto'            => $e['0']->concepto,
                                    'ruta_imagen'         => $e['0']->ruta_imagen,
                                    'ubicacion'           => $e['0']->ubicacion,
                                    'estado'              => $e['0']->estado,
                                    'etapa_1'             => $e['0']->etapa_1,
                                    'etapa_2'             => $e['0']->etapa_2,
                                    'etapa_3'             => $e['0']->etapa_3,
                                    'etapa_4'             => $e['0']->etapa_4,
                                    'id_documento'        => $update_estad['id_documento'],
                                    'movimiento'          => 'Validacion 1 etapa'
                                    );

            $this->db->insert('historico_digitalizados', $historico_data);

            return $result;

        }elseif(!empty($update_estad['etapa_4'])){

            if($update_estad['etapa_1_neg']){
                $movimiento = 'desaprobado 1 - 4 etapa';
                $data = array(
                                'cedula_user_digitaliza'    => $update_estad['cedula_user_digitaliza'],
                                'dependencia'               => $update_estad['dependencia'],
                                'fecha_ingreso'             => $update_estad['fecha_ingreso'],
                                'etapa_4'                   => $update_estad['etapa_4'],
                                'etapa_1'                   => $update_estad['etapa_4'],
                                'observaciones_negado_1_4'  => $update_estad['observaciones_negado_1_4']
                              );
            }else{
                $movimiento = 'Validacion 4 etapa';
                $data = array(
                                'cedula_user_digitaliza'    => $update_estad['cedula_user_digitaliza'],
                                'dependencia'               => $update_estad['dependencia'],
                                'fecha_ingreso'             => $update_estad['fecha_ingreso'],
                                'etapa_4'                   => $update_estad['etapa_4'],
                              );
            }


            $result = $this->db->where('id_documento', $update_estad['id_documento']);
            $result = $this->db->update('documentos_digitalizados', $data); 
          

            $this->db->select('nro_factura, concepto, ruta_imagen, ubicacion, estado, etapa_1, etapa_2, etapa_3, etapa_4');
            $this->db->from('documentos_digitalizados');
            $this->db->where('id_documento', $update_estad['id_documento']);
            $query = $this->db->get();
            $result_consul = array();
            $result_consul = $query->result();

            if($result_consul['0']->etapa_1 == '1' && $result_consul['0']->etapa_2 == '1' && $result_consul['0']->etapa_3 == '1' && $result_consul['0']->etapa_4 == '1'){
                $datas = array(
                                'estado' => 'Aprobado',
                              );
                $result = $this->db->where('id_documento', $update_estad['id_documento']);
                $result = $this->db->update('documentos_digitalizados', $datas); 
            }

            $this->db->select('nro_factura, concepto, ruta_imagen, ubicacion, estado, etapa_1, etapa_2, etapa_3, etapa_4');
            $this->db->from('documentos_digitalizados');
            $this->db->where('id_documento', $update_estad['id_documento']);
            $que = $this->db->get();
            $e = array();
            $e = $query->result();


            if(!empty($update_estad['observaciones_negado_1_4'])){
                $historico_data = array(
                                        'cedula_user_cambios' => $update_estad['cedula_user_digitaliza'],
                                        'dependencia'         => $update_estad['dependencia'],
                                        'fecha_movimiento'    => $update_estad['fecha_ingreso'],
                                        'nro_factura'         => $e['0']->nro_factura,
                                        'concepto'            => $e['0']->concepto,
                                        'ruta_imagen'         => $e['0']->ruta_imagen,
                                        'ubicacion'           => $e['0']->ubicacion,
                                        'estado'              => $e['0']->estado,
                                        'etapa_1'             => $e['0']->etapa_1,
                                        'etapa_2'             => $e['0']->etapa_2,
                                        'etapa_3'             => $e['0']->etapa_3,
                                        'etapa_4'             => $e['0']->etapa_4,
                                        'id_documento'        => $update_estad['id_documento'],
                                        'movimiento'          => $movimiento,
                                        'observaciones_negado_1_4'  => $update_estad['observaciones_negado_1_4']
                                        );
            }else{

            $historico_data = array(
                                    'cedula_user_cambios' => $update_estad['cedula_user_digitaliza'],
                                    'dependencia'         => $update_estad['dependencia'],
                                    'fecha_movimiento'    => $update_estad['fecha_ingreso'],
                                    'nro_factura'         => $e['0']->nro_factura,
                                    'concepto'            => $e['0']->concepto,
                                    'ruta_imagen'         => $e['0']->ruta_imagen,
                                    'ubicacion'           => $e['0']->ubicacion,
                                    'estado'              => $e['0']->estado,
                                    'etapa_1'             => $e['0']->etapa_1,
                                    'etapa_2'             => $e['0']->etapa_2,
                                    'etapa_3'             => $e['0']->etapa_3,
                                    'etapa_4'             => $e['0']->etapa_4,
                                    'id_documento'        => $update_estad['id_documento'],
                                    'movimiento'          => $movimiento,
                                    'observaciones_negado_1_4'  => ''
                                    );
            }



            $this->db->insert('historico_digitalizados', $historico_data);

            return $result;

        }elseif(!empty($update_estad['etapa_2'])){


            if($update_estad['etapa_2_neg']){

                $movimiento = 'desaprobado 2 etapa';
                $data = array(
                                'cedula_user_digitaliza'    => $update_estad['cedula_user_digitaliza'],
                                'dependencia'               => $update_estad['dependencia'],
                                'fecha_ingreso'             => $update_estad['fecha_ingreso'],
                                'etapa_2'                   => $update_estad['etapa_2'],
                                'observaciones_negado_2'    => $update_estad['observaciones_negado_2']
                              );

            }else{
                $movimiento = 'Validacion 2 etapa';
                $data = array(
                                'cedula_user_digitaliza' => $update_estad['cedula_user_digitaliza'],
                                'dependencia'            => $update_estad['dependencia'],
                                'fecha_ingreso'          => $update_estad['fecha_ingreso'],
                                'etapa_2'                => $update_estad['etapa_2'] 
                              );
            }


            $result = $this->db->where('id_documento', $update_estad['id_documento']);
            $result = $this->db->update('documentos_digitalizados', $data); 
          

            $this->db->select('nro_factura, concepto, ruta_imagen, ubicacion, estado, etapa_1, etapa_2, etapa_3, etapa_4');
            $this->db->from('documentos_digitalizados');
            $this->db->where('id_documento', $update_estad['id_documento']);
            $query = $this->db->get();
            $result_consul = array();
            $result_consul = $query->result();

            if($result_consul['0']->etapa_1 == '1' && $result_consul['0']->etapa_2 == '1' && $result_consul['0']->etapa_3 == '1' && $result_consul['0']->etapa_4 == '1'){
                $datas = array(
                                'estado' => 'Aprobado',
                              );
                $result = $this->db->where('id_documento', $update_estad['id_documento']);
                $result = $this->db->update('documentos_digitalizados', $datas); 
            }


            $this->db->select('nro_factura, concepto, ruta_imagen, ubicacion, estado, etapa_1, etapa_2, etapa_3, etapa_4');
            $this->db->from('documentos_digitalizados');
            $this->db->where('id_documento', $update_estad['id_documento']);
            $que = $this->db->get();
            $e = array();
            $e = $query->result();

            if(!empty($update_estad['observaciones_negado_2'])){

                $historico_data = array(
                                        'cedula_user_cambios' => $update_estad['cedula_user_digitaliza'],
                                        'dependencia'         => $update_estad['dependencia'],
                                        'fecha_movimiento'    => $update_estad['fecha_ingreso'],
                                        'nro_factura'         => $e['0']->nro_factura,
                                        'concepto'            => $e['0']->concepto,
                                        'ruta_imagen'         => $e['0']->ruta_imagen,
                                        'ubicacion'           => $e['0']->ubicacion,
                                        'estado'              => $e['0']->estado,
                                        'etapa_1'             => $e['0']->etapa_1,
                                        'etapa_2'             => $e['0']->etapa_2,
                                        'etapa_3'             => $e['0']->etapa_3,
                                        'etapa_4'             => $e['0']->etapa_4,
                                        'id_documento'        => $update_estad['id_documento'],
                                        'movimiento'          => $movimiento,
                                        'observaciones_negado_2'  => $update_estad['observaciones_negado_2']
                                        );

            }else{
                $historico_data = array(
                                        'cedula_user_cambios' => $update_estad['cedula_user_digitaliza'],
                                        'dependencia'         => $update_estad['dependencia'],
                                        'fecha_movimiento'    => $update_estad['fecha_ingreso'],
                                        'nro_factura'         => $e['0']->nro_factura,
                                        'concepto'            => $e['0']->concepto,
                                        'ruta_imagen'         => $e['0']->ruta_imagen,
                                        'ubicacion'           => $e['0']->ubicacion,
                                        'estado'              => $e['0']->estado,
                                        'etapa_1'             => $e['0']->etapa_1,
                                        'etapa_2'             => $e['0']->etapa_2,
                                        'etapa_3'             => $e['0']->etapa_3,
                                        'etapa_4'             => $e['0']->etapa_4,
                                        'id_documento'        => $update_estad['id_documento'],
                                        'movimiento'          => 'Validacion 2 etapa'
                                        );
            }

            


            $this->db->insert('historico_digitalizados', $historico_data);

            return $result;

        }elseif(!empty($update_estad['etapa_3'])){

            if($update_estad['etapa_3_neg']){

                $movimiento = 'desaprobado 3 etapa';
                $data = array(
                                'cedula_user_digitaliza'    => $update_estad['cedula_user_digitaliza'],
                                'dependencia'               => $update_estad['dependencia'],
                                'fecha_ingreso'             => $update_estad['fecha_ingreso'],
                                'etapa_3'                   => $update_estad['etapa_3'],
                                'observaciones_negado_3'    => $update_estad['observaciones_negado_3']
                              );

            }else{
                $movimiento = 'Validacion 3 etapa';
                $data = array(
                                'cedula_user_digitaliza' => $update_estad['cedula_user_digitaliza'],
                                'dependencia'            => $update_estad['dependencia'],
                                'fecha_ingreso'          => $update_estad['fecha_ingreso'],
                                'etapa_3'                => $update_estad['etapa_3'] 
                              );
            }


            $result = $this->db->where('id_documento', $update_estad['id_documento']);
            $result = $this->db->update('documentos_digitalizados', $data); 
          

            $this->db->select('nro_factura, concepto, ruta_imagen, ubicacion, estado, etapa_1, etapa_2, etapa_3, etapa_4');
            $this->db->from('documentos_digitalizados');
            $this->db->where('id_documento', $update_estad['id_documento']);
            $query = $this->db->get();
            $result_consul = array();
            $result_consul = $query->result();

            if($result_consul['0']->etapa_1 == '1' && $result_consul['0']->etapa_2 == '1' && $result_consul['0']->etapa_3 == '1' && $result_consul['0']->etapa_4 == '1'){
                $datas = array(
                                'estado' => 'Aprobado',
                              );
                $result = $this->db->where('id_documento', $update_estad['id_documento']);
                $result = $this->db->update('documentos_digitalizados', $datas); 
            }

            $this->db->select('nro_factura, concepto, ruta_imagen, ubicacion, estado, etapa_1, etapa_2, etapa_3, etapa_4');
            $this->db->from('documentos_digitalizados');
            $this->db->where('id_documento', $update_estad['id_documento']);
            $que = $this->db->get();
            $e = array();
            $e = $query->result();


            if(!empty($update_estad['observaciones_negado_3'])){

                $historico_data = array(
                                        'cedula_user_cambios' => $update_estad['cedula_user_digitaliza'],
                                        'dependencia'         => $update_estad['dependencia'],
                                        'fecha_movimiento'    => $update_estad['fecha_ingreso'],
                                        'nro_factura'         => $e['0']->nro_factura,
                                        'concepto'            => $e['0']->concepto,
                                        'ruta_imagen'         => $e['0']->ruta_imagen,
                                        'ubicacion'           => $e['0']->ubicacion,
                                        'estado'              => $e['0']->estado,
                                        'etapa_1'             => $e['0']->etapa_1,
                                        'etapa_2'             => $e['0']->etapa_2,
                                        'etapa_3'             => $e['0']->etapa_3,
                                        'etapa_4'             => $e['0']->etapa_4,
                                        'id_documento'        => $update_estad['id_documento'],
                                        'movimiento'          => $movimiento,
                                        'observaciones_negado_3'  => $update_estad['observaciones_negado_3']
                                        );

            }else{

                $historico_data = array(
                                        'cedula_user_cambios' => $update_estad['cedula_user_digitaliza'],
                                        'dependencia'         => $update_estad['dependencia'],
                                        'fecha_movimiento'    => $update_estad['fecha_ingreso'],
                                        'nro_factura'         => $e['0']->nro_factura,
                                        'concepto'            => $e['0']->concepto,
                                        'ruta_imagen'         => $e['0']->ruta_imagen,
                                        'ubicacion'           => $e['0']->ubicacion,
                                        'estado'              => $e['0']->estado,
                                        'etapa_1'             => $e['0']->etapa_1,
                                        'etapa_2'             => $e['0']->etapa_2,
                                        'etapa_3'             => $e['0']->etapa_3,
                                        'etapa_4'             => $e['0']->etapa_4,
                                        'id_documento'        => $update_estad['id_documento'],
                                        'movimiento'          => $movimiento
                                        );
            }

            $this->db->insert('historico_digitalizados', $historico_data);

            return $result;

        }


        elseif(!empty($update_estad['estado'])){

       

            $date_i = array(
                             'cedula_user_digitaliza' => $update_estad['cedula_user_digitaliza'],
                             'dependencia' => $update_estad['dependencia'],
                             'fecha_ingreso' => $update_estad['fecha_ingreso'],
                             'estado' => $update_estad['estado'], 
                           );

            $result = $this->db->where('id_documento', $update_estad['id_documento']);
            $result = $this->db->update('documentos_digitalizados', $date_i);

        }
        
    }

}