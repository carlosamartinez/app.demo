<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Digital extends CI_Controller {

	public function __construct()
   	{
        parent::__construct();
        // Your own constructor code
        $this->load->helper('form');
        $this->load->database();
		$this->load->model('digital_model');
		$this->load->model('concepto_model');
		$this->load->model('ubicacion_model');
		$this->load->model('usuarios_model');

		$this->load->helper('url');
		// Notificar solamente errores de ejecución
		error_reporting(E_ERROR | E_WARNING | E_PARSE);
   	}
	
	public function index()
	{
		if(empty($_POST['cedula']))
		{
			$rute = $_SERVER['HTTP_HOST'];
			redirect('http://'.$rute.'/prueba/');
		}

		if(!empty($_POST['Digitalización'])){

			$cedula = $_POST['cedula'];
			$resultado = $this->digital_model->get_date_user($cedula);
			$info_table = $this->digital_model->get_table_digitalizados();
			// die('ok');
			$info_table_p = $this->digital_model->get_table_proveedores();
			$info_table_c = $this->digital_model->get_table_calificaciones();

			$view_all['table'] = $info_table;
			$view_all['table_p'] = $info_table_p;
			$view_all['table_c'] = $info_table_c;
			$view_all['cedula'] 		= $resultado['0']->cedula;
			$view_all['dependencia']  = $resultado['0']->dependencia;
			$view_all['nombres']  	= $resultado['0']->nombres;
			$view_all['rol']  	= $resultado['0']->rol;
			$this->load->view('digital_view', $view_all);

		}elseif(!empty($_POST['Creación_formatos'])){

			$cedula = $_POST['cedula'];
			$resultado = $this->digital_model->get_date_user($cedula);
			$datos_formatos = $this->digital_model->get_table_formatos();

			$view_all['table_formatos'] = $datos_formatos;
			$view_all['cedula'] 		= $resultado['0']->cedula;
			$view_all['dependencia']  = $resultado['0']->dependencia;
			$view_all['nombres']  	= $resultado['0']->nombres;
			$view_all['rol']  	= $resultado['0']->rol;
			$this->load->view('prueba/formato', $view_all);

		}else{
			$cedula = $_POST['cedula'];
			$resultado = $this->digital_model->get_date_user($cedula);
			$info_table = $this->digital_model->get_table_digitalizados();
			$info_table_p = $this->digital_model->get_table_proveedores();
			$info_table_c = $this->digital_model->get_table_calificaciones();

			$view_all['table'] = $info_table;
			$view_all['table_p'] = $info_table_p;
			$view_all['table_c'] = $info_table_c;
			$view_all['cedula'] 		= $resultado['0']->cedula;
			$view_all['dependencia']  = $resultado['0']->dependencia;
			$view_all['nombres']  	= $resultado['0']->nombres;
			$view_all['rol']  	= $resultado['0']->rol;
			$this->load->view('digital_view', $view_all);
		}

	}

	public function creacion_formato(){

		if(empty($_POST['cedula']))
		{
			$rute = $_SERVER['HTTP_HOST'];
			redirect('http://'.$rute.'/prueba/');

			// redirect('http://localhost:8080/prueba/');
		}

		$cedula = $_POST['cedula'];
		$resultado = $this->digital_model->get_date_user($cedula);
		$areas = $this->digital_model->get_areas();

		$view_all['cedula'] 		= $resultado['0']->cedula;
		$view_all['dependencia']  = $resultado['0']->dependencia;
		$view_all['nombres']  	= $resultado['0']->nombres;
		$view_all['rol']  	= $resultado['0']->rol;
		$view_all['areas']  	= $areas;



		$this->load->view('prueba/creacion_formato', $view_all);
	}

	public function load_conceptos()
	{
		$resul = $this->concepto_model->get_conceptos();
		$concepto = array();
		for($i=0; $i < count($resul); $i++){ 
			$concepto[$i] = $resul[$i]->nombre_concepto;
		}
		echo json_encode($concepto);
		die();
	}

	public function load_ubicaciones()
	{
		$resul = $this->ubicacion_model->get_ubicaciones();
		$ubicaciones = array();
		for($i=0; $i < count($resul); $i++) { 
			$ubicaciones[$i] = $resul[$i]->nombre_ubicacion;
		}
		echo json_encode($ubicaciones);
		die();
	}

	public function load_usuario_aprueba()
	{
		$resul = $this->usuarios_model->get_usuarios();
		$usuarios = array();
		for($i=0; $i < count($resul); $i++) { 
			$usuarios[$i]['nombres'] = $resul[$i]->nombres;
			$usuarios[$i]['id_usuario'] = $resul[$i]->id_usuario;
		}
		echo json_encode($usuarios);
		die();	
	}

	public function creacion()
	{
		if(empty($_POST['cedula']))
		{
			$rute = $_SERVER['HTTP_HOST'];
			redirect('http://'.$rute.'/prueba/');
			// redirect('http://localhost:8080/prueba/');
		}

		$cedula_user   = $this->input->post('cedula');
		$datos_usuario = $this->digital_model->get_date_user($cedula_user);

		$adjunto_info = array(
							'cedula'      => $datos_usuario['0']->cedula,
							'dependencia' => $datos_usuario['0']->dependencia,
							'nombres' => $datos_usuario['0']->nombres,
							'rol' => $datos_usuario['0']->rol 
						  );
		$this->load->view('creacion_digitalizacion', $adjunto_info);
	}


	public function creacion_proveedor()
	{
		if(empty($_POST['cedula']))
		{
			$rute = $_SERVER['HTTP_HOST'];
			redirect('http://'.$rute.'/prueba/');
			// redirect('http://localhost:8080/prueba/');
		}

		$cedula = $_POST['cedula'];
		$resultado = $this->digital_model->get_date_user($cedula);

		$view_header['cedula'] 			= $resultado['0']->cedula;
		$view_header['dependencia']     = $resultado['0']->dependencia;
		$view_header['nombres']  		= $resultado['0']->nombres;
		$view_header['rol']  			= $resultado['0']->rol;

		$vie_body['cedula'] = $resultado['0']->cedula;

		$this->load->view('prueba/maqueta_header', $view_header);
		$this->load->view('prueba/creacion_proveedor', $vie_body);
		$this->load->view('prueba/maqueta_footer');
	}

	public function datos_proveedor(){
		if(empty($_POST['cedula']))
		{
			$rute = $_SERVER['HTTP_HOST'];
			redirect('http://'.$rute.'/prueba/');
			// redirect('http://localhost:8080/prueba/');
		}

		$cedula = $_POST['cedula'];
		$resultado = $this->digital_model->get_date_user($cedula);

		$proveedor = array();
		$proveedor['nombreProveedor'] = $this->input->post('nombre');
		$proveedor['identificacionProveedor'] = $this->input->post('identificacion');
		$proveedor['nitProveedor'] = $this->input->post('nit');
		$proveedor['direccionProveedor'] = $this->input->post('direccion');
		$proveedor['ciudadProveedor'] = $this->input->post('ciudad');
		$proveedor['paisProveedor'] = $this->input->post('pais');
		$proveedor['telefonoProveedor'] = $this->input->post('telefono');
		$proveedor['contactoProveedor'] = $this->input->post('contacto');
		$proveedor['emailProveedor'] = $this->input->post('email');
		$proveedor['responsabilidadTributaria'] = $this->input->post('resp_tributaria');
		$proveedor['tipoEmpresa'] = $this->input->post('tipo_empresa');
		$proveedor['areaProveedor'] = $this->input->post('ubicacion');
		$proveedor['nombreReferencia1'] = $this->input->post('empresa1');
		$proveedor['ciudadReferencia1']	= $this->input->post('ciudad1');
		$proveedor['telefonoReferencia1'] = $this->input->post('telefono1');
		$proveedor['nombreReferencia2'] = $this->input->post('empresa2');
		$proveedor['ciudadReferencia2']	= $this->input->post('ciudad2');
		$proveedor['telefonoReferencia2'] = $this->input->post('telefono2');
		$proveedor['fechaDiligenciamiento'] = $this->input->post('fecha_diligenciamiento');
		$archivo_certificado = $_FILES['file_certificado']['name'];
		$archivo_rut = $_FILES['file_rut']['name'];
		$archivo_nit_cedula = $_FILES['file_nit_cedula']['name'];
		$archivo_precios = $_FILES['file_precios']['name'];

		if ($_POST['forma_pago']=='otro') {
			$proveedor['formaPago'] = $this->input->post('otro_tipo');	
		}else{
			$proveedor['formaPago'] = $this->input->post('forma_pago');	
		}

		$datos_proveedor = $this->digital_model->insert_proveedor($proveedor);
		
		if ($datos_proveedor == 'exito') {
			$vie_body['message'] = 'exito';
		}
		// elseif ($datos_proveedor == 'identificacionExistente') {
		// 	$vie_body['message'] = 'identificacionExistente';
		// }
		elseif ($datos_proveedor == 'nitIdentificacionExistente') {
			$vie_body['message'] = 'nitIdentificacionExistente';			
		}

		$view_header['cedula'] 			= $resultado['0']->cedula;
		$view_header['dependencia']     = $resultado['0']->dependencia;
		$view_header['nombres']  		= $resultado['0']->nombres;
		$view_header['rol']  			= $resultado['0']->rol;
		$vie_body['cedula'] = $resultado['0']->cedula;
		
		
		$this->load->view('prueba/maqueta_header', $view_header);
		$this->load->view('prueba/creacion_proveedor', $vie_body);
		$this->load->view('prueba/maqueta_footer');

		
	}


	public function insert_dates()
	{
		if(empty($_POST['cedula_user_digitaliza']))
		{
			$rute = $_SERVER['HTTP_HOST'];
			redirect('http://'.$rute.'/prueba/');
			// redirect('http://localhost:8080/prueba/');
		}
		
		

		$datos_insert = array(
								'cedula_user_digitaliza' => $this->input->post('cedula_user_digitaliza'),
								'dependencia' 			 => $this->input->post('dependencia'),
								'fecha_ingreso' 		 => $this->input->post('fecha_ingreso'),
								'nro_factura' 			 => $this->input->post('nro_factura'),
								'concepto' 				 => $this->input->post('concepto'),
								'ubicacion' 			 => $this->input->post('ubicacion'),
								'usuario_aprueba' 		 => $this->input->post('usuario_aprueba')
						     );
		$name_archivo = $_FILES['uploadedfile']['name'];
		if(!empty($name_archivo) && !empty($datos_insert['nro_factura']) && !empty($datos_insert['concepto']) && !empty($datos_insert['ubicacion']) && !empty($datos_insert['usuario_aprueba']))
		{
			
			$datos_insert['estado'] = 'Pendiente';

			$return    = $this->digital_model->insert_values($datos_insert);
			$user_info = $this->digital_model->get_date_user($datos_insert['cedula_user_digitaliza']);
			$info_table = $this->digital_model->get_table_digitalizados();
			$info_table_p = $this->digital_model->get_table_proveedores();
			$info_table_c = $this->digital_model->get_table_calificaciones();			

			$return_respuest_bd  = array(
											'message_creacion' => $return,
											'cedula'      	   => $user_info['0']->cedula,
											'dependencia'      => $user_info['0']->dependencia,
											'nombres'      	   => $user_info['0']->nombres,
											'rol'      	   	   => $user_info['0']->rol,
											'table'            => $info_table,
											'table_p'		   => $info_table_p,
											'table_c'		   => $info_table_c
										);

			$this->load->view('digital_view', $return_respuest_bd);
			
		}else{
			$user_info = $this->digital_model->get_date_user($datos_insert['cedula_user_digitaliza']);
			$message = array(
							 'message'      => 1,
							 'cedula' 		=> $user_info['0']->cedula,
							 'dependencia'  => $user_info['0']->dependencia,
							 'nombres'  	=> $user_info['0']->nombres,
							 'rol'		  	=> $user_info['0']->rol
							);

			$this->load->view('creacion_digitalizacion', $message);
		}

	}

	public function insert_formato(){

		if(empty($_POST['cedula']))
		{
			$rute = $_SERVER['HTTP_HOST'];
			redirect('http://'.$rute.'/prueba/');
			// redirect('http://localhost:8080/prueba/');
		}

		$formato = array();
		$formato['nombreFormato'] = $this->input->post('nombre');
		$formato['tipoEvaluacion'] = $this->input->post('tipo_evaluacion');
		$formato['areas'] = json_encode($_POST['ubicacion']);
		$formato['preguntas'] = json_encode($_POST['pregunta']);


		$datosFormato = $this->digital_model->insert_formato($formato);

		if ($datosFormato == 'exito') {
			$view_all['message'] = 'exito';
		}elseif ($datosFormato == 'existente') {
			$view_all['message'] = 'existente';			
		}

		$cedula = $_POST['cedula'];
		$resultado = $this->digital_model->get_date_user($cedula);
		$areas = $this->digital_model->get_areas();

		$view_all['mensaje'] = $datosFormato;
		$view_all['cedula'] 		= $resultado['0']->cedula;
		$view_all['dependencia']  = $resultado['0']->dependencia;
		$view_all['nombres']  	= $resultado['0']->nombres;
		$view_all['rol']  	= $resultado['0']->rol;
		$view_all['areas']  	= $areas;

		$this->load->view('prueba/creacion_formato', $view_all);
		

	}

	public function edit_digitalizacion()
	{
		if(empty($_POST['cedula']))
		{
			$rute = $_SERVER['HTTP_HOST'];
			redirect('http://'.$rute.'/prueba/');
			// redirect('http://localhost:8080/prueba/');
		}

		$datos_seleccionados = $this->digital_model->get_info_seleccionada($_POST['id']);
		$resultado_user = $this->digital_model->get_date_user($_POST['cedula']);
		$adjunto_info = array();
		$adjunto_info['datos_editar'] = $datos_seleccionados;
		$adjunto_info['cedula'] = $resultado_user['0']->cedula;
		$adjunto_info['dependencia'] = $resultado_user['0']->dependencia;
		$adjunto_info['nombres'] = $resultado_user['0']->nombres;
		$adjunto_info['rol'] = $resultado_user['0']->rol;

		$this->load->view('edit_digitalizacion', $adjunto_info);
	}

	public function edit_proveedor(){
		if(empty($_POST['cedula']))
		{
			$rute = $_SERVER['HTTP_HOST'];
			redirect('http://'.$rute.'/prueba/');
			// redirect('http://localhost:8080/prueba/');
		}

		$datos_seleccionados = $this->digital_model->get_info_proveedor($_POST['id']);

		$resultado_user = $this->digital_model->get_date_user($_POST['cedula']);
		$adjunto_info = array();
		$adjunto_info['datos_editar'] = $datos_seleccionados;
		$adjunto_info['otro_tipo']=$banderaTipoPago;
		$adjunto_info['cedula'] = $resultado_user['0']->cedula;
		$adjunto_info['dependencia'] = $resultado_user['0']->dependencia;
		$adjunto_info['nombres'] = $resultado_user['0']->nombres;
		$adjunto_info['rol'] = $resultado_user['0']->rol;

		$this->load->view('prueba/editar_proveedor', $adjunto_info);
	}

	public function editar_formato(){
		if(empty($_POST['cedula']))
		{
			$rute = $_SERVER['HTTP_HOST'];
			redirect('http://'.$rute.'/prueba/');
		}

		$cedula = $_POST['cedula'];
		$resultado = $this->digital_model->get_date_user($cedula);
		$datos_formatos = $this->digital_model->get_info_formatos($_POST['id']);
		$areas = $this->digital_model->get_areas();

		$view_all['datos_formato'] = $datos_formatos;
		$view_all['cedula'] 		= $resultado['0']->cedula;
		$view_all['dependencia']  = $resultado['0']->dependencia;
		$view_all['nombres']  	= $resultado['0']->nombres;
		$view_all['rol']  	= $resultado['0']->rol;
		$view_all['areas']  	= $areas;
		$this->load->view('prueba/editar_formato', $view_all);
	}

	public function delete_proveedor(){

		if(empty($_POST['cedula']))
		{
			$rute = $_SERVER['HTTP_HOST'];
			redirect('http://'.$rute.'/prueba/');
			// redirect('http://localhost:8080/prueba/');
		}

		$return_delete = $this->digital_model->delete_proveedor($this->input->post('id'));
		$resultado_user = $this->digital_model->get_date_user($this->input->post('cedula'));
		$info_table = $this->digital_model->get_table_digitalizados();
		$info_table_p = $this->digital_model->get_table_proveedores();
		$info_table_c = $this->digital_model->get_table_calificaciones();
		

		$view_all['table'] = $info_table;
		$view_all['table_p'] = $info_table_p;
		$view_all['table_c'] = $info_table_c;
		$view_all['cedula'] 			= $resultado_user['0']->cedula;
		$view_all['dependencia']  		= $resultado_user['0']->dependencia;
		$view_all['nombres']  			= $resultado_user['0']->nombres;
		$view_all['rol']  				= $resultado_user['0']->rol;
		$view_all['message_delete']		= $return_delete;

		$this->load->view('digital_view', $view_all);
	}

	public function delete_formato(){

		if(empty($_POST['cedula']))
		{
			$rute = $_SERVER['HTTP_HOST'];
			redirect('http://'.$rute.'/prueba/');
			// redirect('http://localhost:8080/prueba/');
		}

		$return_delete = $this->digital_model->delete_formato($this->input->post('id'));
		$resultado_user = $this->digital_model->get_date_user($this->input->post('cedula'));

		$cedula = $_POST['cedula'];
		$resultado = $this->digital_model->get_date_user($cedula);
		$datos_formatos = $this->digital_model->get_table_formatos();

		$view_all['message'] = $return_delete;
		$view_all['table_formatos'] = $datos_formatos;
		$view_all['cedula'] 		= $resultado['0']->cedula;
		$view_all['dependencia']  = $resultado['0']->dependencia;
		$view_all['nombres']  	= $resultado['0']->nombres;
		$view_all['rol']  	= $resultado['0']->rol;
		$this->load->view('prueba/formato', $view_all);
		

	}

	public function delete_digitalizacion()
	{
		if(empty($_POST['cedula']))
		{
			$rute = $_SERVER['HTTP_HOST'];
			redirect('http://'.$rute.'/prueba/');
			// redirect('http://localhost:8080/prueba/');
		}

		$return_delete = $this->digital_model->delete_digitalizacion($this->input->post('id'));
		$resultado_user = $this->digital_model->get_date_user($this->input->post('cedula'));
		$info_table = $this->digital_model->get_table_digitalizados();
		$info_table_p = $this->digital_model->get_table_proveedores();
		$info_table_c = $this->digital_model->get_table_calificaciones();
		

		$view_all['table'] 				= $info_table;
		$view_all['table_p'] 			= $info_table_p;
		$view_all['table_c'] 			= $info_table_c;
		$view_all['cedula'] 			= $resultado_user['0']->cedula;
		$view_all['dependencia']  		= $resultado_user['0']->dependencia;
		$view_all['nombres']  			= $resultado_user['0']->nombres;
		$view_all['rol']  				= $resultado_user['0']->rol;
		$view_all['message_delete']		= $return_delete;

		$this->load->view('digital_view', $view_all);
	}

	public function change_info_documents()
	{

		$img_eliminar = $this->input->post('img_eliminar');
		$nro_factura  = $this->input->post('nro_factura');
		$file_upload  = $_FILES['uploadedfile']['name'];
		
		if(!empty($nro_factura)){
			$ban = 0;
			$destino = '';
			if(!empty($file_upload)){
				$ban = 1;

				// delete image vieja
				// unlink('..'.$img_eliminar);
				
				// upload imagen en folder
				$archivo = $_FILES["uploadedfile"]['name'];
				$prefijo = substr(md5(uniqid(rand())),0,6);
				$destino = "../prueba/img_digitalizados/".$prefijo."_".$archivo;
				if (copy($_FILES['uploadedfile']['tmp_name'],$destino)) {
					$status = "Archivo subido: <b>".$archivo."</b>";
				}else{
					$status = "Error al subir el archivo";
				}         
			}
			$change_info_document = array();
			$change_info_document['cedula_user_digitaliza']  = $this->input->post('cedula_user_digitaliza');
			$change_info_document['dependencia']			 = $this->input->post('dependencia');
			$change_info_document['fecha_ingreso']			 = $this->input->post('fecha_ingreso');
			$change_info_document['nro_factura']			 = $this->input->post('nro_factura');
			$change_info_document['concepto']			 	 = $this->input->post('concepto');
			$change_info_document['id_documento']			 = $this->input->post('id_documento');
			$change_info_document['ubicacion']			 	 = $this->input->post('ubicacion');
			$change_info_document['usuario_aprueba']		 = $this->input->post('usuario_aprueba');
			$change_info_document['ruta_imagen']			 = $destino;
			$change_info_document['bandera']				 = $ban;
			

			$return = $this->digital_model->update_info($change_info_document);
			$info_table = $this->digital_model->get_table_digitalizados();
			$info_table_p = $this->digital_model->get_table_proveedores();
			$info_table_c = $this->digital_model->get_table_calificaciones();

			
			// $return_use_log = array(
			// 	'cedula' 	  => $this->input->post('cedula_user_digitaliza'),
			// 	'dependencia' => $this->input->post('dependencia'),
			// 	'nombres'     => $this->input->post('nombres'),
			// 	'message'     => $return,
			// 	'table'       => $info_table

			// );

			$rtn = $this->digital_model->get_date_user($this->input->post('cedula_user_digitaliza'));

			$return_use_log = array(
				'cedula' 	  => $rtn['0']->cedula,
				'dependencia' => $rtn['0']->dependencia,
				'nombres'     => $rtn['0']->nombres,
				'rol'	      => $rtn['0']->rol,
				'message'     => $return,
				'table'       => $info_table,
				'table_p'	  => $info_table_p,
				'table_c'	  => $info_table_c
			);


			if(empty($_POST['cedula_user_digitaliza']))
			{
				$rute = $_SERVER['HTTP_HOST'];
				redirect('http://'.$rute.'/prueba/');
				// redirect('http://localhost:8080/prueba/');
			}

			$this->load->view('digital_view', $return_use_log);



		}else{
			$id = $this->input->post('id_documento');
			$datos_seleccionados = $this->digital_model->get_info_seleccionada($id);
			$resultado_user = $this->digital_model->get_date_user($_POST['cedula_user_digitaliza']);
			

			$adjunto_info = array();
			$adjunto_info['datos_editar'] = $datos_seleccionados;
			$adjunto_info['cedula'] = $resultado_user['0']->cedula;
			$adjunto_info['dependencia'] = $resultado_user['0']->dependencia;
			$adjunto_info['nombres'] = $resultado_user['0']->nombres;
			$adjunto_info['rol'] = $resultado_user['0']->rol;
			$adjunto_info['message'] = 1;

			if(empty($_POST['cedula_user_digitaliza']))
			{
				$rute = $_SERVER['HTTP_HOST'];
				redirect('http://'.$rute.'/prueba/');
				// redirect('http://localhost:8080/prueba/');
			}

			$this->load->view('edit_digitalizacion', $adjunto_info);
		}
	}

	public function update_info_formato(){

		if(empty($_POST['cedula']))
		{
			$rute = $_SERVER['HTTP_HOST'];
			redirect('http://'.$rute.'/prueba/');
			// redirect('http://localhost:8080/prueba/');
		}

	

		$id = $this->input->post('idFormato');

		$formato_update = array();				
		$formato_update['idFormato'] = $id;
		$formato_update['nombreFormato'] = $this->input->post('nombre');
		$formato_update['tipoEvaluacion'] = $this->input->post('tipo_evaluacion');
		$formato_update['areas'] = json_encode($_POST['ubicacion']);
		$formato_update['preguntas'] = json_encode($_POST['pregunta']);
		

		$datos_actualizados = $this->digital_model->update_formato($formato_update);

		if ($datosFormato == 'exito') {
			$view_all['message'] = 'exito';
		}elseif ($datosFormato == 'existente') {
			$view_all['message'] = 'existente';			
		}
		
		$cedula = $_POST['cedula'];
		$resultado = $this->digital_model->get_date_user($cedula);
		$datos_formatos = $this->digital_model->get_table_formatos();

		$view_all['message'] = $datos_actualizados;
		$view_all['table_formatos'] = $datos_formatos;
		$view_all['cedula'] 		= $resultado['0']->cedula;
		$view_all['dependencia']  = $resultado['0']->dependencia;
		$view_all['nombres']  	= $resultado['0']->nombres;
		$view_all['rol']  	= $resultado['0']->rol;
		$this->load->view('prueba/formato', $view_all);

	}

	public function update_info_proveedor(){

		if(empty($_POST['cedula']))
		{
			$rute = $_SERVER['HTTP_HOST'];
			redirect('http://'.$rute.'/prueba/');
			// redirect('http://localhost:8080/prueba/');
		}

		$id = $this->input->post('idProveedor');

		$file_dlt_certificado = $this->input->post('file_dlt_certificado');
		$file_dlt_rut = $this->input->post('file_dlt_rut');
		$file_dlt_nitCedula = $this->input->post('file_dlt_nitCedula');
		$file_dlt_precios = $this->input->post('file_dlt_precios');
		
		$archivo_certificado = $_FILES['file_certificado']['name'];
		$archivo_rut = $_FILES['file_rut']['name'];
		$archivo_nit_cedula = $_FILES['file_nit_cedula']['name'];
		$archivo_precios = $_FILES['file_precios']['name'];

		$destino_certificado = '';
		$destino_rut = '';
		$destino_nit_cedula = '';
		$destino_archivo_precios = '';

    	if (!empty($archivo_certificado)) {
            $array_archivo_certificado = explode('.', $archivo_certificado);
            $prefijo_certificado = substr(md5(uniqid(rand())),0,6);
            $destino_certificado = "../prueba/files_proveedores/".$prefijo_certificado.".".$array_archivo_certificado[1];
            (copy($_FILES['file_certificado']['tmp_name'],$destino_certificado));
    	}

        if (!empty($archivo_rut)) {
            $array_archivo_rut = explode('.', $archivo_rut);
            $prefijo_rut = substr(md5(uniqid(rand())),0,6);
            $destino_rut = "../prueba/files_proveedores/".$prefijo_rut.".".$array_archivo_rut[1];
            (copy($_FILES['file_rut']['tmp_name'],$destino_rut));
        }
    

        if (!empty($archivo_nit_cedula)) {
            $array_archivo_nit_cedula = explode('.', $archivo_nit_cedula);
            $prefijo_nit_cedula = substr(md5(uniqid(rand())),0,6);
            $destino_nit_cedula = "../prueba/files_proveedores/".$prefijo_nit_cedula.".".$array_archivo_nit_cedula[1];
            (copy($_FILES['file_nit_cedula']['tmp_name'],$destino_nit_cedula));
        }

        if (!empty($archivo_precios)) {
            $array_archivo_precios = explode('.', $archivo_precios);
            $prefijo_archivo_precios = substr(md5(uniqid(rand())),0,6);
            $destino_archivo_precios = "../prueba/files_proveedores/".$prefijo_archivo_precios.".".$array_archivo_precios[1];
            (copy($_FILES['file_precios']['tmp_name'],$destino_archivo_precios));
        }
			
		$proveedor_update = array();				
		$proveedor_update['idProveedor'] = $id;
		$proveedor_update['nombreProveedor'] = $this->input->post('nombre');
		$proveedor_update['identificacionProveedor'] = $this->input->post('identificacion');
		$proveedor_update['nitProveedor'] = $this->input->post('nit');
		$proveedor_update['direccionProveedor'] = $this->input->post('direccion');
		$proveedor_update['ciudadProveedor'] = $this->input->post('ciudad');
		$proveedor_update['paisProveedor'] = $this->input->post('pais');
		$proveedor_update['telefonoProveedor'] = $this->input->post('telefono');
		$proveedor_update['contactoProveedor'] = $this->input->post('contacto');
		$proveedor_update['emailProveedor'] = $this->input->post('email');
		$proveedor_update['responsabilidadTributaria'] = $this->input->post('resp_tributaria');
		$proveedor_update['tipoEmpresa'] = $this->input->post('tipo_empresa');
		$proveedor_update['areaProveedor'] = $this->input->post('ubicacion');
		$proveedor_update['nombreReferencia1'] = $this->input->post('empresa1');
		$proveedor_update['ciudadReferencia1']	= $this->input->post('ciudad1');
		$proveedor_update['telefonoReferencia1'] = $this->input->post('telefono1');
		$proveedor_update['nombreReferencia2'] = $this->input->post('empresa2');
		$proveedor_update['ciudadReferencia2']	= $this->input->post('ciudad2');
		$proveedor_update['telefonoReferencia2'] = $this->input->post('telefono2');
		$proveedor_update['fechaDiligenciamiento'] = $this->input->post('fecha_diligenciamiento');
		$proveedor_update['rutaCertificadoRepresentacion'] = $destino_certificado;
		$proveedor_update['rutaRut'] = $destino_rut;
		$proveedor_update['rutaNitCedula'] = $destino_nit_cedula;
		$proveedor_update['rutaPrecios'] = $destino_archivo_precios;

		if ($_POST['forma_pago']=='otro') {
			$proveedor_update['formaPago'] = $this->input->post('otro_tipo');	
		}else{
			$proveedor_update['formaPago'] = $this->input->post('forma_pago');	
		}
		
		$return_proveedor = $this->digital_model->update_proveedor($proveedor_update);
		$info_table = $this->digital_model->get_table_digitalizados();
		$info_table_p = $this->digital_model->get_table_proveedores();

		$rtn = $this->digital_model->get_date_user($this->input->post('cedula'));

		$return_use_log = array(
			'mensaje'	  => $return_proveedor,
			'cedula' 	  => $rtn['0']->cedula,
			'dependencia' => $rtn['0']->dependencia,
			'nombres'     => $rtn['0']->nombres,
			'rol'	      => $rtn['0']->rol,
			'message'     => $return,
			'table'       => $info_table,
			'table_p'	  => $info_table_p
		);

		$this->load->view('digital_view', $return_use_log);
	}



	public function evaluacion_view_present()
	{
		$idProveedor = $this->uri->segment(3);
		$cedula = $this->uri->segment(4);
		$tipo_evaluacion = $this->uri->segment(5);	
		$get_date = explode("_", $tipo_evaluacion);

		if(empty($cedula))
		{
			$rute = $_SERVER['HTTP_HOST'];
			redirect('http://'.$rute.'/prueba/');
		}

		$resultado = $this->digital_model->get_date_user($cedula);
		$evaluacion = $this->digital_model->get_evalua($get_date);
		$datos_proveedor = $this->digital_model->get_info_proveedor($idProveedor);

		$view_all['evaluacion'] = $evaluacion;
		$view_all['proveedor'] = $datos_proveedor;
		$view_all['cedula'] 		= $resultado['0']->cedula;
		$view_all['dependencia']  = $resultado['0']->dependencia;
		$view_all['nombres']  	= $resultado['0']->nombres;
		$view_all['rol']  	= $resultado['0']->rol;
		$this->load->view('prueba/evaluar_proveedor', $view_all);

	}

	public function evaluacion_proveedor()
	{	
		if(empty($_POST['cedula']))
		{
			$rute = $_SERVER['HTTP_HOST'];
			redirect('http://'.$rute.'/prueba/');
		}

		$cedula = $_POST['cedula'];
		$resultado = $this->digital_model->get_date_user($cedula);

		$evaluarProveedor = array();
		$evaluarProveedor['nombreProveedorEvaluado'] = $this->input->post('nombreProveedor');
		$evaluarProveedor['nombreEvaluacion'] = $this->input->post('nombreFormato');
		$evaluarProveedor['areaEvaluacion'] = $this->input->post('area');
		$evaluarProveedor['tipoEvaluacion'] = $this->input->post('tipoEvaluacion');



		$evaluarProveedor['preguntas'] = json_encode($this->input->post('pregunta'));
		// $evaluarProveedor['respuestas'] = $this->input->post('califone');
		$contador = 1;
		for($i=0; $i < count($_POST['pregunta']) ; $i++){
			$respuestas[$contador] = $_POST['calif_'.$contador];
			$contador++;
		}

		$evaluarProveedor['respuestas'] = $respuestas;
		
		$datosEvaluacion = $this->digital_model->insertarEvaluacion($evaluarProveedor);

		$info_table = $this->digital_model->get_table_digitalizados();
		$info_table_p = $this->digital_model->get_table_proveedores();
		$info_table_c = $this->digital_model->get_table_calificaciones();

		$view_all['table'] = $info_table;
		$view_all['table_p'] = $info_table_p;
		$view_all['table_c'] = $info_table_c;
		$view_all['mensaje'] = $datosEvaluacion;
		$view_all['cedula'] 		= $resultado['0']->cedula;
		$view_all['dependencia']  = $resultado['0']->dependencia;
		$view_all['nombres']  	= $resultado['0']->nombres;
		$view_all['rol']  	= $resultado['0']->rol;
		$this->load->view('digital_view', $view_all);
		
	}
}
