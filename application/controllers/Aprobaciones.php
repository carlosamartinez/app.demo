<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aprobaciones extends CI_Controller {

	public function __construct()
   	{
        parent::__construct();
        // Your own constructor code
        $this->load->helper('form');
		$this->load->helper('url');
        $this->load->database();
		$this->load->model('digital_model');
		$this->load->model('aprobaciones_model');
		$this->load->model('empleados/empleados_model');
		$this->load->model('usuarios_model');

		// Notificar solamente errores de ejecución
		error_reporting(E_ERROR | E_WARNING | E_PARSE);
   	}
	
	public function index()
	{
		if(empty($_POST['cedula']))
		{
			$rute = $_SERVER['HTTP_HOST'];
			redirect('http://'.$rute.'/prueba/');
		}

		if(!empty($_POST['etapa-1-4'])){

			$cedula = $this->input->post('cedula');
			$resultado = $this->digital_model->get_date_user($cedula);
			$info_table = $this->digital_model->get_table_digitalizados_one_and_four();
			$view_all['table'] = $info_table;
			$view_all['cedula'] 		= $resultado['0']->cedula;
			$view_all['dependencia']    = $resultado['0']->dependencia;
			$view_all['nombres']  		= $resultado['0']->nombres;
			$view_all['rol']  			= $resultado['0']->rol;
			$this->load->view('aprovaciones_view', $view_all);

		}elseif(!empty($_POST['etapa-2'])){

			$area = $this->usuarios_model->get_area_user($_POST['cedula']);

			$cedula = $this->input->post('cedula');
			$resultado = $this->digital_model->get_date_user($cedula);
			$info_table = $this->digital_model->get_table_digitalizados_two($area);


			$view_all['table'] = $info_table;
			$view_all['cedula'] 		= $resultado['0']->cedula;
			$view_all['dependencia']    = $resultado['0']->dependencia;
			$view_all['nombres']  		= $resultado['0']->nombres;
			$view_all['rol']  			= $resultado['0']->rol;

			// get empleados proximos a salir a vaciones 
			$employe_vacaciones = $this->empleados_model->get_empleados_proximos_vacaciones($resultado['0']->dependencia);

			// retornamos los empleados proximos a salir a vaciones a la vista
			$view_all['employee_next_vaca'] = $employe_vacaciones;
			$view_all['seleted'] = 'proximos a vacaciones';
			$this->load->view('aprovaciones_two', $view_all);

		}elseif(!empty($_POST['etapa-3'])){

			$cedula = $this->input->post('cedula');
			$resultado = $this->digital_model->get_date_user($cedula);
			$info_table = $this->digital_model->get_table_digitalizados_tree();
			$view_all['table'] = $info_table;
			$view_all['cedula'] 		= $resultado['0']->cedula;
			$view_all['dependencia']    = $resultado['0']->dependencia;
			$view_all['nombres']  		= $resultado['0']->nombres;
			$view_all['rol']  			= $resultado['0']->rol;
			$this->load->view('aprovaciones_tree', $view_all);

		}

	}

	public function test()
	{
		switch ($this->input->post('filter')){

			case 'empleados de area':
				$area = $this->usuarios_model->get_area_user($_POST['cedula']);
				$cedula = $this->input->post('cedula');
				$dependencia = $this->input->post('dependencia');
				$resultado = $this->digital_model->get_date_user($cedula);
				$info_table = $this->digital_model->get_table_digitalizados_two($area);


				$view_all['table'] = $info_table;
				$view_all['cedula'] 		= $resultado['0']->cedula;
				$view_all['dependencia']    = $resultado['0']->dependencia;
				$view_all['nombres']  		= $resultado['0']->nombres;
				$view_all['rol']  			= $resultado['0']->rol;

				// get empleados de la dependencia
				$employe_area = $this->empleados_model->employeee_area($dependencia);
				// retornamos los empleados
				$view_all['employee_next_vaca'] = $employe_area;
				$view_all['seleted'] = 'empleados de area';
				$this->load->view('aprovaciones_two', $view_all);
				break;


			case 'proximos a vacaciones':
				$area = $this->usuarios_model->get_area_user($_POST['cedula']);
				$cedula = $this->input->post('cedula');
				$dependencia = $this->input->post('dependencia');
				$resultado = $this->digital_model->get_date_user($cedula);
				$info_table = $this->digital_model->get_table_digitalizados_two($area);



				$view_all['table'] = $info_table;
				$view_all['cedula'] 		= $resultado['0']->cedula;
				$view_all['dependencia']    = $resultado['0']->dependencia;
				$view_all['nombres']  		= $resultado['0']->nombres;
				$view_all['rol']  			= $resultado['0']->rol;

				// get empleados proximos a salir a vaciones 
				$employe_vacaciones = $this->empleados_model->get_empleados_proximos_vacaciones($dependencia);

				// retornamos los empleados proximos a salir a vaciones a la vista
				$view_all['employee_next_vaca'] = $employe_vacaciones;
				$view_all['seleted'] = 'proximos a vacaciones';
				$this->load->view('aprovaciones_two', $view_all);
				break;


			case 'activos':
				$area = $this->usuarios_model->get_area_user($_POST['cedula']);
				$cedula = $this->input->post('cedula');
				$dependencia = $this->input->post('dependencia');
				$resultado = $this->digital_model->get_date_user($cedula);
				$info_table = $this->digital_model->get_table_digitalizados_two($area);


				$view_all['table'] = $info_table;
				$view_all['cedula'] 		= $resultado['0']->cedula;
				$view_all['dependencia']    = $resultado['0']->dependencia;
				$view_all['nombres']  		= $resultado['0']->nombres;
				$view_all['rol']  			= $resultado['0']->rol;

				// get empleados de la dependencia y que esten activos
				$employeee_area_activos = $this->empleados_model->employeee_area_activos($dependencia);
				// retornamos los empleados
				$view_all['employee_next_vaca'] = $employeee_area_activos;
				$view_all['seleted'] = 'activos';
				$this->load->view('aprovaciones_two', $view_all);
				break;


			case 'Empleados en vacaciones':
				$area = $this->usuarios_model->get_area_user($_POST['cedula']);
				$cedula = $this->input->post('cedula');
				$dependencia = $this->input->post('dependencia');
				$resultado = $this->digital_model->get_date_user($cedula);
				$info_table = $this->digital_model->get_table_digitalizados_two($area);


				$view_all['table'] = $info_table;
				$view_all['cedula'] 		= $resultado['0']->cedula;
				$view_all['dependencia']    = $resultado['0']->dependencia;
				$view_all['nombres']  		= $resultado['0']->nombres;
				$view_all['rol']  			= $resultado['0']->rol;

				// get empleados de la dependencia y que esten activos
				$employeee_area_vacaciones = $this->empleados_model->employeee_area_vacaciones($dependencia);
				// retornamos los empleados
				$view_all['employee_next_vaca'] = $employeee_area_vacaciones;
				$view_all['seleted'] = 'Empleados en vacaciones';
				$this->load->view('aprovaciones_two', $view_all);
				break;	

			
			default:
				# code...
				break;
		}

		



	}

	public function get_cant_emp_vacas_notif()
	{
		$this->empleados_model->get_empleados_proximos_vacaciones($_POST['dependencia']);
	}

	public function view_pagos()
	{
		if(empty($_POST['cedula']))
		{
			$rute = $_SERVER['HTTP_HOST'];
			redirect('http://'.$rute.'/prueba/');
		}

		$cedula = $this->input->post('cedula');
		$resultado = $this->digital_model->get_date_user($cedula);
		$info_table = $this->digital_model->get_table_digitalizados_listo_pago();

		$view_all['table'] = $info_table;
		$view_all['cedula'] 		= $resultado['0']->cedula;
		$view_all['dependencia']    = $resultado['0']->dependencia;
		$view_all['nombres']  		= $resultado['0']->nombres;
		$view_all['rol']	  		= $resultado['0']->rol;

		$this->load->view('pagos', $view_all);
	}


	public function view_contabilidad()
	{
		if(empty($_POST['cedula']))
		{
			$rute = $_SERVER['HTTP_HOST'];
			redirect('http://'.$rute.'/prueba/');
		}

		$cedula = $this->input->post('cedula');
		$resultado = $this->digital_model->get_date_user($cedula);
		$info_table = $this->digital_model->get_table_digitalizados_aprobados();

		$view_all['table'] = $info_table;
		$view_all['cedula'] 		= $resultado['0']->cedula;
		$view_all['dependencia']    = $resultado['0']->dependencia;
		$view_all['nombres']  		= $resultado['0']->nombres;
		$view_all['rol']	  		= $resultado['0']->rol;

		$this->load->view('aprueba_contable', $view_all);
	}

	public function update_estado_apro()
	{
		$hoy = getdate();
		if(empty($_POST['cedula']))
		{
			$rute = $_SERVER['HTTP_HOST'];
			redirect('http://'.$rute.'/prueba/');
		}

		if(!empty($_POST['etapa_1'])){

			$cedula		  = $this->input->post('cedula');
			$id_documento = $this->input->post('id_documento');
			$dependencia  = $this->input->post('dependencia');
			$fecha 		  = $hoy['year']."-".$hoy['mon']."-".$hoy['mday'];
			$etapa_1	  = $this->input->post('etapa_1');

			$update_estad = array(
									'cedula_user_digitaliza' => $cedula,
									'dependencia' => $dependencia,
									'fecha_ingreso' => $fecha,
									'id_documento' => $id_documento, 
									'etapa_1' 	   => $etapa_1
								 );

			$retun = $this->aprobaciones_model->update_estado($update_estad);
			$resultado = $this->digital_model->get_date_user($cedula);
			$info_table = $this->digital_model->get_table_digitalizados_one_and_four();

			$view_all['table'] = $info_table;
			$view_all['cedula'] 		= $resultado['0']->cedula;
			$view_all['dependencia']    = $resultado['0']->dependencia;
			$view_all['nombres']  		= $resultado['0']->nombres;
			$view_all['rol']	  		= $resultado['0']->rol;
			$view_all['message']  		= $retun;

			$this->load->view('aprovaciones_view', $view_all);

		}elseif(!empty($_POST['etapa_4'])){

			$cedula		  = $this->input->post('cedula');
			$id_documento = $this->input->post('id_documento');
			$dependencia  = $this->input->post('dependencia');
			$fecha 		  = $hoy['year']."-".$hoy['mon']."-".$hoy['mday'];
			$etapa_4	  = $this->input->post('etapa_4');


			if($this->input->post('negado_4')){
			
				$update_estad = array(
										'cedula_user_digitaliza' => $cedula,
										'dependencia' => $dependencia,
										'fecha_ingreso' => $fecha,
										'id_documento' => $id_documento, 
										'etapa_4' 	   => $etapa_4,
										'etapa_1_neg' 	   => 1,
										'observaciones_negado_1_4'  => $this->input->post('observaciones')
									 );
			}else{
				$update_estad = array(
										'cedula_user_digitaliza' => $cedula,
										'dependencia' => $dependencia,
										'fecha_ingreso' => $fecha,
										'id_documento' => $id_documento, 
										'etapa_4' 	   => $etapa_4
									 );	
			}

			$retun = $this->aprobaciones_model->update_estado($update_estad);
			$resultado = $this->digital_model->get_date_user($cedula);
			$info_table = $this->digital_model->get_table_digitalizados_one_and_four();

			$view_all['table'] = $info_table;
			$view_all['cedula'] 		= $resultado['0']->cedula;
			$view_all['dependencia']    = $resultado['0']->dependencia;
			$view_all['nombres']  		= $resultado['0']->nombres;
			$view_all['rol']	  		= $resultado['0']->rol;
			$view_all['message']  		= $retun;

			$this->load->view('aprovaciones_view', $view_all);

		}elseif(!empty($_POST['etapa_2'])){


			$cedula		  = $this->input->post('cedula');
			$id_documento = $this->input->post('id_documento');
			$dependencia  = $this->input->post('dependencia');
			$fecha 		  = $hoy['year']."-".$hoy['mon']."-".$hoy['mday'];
			$etapa_2	  = $this->input->post('etapa_2');

			if($this->input->post('negado_2')){
				
				$update_estad = array(
										'cedula_user_digitaliza' => $cedula,
										'dependencia' => $dependencia,
										'fecha_ingreso' => $fecha,
										'id_documento' => $id_documento, 
										'etapa_2' 	   => $etapa_2,
										'etapa_2_neg' 	   => 1,
										'observaciones_negado_2'  => $this->input->post('observaciones')
									 );

			}else{

				$update_estad = array(
										'cedula_user_digitaliza' => $cedula,
										'dependencia' => $dependencia,
										'fecha_ingreso' => $fecha,
										'id_documento' => $id_documento, 
										'etapa_2' 	   => $etapa_2
									 );
			}

			$area = $this->usuarios_model->get_area_user($_POST['cedula']);
			$retun = $this->aprobaciones_model->update_estado($update_estad);
			$resultado = $this->digital_model->get_date_user($cedula);
			$info_table = $this->digital_model->get_table_digitalizados_two($area);

			// get empleados proximos a salir a vaciones 
			$employe_vacaciones = $this->empleados_model->get_empleados_proximos_vacaciones($resultado['0']->dependencia);

			$view_all['table'] 			= $info_table;
			$view_all['cedula'] 		= $resultado['0']->cedula;
			$view_all['dependencia']    = $resultado['0']->dependencia;
			$view_all['nombres']  		= $resultado['0']->nombres;
			$view_all['rol']	  		= $resultado['0']->rol;
			$view_all['message']  		= $retun;
			// retornamos los empleados proximos a salir a vaciones a la vista
			$view_all['employee_next_vaca'] = $employe_vacaciones;

			$this->load->view('aprovaciones_two', $view_all);

		}elseif(!empty($_POST['etapa_3'])){

			$cedula		  = $this->input->post('cedula');
			$id_documento = $this->input->post('id_documento');
			$dependencia  = $this->input->post('dependencia');
			$fecha 		  = $hoy['year']."-".$hoy['mon']."-".$hoy['mday'];
			$etapa_3	  = $this->input->post('etapa_3');

			if($this->input->post('negado_3')){
				
				$update_estad = array(
										'cedula_user_digitaliza' => $cedula,
										'dependencia' => $dependencia,
										'fecha_ingreso' => $fecha,
										'id_documento' => $id_documento, 
										'etapa_3' 	   => $etapa_3,
										'etapa_3_neg' 	   => 1,
										'observaciones_negado_3'  => $this->input->post('observaciones')
									 );

			}else{

				$update_estad = array(
										'cedula_user_digitaliza' => $cedula,
										'dependencia' => $dependencia,
										'fecha_ingreso' => $fecha,
										'id_documento' => $id_documento, 
										'etapa_3' 	   => $etapa_3
									 );
			}


			$retun = $this->aprobaciones_model->update_estado($update_estad);
			$resultado = $this->digital_model->get_date_user($cedula);
			$info_table = $this->digital_model->get_table_digitalizados_tree();

			$view_all['table'] 			= $info_table;
			$view_all['cedula'] 		= $resultado['0']->cedula;
			$view_all['dependencia']    = $resultado['0']->dependencia;
			$view_all['nombres']  		= $resultado['0']->nombres;
			$view_all['rol']	  		= $resultado['0']->rol;
			$view_all['message']  		= $retun;

			$this->load->view('aprovaciones_tree', $view_all);

		}
	}

	public function update_estado_pago()
	{
		if(empty($_POST['cedula']))
		{
			$rute = $_SERVER['HTTP_HOST'];
			redirect('http://'.$rute.'/prueba/');
		}
		
		$hoy = getdate();
		$cedula		  = $this->input->post('cedula');
		$id_documento = $this->input->post('id_documento');
		$estado 	  = $this->input->post('estado');
		$dependencia  = $this->input->post('dependencia');
		$fecha 		  = $hoy['year']."-".$hoy['mon']."-".$hoy['mday'];
		$etapa_4	  = $this->input->post('etapa_4');

		$update_estad = array(
								'cedula_user_digitaliza' => $cedula,
								'dependencia' => $dependencia,
								'fecha_ingreso' => $fecha,
								'estado' => $estado,
								'id_documento' => $id_documento, 
							 );
		
		$retun = $this->aprobaciones_model->update_estado($update_estad);

		$cedula = $this->input->post('cedula');
		$resultado = $this->digital_model->get_date_user($cedula);


		$info_table = $this->digital_model->get_table_digitalizados_listo_pago();
	
		
		$view_all['cedula'] 		= $resultado['0']->cedula;
		$view_all['dependencia']    = $resultado['0']->dependencia;
		$view_all['nombres']  		= $resultado['0']->nombres;
		$view_all['rol']  			= $resultado['0']->rol;
		$view_all['message']  		= $retun;

		if($estado == 'listo para pago'){
			$view_all['table'] = $this->digital_model->get_table_digitalizados_aprobados();
			$this->load->view('aprueba_contable', $view_all);
		}elseif ($estado == 'Pagado'){
			$view_all['table'] = $info_table;
			$this->load->view('pagos', $view_all);
		}
	}


	public function insert_registre_vacaciones()
	{
		$employee_registre_vaca = array();
		$employee_registre_vaca['id_empleado'] = $_POST['id_empleado'];
		$employee_registre_vaca['fecha_inicial'] = $_POST['fecha_inicial'];
		$employee_registre_vaca['fecha_final'] = $_POST['fecha_final'];
		$employee_registre_vaca['periodo'] = $_POST['periodo'];
		$employee_registre_vaca['cantidad_dias_tomados'] = $_POST['cantidad_dias_tomados'];
		$employee_registre_vaca['observacion'] = $_POST['observacion'];

		$update_esta_vaca = array();
		$update_esta_vaca['fecha_establece_bandera'] = $_POST['fecha_establece_bandera'];
		$update_esta_vaca['bandera_view_bacaciones'] = $_POST['bandera_view_bacaciones'];
		$update_esta_vaca['id_empleado'] = $_POST['id_empleado'];

		$return  = $this->empleados_model->inser_vaca_emple($employee_registre_vaca);
		$return1 = $this->empleados_model->inser_vaca_estado($update_esta_vaca);
		$respuesta_retorna = array('respuest1' => $return, 'respuest2' => $return1);
		echo json_encode($respuesta_retorna);
		die();
	}

	public function insert_date_gety()
	{
		echo "<pre>";
			print_r($_POST);
		echo "</pre>";
		die('here');
	}

}
