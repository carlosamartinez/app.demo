<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Empleados extends CI_Controller{

	public function __construct()
   	{
        parent::__construct();
        // Your own constructor code
        $this->load->helper('form');
        $this->load->database();
		$this->load->model('digital_model');
		$this->load->model('empleados/empleados_model');
		$this->load->model('ubicacion_model');
		// $this->load->model('usuarios_model');

		$this->load->helper('url');
		// Notificar solamente errores de ejecución
		error_reporting(E_ERROR | E_WARNING | E_PARSE);
   	}

   	public function reporte_empleados(){
		$this->empleados_model->get_info_reportes_empleados();		
    }

    public function reporte_afiliaciones(){
    	$this->empleados_model->get_info_reportes_afiliaciones();
    }

    public function reporte_perfiles(){
    	$this->empleados_model->get_info_reportes_perfiles();
    }

   	public function reportes(){

		if(empty($_POST['cedula']))
		{
			$rute = $_SERVER['HTTP_HOST'];
			redirect('http://'.$rute.'/prueba/');
			// redirect('http://localhost:8080/prueba/');
		}

		$cedula = $_POST['cedula'];
		$resultado = $this->digital_model->get_date_user($cedula);

		$view_header['cedula'] 			= $resultado['0']->cedula;
		$view_header['dependencia']     = $resultado['0']->dependencia;
		$view_header['nombres']  		= $resultado['0']->nombres;
		$view_header['rol']  			= $resultado['0']->rol;

		$vie_body['cedula'] = $resultado['0']->cedula;

		$this->load->view('empleados/maqueta_header', $view_header);
		$this->load->view('empleados/reportes', $vie_body);
		$this->load->view('empleados/maqueta_footer');
   			

   	}
	// funcion inicial cuando el usuario ingresa al modulo de 
	// empleados donde cargamos toda la informaxion de todos los empleados
	public function index()
	{
		if(empty($_POST['cedula']))
		{
			$rute = $_SERVER['HTTP_HOST'];
			redirect('http://'.$rute.'/prueba/');
			// redirect('http://localhost:8080/prueba/');
		}
		
		$cedula = $_POST['cedula'];
		$resultado = $this->digital_model->get_date_user($cedula);
		$return_employee = $this->empleados_model->get_empleados();


		$view_all['empleados'] 			= $return_employee; 	
		$view_all['cedula'] 			= $resultado['0']->cedula;
		$view_header['cedula'] 			= $resultado['0']->cedula;
		$view_header['dependencia']     = $resultado['0']->dependencia;
		$view_header['nombres']  		= $resultado['0']->nombres;
		$view_header['rol']  			= $resultado['0']->rol;

		$this->load->view('empleados/maqueta_header', $view_header);
		$this->load->view('empleados/empleados', $view_all);
		$this->load->view('empleados/maqueta_footer');
	}

	// funcion encargada de mostrar la vista donde se crearan
	// por primera vez los empleados en la bd para anexar sus hojas de vida
	public function nueva_hoja()
	{
		if(empty($_POST['cedula']))
		{
			$rute = $_SERVER['HTTP_HOST'];
			redirect('http://'.$rute.'/prueba/');
			// redirect('http://localhost:8080/prueba/');
		}

		$cedula = $_POST['cedula'];
		$resultado = $this->digital_model->get_date_user($cedula);
		$ubicaciones = $this->ubicacion_model->get_ubicaciones();
		$cargos = $this->empleados_model->get_cargos();

		$departamentos = array();
		for($i=0; $i < count($ubicaciones); $i++){ 
			$departamentos[$i] = $ubicaciones[$i]->nombre_ubicacion;
		}

		$cargos_pr = array();
		for($i=0; $i < count($cargos); $i++){ 
			$cargos_pr[$i] =  utf8_decode($cargos[$i]->nombre_cargo);
		}

		$view_header['cedula'] 			= $resultado['0']->cedula;
		$view_header['dependencia']     = $resultado['0']->dependencia;
		$view_header['nombres']  		= $resultado['0']->nombres;
		$view_header['rol']  			= $resultado['0']->rol;

		$vie_body['cedula_user'] = $resultado['0']->cedula;
		$vie_body['departamentos_organizacionales'] = $departamentos;
		$vie_body['cargos'] = $cargos_pr;

		$this->load->view('empleados/maqueta_header', $view_header);
		$this->load->view('empleados/nuevo_empleado', $vie_body);
		$this->load->view('empleados/maqueta_footer');
	}

	// funcion encargada de mostrarnos la vista de 
	// edicion de hojas de vida de los diferentes empleados
	public function modificar_hoja()
	{
		if(empty($_POST['cedula_user']))
		{
			$rute = $_SERVER['HTTP_HOST'];
			redirect('http://'.$rute.'/prueba/');
			// redirect('http://localhost:8080/prueba/');
		}

		$cedula = $_POST['cedula_user'];
		$resultado = $this->digital_model->get_date_user($cedula);
		$ubicaciones = $this->ubicacion_model->get_ubicaciones();
		$cargos = $this->empleados_model->get_cargos();
		$info_selec = $this->empleados_model->info_selec($this->input->post('id_empleado'));

		$departamentos = array();
		for($i=0; $i < count($ubicaciones); $i++){ 
			$departamentos[$i] = $ubicaciones[$i]->nombre_ubicacion;
		}

		$cargos_pr = array();
		for($i=0; $i < count($cargos); $i++){ 
			$cargos_pr[$i] =  utf8_decode($cargos[$i]->nombre_cargo);
		}

		$view_header['cedula'] 			= $resultado['0']->cedula;
		$view_header['dependencia']     = $resultado['0']->dependencia;
		$view_header['nombres']  		= $resultado['0']->nombres;
		$view_header['rol']  			= $resultado['0']->rol;

		$vie_body['cedula_user'] = $resultado['0']->cedula;
		$vie_body['departamentos_organizacionales'] = $departamentos;
		$vie_body['cargos'] = $cargos_pr;
		$vie_body['dates_consul'] = $info_selec;

		$this->load->view('empleados/maqueta_header', $view_header);
		$this->load->view('empleados/editar_empleado', $vie_body);
		$this->load->view('empleados/maqueta_footer');
	}

	// funcion encargada de insetar la informacion al momento que se crean los empleados
	public function insert_dates()
	{
		// array que se encarga de almacenar los datos que va para la tabla empleados
		$empleado = array();
		$empleado['nombres'] = $this->input->post('nombres');
		$empleado['apellidos'] = $this->input->post('apellidos');
		$empleado['cedula'] = $this->input->post('cedula');
		$empleado['direccion'] = $this->input->post('direccion');
		$empleado['telefono_fijo'] = $this->input->post('telefono_fijo');
		$empleado['celular'] = $this->input->post('celular');
		$empleado['fecha_ingreso'] = $this->input->post('fecha_ingreso');
		$empleado['estado'] = 'Activo';
		//enviamos datos a insertar a la el modelo (funcion -> insert)
		$respues_emple_inser = $this->empleados_model->insert_empleados($empleado);

		//array que se encarga de almacenar los datos que van para la tabla de perfil
		$perfil_emple = array();
		$perfil_emple['id_empleado']   			= $respues_emple_inser['id_empleado'];
		$perfil_emple['imagenes_hoja_vida'] 	= $respues_emple_inser['imagenes_hoja_vida'];
		$perfil_emple['cargo_laboral'] 			= $this->input->post('cargo_laboral');
		$perfil_emple['departamento']  			= $this->input->post('departamento');
		if($respues_emple_inser['medidas_diciplinarias']){
			$perfil_emple['medidas_diciplinarias'] 	= $respues_emple_inser['medidas_diciplinarias'];
		}


		//enviamos datos a insertar a la el modelo (funcion -> insert)
		$respues_perfi_inser = $this->empleados_model->insert_empleados_perfil($perfil_emple);

		$afiliaciones = array();
		$afiliaciones['id_empleado']  = $respues_emple_inser['id_empleado'];
		$afiliaciones['arl']  = $this->input->post('arl');
		$afiliaciones['eps']  = $this->input->post('eps');
		$afiliaciones['pension'] =  $this->input->post('pension');
		$afiliaciones['cesantias'] = $this->input->post('cesantias');
		//enviamos datos al modelo para que se haga el insert a la tabla afiliaciones
		$respues_afiliaci_inser = $this->empleados_model->insert_empleados_afiliaci($afiliaciones);
		
		// empezamos a gestionar vista nuevamente de retorno despues de guardar
		$cedula = $this->input->post('cedula_user');
		$resultado = $this->digital_model->get_date_user($cedula);
		$ubicaciones = $this->ubicacion_model->get_ubicaciones();
		$cargos = $this->empleados_model->get_cargos();

		$departamentos = array();
		for($i=0; $i < count($ubicaciones); $i++){ 
			$departamentos[$i] = $ubicaciones[$i]->nombre_ubicacion;
		}

		$cargos_pr = array();
		for($i=0; $i < count($cargos); $i++){ 
			$cargos_pr[$i] =  utf8_decode($cargos[$i]->nombre_cargo);
		}
		$view_header['cedula'] 			= $resultado['0']->cedula;
		$view_header['dependencia']     = $resultado['0']->dependencia;
		$view_header['nombres']  		= $resultado['0']->nombres;
		$view_header['rol']  			= $resultado['0']->rol;

		$vie_body['cedula_user'] = $resultado['0']->cedula;
		$vie_body['departamentos_organizacionales'] = $departamentos;
		$vie_body['cargos'] = $cargos_pr;
		$vie_body['message'] = 'exito';

		$this->load->view('empleados/maqueta_header', $view_header);
		$this->load->view('empleados/nuevo_empleado', $vie_body);
		$this->load->view('empleados/maqueta_footer');
	}

	// funcion que se encarga de validar si la cedula que 
	// ingresaron existe o no  en la bd para no permitir ingreso repetitivo
	function validar_exist_emplo()
	{
		$cedula = $this->input->post('cedula');
		$exis_value = $this->empleados_model->cons_exit($cedula);
		echo $exis_value;
		die();
	}

	function delete_documento()
	{
		$dele_doc = array('id_empleado' => $_POST['id'], 'doc' => $_POST['doc'], );
		$this->empleados_model->delete_doc_espe($dele_doc);
	}

	function delete_documento_medidas()
	{
		$dele_doc_me = array('id_empleado' => $_POST['id'], 'doc' => $_POST['doc'], );
		$this->empleados_model->delete_doc_espe_med($dele_doc_me);
	}

	function update_emple()
	{	

		$empledos_modif = array(
								'id_empleado' => $this->input->post('id_empleado'),
								'nombres' => $this->input->post('nombres'),
								'apellidos' => $this->input->post('apellidos'),
								'cedula' => $this->input->post('cedula'),
								'direccion' => $this->input->post('direccion'),
								'telefono_fijo' => $this->input->post('telefono_fijo'),
								'celular' => $this->input->post('celular'),
								'fecha_ingreso' => $this->input->post('fecha_ingreso'),
								'estado' => $this->input->post('estados')
		 				  );

		$this->empleados_model->update_emple($empledos_modif);
		

		$afiliaciones_modif = array(
									'id_empleado' => $this->input->post('id_empleado'),
									'arl' => $this->input->post('arl'), 
									'eps' => $this->input->post('eps'),
									'pension' => $this->input->post('pension'),
									'cesantias' => $this->input->post('cesantias')
							  );

		$this->empleados_model->update_afili($afiliaciones_modif);

		$perfiles_modif = array(
								'id_empleado' => $this->input->post('id_empleado'),
								'cargo_laboral' => $this->input->post('cargo_laboral'),
								'departamento' => $this->input->post('departamento')
								);

		$te = $this->empleados_model->update_perfil($perfiles_modif);

		// validamos que la actualizacion tenga exito y pasamos a cargar la pagina inicial del modulo
		if($te){
			if(empty($_POST['cedula_user']))
			{
				$rute = $_SERVER['HTTP_HOST'];
				redirect('http://'.$rute.'/prueba/');
				// redirect('http://localhost:8080/prueba/');
			}
			
			$cedula = $_POST['cedula_user'];
			$resultado = $this->digital_model->get_date_user($cedula);
			$return_employee = $this->empleados_model->get_empleados();


			$view_all['empleados'] 			= $return_employee; 	
			$view_all['cedula'] 			= $resultado['0']->cedula;
			$view_header['cedula'] 			= $resultado['0']->cedula;
			$view_header['dependencia']     = $resultado['0']->dependencia;
			$view_header['nombres']  		= $resultado['0']->nombres;
			$view_header['rol']  			= $resultado['0']->rol;

			$this->load->view('empleados/maqueta_header', $view_header);
			$this->load->view('empleados/empleados', $view_all);
			$this->load->view('empleados/maqueta_footer');
		}
				
	}


	public function generar_zip_hoja_vida()
	{
		// die('okokokok');
		$id = $this->input->post('id_empleado');

		if(!$id){
			$id = $this->uri->segment(4);
		}
		$this->empleados_model->get_zip_hoja_vida($id);
	}

	public function generar_zip_medidas()
	{
		$id = $this->input->post('id_empleado');
		if(!$id){
			$id = $this->uri->segment(4);
		}
		$this->empleados_model->get_zip_medidias($id);
	}

	
}
