<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reportes extends CI_Controller {

	public function __construct()
   	{
        parent::__construct();
        // Your own constructor code
        $this->load->helper('form');
        $this->load->database();
		$this->load->model('empleados/empleados_model');
		

		$this->load->helper('url');
		// Notificar solamente errores de ejecución
		error_reporting(E_ERROR | E_WARNING | E_PARSE);
   	}
	
	public function hoja_report()
	{
		// get variables parametros
		$id = $this->input->post('id_empleado');
		$cedula_user = $this->input->post('cedula_user');
		$type = $this->input->post('type');

		switch($type){
			case 'hoja_basica':
				$hoja_basica = $this->empleados_model->get_report($id);
				$dat_hja = array();
				$dat_hja['basic_empl_hja'] = (array) $hoja_basica[0];
				$this->load->view('empleados/view_pdf_basic',$dat_hja);
				break;

			case 'hoja_completa':
				$hoja_basica = $this->empleados_model->get_report_complet($id);
				$dat_hja = array();
				$dat_hja['basic_empl_hja'] = (array) $hoja_basica[0];
				$this->load->view('empleados/view_pdf_basic',$dat_hja);				
				break;

			case 'hoja_medidas':
				$hoja_basica = $this->empleados_model->get_report_medida($id);
				$dat_hja = array();
				$dat_hja['basic_empl_hja'] = (array) $hoja_basica[0];

				
				$dl_exist = json_encode($dat_hja['basic_empl_hja']['medidas_diciplinarias']);
				
				if($dl_exist == '"[]"' or $dl_exist == ''){
					die('<h3 style="margin-top: 0; color: #352727; padding: 3.2em;">El empleado no tiene medidas diciplinarias</h3>');
					// die('hey amigo el valor el viene vacio');
				}else{
					$this->load->view('empleados/view_pdf_basic',$dat_hja);	
				}	
				
				break;
				

			
			case 'hoja_carnet':
				die('opción en desarrollo parcialmente desactivada');
				break;		

			default:
				# code...
				break;
		}
		

	}
}
