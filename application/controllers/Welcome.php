<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller{


	public function __construct()
   	{
        parent::__construct();
        // Your own constructor code
        $this->load->helper('form');
        $this->load->database();
		$this->load->model('welcome_model');
		$this->load->model('inicio/inicio_model');
		$this->load->helper('url');
		//$this->load->model('evaluacion/evaluacion_model');
		//$this->load->model('empleados/Empleados_model');


		// Notificar solamente errores de ejecución
		error_reporting(E_ERROR | E_WARNING | E_PARSE);
   	}


	public function index()
	{		
		$this->load->view('header');
		$this->load->view('welcome_message');
		$this->load->view('footer');
	}

	public function inicio()
	{	
		$this->load->view('inicio');
	}

	// por medio de esta funcion traemos la informacion de los estados de las facturas digitalizadas
	// para graficar en la pagina principal
	public function charts()
	{
		$cantidades_estados = $this->welcome_model->get_cantidad_estados();
	}

	public function charts_fl()
	{
		$cumplimiento_user_nn = $this->inicio_model->get_cumplimiento();
	}

	public function re_inicio()
	{	
		$info_user = $this->welcome_model->get_info_users($_POST['cedula']);
		$cantidades_estados = $this->welcome_model->get_cantidad_estados();

		$alerta = array();
		$alerta['nombres'] 		= $info_user['0']->nombres;
		$alerta['cedula'] 		= $info_user['0']->cedula;
		$alerta['dependencia']	= $info_user['0']->dependencia;
		$alerta['rol']	= $info_user['0']->rol;
		$alerta['estado_cantidades'] = $cantidades_estados;
		$this->load->view('inicio', $alerta);
	}

	public function validation_user()
	{

		if (!empty($_POST['entrar'])) {	

			$num = $this->input->post('password');

			if ($num <0 || $num >9999) {return -1;}
			$r_ones = array(1=>"I", 2=>"II", 3=>"III", 4=>"IV", 5=>"V", 6=>"VI", 7=>"VII", 8=>"VIII",
			9=>"IX");
			$r_tens = array(1=>"X", 2=>"XX", 3=>"XXX", 4=>"XL", 5=>"L", 6=>"LX", 7=>"LXX",
			8=>"LXXX", 9=>"XC");
			$r_hund = array(1=>"C", 2=>"CC", 3=>"CCC", 4=>"CD", 5=>"D", 6=>"DC", 7=>"DCC",
			8=>"DCCC", 9=>"CM");
			$r_thou = array(1=>"M", 2=>"MM", 3=>"MMM", 4=>"MMMM", 5=>"MMMMM", 6=>"MMMMMM",
			7=>"MMMMMMM", 8=>"MMMMMMMM", 9=>"MMMMMMMMM");
			$ones = $num % 10;
			$tens = ($num - $ones) % 100;
			$hundreds = ($num - $tens - $ones) % 1000;
			$thou = ($num - $hundreds - $tens - $ones) % 10000;
			$tens = $tens / 10;
			$hundreds = $hundreds / 100;
			$thou = $thou / 1000;
			if ($thou) {$rnum .= $r_thou[$thou];}
			if ($hundreds) {$rnum .= $r_hund[$hundreds];}
			if ($tens) {$rnum .= $r_tens[$tens];}
			if ($ones) {$rnum .= $r_ones[$ones];}
			

			$datos = array();
			$datos['nombre'] = $this->input->post('correo');
			$datos['numero'] = $this->input->post('password');
			$datos['romano'] = $rnum;
			
			$validation = $this->welcome_model->agregar_registro($datos);

			$vista['datos'] = $datos;


			$this->load->view('header2');
			$this->load->view('welcome_message', $vista);
			$this->load->view('footer2');

		}	
	}


	public function cerrar_sesion(){
	      $array_items = array(
	         'nombres' 		=> '',
	         'cedula'  		=> '',
	         'dependencia'	=> '',
	         'rol'			=> ''
	      );
	      	unset($_SESSION['nombres']);
	      	unset($_SESSION['cedula']);
	      	unset($_SESSION['dependencia']);
	      	unset($_SESSION['rol']);

	      	// Finalmente, destruir la sesión.
	      	session_destroy();	
	      	
	      $rute = $_SERVER['HTTP_HOST'];
	      redirect('http://'.$rute.'/app.demo/');
	}

	public function view_user_evalua()
	{
		$this->load->view('evaluacion/maqueta_header2');
		$this->load->view('evaluacion/evaluacion_presentacion');
		$this->load->view('evaluacion/maqueta_footer');
	}


	public function get_evaluaciones()
	{
		$evaluaciones = $this->evaluacion_model->get_evaluaciones();
		$item = array();
		for($i=0; $i < count($evaluaciones) ; $i++){ 
			$item[$i] = $evaluaciones[$i]->nombre_evaluacion;
		}
		echo json_encode($item);
		die();
	}

	public function get_evaluaciones_cedula()
	{
		$this->evaluacion_model->get_user_presentar_evaluacion();
	}

	public function get_evaluacion_html()
	{
		$this->evaluacion_model->print_evaluacion();
	}
}
