<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Conceptos extends CI_Controller {

	public function __construct()
   	{
        parent::__construct();
        // Your own constructor code
        $this->load->helper('form');
        $this->load->database();
		
		$this->load->model('concepto_model');
		$this->load->model('ubicacion_model');
		$this->load->model('digital_model');

		

		$this->load->helper('url');
		// Notificar solamente errores de ejecución
		error_reporting(E_ERROR | E_WARNING | E_PARSE);
   	}
	
	public function index()
	{
		if(empty($_POST['cedula']))
		{
			$rute = $_SERVER['HTTP_HOST'];
			redirect('http://'.$rute.'/prueba/');
			// redirect('http://localhost:8080/prueba/');
		}
		
		$cedula = $_POST['cedula'];
		$resultado = $this->digital_model->get_date_user($cedula);

		$info = $this->concepto_model->get_conceptos_all();
		$info2 = $this->ubicacion_model->get_ubicaciones_all();
		$info_table = array(
							 'table' 		=> $info, 
							 'table2' 		=> $info2,
							 'cedula' 		=> $resultado['0']->cedula,
							 'dependencia'  => $resultado['0']->dependencia,
							 'nombres'		=> $resultado['0']->nombres,
							 'rol' 			=> $resultado['0']->rol
						   ); 
		
		$this->load->view('conceptos', $info_table);
	}

	public function create_save_conceptos()
	{
		
		$info_insert_conceptos = array(
										'nombre_concepto' => $this->input->post('nombre_concepto'),
										'observaciones'   => $this->input->post('observacion_concepto') 
									  );

		$return = $this->concepto_model->save_info_conceptos($info_insert_conceptos);
		echo json_encode($return);
		die();
	}

	public function load_registros_conceptos()
	{
		$info = $this->concepto_model->get_conceptos_all();
		echo json_encode($info);
		die();
	}

	public function delete_regis()
	{
		$id = $this->input->post('id');
		$this->concepto_model->delete_registros($id);
		$info = $this->concepto_model->get_conceptos_all();	
		echo json_encode($info);
		die();
	}

	public function loading_unico_concepto()
	{
		$id = $this->input->post('id');
		$return = $this->concepto_model->loading_unico_concept($id);
		$return_concepto = array(
								  'nombre_concepto' => $return['0']->nombre_concepto,
								  'observaciones'   => $return['0']->observaciones
								);
		echo json_encode($return_concepto);
		die();
	}

	public function update_regis()
	{
		$info_update = array(
							  'id_concepto'     => $this->input->post('id'),
							  'concepto' 		=> $this->input->post('concepto'),
							  'observaciones'   => $this->input->post('observaciones')
							);

		$this->concepto_model->update_concepts($info_update);
		$info = $this->concepto_model->get_conceptos_all();
		echo json_encode($info);
		die();
	}
	
}
