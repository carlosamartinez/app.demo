<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {

	public function __construct()
   	{
        parent::__construct();
        // Your own constructor code
        $this->load->helper('form');
        $this->load->database();
		$this->load->model('digital_model');
		$this->load->model('usuarios_model');
		// $this->load->model('concepto_model');
		$this->load->model('ubicacion_model');

		$this->load->helper('url');
		// Notificar solamente errores de ejecución
		error_reporting(E_ERROR | E_WARNING | E_PARSE);
   	}
	
	public function index()
	{
		if(empty($_POST['cedula']))
		{
			$rute = $_SERVER['HTTP_HOST'];
			redirect('http://'.$rute.'/prueba/');
		}

		$ubicaciones = $this->ubicacion_model->get_ubicaciones();
		

		$departamentos = array();
		for($i=0; $i < count($ubicaciones); $i++){ 
			$departamentos[$i] = $ubicaciones[$i]->nombre_ubicacion;
		}


		$cedula = $this->input->post('cedula');
		$resultado = $this->digital_model->get_date_user($cedula);


		$view_all['cedula'] 			= $resultado['0']->cedula;
		$view_all['dependencia']  		= $resultado['0']->dependencia;
		$view_all['nombres']  			= $resultado['0']->nombres;
		$view_all['rol']  				= $resultado['0']->rol;
		$view_all['departamentos']  	= $departamentos;

		$this->load->view('usuarios', $view_all);
	}


	public function insert_dates()
	{
		if(empty($_POST['cedula_usuario_login']))
		{
			redirect('http://localhost:8080/prueba/');
		}		

		$cedula_usuario_login = $this->input->post('cedula_usuario_login');
		$password2 = $this->input->post('password2');
		$dates = array(
						'nombres' 		=> $this->input->post('nombres'),
						'cedula' 		=> $this->input->post('cedula'),
						'dependencia' 	=> $this->input->post('departamento'),
						'correo' 		=> $this->input->post('correo'),
						'password' 		=> $this->input->post('password'),
						'rol' 			=> $this->input->post('rol')
					  );


		if(!empty($dates['nombres']) && !empty($dates['cedula']) && !empty($dates['correo']) && !empty($dates['password']) && !empty($_POST['password2']) && !empty($dates['rol']))
		{
			if($dates['password'] == $password2){

				if(!empty($_POST['modificar_registro']))
				{
					$dates['id_usuario'] = $this->input->post('id_change');
					$this->usuarios_model->update_user($dates);
					$even = 4;
						
				}elseif(!empty($_POST['eliminar_registro'])){
					$id_delete = $this->input->post('id_change');
					$this->usuarios_model->delete_user($id_delete);
					$even = 5;
				}else{
					$this->usuarios_model->insert_new_user($dates);
					$even = 3;
				}	

				$resultado = $this->digital_model->get_date_user($cedula_usuario_login);
				$view_all['cedula'] 		= $resultado['0']->cedula;
				$view_all['dependencia']  	= $resultado['0']->dependencia;
				$view_all['nombres']  		= $resultado['0']->nombres;
				$view_all['rol']	  		= $resultado['0']->rol;
				$view_all['message']    	= $even;
				$this->load->view('usuarios', $view_all);

			}else{				
				$resultado = $this->digital_model->get_date_user($cedula_usuario_login);
				$view_all['cedula'] 		= $resultado['0']->cedula;
				$view_all['dependencia']  	= $resultado['0']->dependencia;
				$view_all['nombres']  		= $resultado['0']->nombres;
				$view_all['rol']	  		= $resultado['0']->rol;
				$view_all['message']    	= 1;
				$this->load->view('usuarios', $view_all);
			}
		}else{
			$resultado = $this->digital_model->get_date_user($cedula_usuario_login);
			$view_all['cedula'] 		= $resultado['0']->cedula;
			$view_all['dependencia']  	= $resultado['0']->dependencia;
			$view_all['nombres']  		= $resultado['0']->nombres;
			$view_all['rol']	  		= $resultado['0']->rol;
			$view_all['message']    	= 2;
			$this->load->view('usuarios', $view_all);
		}
	}

	public function load_unico_user()
	{
		$id = $this->input->post('id_usuario');
		$ajax_user = $this->usuarios_model->load_unic_user_consul($id);		
		echo json_encode($ajax_user);
		die();
	}

	

	

	
	

	

	

	
}
