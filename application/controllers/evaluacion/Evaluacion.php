<?php
defined('BASEPATH') OR exit('No direct script  cess allowed');

class Evaluacion extends CI_Controller{

	public function __construct(){
        parent::__construct();
        // Your own constructor code
        $this->load->helper('form');
        $this->load->database();
		$this->load->model('digital_model');
		$this->load->model('empleados/empleados_model');
		$this->load->model('ubicacion_model');
		$this->load->model('evaluacion/evaluacion_model');

		$this->load->helper('url');
		// Notificar solamente errores de ejecución
		error_reporting(E_ERROR | E_WARNING | E_PARSE);
   	}

	// funcion inicial cuando el usuario ingresa al modulo de 
	// empleados donde cargamos toda la informaxion de todos los empleados
	public function index(){

		if(empty($_POST['cedula'])){
			$rute = $_SERVER['HTTP_HOST'];
			redirect('http://'.$rute.'/prueba/');
			// redirect('http://localhost:8080/prueba/');
		}
		
		$cedula = $_POST['cedula'];
		$resultado = $this->digital_model->get_date_user($cedula);

		$visualizarBoton = $this->evaluacion_model->validarBoton();
		$info_table = $this->evaluacion_model->consultar_tabla();
		$info_table_c = $this->evaluacion_model->get_calificaciones();

		$view_all['visualizarBoton'] = $visualizarBoton['0']->visualizarBoton;
	 	$view_all['info_table']			= $info_table;
	 	$view_all['info_table_c']			= $info_table_c;
		$view_all['cedula'] 			= $resultado['0']->cedula;
		$view_header['cedula'] 			= $resultado['0']->cedula;
		$view_header['dependencia']     = $resultado['0']->dependencia;
		$view_header['nombres']  		= $resultado['0']->nombres;
		$view_header['rol']  			= $resultado['0']->rol;

		$this->load->view('evaluacion/maqueta_header', $view_header);
		$this->load->view('evaluacion/evaluacion', $view_all);
		$this->load->view('evaluacion/maqueta_footer');
	}

	public function visualizarBoton(){

		if(empty($_POST['cedula'])){
			$rute = $_SERVER['HTTP_HOST'];
			redirect('http://'.$rute.'/prueba/');
			// redirect('http://localhost:8080/prueba/');
		}
		
		$cedula = $_POST['cedula'];
		$resultado = $this->digital_model->get_date_user($cedula);

		$dato = array(
			'visualizarBoton'	=> $_POST['habilitarEvaluacion']
			);

		$retorno = $this->evaluacion_model->visualizarBoton($dato);

		$info_table = $this->evaluacion_model->consultar_tabla();
		$info_table_c = $this->evaluacion_model->get_calificaciones();
		$visualizarBoton = $this->evaluacion_model->validarBoton();

		$view_all['visualizarBoton'] = $visualizarBoton['0']->visualizarBoton;
	 	$view_all['info_table']			= $info_table;
	 	$view_all['info_table_c']			= $info_table_c;
		$view_all['cedula'] 			= $resultado['0']->cedula;
		$view_header['cedula'] 			= $resultado['0']->cedula;
		$view_header['dependencia']     = $resultado['0']->dependencia;
		$view_header['nombres']  		= $resultado['0']->nombres;
		$view_header['rol']  			= $resultado['0']->rol;

		$this->load->view('evaluacion/maqueta_header', $view_header);
		$this->load->view('evaluacion/evaluacion', $view_all);
		$this->load->view('evaluacion/maqueta_footer');
	}

	public function get_save_evalutions(){
		$this->evaluacion_model->insert_evaluacion();
	}

	public function get_save_edit_evalutions(){
		$this->evaluacion_model->act_evaluacion();
	}

	public function consultar_tabla(){
		$this->evaluacion_model->consultar_evaluaciones();
	}

	public function delete_evaluacion(){

		if(empty($_POST['cedula'])){
			$rute = $_SERVER['HTTP_HOST'];
			redirect('http://'.$rute.'/prueba/');
			// redirect('http://localhost:8080/prueba/');
		}

		$cedula = $_POST['cedula'];
	
		$return_delete = $this->evaluacion_model->delete_evaluacion($this->input->post('id'));
		$resultado = $this->digital_model->get_date_user($cedula);
		$info_table = $this->evaluacion_model->get_table_evaluacion();
		$info_table_c = $this->evaluacion_model->get_calificaciones();
		$visualizarBoton = $this->evaluacion_model->validarBoton();

		$view_all['visualizarBoton'] = $visualizarBoton['0']->visualizarBoton;
		$view_all['info_table']			= $info_table;
		$view_all['info_table_c']			= $info_table_c;
		$view_all['cedula'] 			= $resultado['0']->cedula;
		$view_header['cedula'] 			= $resultado['0']->cedula;
		$view_header['dependencia']     = $resultado['0']->dependencia;
		$view_header['nombres']  		= $resultado['0']->nombres;
		$view_header['rol']  			= $resultado['0']->rol;
		$view_all['message_delete']		= $return_delete;

		$this->load->view('evaluacion/maqueta_header', $view_header);
		$this->load->view('evaluacion/evaluacion', $view_all);
		$this->load->view('evaluacion/maqueta_footer');

	}

	public function borrarEvaluado(){

		if(empty($_POST['cedula'])){
			$rute = $_SERVER['HTTP_HOST'];
			redirect('http://'.$rute.'/prueba/');
			// redirect('http://localhost:8080/prueba/');
		}

		$cedula = $_POST['cedula'];
	
		$return_delete = $this->evaluacion_model->borrarEvaluado($this->input->post('id'));
		$resultado = $this->digital_model->get_date_user($cedula);
		$info_table = $this->evaluacion_model->get_table_evaluacion();
		$info_table_c = $this->evaluacion_model->get_calificaciones();
		$visualizarBoton = $this->evaluacion_model->validarBoton();

		$view_all['visualizarBoton'] = $visualizarBoton['0']->visualizarBoton;
		$view_all['info_table']			= $info_table;
		$view_all['info_table_c']			= $info_table_c;
		$view_all['cedula'] 			= $resultado['0']->cedula;
		$view_header['cedula'] 			= $resultado['0']->cedula;
		$view_header['dependencia']     = $resultado['0']->dependencia;
		$view_header['nombres']  		= $resultado['0']->nombres;
		$view_header['rol']  			= $resultado['0']->rol;
		$view_all['message_delete']		= $return_delete;

		$this->load->view('evaluacion/maqueta_header', $view_header);
		$this->load->view('evaluacion/evaluacion', $view_all);
		$this->load->view('evaluacion/maqueta_footer');
	}

	//consulta de la evaluacion para que posteriormente pueda actualizar
	public function actualizar_evaluacion(){

		if(empty($_POST['cedula']))
		{
			$rute = $_SERVER['HTTP_HOST'];
			redirect('http://'.$rute.'/prueba/');
		}
		
		$cedula = $_POST['cedula'];
		$resultado_user = $this->digital_model->get_date_user($cedula);
		
		$datos_seleccionados = $this->evaluacion_model->get_info_seleccionada_act($_POST['id']);

		$adjunto_info = array();
		$adjunto_info['cedula']			 = $resultado_user['0']->cedula;
		$adjunto_info['dependencia']	 = $resultado_user['0']->dependencia;
		$adjunto_info['nombres'] 		 = $resultado_user['0']->nombres;
		$adjunto_info['rol'] 			 = $resultado_user['0']->rol;
		$view_body['cedula']			 = $resultado_user['0']->cedula;
		$view_body['datos_editar']	 	= $datos_seleccionados;

		$this->load->view('evaluacion/maqueta_header', $adjunto_info);
		$this->load->view('evaluacion/edit_evaluacion', $view_body);
		$this->load->view('evaluacion/maqueta_footer');

	}
		//Esta funcion es para editar la evaluacion
		public function edit_evaluacion(){

		if(empty($_POST['cedula'])){
			$rute = $_SERVER['HTTP_HOST'];
			redirect('http://'.$rute.'/prueba/');
			// redirect('http://localhost:8080/prueba/');
		}

		$cedula = $_POST['cedula'];
		$resultado = $this->digital_model->get_date_user($cedula);

	 	$view_all['info_table']			= $info_table;
		$view_all['cedula'] 			= $resultado['0']->cedula;
		$view_header['cedula'] 			= $resultado['0']->cedula;
		$view_header['dependencia']     = $resultado['0']->dependencia;
		$view_header['nombres']  		= $resultado['0']->nombres;
		$view_header['rol']  			= $resultado['0']->rol;
		
		$this->load->view('evaluacion/maqueta_header', $view_header);
		$this->load->view('evaluacion/edit_evaluacion', $view_all);
		$this->load->view('evaluacion/maqueta_footer');
	}

	public function testy(){

		if(empty($_POST['cedula'])){
			$rute = $_SERVER['HTTP_HOST'];
			redirect('http://'.$rute.'/prueba/');
			// redirect('http://localhost:8080/prueba/');
		}


		$id = $this->input->post('id_evaluacion');
		$this->evaluacion_model->editar_evaluacion($id);

		$cedula = $_POST['cedula'];
		$resultado = $this->digital_model->get_date_user($cedula);

		$info_table= $this->evaluacion_model->consultar_evaluaciones_a_realizar();

	 	$view_all['info_table']			= $info_table;
		$view_all['cedula'] 			= $resultado['0']->cedula;
		$view_header['cedula'] 			= $resultado['0']->cedula;
		$view_header['dependencia']     = $resultado['0']->dependencia;
		$view_header['nombres']  		= $resultado['0']->nombres;
		$view_header['rol']  			= $resultado['0']->rol;
		
		$this->load->view('evaluacion/maqueta_header', $view_header);
		$this->load->view('evaluacion/evaluacion', $view_all);
		$this->load->view('evaluacion/maqueta_footer');

	}

	public function calificarEvaluacion(){

		$nombreEvaluado 	= $_POST['nombreEvaluado'];
		$cedulaEvaluado 	= $_POST['cedulaEvaluado'];
		$nombre_evaluacion 	= $_POST['nombre_evaluacion'];
		$descripcion 		= $_POST['descripcion'];

		$desde_one 					= $_POST['desde_one'];
		$hasta_one 					= $_POST['hasta_one'];
		$equivalente_porcentaje_one = $_POST['equivalente_porcentaje_one'];
		$equivalente_unitaria_one 	= $_POST['equivalente_unitaria_one'];

		$desde_two 					= $_POST['desde_two'];
		$hasta_two 					= $_POST['hasta_two'];
		$equivalente_porcentaje_two = $_POST['equivalente_porcentaje_two'];
		$equivalente_unitaria_two 	= $_POST['equivalente_unitaria_two'];

		$calificacion_excelente_desde 	= $_POST['calificacion_excelente_desde'];
		$calificacion_excelente_hasta 	= $_POST['calificacion_excelente_hasta'];
		$calificacion_bueno_desde 		= $_POST['calificacion_bueno_desde'];
		$calificacion_bueno_hasta 		= $_POST['calificacion_bueno_hasta'];
		$calificacion_aceptable_desde 	= $_POST['calificacion_aceptable_desde'];
		$calificacion_aceptable_hasta 	= $_POST['calificacion_aceptable_hasta'];
		$calificacion_deficiente_desde 	= $_POST['calificacion_deficiente_desde'];
		$calificacion_deficiente_hasta 	= $_POST['calificacion_deficiente_hasta'];


		

		$preguntas = $_POST['json_cuestionario'];
		$cont = count($_POST['json_cuestionario']);

		$arrayjson = array();

		for ($i=0; $i <$cont ; $i++) { 
			$arrayjson[$i+1]['pregunta'] = $preguntas[$i];  
		}

		for ($i=1; $i <=$cont ; $i++) { 
			$respuesta = implode("_", $_POST['respuestasSeleccionadas_'.$i.'_']);
			$arrayjson[$i]['respuestas_seleccionadas'] = $respuesta;  
		}

		$json_cuestionario = json_encode($arrayjson);
		

		$puntaje=0;
		$calificacion="Deficiente";

		for ($i=1; $i <= $cont ; $i++) { 

			if ($_POST['respuestasVerdaderas_'.$i.'_'] == $_POST['respuestasSeleccionadas_'.$i.'_']) {
				if ($i >= $desde_one && $i <= $hasta_one) {
					$puntaje += $equivalente_unitaria_one;
				}else if ($i >= $desde_two && $i <= $hasta_two) {
					$puntaje += $equivalente_unitaria_two;
				}	
			}			
		}


		$puntaje = round($puntaje);

		if ($puntaje >= $calificacion_excelente_desde && $puntaje <= $calificacion_excelente_hasta) {
			$calificacion = "Excelente";
		}elseif ($puntaje >= $calificacion_bueno_desde && $puntaje <= $calificacion_bueno_hasta) {
			$calificacion = "Bueno";
		}elseif ($puntaje >= $calificacion_aceptable_desde && $puntaje <= $calificacion_aceptable_hasta) {
			$calificacion = "Aceptable";
		}elseif ($puntaje >= $calificacion_deficiente_desde && $puntaje <= $calificacion_deficiente_hasta) {
			$calificacion = "Deficiente";
		}

		$infoEvaluacion = array();
		$infoEvaluacion['nombreEvaluado']  = $nombreEvaluado;
		$infoEvaluacion['cedulaEvaluado']  = $cedulaEvaluado;
		$infoEvaluacion['nombreEvaluacion']  = $nombre_evaluacion;
		$infoEvaluacion['jsonCuestionario']  = $json_cuestionario;
		$infoEvaluacion['descripcion']  = $descripcion;
		$infoEvaluacion['puntaje']  = $puntaje;
		$infoEvaluacion['calificacion']  = $calificacion;
		$infoModelo = $this->evaluacion_model->calificarEvaluacion($infoEvaluacion);


		$evaluaciones = $this->evaluacion_model->get_evaluaciones();

		$get_nombres = array();
		for($i=0; $i < count($evaluaciones); $i++){ 
			$get_nombres[$i] = $evaluaciones[$i]->nombre_evaluacion;
		}

		$info_table= $this->evaluacion_model->consultar_evaluaciones_a_realizar();
		$view_all['info_table']			= $info_table;
		$view_all['get_nombres']		= $get_nombres;
		$view_all['mensaje'] 			= $infoModelo;

		$this->load->view('evaluacion/realizar_evaluacion', $view_all);

		/*die();*/


		/*$data['json_cuestionario'] = $this->input->post('json_cuestionario');
		$data['respuestaSeleccionada'] = $this->input->post('respuestaSeleccionada');
		$data['respuestasVerdaderas'] = $this->input->post('respuestasVerdaderas');*/
		
        /*echo "<pre>";
            print_r($_POST);
        echo "</pre>";*/

	}

	public function realizar_evaluacion(){

		$evaluaciones = $this->evaluacion_model->get_evaluaciones();

		$get_nombres = array();
		for($i=0; $i < count($evaluaciones); $i++){ 
			$get_nombres[$i] = $evaluaciones[$i]->nombre_evaluacion;
		}

		$info_table= $this->evaluacion_model->consultar_evaluaciones_a_realizar();
		$view_all['info_table']			= $info_table;
		$view_all['get_nombres']		= $get_nombres;

		$this->load->view('evaluacion/realizar_evaluacion', $view_all);
	}

	public function realizacion_evaluacion(){

		$datos['nombre_evaluacion'] = $_POST['nombre_evaluacion'];
		$datos['cedula_evaluado'] = $_POST['cedula_evaluado'];

		$validacion = $this->evaluacion_model->validarDatosEvaluacion($datos);

		if ($validacion == 'existente') {

			$evaluaciones = $this->evaluacion_model->get_evaluaciones();

			$get_nombres = array();
			for($i=0; $i < count($evaluaciones); $i++){ 
				$get_nombres[$i] = $evaluaciones[$i]->nombre_evaluacion;
			}

			$info_table= $this->evaluacion_model->consultar_evaluaciones_a_realizar();
			$view_all['info_table']			= $info_table;
			$view_all['get_nombres']		= $get_nombres;
			$view_all['mensaje'] = $validacion;

			$this->load->view('evaluacion/realizar_evaluacion', $view_all);
		}else{

			$datos_seleccionados = $this->evaluacion_model->get_info_seleccionada_realizacion_evaluacion($_POST['nombre_evaluacion']);
			$view_body['datos_editar']	 = $datos_seleccionados;
			$view_body['cedulaEvaluado'] = $_POST['cedula_evaluado'];
			$view_body['nombreEvaluado'] = $_POST['nombre_evaluado'];

			$this->load->view('evaluacion/realizacion_evaluacion', $view_body);
		}

	}

	public function ver_evaluacion(){
		$this->evaluacion_model->get_evaluacion_por_empleado();		
	}

	public function reporte_evaluacion(){
		$this->evaluacion_model->get_reporte_evaluacion();
	}
}