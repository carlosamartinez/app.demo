<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cuestionario extends CI_Controller {

	public function __construct()
   	{
        parent::__construct();
        // Your own constructor code
        $this->load->helper('form');
        $this->load->database();
		$this->load->model('formatos/formatos_model');
		$this->load->model('concepto_model');
		$this->load->model('ubicacion_model');
		$this->load->model('usuarios_model');
		$this->load->model('digital_model');
		$this->load->model('cuestionario/cuestionario_model');

		$this->load->helper('url');
		// Notificar solamente errores de ejecución
		error_reporting(E_ERROR | E_WARNING | E_PARSE);
   	}
	
	public function index()
	{
		$this->load->view('cuestionario/realizar_cuestionario');
	}

	public function validarCuestionario(){

		$datos['cedula_persona'] = $_POST['cedula_persona'];

		$validacion = $this->cuestionario_model->validarDatosCuestionario($datos);

		if ($validacion == 'existente') {

			$view_all['mensaje'] = $validacion;

			$this->load->view('cuestionario/realizar_cuestionario', $view_all);
		}else{

			$view_all['cedula_persona'] = $_POST['cedula_persona'];

			if ($_POST['nombre_cuestionario'] == "Comercial") {

				$this->load->view('cuestionario/cuestionario_comercial', $view_all);

			}elseif ($_POST['nombre_cuestionario'] == "Administrativo") {

				$this->load->view('cuestionario/cuestionario_administrativo', $view_all);

			}
		}
	}

	public function datosCuestionario(){

		$retorno = $this->cuestionario_model->viewArray();

		$view_all['mensaje']  	= $retorno;

		$this->load->view('cuestionario/realizar_cuestionario', $view_all);

	}

	public function registros(){
		if(empty($_POST['cedula']))
		{
			$rute = $_SERVER['HTTP_HOST'];
			redirect('http://'.$rute.'/prueba/');
		}

		$cedula = $_POST['cedula'];
		$resultado = $this->digital_model->get_date_user($cedula);
		$info_table = $this->cuestionario_model->get_cuestionarios();
		$visualizarBoton = $this->cuestionario_model->validarBoton();
	
		$view_all['visualizarBoton'] = $visualizarBoton['0']->verBotonCuestionario;
		$view_all['info_table'] = $info_table;
		$view_all['cedula'] 		= $resultado['0']->cedula;
		$view_all['dependencia']  = $resultado['0']->dependencia;
		$view_all['nombres']  	= $resultado['0']->nombres;
		$view_all['rol']  	= $resultado['0']->rol;
		$this->load->view('cuestionario/cuestionario_resultados', $view_all);
	}

	public function visualizarBoton(){

		if(empty($_POST['cedula']))
		{
			$rute = $_SERVER['HTTP_HOST'];
			redirect('http://'.$rute.'/prueba/');
		}

		$cedula = $_POST['cedula'];
		$resultado = $this->digital_model->get_date_user($cedula);

		$dato = array(
			'verBotonCuestionario'	=> $_POST['habilitarCuestionarios']
			);

		$retorno = $this->cuestionario_model->visualizarBoton($dato);
		$visualizarBoton = $this->cuestionario_model->validarBoton();
		$info_table = $this->cuestionario_model->get_cuestionarios();

		$view_all['info_table'] = $info_table;
		$view_all['visualizarBoton'] = $visualizarBoton['0']->verBotonCuestionario;
		$view_all['cedula'] 		= $resultado['0']->cedula;
		$view_all['dependencia']    = $resultado['0']->dependencia;
		$view_all['nombres']  		= $resultado['0']->nombres;
		$view_all['rol']  			= $resultado['0']->rol;

		$this->load->view('cuestionario/cuestionario_resultados', $view_all);
	}

	public function verCuestionarios(){
		if(empty($_POST['cedula'])){
			$rute = $_SERVER['HTTP_HOST'];
			redirect('http://'.$rute.'/prueba/');
			// redirect('http://localhost:8080/prueba/');
		}
		
		$cedula = $_POST['cedula'];
		$resultado = $this->digital_model->get_date_user($cedula);

		$cuestionario = $this->cuestionario_model->verCuestionarios($_POST['id']);

		$view_all['cuestionario']	 	= $cuestionario;
		$view_all['cedula'] 			= $resultado['0']->cedula;
		$view_all['cedula'] 			= $resultado['0']->cedula;
		$view_all['dependencia']     = $resultado['0']->dependencia;
		$view_all['nombres']  		= $resultado['0']->nombres;
		$view_all['rol']  			= $resultado['0']->rol;

		$this->load->view('cuestionario/ver_cuestionarios', $view_all);
	}

	public function deleteDatosVendedor(){
		if(empty($_POST['cedula'])){
			$rute = $_SERVER['HTTP_HOST'];
			redirect('http://'.$rute.'/prueba/');
			// redirect('http://localhost:8080/prueba/');
		}
		
		$cedula = $_POST['cedula'];
		$resultado = $this->digital_model->get_date_user($cedula);

		$this->cuestionario_model->deleteDatosVendedor($_POST['id']);
		
		$cedula = $_POST['cedula'];
		$resultado = $this->digital_model->get_date_user($cedula);
		$info_table = $this->cuestionario_model->get_cuestionarios();
		$visualizarBoton = $this->cuestionario_model->validarBoton();
	
		$view_all['visualizarBoton'] = $visualizarBoton['0']->verBotonCuestionario;
		$view_all['info_table'] = $info_table;
		$view_all['cedula'] 		= $resultado['0']->cedula;
		$view_all['dependencia']  = $resultado['0']->dependencia;
		$view_all['nombres']  	= $resultado['0']->nombres;
		$view_all['rol']  	= $resultado['0']->rol;
		$this->load->view('cuestionario/cuestionario_resultados', $view_all);
	}

	public function getReporte(){
		if(empty($_POST['cedula'])){
			$rute = $_SERVER['HTTP_HOST'];
			redirect('http://'.$rute.'/prueba/');
			// redirect('http://localhost:8080/prueba/');
		}
		
		$cedula = $_POST['cedula'];
		$resultado = $this->digital_model->get_date_user($cedula);

		$this->cuestionario_model->deleteDatosVendedor($_POST['id']);
		
		$cedula = $_POST['cedula'];
		$resultado = $this->digital_model->get_date_user($cedula);
		$this->cuestionario_model->exportarDatos();
		$info_table = $this->cuestionario_model->get_cuestionarios();
		$visualizarBoton = $this->cuestionario_model->validarBoton();
	
		$view_all['visualizarBoton'] = $visualizarBoton['0']->verBotonCuestionario;
		$view_all['info_table'] = $info_table;
		$view_all['cedula'] 		= $resultado['0']->cedula;
		$view_all['dependencia']  = $resultado['0']->dependencia;
		$view_all['nombres']  	= $resultado['0']->nombres;
		$view_all['rol']  	= $resultado['0']->rol;
		$this->load->view('cuestionario/cuestionario_resultados', $view_all);
	}
	
}
