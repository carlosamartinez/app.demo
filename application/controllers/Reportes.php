<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reportes extends CI_Controller {

	public function __construct()
   	{
        parent::__construct();
        // Your own constructor code
        $this->load->helper('form');
        $this->load->database();
		$this->load->model('digital_model');
		$this->load->model('concepto_model');
		$this->load->model('ubicacion_model');
		$this->load->model('usuarios_model');
		$this->load->model('welcome_model');

		$this->load->helper('url');
		// Notificar solamente errores de ejecución
		error_reporting(E_ERROR | E_WARNING | E_PARSE);
   	}
	
	public function pdf_documentos_estado_pendientes()
	{

		switch ($_POST['type_reports']){
			
			case 'Pendiente':
				$state_pendiente_pdf = $this->digital_model->get_document_state_pendiente_pdf();
				
				if(empty($state_pendiente_pdf)){

					$cantidades_estados = $this->welcome_model->get_cantidad_estados();

					$cedula = $_POST['cedula'];
					$resultado = $this->digital_model->get_date_user($cedula);
					$view_all['cedula'] 		= $resultado['0']->cedula;
					$view_all['dependencia']  = $resultado['0']->dependencia;
					$view_all['nombres']  	= $resultado['0']->nombres;
					$view_all['rol']  	= $resultado['0']->rol;
					$view_all['message']	= 1;
					$view_all['estado_cantidades'] = $cantidades_estados;
					$this->load->view('inicio', $view_all);

				}else{
					$data = array();
					$data['pdf_dates'] = $state_pendiente_pdf;
					$data['view_state'] = 'pendiente';
					$this->load->view('pdfview', $data);
				}
				
				break;

			case 'Aprobado':
				$state_aprobado_pdf = $this->digital_model->get_document_state_aprobado_pdf();
				
				if(empty($state_aprobado_pdf)){

					$cantidades_estados = $this->welcome_model->get_cantidad_estados();

					$cedula = $_POST['cedula'];
					$resultado = $this->digital_model->get_date_user($cedula);
					$view_all['cedula'] 		= $resultado['0']->cedula;
					$view_all['dependencia']  = $resultado['0']->dependencia;
					$view_all['nombres']  	= $resultado['0']->nombres;
					$view_all['rol']  	= $resultado['0']->rol;
					$view_all['message']	= 1;
					$view_all['estado_cantidades'] = $cantidades_estados;
					$this->load->view('inicio', $view_all);

				}else{
					$data = array();
					$data['pdf_dates'] = $state_aprobado_pdf;
					$data['view_state'] = 'aprobado';
					$this->load->view('pdfview', $data);
				}
				
				break;

			case 'listo para pago':
				$state_espera_pdf = $this->digital_model->get_document_state_espera_pdf();
				
				if(empty($state_espera_pdf)){

					$cantidades_estados = $this->welcome_model->get_cantidad_estados();

					$cedula = $_POST['cedula'];
					$resultado = $this->digital_model->get_date_user($cedula);
					$view_all['cedula'] 		= $resultado['0']->cedula;
					$view_all['dependencia']  = $resultado['0']->dependencia;
					$view_all['nombres']  	= $resultado['0']->nombres;
					$view_all['rol']  	= $resultado['0']->rol;
					$view_all['message']	= 1;
					$view_all['estado_cantidades'] = $cantidades_estados;
					$this->load->view('inicio', $view_all);

				}else{
					$data = array();
					$data['pdf_dates'] = $state_espera_pdf;
					$data['view_state'] = 'de espera';
					$this->load->view('pdfview', $data);
				}
				break;
			
			case 'Pagado':
				$state_pagado_pdf = $this->digital_model->get_document_state_pagado_pdf();
				
				if(empty($state_pagado_pdf)){

					$cantidades_estados = $this->welcome_model->get_cantidad_estados();

					$cedula = $_POST['cedula'];
					$resultado = $this->digital_model->get_date_user($cedula);
					$view_all['cedula'] 		= $resultado['0']->cedula;
					$view_all['dependencia']  = $resultado['0']->dependencia;
					$view_all['nombres']  	= $resultado['0']->nombres;
					$view_all['rol']  	= $resultado['0']->rol;
					$view_all['message']	= 1;
					$view_all['estado_cantidades'] = $cantidades_estados;
					$this->load->view('inicio', $view_all);

				}else{
					$data = array();
					$data['pdf_dates'] = $state_pagado_pdf;
					$data['view_state'] = 'pagado';
					$this->load->view('pdfview', $data);
				}
				break;		

			default:
				# code...
				break;
		}
		

	}
}
