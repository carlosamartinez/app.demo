﻿<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Formatos extends CI_Controller {

	public function __construct()
   	{
        parent::__construct();
        // Your own constructor code
        $this->load->helper('form');
        $this->load->database();
		$this->load->model('formatos/formatos_model');
		$this->load->model('concepto_model');
		$this->load->model('ubicacion_model');
		$this->load->model('usuarios_model');
		$this->load->model('digital_model');

		$this->load->helper('url');
		// Notificar solamente errores de ejecución
		error_reporting(E_ERROR | E_WARNING | E_PARSE);
   	}
	
	public function index()
	{
		if(empty($_POST['cedula']))
		{
			$rute = $_SERVER['HTTP_HOST'];
			redirect('http://'.$rute.'/prueba/');
		}

		if(!empty($_POST['Ingresar_datos'])){

			$cedula = $_POST['cedula'];
			$resultado = $this->digital_model->get_date_user($cedula);
			$data = $this->formatos_model->get_information();

			 
			
			$view_all['data'] = $data;
			
			$view_all['cedula'] 		= $resultado['0']->cedula;
			$view_all['dependencia']  = $resultado['0']->dependencia;
			$view_all['nombres']  	= $resultado['0']->nombres;
			$view_all['rol']  	= $resultado['0']->rol;
			$this->load->view('formatos/ingresar_datos', $view_all);

		}elseif(!empty($_POST['Gestión_formatos'])){

			$cedula = $_POST['cedula'];
			$resultado = $this->digital_model->get_date_user($cedula);
			
			$view_all['cedula'] 		= $resultado['0']->cedula;
			$view_all['dependencia']  = $resultado['0']->dependencia;
			$view_all['nombres']  	= $resultado['0']->nombres;
			$view_all['rol']  	= $resultado['0']->rol;
			$this->load->view('formatos/gestionar_formatos', $view_all);

		}else{
			$cedula = $_POST['cedula'];
			$resultado = $this->digital_model->get_date_user($cedula);
			
			$view_all['cedula'] 		= $resultado['0']->cedula;
			$view_all['dependencia']  = $resultado['0']->dependencia;
			$view_all['nombres']  	= $resultado['0']->nombres;
			$view_all['rol']  	= $resultado['0']->rol;
			$this->load->view('digital_view', $view_all);
		}

	}

	public function crearFormato(){

		if(empty($_POST['cedula']))
		{
			$rute = $_SERVER['HTTP_HOST'];
			redirect('http://'.$rute.'/prueba/');
			// redirect('http://localhost:8080/prueba/');
		}

		$cedula = $_POST['cedula'];
		$resultado = $this->digital_model->get_date_user($cedula);

		$vacacionesJson['1']['año'] = $_POST['añoActual'];
		$vacacionesJson['1']['inicio'] = $_POST['añoActualInicio'];
		$vacacionesJson['1']['fin'] = $_POST['añoActualFin'];
		$vacacionesJson['2']['año'] = $_POST['añoPasado'];
		$vacacionesJson['2']['inicio'] = $_POST['añoPasadoInicio'];
		$vacacionesJson['2']['fin'] = $_POST['añoPasadoFin'];
		$vacacionesJson['3']['año'] = $_POST['añoAntepasado'];
		$vacacionesJson['3']['inicio'] = $_POST['añoAntepasadoInicio'];
		$vacacionesJson['3']['fin'] = $_POST['añoAntepasadoFin'];

		/*echo "<pre>";
			print_r($vacacionesJson);
		echo "</pre><br>";
		
		$envuelto = json_encode($vacacionesJson);
		echo "<pre>";
			print_r($envuelto);
		echo "</pre><br>";

		$array = json_decode($envuelto);
		echo "<pre>";
			print_r($array);
		echo "</pre><br>";
		die();*/

		$formato = array();
		$formato['nombreLaboral'] = $this->input->post('nombre');
		$formato['cedulaLaboral'] = $this->input->post('cedula_laboral');
		$formato['codigoLaboral'] = $this->input->post('codigo');
		$formato['telefonoLaboral'] = $this->input->post('telefono_laboral');
		$formato['cargoLaboral'] = $this->input->post('cargo');
		$formato['areaLaboral'] = $this->input->post('area');
		$formato['fechaIngreso'] = $this->input->post('fecha_ingreso');
		$formato['fechaRetiro'] = $this->input->post('fecha_retiro');
		$formato['tipoContrato'] = $this->input->post('tipo_contrato');
		$formato['comparendosEducativos'] = $this->input->post('comparendos_educativos');
		$formato['llamadosAtención'] = $this->input->post('llamados_atencion');
		$formato['suspensiones'] = $this->input->post('suspensiones');
		$formato['diasAusentismos'] = $this->input->post('dias_ausentismos');
		$formato['diasLaborados'] = $this->input->post('dias_laborados');
		$formato['eps'] = $this->input->post('eps');
		$formato['afp'] = $this->input->post('afp');
		$formato['cesantias'] = $this->input->post('cesantias');
		$formato['valorCesantias'] = $this->input->post('valor_cesantias');
		$formato['causalRetiro'] = $this->input->post('causal_retiro');
		$formato['salarioDevengado'] = $this->input->post('salario_devengado');
		$formato['vacacionesJson'] = json_encode($vacacionesJson);

		$retorno = $this->formatos_model->insertFormato($formato);

		if ($retorno == 'exito') {
			$view_all['mensaje'] = 'exito';
		}
		elseif ($retorno == 'cedulaExistente') {
			$view_all['mensaje'] = 'cedulaExistente';			
		}
		$data = $this->formatos_model->get_information();

			 
			
		$view_all['data'] = $data;

		$view_all['cedula'] 		= $resultado['0']->cedula;
		$view_all['dependencia']  = $resultado['0']->dependencia;
		$view_all['nombres']  	= $resultado['0']->nombres;
		$view_all['rol']  	= $resultado['0']->rol;
		$this->load->view('formatos/ingresar_datos', $view_all);
	}

	public function editarFormato(){

		if(empty($_POST['cedula']))
		{
			$rute = $_SERVER['HTTP_HOST'];
			redirect('http://'.$rute.'/prueba/');
			// redirect('http://localhost:8080/prueba/');
		}

		$cedula = $_POST['cedula'];
		$resultado = $this->digital_model->get_date_user($cedula);

		$vacacionesJson['1']['año'] = $_POST['añoActual'];
		$vacacionesJson['1']['inicio'] = $_POST['añoActualInicio'];
		$vacacionesJson['1']['fin'] = $_POST['añoActualFin'];
		$vacacionesJson['2']['año'] = $_POST['añoPasado'];
		$vacacionesJson['2']['inicio'] = $_POST['añoPasadoInicio'];
		$vacacionesJson['2']['fin'] = $_POST['añoPasadoFin'];
		$vacacionesJson['3']['año'] = $_POST['añoAntepasado'];
		$vacacionesJson['3']['inicio'] = $_POST['añoAntepasadoInicio'];
		$vacacionesJson['3']['fin'] = $_POST['añoAntepasadoFin'];

		$formato = array();
		$formato['idHistorialLaboral'] = $this->input->post('id');
		$formato['nombreLaboral'] = $this->input->post('nombre');
		$formato['cedulaLaboral'] = $this->input->post('cedula_laboral');
		$formato['codigoLaboral'] = $this->input->post('codigo');
		$formato['telefonoLaboral'] = $this->input->post('telefono_laboral');
		$formato['cargoLaboral'] = $this->input->post('cargo');
		$formato['areaLaboral'] = $this->input->post('area');
		$formato['fechaIngreso'] = $this->input->post('fecha_ingreso');
		$formato['fechaRetiro'] = $this->input->post('fecha_retiro');
		$formato['tipoContrato'] = $this->input->post('tipo_contrato');
		$formato['comparendosEducativos'] = $this->input->post('comparendos_educativos');
		$formato['llamadosAtención'] = $this->input->post('llamados_atencion');
		$formato['suspensiones'] = $this->input->post('suspensiones');
		$formato['diasAusentismos'] = $this->input->post('dias_ausentismos');
		$formato['diasLaborados'] = $this->input->post('dias_laborados');
		$formato['eps'] = $this->input->post('eps');
		$formato['afp'] = $this->input->post('afp');
		$formato['cesantias'] = $this->input->post('cesantias');
		$formato['valorCesantias'] = $this->input->post('valor_cesantias');
		$formato['causalRetiro'] = $this->input->post('causal_retiro');
		$formato['salarioDevengado'] = $this->input->post('salario_devengado');
		$formato['vacacionesJson'] = json_encode($vacacionesJson);

		$retorno = $this->formatos_model->updateFormato($formato);

		if ($retorno == 'actualizado') {
			$view_all['mensaje'] = 'actualizado';
		}
		elseif ($retorno == 'cedulaExistente') {
			$view_all['mensaje'] = 'cedulaExistente';			
		}
		$data = $this->formatos_model->get_information();

			 
			
		$view_all['data'] = $data;

		$view_all['cedula'] 		= $resultado['0']->cedula;
		$view_all['dependencia']  = $resultado['0']->dependencia;
		$view_all['nombres']  	= $resultado['0']->nombres;
		$view_all['rol']  	= $resultado['0']->rol;
		$this->load->view('formatos/ingresar_datos', $view_all);
	}

	public function delete_formato(){

		$cedula = $_POST['cedula'];
		$resultado = $this->digital_model->get_date_user($cedula);
		$eliminar = $this->formatos_model->delete_formato($_POST['id']);
		$data = $this->formatos_model->get_information();

		$view_all['data'] = $data;
		$view_all['mensaje'] = $eliminar;
		$view_all['cedula'] 		= $resultado['0']->cedula;
		$view_all['dependencia']  = $resultado['0']->dependencia;
		$view_all['nombres']  	= $resultado['0']->nombres;
		$view_all['rol']  	= $resultado['0']->rol;
		$this->load->view('formatos/ingresar_datos', $view_all);

	}

	public function get_datos(){

		$cedula = $_POST['cedula'];
		$resultado = $this->digital_model->get_date_user($cedula);
		$datos = $this->formatos_model->get_historial($_POST['id']);
		
		$view_all['datos'] = $datos;
		$view_all['cedula'] 		= $resultado['0']->cedula;
		$view_all['dependencia']  = $resultado['0']->dependencia;
		$view_all['nombres']  	= $resultado['0']->nombres;
		$view_all['rol']  	= $resultado['0']->rol;
		$this->load->view('formatos/editar_datos', $view_all);
	}

	public function pdf(){
		
		if ($_POST['tipo_formato'] == 'Paz y salvo') {
			
			$retorno = $this->formatos_model->pazSalvo($_POST['cedula_ingresada']);
			
			if ($retorno=='noExiste') {
				$cedula = $_POST['cedula'];
				$resultado = $this->digital_model->get_date_user($cedula);
				
				$view_all['mensaje'] = 'noExiste';
				$view_all['cedula'] 		= $resultado['0']->cedula;
				$view_all['dependencia']  = $resultado['0']->dependencia;
				$view_all['nombres']  	= $resultado['0']->nombres;
				$view_all['rol']  	= $resultado['0']->rol;
				$this->load->view('formatos/gestionar_formatos', $view_all);
			}
		}elseif ($_POST['tipo_formato'] == 'Retiro de cesantias') {

			$retorno = $this->formatos_model->retiroCesantias($_POST['cedula_ingresada']);
			
			if ($retorno=='noExiste') {
				$cedula = $_POST['cedula'];
				$resultado = $this->digital_model->get_date_user($cedula);
				
				$view_all['mensaje'] = 'noExiste';
				$view_all['cedula'] 		= $resultado['0']->cedula;
				$view_all['dependencia']  = $resultado['0']->dependencia;
				$view_all['nombres']  	= $resultado['0']->nombres;
				$view_all['rol']  	= $resultado['0']->rol;
				$this->load->view('formatos/gestionar_formatos', $view_all);
			}
		}elseif ($_POST['tipo_formato'] == 'Seguridad social') {

			$retorno = $this->formatos_model->seguridadSocial($_POST['cedula_ingresada']);
			
			if ($retorno=='noExiste') {
				$cedula = $_POST['cedula'];
				$resultado = $this->digital_model->get_date_user($cedula);
				
				$view_all['mensaje'] = 'noExiste';
				$view_all['cedula'] 		= $resultado['0']->cedula;
				$view_all['dependencia']  = $resultado['0']->dependencia;
				$view_all['nombres']  	= $resultado['0']->nombres;
				$view_all['rol']  	= $resultado['0']->rol;
				$this->load->view('formatos/gestionar_formatos', $view_all);
			}
		}elseif ($_POST['tipo_formato'] == 'Historia laboral') {

			$retorno = $this->formatos_model->historiaLaboral($_POST['cedula_ingresada']);
			
			if ($retorno=='noExiste') {
				$cedula = $_POST['cedula'];
				$resultado = $this->digital_model->get_date_user($cedula);
				
				$view_all['mensaje'] = 'noExiste';
				$view_all['cedula'] 		= $resultado['0']->cedula;
				$view_all['dependencia']  = $resultado['0']->dependencia;
				$view_all['nombres']  	= $resultado['0']->nombres;
				$view_all['rol']  	= $resultado['0']->rol;
				$this->load->view('formatos/gestionar_formatos', $view_all);
			}
			
		}elseif ($_POST['tipo_formato'] == 'Certificado laboral') {

			$retorno = $this->formatos_model->certificadoLaboral($_POST['cedula_ingresada']);
			
			if ($retorno=='noExiste') {
				$cedula = $_POST['cedula'];
				$resultado = $this->digital_model->get_date_user($cedula);
				
				$view_all['mensaje'] = 'noExiste';
				$view_all['cedula'] 		= $resultado['0']->cedula;
				$view_all['dependencia']  = $resultado['0']->dependencia;
				$view_all['nombres']  	= $resultado['0']->nombres;
				$view_all['rol']  	= $resultado['0']->rol;
				$this->load->view('formatos/gestionar_formatos', $view_all);
			}
		}

	}

}
