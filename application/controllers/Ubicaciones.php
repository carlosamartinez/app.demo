<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ubicaciones extends CI_Controller {

	public function __construct()
   	{
        parent::__construct();
        // Your own constructor code
        $this->load->helper('form');
        $this->load->database();
		$this->load->model('ubicacion_model');



		$this->load->helper('url');
		// Notificar solamente errores de ejecución
		error_reporting(E_ERROR | E_WARNING | E_PARSE);
   	}
	
	
	public function create_save_ubicaciones()
	{
		
		$info_insert_ubicaciones = array(
										'nombre_ubicacion' => $this->input->post('nombre_ubicacion'),
										'observaciones'   => $this->input->post('observacion_ubicacion') 
									  );

		$return = $this->ubicacion_model->save_info_ubicaciones($info_insert_ubicaciones);
		echo json_encode($return);
		die();
	}

	public function load_registros_ubicaciones()
	{
		$info = $this->ubicacion_model->get_ubicaciones_all();
		echo json_encode($info);
		die();
	}

	public function delete_regis()
	{
		$id = $this->input->post('id');
		$this->ubicacion_model->delete_registros($id);
		$info = $this->ubicacion_model->get_ubicaciones_all();	
		echo json_encode($info);
		die();
	}

	public function loading_unico_ubicaciones()
	{
		$id = $this->input->post('id');
		$return = $this->ubicacion_model->loading_unico_ubicacion($id);
		$return_ubicacion = array(
								  'nombre_ubicacion' => $return['0']->nombre_ubicacion,
								  'observaciones'   => $return['0']->observaciones
								);
		echo json_encode($return_ubicacion);
		die();
	}

	public function update_regis()
	{
		$info_update = array(
							  'id_ubicacion'    	    => $this->input->post('id'),
							  'nombre_ubicacion' 		=> $this->input->post('ubicacion'),
							  'observaciones'   		=> $this->input->post('observaciones')
							);

		$this->ubicacion_model->update_ubicacions($info_update);
		$info = $this->ubicacion_model->get_ubicaciones_all();
		echo json_encode($info);
		die();
	}

	

	
}
