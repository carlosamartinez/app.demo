<!DOCTYPE html">
<html>
	<head>
		
		<title>Ejemplo de números Romanos en Javascript</title>
		<meta charset="UTF-8" />
		<link type="text/css" rel="stylesheet" href="css/styles.css">
		<script src="js/RomanNumbers.js"></script>

		<script>

			window.onload = function(){

				//---Declaración de variables
				var arabic = document.getElementById("arabic");
				var roman = document.getElementById("roman");
				var manager = new RomanNumbers();

				arabic.addEventListener("change", convertToRoman);
				arabic.addEventListener("keyup", convertToRoman);

				roman.addEventListener("change", convertToArabic);
				roman.addEventListener("keyup", convertToArabic);

				//---Convertir a romano
				function convertToRoman(){

					roman.value = (Number(arabic.value) < 3999999) ? manager.getRomanNumber(Number(arabic.value)) : "El número arábigo no es válido";					

				}

				//---Convertir a arábigo
				function convertToArabic(){

					arabic.value = (manager.testRoman(roman.value)) ? manager.getArabicNumber(roman.value) : "El número romano no es válido";			

				}

			}

		</script>

	</head>

	<body>

		<div class="inline">
			<label>Ingrese Numero natural</label>
			<br>
			<input type="text" class="roman-input" id="arabic" />
		</div><div class="inline">
			<label>Ingrese numero romano</label>
			<br>
			<input type="text" type="text" class="roman-input" id="roman" />
		</div>

	</body>
</html>