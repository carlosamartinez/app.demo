<?php

echo "El Numero 1978 en numero romano es: <strong>".convertirNum(1976)."</strong>";
echo "<br/>";
echo "El Numero 799 en numero romano es: <strong>".convertirNum(799)."</strong>";

function convertirNum($num)
    {
        /** intval (xxx) para que convierta explicitamente a int*/
        $n = intval($num);
        $res = '';

        /**Arreglo de numeros romanos */
        $numerales_romanos = array(
            'M'  => 1000,
            'CM' => 900,
            'D'  => 500,
            'CD' => 400,
            'C'  => 100,
            'XC' => 90,
            'L'  => 50,
            'XL' => 40,
            'X'  => 10,
            'IX' => 9,
            'V'  => 5,
            'IV' => 4,
            'I'  => 1
        );

        foreach ($numerales_romanos as $key => $numero) {

            /** Dividir para encontrar resultado, se pone en array */
            $coincidencias = intval($n / $numero);
            /** Asignar numero romano  al resultado */
            $res .= str_repeat($key, $coincidencias);
            /** Descontar el numero romano al total */
            $n = $n % $numero;
        }
        return $res;

    }
?>