<?php
// just require TCPDF instead of FPDF
require_once('tcpdf/tcpdf.php');
require_once('fpdi.php');

// Original file with multiple pages 
$fullPathToFile = 'NOTAS.pdf';

class PDF extends FPDI {

    var $_tplIdx;

    function Header() {

        global $fullPathToFile;

        if (is_null($this->_tplIdx)) {

            // THIS IS WHERE YOU GET THE NUMBER OF PAGES
            $this->numPages = $this->setSourceFile($fullPathToFile);
            $this->_tplIdx = $this->importPage(1);

        }
        $this->useTemplate($this->_tplIdx);

    }

    function Footer() {}

}

// initiate PDF
$pdf = new PDF();
$pdf->setFontSubsetting(true);


// add a page
$pdf->AddPage();

$html = '<h1>here luis fernando hernandez calderon</h1>';

// The new content
$pdf->SetFont("helvetica", "B", 14);
// $pdf->Text(10,10,'Some text here');
$pdf->writeHTML($html, true, false, true, false, '');

// THIS PUTS THE REMAINDER OF THE PAGES IN
if($pdf->numPages>1) {
    for($i=2;$i<=$pdf->numPages;$i++) {
        $pdf->endPage();
        $pdf->_tplIdx = $pdf->importPage($i);
        $pdf->AddPage();
    }
}

// Output the file as forced download
// $pdf->Output('theNewFile.pdf', 'D');
$pdf->Output();